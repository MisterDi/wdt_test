----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    13:01:22 04/13/2018 
-- Design Name: 
-- Module Name:    ADS8665 - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity ADS8665 is
    Port ( Clk 			: in  STD_LOGIC;
           Reset 			: in  STD_LOGIC;
           AdcData 		: out  STD_LOGIC_VECTOR (31 downto 0);
           AdcControl 	: in  STD_LOGIC_VECTOR (31 downto 0);
           CmdDataRead 	: in  STD_LOGIC;
           CmdCfgWrite 	: in  STD_LOGIC;
           CmdRawRd 		: in  STD_LOGIC;
           CmdRawWr 		: in  STD_LOGIC;
           Ads8665_SCK 	: out  STD_LOGIC;
           Ads8665_MOSI : out  STD_LOGIC;
           Ads8665_MISO : in  STD_LOGIC;
           Ads8665_CS 	: out  STD_LOGIC
			  );
end ADS8665;

architecture Behavioral of ADS8665 is

component SPI_Master
    Generic (   
        N : positive := 32;                                             -- 32bit serial word length is default
        CPOL : std_logic := '0';                                        -- SPI mode selection (mode 0 default)
        CPHA : std_logic := '0';                                        -- CPOL = clock polarity, CPHA = clock phase.
        PREFETCH : positive := 1;                                       -- prefetch lookahead cycles
        SPI_2X_CLK_DIV : positive := 10);                                -- for a 100MHz sclk_i, yields a 10MHz SCK
    Port (  
        sclk_i : in std_logic := 'X';                                   -- high-speed serial interface system clock
        pclk_i : in std_logic := 'X';                                   -- high-speed parallel interface system clock
        rst_i : in std_logic := 'X';                                    -- reset core
        ---- serial interface ----
        spi_ssel_o : out std_logic;                                     -- spi bus slave select line
        spi_sck_o : out std_logic;                                      -- spi bus sck
        spi_mosi_o : out std_logic;                                     -- spi bus mosi output
        spi_miso_i : in std_logic := 'X';                               -- spi bus spi_miso_i input
        ---- parallel interface ----
        di_req_o : out std_logic;                                       -- preload lookahead data request line
        di_i : in  std_logic_vector (N-1 downto 0) := (others => 'X');  -- parallel data in (clocked on rising spi_clk after last bit)
        wren_i : in std_logic := 'X';                                   -- user data write enable, starts transmission when interface is idle
        wr_ack_o : out std_logic;                                       -- write acknowledge
        do_valid_o : out std_logic;                                     -- do_o data valid signal, valid during one spi_clk rising edge.
        do_o : out  std_logic_vector (N-1 downto 0);                    -- parallel output (clocked on rising spi_clk after last bit)
        --- debug ports: can be removed or left unconnected for the application circuit ---
        sck_ena_o : out std_logic;                                      -- debug: internal sck enable signal
        sck_ena_ce_o : out std_logic;                                   -- debug: internal sck clock enable signal
        do_transfer_o : out std_logic;                                  -- debug: internal transfer driver
        wren_o : out std_logic;                                         -- debug: internal state of the wren_i pulse stretcher
        rx_bit_reg_o : out std_logic;                                   -- debug: internal rx bit
        state_dbg_o : out std_logic_vector (3 downto 0);                -- debug: internal state register
        core_clk_o : out std_logic;
        core_n_clk_o : out std_logic;
        core_ce_o : out std_logic;
        core_n_ce_o : out std_logic;
        sh_reg_dbg_o : out std_logic_vector (N-1 downto 0)              -- debug: internal shift register
    );                      

end component;

type	AdcState is (
		Adc_Reset,
		Adc_Wait,
		Adc_Idle,
		Adc_GetCmd,
		Adc_Ready
);

signal	AdcFsm	:AdcState;

signal DataVal	: STD_LOGIC;
signal DataReq	: STD_LOGIC;
signal ReadEn	: STD_LOGIC;
signal WriteEn	: STD_LOGIC;
signal WrAsk	: STD_LOGIC;

signal DataIn	: STD_LOGIC_VECTOR(31 downto 0);
signal DataOut	: STD_LOGIC_VECTOR(31 downto 0);

signal Adc_Timer: STD_LOGIC_VECTOR(7 downto 0);

begin

i_SPI_Master: SPI_Master
PORT MAP(
        sclk_i => Clk,                                  -- high-speed serial interface system clock
        pclk_i => Clk,                                  -- high-speed parallel interface system clock
        rst_i  => Reset,                                -- reset core
        ---- serial interface ----
        spi_ssel_o => 	Ads8665_CS,                     -- spi bus slave select line
        spi_sck_o  => 	Ads8665_SCK,                    -- spi bus sck
        spi_mosi_o =>	Ads8665_MOSI,                   -- spi bus mosi output
        spi_miso_i =>	Ads8665_MISO,                   -- spi bus spi_miso_i input
        ---- parallel interface ----
        di_req_o =>		DataReq,                        -- preload lookahead data request line
        di_i 	  =>		DataIn,  							  -- parallel data in (clocked on rising spi_clk after last bit)
        wren_i   =>		WriteEn,                        -- user data write enable, starts transmission when interface is idle
        wr_ack_o =>		WrAsk,                          -- write acknowledge
        do_valid_o =>	DataVal,                        -- do_o data valid signal, valid during one spi_clk rising edge.
        do_o  		=>		DataOut                    	  -- parallel output (clocked on rising spi_clk after last bit)
);

process(Clk,Reset)
begin
	if Reset = '1' then
		AdcFsm <= Adc_Reset;
		Adc_Timer <= X"7F";
	else
		if Clk'event and Clk = '1' then
		
			if CmdRawRd = '1' then
				AdcFsm <= Adc_Idle;
			end if;
			
			if CmdRawWr = '1' then
				DataIn <= AdcControl;
				AdcFsm <= Adc_GetCmd;
			end if;
			
			case AdcFsm is
				when Adc_Reset =>
					AdcFsm <= Adc_Idle;
					
				when Adc_Idle 	=>
					
				when Adc_GetCmd =>
					AdcFsm <= Adc_Wait;
					WriteEn <= '1';
					
				when Adc_Wait	=>
					WriteEn <= '0';
					if DataVal = '1' then
						AdcData <= DataOut;
						AdcFsm <= Adc_Ready;
						Adc_Timer <= X"7F";
					end if;
					
				when Adc_Ready =>
					if Adc_Timer = X"00" then
						AdcFsm <= Adc_Idle;
					else
						Adc_Timer <= Adc_Timer - X"01";
					end if;
				when others 	=>				
			end case;
		end if;
	end if;
end process;
end Behavioral;

