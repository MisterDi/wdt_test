----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    13:27:31 03/16/2018 
-- Design Name: 
-- Module Name:    DIO_DRV8804 - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

library UNISIM;
use UNISIM.Vcomponents.ALL;

entity DIO_DRV8804 is
	Port (
		RESET          : in  STD_LOGIC;
		RESET_CLK      : in  STD_LOGIC;
		
		CLK            : in  STD_LOGIC;
		
		DATA           : in  STD_LOGIC_VECTOR (23 downto 0);
		DATA_CLK       : in  STD_LOGIC;
		DATA_EN        : in  STD_LOGIC;
		
		OUT_CS         : out STD_LOGIC := '0';
		OUT_SCK        : out STD_LOGIC := '1';
		OUT_MOSI       : out STD_LOGIC;
		
		OUT_ENBL_1     : out STD_LOGIC;
		OUT_RESET_1    : out STD_LOGIC;
		OUT_ENBL_2     : out STD_LOGIC;
		OUT_RESET_2    : out STD_LOGIC;
		OUT_ENBL_3     : out STD_LOGIC;
		OUT_RESET_3    : out STD_LOGIC;
		OUT_ENBL_4     : out STD_LOGIC;
		OUT_RESET_4    : out STD_LOGIC;
		
		READY          : out STD_LOGIC
	);
end DIO_DRV8804;

architecture Behavioral of DIO_DRV8804 is

COMPONENT dout_fifio
	PORT (
		rst : IN STD_LOGIC;
		
		wr_clk : IN STD_LOGIC;
		wr_en : IN STD_LOGIC;
		din : IN STD_LOGIC_VECTOR(23 DOWNTO 0);
		
		rd_clk : IN STD_LOGIC;
		rd_en : IN STD_LOGIC;
		dout : OUT STD_LOGIC_VECTOR(23 DOWNTO 0);
		
		empty : OUT STD_LOGIC
	);
END COMPONENT;

signal PRECNT        : STD_LOGIC_VECTOR(4 DOWNTO 0);
signal i_DATA        : STD_LOGIC_VECTOR (23 downto 0);
signal t_DATA        : STD_LOGIC_VECTOR (23 downto 0);
signal RD_EN         : STD_LOGIC;
signal EMPTY         : STD_LOGIC;
signal SPI_CNT       : STD_LOGIC_VECTOR (3 downto 0);

type dout_state_type is (
	DOUT_WAIT,
	DOUT_SPI_START,
	DOUT_SPI_LATCH_W,
	DOUT_SPI_LATCH
);
signal dout_state : dout_state_type;

COMPONENT cdc_bit
	PORT(
		CLK_IN : IN std_logic;
		D_IN : IN std_logic;
		CLK_OUT : IN std_logic;          
		D_OUT : OUT std_logic
	);
	END COMPONENT;
signal t_reset : std_logic;

begin
	reset_cdc_i: cdc_bit PORT MAP(
		CLK_IN => RESET_CLK,
		D_IN => RESET,
		CLK_OUT => CLK,
		D_OUT => t_reset
	);

i_dout_fifio : dout_fifio
	PORT MAP (
		rst => t_reset,
		
		wr_clk => DATA_CLK,
		wr_en => DATA_EN,
		din => DATA,
		
		rd_clk => CLK,
		rd_en => RD_EN,
		dout => i_DATA,
		empty => EMPTY
	);

DRV: process (CLK, RESET)
begin
	if CLK'event and CLK = '1' then
		if t_reset = '1' then
			dout_state <= DOUT_WAIT;
			
			OUT_ENBL_1  <= '1';
			OUT_ENBL_2  <= '1';
			OUT_ENBL_3  <= '1';
			OUT_ENBL_4  <= '1';
			
			OUT_RESET_1 <= '1';
			OUT_RESET_2 <= '1';
			OUT_RESET_3 <= '1';
			OUT_RESET_4 <= '1';
			
			OUT_CS <= '0';
			OUT_SCK <= '1';
			
			READY <= '0';
			RD_EN <= '0';
			
		else
			RD_EN <= '0';
			READY <= '1';
			
			case (dout_state) is
				when DOUT_WAIT =>
					if EMPTY = '0' then
						RD_EN <= '1';
						READY <= '0';
						
						t_DATA <= i_DATA(23 downto 16)
									 & i_DATA(9)  
									 & i_DATA(8)  
									 & i_DATA(10)  
									 & i_DATA(7)  
									 & i_DATA(1)  
									 & i_DATA(0)  
									 & i_DATA(4)  
									 & i_DATA(5)  
									 & i_DATA(13)  
									 & i_DATA(12)  
									 & i_DATA(15)  
									 & i_DATA(14)  
									 & i_DATA(2)  
									 & i_DATA(3)  
									 & i_DATA(11) 
									 & i_DATA(6);
						
						dout_state <= DOUT_SPI_START;
						
						PRECNT <= (others => '0');
						SPI_CNT <= (others => '1');
					end if;
					
				when DOUT_SPI_START =>
					READY  <= '0';
					PRECNT <= PRECNT + '1';
					
					if PRECNT = "00000" then
						OUT_MOSI <= t_DATA(15);
						OUT_SCK  <= '0';
					elsif PRECNT = "10000" then
						OUT_SCK  <= '1';
						SPI_CNT  <= SPI_CNT - '1';
						t_DATA(15 downto 0) <= t_DATA(14 downto 0) & '0';
						
						if SPI_CNT = "0000" then
							dout_state <= DOUT_SPI_LATCH_W;
							PRECNT <= "00001";
						end if;
					end if;
				
				when DOUT_SPI_LATCH_W =>
					READY  <= '0';
					PRECNT <= PRECNT + '1';
					if PRECNT = "00000" then
						OUT_CS <= '1';
						dout_state <= DOUT_SPI_LATCH;
					end if;
					
				when DOUT_SPI_LATCH =>
					READY  <= '0';
					PRECNT <= PRECNT + 1;
					
					if PRECNT = "00000" then
						OUT_CS <= '0';
						OUT_SCK <= '1';
						dout_state <= DOUT_WAIT;
						
						OUT_ENBL_1  <= not t_DATA(16);
						OUT_ENBL_2  <= not t_DATA(17);
						OUT_ENBL_3  <= not t_DATA(18);
						OUT_ENBL_4  <= not t_DATA(19);
						OUT_RESET_1 <= t_DATA(20);
						OUT_RESET_2 <= t_DATA(21);
						OUT_RESET_3 <= t_DATA(22);
						OUT_RESET_4 <= t_DATA(23);
					end if;
					
				when others =>
			end case;
		end if;
	end if;
end process;
end Behavioral;

