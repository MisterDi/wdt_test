----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    22:55:52 03/23/2018 
-- Design Name: 
-- Module Name:    cdc_bit - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

library UNISIM;
use UNISIM.Vcomponents.ALL;

entity cdc_bit is
	Port (
		CLK_IN : in  STD_LOGIC;
		D_IN : in  STD_LOGIC;

		CLK_OUT : in  STD_LOGIC;
		D_OUT : out  STD_LOGIC
	);
end cdc_bit;

architecture Behavioral of cdc_bit is

signal A, B : std_logic;

begin

pr_in: process (CLK_IN)
begin
	if CLK_IN'event and CLK_IN = '1' then
		A <= D_IN;
	end if;
end process;

pr_out: process (CLK_OUT)
begin
	if CLK_OUT'event and CLK_OUT = '1' then
		B <= A;
		D_OUT <= B;
	end if;
end process;

end Behavioral;

