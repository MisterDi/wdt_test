----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    16:46:20 03/04/2018 
-- Design Name: 
-- Module Name:    top - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

library UNISIM;
use UNISIM.Vcomponents.ALL;

use work.dma_pack.all;

entity top is
	generic (
		enable_arinc_429  : boolean := true;
		enable_dio_input  : boolean := true;
		enable_dio_output : boolean := true;
		enable_arinc_708  : boolean := true;
		enable_sound      : boolean := true
	);
	
	Port (
		-- 50 Mhz global clock input
		GCLK             : in  STD_LOGIC;

		-- PCIe
		pcie_rst         : in  STD_LOGIC;
		sys_clk_p        : in  STD_LOGIC;
		sys_clk_n        : in  STD_LOGIC;
		pci_exp_txp      : out  STD_LOGIC;
		pci_exp_txn      : out  STD_LOGIC;
		pci_exp_rxp      : in  STD_LOGIC;
		pci_exp_rxn      : in  STD_LOGIC;
		
		-- Power control and WDT
	   BAT_CHRG			: out STD_LOGIC;
	   BAT_STATE		: in  STD_LOGIC;
	   WATCHDOG_IN		: out STD_LOGIC;
	   WATCHDOG_VM		: out STD_LOGIC;
	   Q7_PWREN			: out STD_LOGIC;
		
		-- ARINC devices
		A429_CS_1		: out	STD_LOGIC := '1';
		A429_SCK_1		: out STD_LOGIC := '0';
		A429_MOSI_1		: out STD_LOGIC := '0';
		A429_MISO_1		: in  STD_LOGIC := '0';
		A429_AINT_1		: in STD_LOGIC := '0';
		A429_AACK_1		: out  STD_LOGIC := '0';
		A429_MINT_1		: in STD_LOGIC := '0';
		A429_MINTACK_1	: out  STD_LOGIC := '0';
		A429_RESET_1	: out  STD_LOGIC := '0';
		A429_TXMSK_1	: out  STD_LOGIC := '0';
		A429_RUN_1		: out  STD_LOGIC := '0';
		A429_READY_1	: in STD_LOGIC := '0';
		
		A429_CS_2		: out	STD_LOGIC := '1';
		A429_SCK_2		: out STD_LOGIC := '0';
		A429_MOSI_2		: out STD_LOGIC := '0';
		A429_MISO_2		: in  STD_LOGIC := '0';
		A429_AINT_2		: in STD_LOGIC := '0';
		A429_AACK_2		: out  STD_LOGIC := '0';
		A429_MINT_2		: in STD_LOGIC := '0';
		A429_MINTACK_2	: out  STD_LOGIC := '0';
		A429_RESET_2	: out  STD_LOGIC := '0';
		A429_TXMSK_2	: out  STD_LOGIC := '0';
		A429_RUN_2		: out  STD_LOGIC := '0';
		A429_READY_2	: in STD_LOGIC := '0';
		
		A429_CS_3		: out	STD_LOGIC := '1';
		A429_SCK_3		: out STD_LOGIC := '0';
		A429_MOSI_3		: out STD_LOGIC := '0';
		A429_MISO_3		: in  STD_LOGIC := '0';
		A429_AINT_3		: in STD_LOGIC := '0';
		A429_AACK_3		: out  STD_LOGIC := '0';
		A429_MINT_3		: in STD_LOGIC := '0';
		A429_MINTACK_3	: out  STD_LOGIC := '0';
		A429_RESET_3	: out  STD_LOGIC := '0';
		A429_TXMSK_3	: out  STD_LOGIC := '0';
		A429_RUN_3		: out  STD_LOGIC := '0';
		A429_READY_3	: in STD_LOGIC := '0';
		
		A429_CS_4		: out	STD_LOGIC := '1';
		A429_SCK_4		: out STD_LOGIC := '0';
		A429_MOSI_4		: out STD_LOGIC := '0';
		A429_MISO_4		: in  STD_LOGIC := '0';
		A429_AINT_4		: in STD_LOGIC := '0';
		A429_AACK_4		: out  STD_LOGIC := '0';
		A429_MINT_4		: in STD_LOGIC := '0';
		A429_MINTACK_4	: out  STD_LOGIC := '0';
		A429_RESET_4	: out  STD_LOGIC := '0';
		A429_TXMSK_4	: out  STD_LOGIC := '0';
		A429_RUN_4		: out  STD_LOGIC := '0';
		A429_READY_4	: in STD_LOGIC := '0';
		
		-- Discrete output
		OUT_CS         : out STD_LOGIC := '1';
		OUT_SCK        : out STD_LOGIC := '0';
		OUT_MOSI       : out STD_LOGIC := '0';
		OUT_ENBL_1     : out STD_LOGIC := '0';
		OUT_RESET_1    : out STD_LOGIC := '0';
		OUT_FAULT_1    : in STD_LOGIC := '0';
		OUT_ENBL_2     : out STD_LOGIC := '0';
		OUT_RESET_2    : out STD_LOGIC := '0';
		OUT_FAULT_2    : in STD_LOGIC := '0';
		OUT_ENBL_3     : out STD_LOGIC := '0';
		OUT_RESET_3    : out STD_LOGIC := '0';
		OUT_FAULT_3    : in STD_LOGIC := '0';
		OUT_ENBL_4     : out STD_LOGIC := '0';
		OUT_RESET_4    : out STD_LOGIC := '0';
		OUT_FAULT_4    : in STD_LOGIC := '0';
		
		-- Discrete inputs
		IN_CS         : out STD_LOGIC := '1';
		IN_SCK        : out STD_LOGIC := '0';
		IN_MOSI       : out STD_LOGIC := '0';
		IN_MISO       : in  STD_LOGIC := '0';
		IN_MRB        : out  STD_LOGIC := '0';
		
		-- ARINC 708
		A708_TXINHA    : out STD_LOGIC := '1';
		A708_TXA_PLUS  : out STD_LOGIC := '0';
		A708_TXA_MINUS : out STD_LOGIC := '0';
		A708_CLKA      : out STD_LOGIC := '0';
		A708_ENCLKA    : out STD_LOGIC := '0';
		A708_TOC0A     : out STD_LOGIC := '0';
		A708_TOC1A     : out STD_LOGIC := '0';
		A708_TOC2A     : out STD_LOGIC := '0';
		A708_RXA_PLUS  : in  STD_LOGIC := '0';
		A708_RXA_MINUS : in  STD_LOGIC := '0';
		A708_RXENA     : out STD_LOGIC := '0';
		A708_ENPEXTA   : out STD_LOGIC := '0';

		A708_TXINHB    : out STD_LOGIC := '1';
		A708_TXB_PLUS  : out STD_LOGIC := '0';
		A708_TXB_MINUS : out STD_LOGIC := '0';
		A708_CLKB      : out STD_LOGIC := '0';
		A708_ENCLKB    : out STD_LOGIC := '0';
		A708_TOC0B     : out STD_LOGIC := '0';
		A708_TOC1B     : out STD_LOGIC := '0';
		A708_TOC2B     : out STD_LOGIC := '0';
		A708_RXB_PLUS  : in  STD_LOGIC := '0';
		A708_RXB_MINUS : in  STD_LOGIC := '0';
		A708_RXENB     : out STD_LOGIC := '0';
		A708_ENPEXTB   : out STD_LOGIC := '0';
		
		-- Serial Port 1
		UART_RX_1		: in	STD_LOGIC;
		UART_TX_1		: out STD_LOGIC;
		
		-- Serial Port 2
		UART_RX_2		: in	STD_LOGIC;
		UART_TX_2		: out STD_LOGIC;

		-- Serial Port 3
		SERIAL_RX_1		: in	STD_LOGIC;
		SERIAL_TX_1		: out STD_LOGIC;
		SERIAL_RX_EN_1	: out	STD_LOGIC;
		SERIAL_TX_EN_1	: out STD_LOGIC;

		-- Serial Port 3
		SERIAL_RX_2		: in	STD_LOGIC;
		SERIAL_TX_2		: out STD_LOGIC;
		SERIAL_RX_EN_2	: out	STD_LOGIC;
		SERIAL_TX_EN_2	: out STD_LOGIC;

		-- Dimming SPI
		DIMM_CS			: out	STD_LOGIC;
		DIMM_SCK			: out	STD_LOGIC;
		DIMM_MOSI		: out	STD_LOGIC;
		DIMM_MISO		: in	STD_LOGIC;

		--
		ETH_RESET      : out STD_LOGIC := '1';
		
		DBG1				: out std_logic;
		DBG2				: out std_logic;
		DBG3				: out std_logic;
		DBG4				: out std_logic
	);
end top;

architecture Behavioral of top is

	--------------------------------------------------------------------------------- PCIe DMA arbiter
	COMPONENT dma_arbiter
	generic (
		clients                  : integer := 1;
		GNT_CNT_size             : integer := 0
	);
	PORT(
		RESET                    : in  STD_LOGIC;
		RESET_CLK                : in  STD_LOGIC;
		
		DMA_CLK                  : in  STD_LOGIC;
				
		-- Connection to DMA controller
		start_address            : out STD_LOGIC_VECTOR(63 DOWNTO 0);
		pci_dma_ready            : in  STD_LOGIC;
		dma_cfg_address_set      : out STD_LOGIC;
		
		dma_cmd                  : out STD_LOGIC_VECTOR(33 DOWNTO 0);
		dma_cmd_rd_en            : in  STD_LOGIC;
		dma_cmd_empty            : out STD_LOGIC;
		
		dma_data                 : out STD_LOGIC_VECTOR(31 DOWNTO 0);
		dma_data_rd_en           : in  STD_LOGIC;
		dma_data_rd_data_count   : out STD_LOGIC_VECTOR(10 DOWNTO 0);
		
		-- Connection to slaves
		IN_CLK                   : in  STD_LOGIC_VECTOR (clients-1 downto 0) := (others => '0');
		REQ                      : in  STD_LOGIC_VECTOR (clients-1 downto 0) := (others => '0');
		GNT                      : out STD_LOGIC_VECTOR (clients-1 downto 0);

		s_start_address          : in  slv64_t(clients-1 downto 0) := (others => (others => '0'));
		s_pci_dma_ready          : out STD_LOGIC_VECTOR(clients-1 downto 0) := (others => '0');
		s_dma_cfg_address_set    : in  STD_LOGIC_VECTOR(clients-1 downto 0) := (others => '0');
		
		dma_data_a               : in  slv32_t(clients-1 downto 0) := (others => (others => '0'));
		dma_data_rd_en_a         : out STD_LOGIC_VECTOR(clients-1 downto 0) := (others => '0');
		dma_data_rd_data_count_a : in  slv11_t(clients-1 downto 0) := (others => (others => '0'));
		
		dma_cmd_a                : in  slv34_t(clients-1 downto 0) := (others => (others => '0'));
		dma_cmd_rd_en_a          : out STD_LOGIC_VECTOR(clients-1 downto 0);
		dma_cmd_empty_a          : in  STD_LOGIC_VECTOR(clients-1 downto 0) := (others => '0')
		);
	END COMPONENT;

	--------------------------------------------------------------------------------- PCIe
	component pcie_app_s6 is
	generic (
		enable_arinc_429  : boolean := false;
		enable_dio_input  : boolean := true;
		enable_dio_output : boolean := true
	);
	
	port (
		-- PCIe
		sys_clk_p               : in  STD_LOGIC;
		sys_clk_n               : in  STD_LOGIC;
		pci_exp_txp             : out STD_LOGIC;
		pci_exp_txn             : out STD_LOGIC;
		pci_exp_rxp             : in  STD_LOGIC;
		pci_exp_rxn             : in  STD_LOGIC;
		
		sys_reset               : in  std_logic;
	
		user_clk                : out std_logic;
		user_reset              : out std_logic;
		user_lnk_up             : out std_logic;
		
		-- Interrupt command input FIFO
		int_cmd_wr_clk          : IN  STD_LOGIC;
		int_cmd_wr_en           : IN  STD_LOGIC;
		int_cmd                 : IN  STD_LOGIC_VECTOR(1 DOWNTO 0);
		int_cmd_num             : IN  STD_LOGIC_VECTOR(1 DOWNTO 0);
		int_cmd_full            : OUT STD_LOGIC;
		
		-- Interrupt state signals (PCIe clock domain)
		cfg_interrupt_mmenable  : OUT std_logic_vector(2 downto 0);
		cfg_interrupt_msienable : OUT std_logic;
		
		-- DMA FIFO
		-- CMD: 00 - reset address for transfer
		--      01 - start DMA transfer with specified length
		--      10 -
		--      11 -
		dma_cmd                : in  STD_LOGIC_VECTOR(33 DOWNTO 0);
		dma_cmd_rd_en          : out STD_LOGIC;
		dma_cmd_empty          : in  STD_LOGIC;
		
		dma_data               : in  STD_LOGIC_VECTOR(31 DOWNTO 0);
		dma_data_rd_en         : out STD_LOGIC;
		dma_data_rd_data_count : in  STD_LOGIC_VECTOR(10 DOWNTO 0);
		
		-- DMA core is ready for trasnfer (or finished last transfer)
		-- PCIe CLOCK DOMAIN
		start_address          : IN  STD_LOGIC_VECTOR(63 DOWNTO 0);
		pci_dma_ready          : OUT STD_LOGIC;
		dma_cfg_address_set    : IN  STD_LOGIC;
		
		debug : out std_logic;
		
		A429_CMD_IN_1 : OUT std_logic_vector(31 downto 0);
		A429_CMD_IN_CLK_1 : OUT std_logic;
		A429_CMD_IN_EN_1 : OUT std_logic;
		A429_STATUS_OUT_1 : IN std_logic_vector(31 downto 0);
		A429_STATUS_OUT_CLK_1 : OUT std_logic;
		A429_ARINC_FIFO_RX_CLK_1 : OUT std_logic;
		A429_ARINC_FIFO_RX_READ_1 : OUT std_logic_vector(7 downto 0);
		A429_ARINC_FIFO_TX_0_1 : OUT std_logic_vector(31 downto 0);
		A429_ARINC_FIFO_TX_1_1 : OUT std_logic_vector(31 downto 0);
		A429_ARINC_FIFO_TX_2_1 : OUT std_logic_vector(31 downto 0);
		A429_ARINC_FIFO_TX_3_1 : OUT std_logic_vector(31 downto 0);
		A429_ARINC_FIFO_TX_CLK_1 : OUT std_logic;
		A429_ARINC_FIFO_RX_0_1 : IN std_logic_vector(31 downto 0);
		A429_ARINC_FIFO_RX_1_1 : IN std_logic_vector(31 downto 0);
		A429_ARINC_FIFO_RX_2_1 : IN std_logic_vector(31 downto 0);
		A429_ARINC_FIFO_RX_3_1 : IN std_logic_vector(31 downto 0);
		A429_ARINC_FIFO_RX_4_1 : IN std_logic_vector(31 downto 0);
		A429_ARINC_FIFO_RX_5_1 : IN std_logic_vector(31 downto 0);
		A429_ARINC_FIFO_RX_6_1 : IN std_logic_vector(31 downto 0);
		A429_ARINC_FIFO_RX_7_1 : IN std_logic_vector(31 downto 0);
		A429_ARINC_FIFO_RX_EMPTY_1 : IN std_logic_vector(7 downto 0);
		A429_ARINC_FIFO_TX_FULL_1 : IN std_logic_vector(3 downto 0);
		A429_ARINC_FIFO_TX_WRITE_1 : OUT std_logic_vector(3 downto 0);

		A429_CMD_IN_2 : OUT std_logic_vector(31 downto 0);
		A429_CMD_IN_CLK_2 : OUT std_logic;
		A429_CMD_IN_EN_2 : OUT std_logic;
		A429_STATUS_OUT_2 : IN std_logic_vector(31 downto 0);
		A429_STATUS_OUT_CLK_2 : OUT std_logic;
		A429_ARINC_FIFO_RX_CLK_2 : OUT std_logic;
		A429_ARINC_FIFO_RX_READ_2 : OUT std_logic_vector(7 downto 0);
		A429_ARINC_FIFO_TX_0_2 : OUT std_logic_vector(31 downto 0);
		A429_ARINC_FIFO_TX_1_2 : OUT std_logic_vector(31 downto 0);
		A429_ARINC_FIFO_TX_2_2 : OUT std_logic_vector(31 downto 0);
		A429_ARINC_FIFO_TX_3_2 : OUT std_logic_vector(31 downto 0);
		A429_ARINC_FIFO_TX_CLK_2 : OUT std_logic;
		A429_ARINC_FIFO_RX_0_2 : IN std_logic_vector(31 downto 0);
		A429_ARINC_FIFO_RX_1_2 : IN std_logic_vector(31 downto 0);
		A429_ARINC_FIFO_RX_2_2 : IN std_logic_vector(31 downto 0);
		A429_ARINC_FIFO_RX_3_2 : IN std_logic_vector(31 downto 0);
		A429_ARINC_FIFO_RX_4_2 : IN std_logic_vector(31 downto 0);
		A429_ARINC_FIFO_RX_5_2 : IN std_logic_vector(31 downto 0);
		A429_ARINC_FIFO_RX_6_2 : IN std_logic_vector(31 downto 0);
		A429_ARINC_FIFO_RX_7_2 : IN std_logic_vector(31 downto 0);
		A429_ARINC_FIFO_RX_EMPTY_2 : IN std_logic_vector(7 downto 0);
		A429_ARINC_FIFO_TX_FULL_2 : IN std_logic_vector(3 downto 0);
		A429_ARINC_FIFO_TX_WRITE_2 : OUT std_logic_vector(3 downto 0);
		
		A429_CMD_IN_3 : OUT std_logic_vector(31 downto 0);
		A429_CMD_IN_CLK_3 : OUT std_logic;
		A429_CMD_IN_EN_3 : OUT std_logic;
		A429_STATUS_OUT_3 : IN std_logic_vector(31 downto 0);
		A429_STATUS_OUT_CLK_3 : OUT std_logic;
		A429_ARINC_FIFO_RX_CLK_3 : OUT std_logic;
		A429_ARINC_FIFO_RX_READ_3 : OUT std_logic_vector(7 downto 0);
		A429_ARINC_FIFO_TX_0_3 : OUT std_logic_vector(31 downto 0);
		A429_ARINC_FIFO_TX_1_3 : OUT std_logic_vector(31 downto 0);
		A429_ARINC_FIFO_TX_2_3 : OUT std_logic_vector(31 downto 0);
		A429_ARINC_FIFO_TX_3_3 : OUT std_logic_vector(31 downto 0);
		A429_ARINC_FIFO_TX_CLK_3 : OUT std_logic;
		A429_ARINC_FIFO_RX_0_3 : IN std_logic_vector(31 downto 0);
		A429_ARINC_FIFO_RX_1_3 : IN std_logic_vector(31 downto 0);
		A429_ARINC_FIFO_RX_2_3 : IN std_logic_vector(31 downto 0);
		A429_ARINC_FIFO_RX_3_3 : IN std_logic_vector(31 downto 0);
		A429_ARINC_FIFO_RX_4_3 : IN std_logic_vector(31 downto 0);
		A429_ARINC_FIFO_RX_5_3 : IN std_logic_vector(31 downto 0);
		A429_ARINC_FIFO_RX_6_3 : IN std_logic_vector(31 downto 0);
		A429_ARINC_FIFO_RX_7_3 : IN std_logic_vector(31 downto 0);
		A429_ARINC_FIFO_RX_EMPTY_3 : IN std_logic_vector(7 downto 0);
		A429_ARINC_FIFO_TX_FULL_3 : IN std_logic_vector(3 downto 0);
		A429_ARINC_FIFO_TX_WRITE_3 : OUT std_logic_vector(3 downto 0);
		
		A429_CMD_IN_4 : OUT std_logic_vector(31 downto 0);
		A429_CMD_IN_CLK_4 : OUT std_logic;
		A429_CMD_IN_EN_4 : OUT std_logic;
		A429_STATUS_OUT_4 : IN std_logic_vector(31 downto 0);
		A429_STATUS_OUT_CLK_4 : OUT std_logic;
		A429_ARINC_FIFO_RX_CLK_4 : OUT std_logic;
		A429_ARINC_FIFO_RX_READ_4 : OUT std_logic_vector(7 downto 0);
		A429_ARINC_FIFO_TX_0_4 : OUT std_logic_vector(31 downto 0);
		A429_ARINC_FIFO_TX_1_4 : OUT std_logic_vector(31 downto 0);
		A429_ARINC_FIFO_TX_2_4 : OUT std_logic_vector(31 downto 0);
		A429_ARINC_FIFO_TX_3_4 : OUT std_logic_vector(31 downto 0);
		A429_ARINC_FIFO_TX_CLK_4 : OUT std_logic;
		A429_ARINC_FIFO_RX_0_4 : IN std_logic_vector(31 downto 0);
		A429_ARINC_FIFO_RX_1_4 : IN std_logic_vector(31 downto 0);
		A429_ARINC_FIFO_RX_2_4 : IN std_logic_vector(31 downto 0);
		A429_ARINC_FIFO_RX_3_4 : IN std_logic_vector(31 downto 0);
		A429_ARINC_FIFO_RX_4_4 : IN std_logic_vector(31 downto 0);
		A429_ARINC_FIFO_RX_5_4 : IN std_logic_vector(31 downto 0);
		A429_ARINC_FIFO_RX_6_4 : IN std_logic_vector(31 downto 0);
		A429_ARINC_FIFO_RX_7_4 : IN std_logic_vector(31 downto 0);
		A429_ARINC_FIFO_RX_EMPTY_4 : IN std_logic_vector(7 downto 0);
		A429_ARINC_FIFO_TX_FULL_4 : IN std_logic_vector(3 downto 0);
		A429_ARINC_FIFO_TX_WRITE_4 : OUT std_logic_vector(3 downto 0);
		
		-- Discrete output
		OUT_FAULT_1    : in  STD_LOGIC;
		OUT_FAULT_2    : in  STD_LOGIC;
		OUT_FAULT_3    : in  STD_LOGIC;
		OUT_FAULT_4    : in  STD_LOGIC;
		OUT_READY      : in  STD_LOGIC;
		OUT_DATA       : out STD_LOGIC_VECTOR (23 downto 0);
		OUT_DATA_CLK   : out STD_LOGIC;
		OUT_DATA_EN    : out STD_LOGIC;
		
		-- Discrete input
		IN_DIN : out STD_LOGIC_VECTOR (31 downto 0);
		IN_DIN_CLK : out STD_LOGIC;
		IN_DIN_EN : out STD_LOGIC;
		
		IN_DOUT : in  STD_LOGIC_VECTOR (31 downto 0);
		IN_DOUT_STATE : in  STD_LOGIC_VECTOR (31 downto 0);
		IN_DOUT_CLK : out STD_LOGIC;
		
		-- 708
		A708_DMA_ADDR_CLK : out STD_LOGIC;
		A708_DMA_ADDR : out STD_LOGIC_VECTOR (63 downto 0);
		
		A708_STATE : in STD_LOGIC_VECTOR (31 downto 0);
		A708_STATE_CLK : out STD_LOGIC;
		A708_CMD : out STD_LOGIC_VECTOR (31 downto 0);
		A708_CMD_EN : out STD_LOGIC;
		A708_CMD_CLK : out STD_LOGIC;

		-- Serial port 1
		RsSysClk		: in	STD_LOGIC;
 	   PortIn_1 	: out	STD_LOGIC_VECTOR (31 downto 0);
	   PortOut_1 	: in	STD_LOGIC_VECTOR (31 downto 0);
 	   CfgIn_1 		: out	STD_LOGIC_VECTOR (31 downto 0);
	   CfgOut_1 	: in	STD_LOGIC_VECTOR (31 downto 0);
	   CmdDataRd_1	: out	STD_LOGIC;
	   CmdDataWr_1	: out	STD_LOGIC;
	   CmdCfgRd_1	: out	STD_LOGIC;
	   CmdCfgWr_1	: out	STD_LOGIC;

 	   PortIn_2 	: out	STD_LOGIC_VECTOR (31 downto 0);
	   PortOut_2 	: in	STD_LOGIC_VECTOR (31 downto 0);
 	   CfgIn_2 		: out	STD_LOGIC_VECTOR (31 downto 0);
	   CfgOut_2 	: in	STD_LOGIC_VECTOR (31 downto 0);
	   CmdDataRd_2	: out	STD_LOGIC;
	   CmdDataWr_2	: out	STD_LOGIC;
	   CmdCfgRd_2	: out	STD_LOGIC;
	   CmdCfgWr_2	: out	STD_LOGIC;

 	   PortIn_3 	: out	STD_LOGIC_VECTOR (31 downto 0);
	   PortOut_3 	: in	STD_LOGIC_VECTOR (31 downto 0);
 	   CfgIn_3 		: out	STD_LOGIC_VECTOR (31 downto 0);
	   CfgOut_3 	: in	STD_LOGIC_VECTOR (31 downto 0);
	   CmdDataRd_3	: out	STD_LOGIC;
	   CmdDataWr_3	: out	STD_LOGIC;
	   CmdCfgRd_3	: out	STD_LOGIC;
	   CmdCfgWr_3	: out	STD_LOGIC;

 	   PortIn_4 	: out	STD_LOGIC_VECTOR (31 downto 0);
	   PortOut_4 	: in	STD_LOGIC_VECTOR (31 downto 0);
 	   CfgIn_4 		: out	STD_LOGIC_VECTOR (31 downto 0);
	   CfgOut_4 	: in	STD_LOGIC_VECTOR (31 downto 0);
	   CmdDataRd_4	: out	STD_LOGIC;
	   CmdDataWr_4	: out	STD_LOGIC;
	   CmdCfgRd_4	: out	STD_LOGIC;
	   CmdCfgWr_4	: out	STD_LOGIC;

	   Wdt_Tick		: out	STD_LOGIC;
		
		DIMM_ADC_DATA	: out	STD_LOGIC_VECTOR(31 downto 0);
		DIMM_ADC_CFG	: in	STD_LOGIC_VECTOR(31 downto 0);
	   CmdAdcRead		: out	STD_LOGIC;
	   CmdAdcCfg		: out	STD_LOGIC;
	   RawAdcRead		: out	STD_LOGIC;
	   RawAdcWrite		: out	STD_LOGIC

	);
	end component pcie_app_s6;
	
	signal i_OUT_READY      : STD_LOGIC;
	signal i_OUT_DATA       : STD_LOGIC_VECTOR (23 downto 0);
	signal i_OUT_DATA_CLK   : STD_LOGIC;
	signal i_OUT_DATA_EN    : STD_LOGIC;
	
	signal pci_lnk_up          : std_logic;
	signal pci_reset           : std_logic;
	signal pci_clk             : std_logic;
	signal pci_sys_reset       : std_logic;
	
	signal int_cmd_wr_clk      : STD_LOGIC;
	signal int_cmd_wr_en       : STD_LOGIC;
	signal int_cmd             : STD_LOGIC_VECTOR(1 DOWNTO 0);
	signal int_cmd_num         : STD_LOGIC_VECTOR(1 DOWNTO 0);
	signal int_cmd_full        : STD_LOGIC;
	
	signal pci_cfg_interrupt_mmenable  : std_logic_vector(2 downto 0);
	signal pci_cfg_interrupt_msienable : std_logic;
	
	-- DMA
	signal pci_dma_ready          : STD_LOGIC;
	signal dma_start_address      : STD_LOGIC_VECTOR(63 DOWNTO 0);
	signal dma_cfg_address_set    : STD_LOGIC;
	
	signal dma_cmd                : STD_LOGIC_VECTOR(33 DOWNTO 0);
	signal dma_cmd_rd_en          : STD_LOGIC;
	signal dma_cmd_empty          : STD_LOGIC;
		
	signal dma_data               : STD_LOGIC_VECTOR(31 DOWNTO 0);
	signal dma_data_rd_en         : STD_LOGIC;
	signal dma_data_rd_data_count : STD_LOGIC_VECTOR(10 DOWNTO 0);
	
	signal pci_ex_debug	: std_logic;

	--------------------------------------------------------------------------------- Main clock system
	signal CLK_25                      : std_logic;
	signal CLK_16                      : std_logic;
	signal CLK_40                      : std_logic;
	signal DCM_RESET                   : std_logic;
	signal DCM_LOCKED                  : std_logic;

	component main_dcm is
	port
	(
		-- Clock in ports
		CLK_IN_50                       : in std_logic;
		
		-- Clock out ports
		CLK_OUT_25                      : out std_logic;
		CLK_OUT_40                      : out std_logic;
		CLK_OUT_16                      : out std_logic;
		
		-- Status and control signals
		RESET                           : in std_logic;
		LOCKED                          : out std_logic
	);
	end component;
	
	--------------------------------------------------------------------------------- ARINC 708
	COMPONENT ARINC_708
	PORT(
		RESET : IN std_logic;
		RESET_CLK : in  STD_LOGIC;
		
		CLK16 : IN std_logic;
		
		RXA_P : IN std_logic;
		RXA_N : IN std_logic;
		RXB_P : IN std_logic;
		RXB_N : IN std_logic;          
		TXINHA : OUT std_logic;
		TXA_P : OUT std_logic;
		TXA_N : OUT std_logic;
		CLKA : OUT std_logic;
		ENCLKA : OUT std_logic;
		TOC0A : OUT std_logic;
		TOC1A : OUT std_logic;
		TOC2A : OUT std_logic;
		RXENA : OUT std_logic;
		ENPEXTA : OUT std_logic;
		TXINHB : OUT std_logic;
		TXB_P : OUT std_logic;
		TXB_N : OUT std_logic;
		CLKB : OUT std_logic;
		ENCLKB : OUT std_logic;
		TOC0B : OUT std_logic;
		TOC1B : OUT std_logic;
		TOC2B : OUT std_logic;
		RXENB : OUT std_logic;
		ENPEXTB : OUT std_logic;
		
		A708_DMA_ADDR_CLK : in STD_LOGIC := '0';
		A708_DMA_ADDR : in STD_LOGIC_VECTOR (63 downto 0) := (others => '0');
		
		A708_STATE : out STD_LOGIC_VECTOR (31 downto 0);
		A708_STATE_CLK : in STD_LOGIC := '0';
		
		A708_CMD : in STD_LOGIC_VECTOR (31 downto 0) := (others => '0');
		A708_CMD_EN : in STD_LOGIC := '0';
		A708_CMD_CLK : in STD_LOGIC := '0';
		
		-- DMA
		dma_start_address        : out STD_LOGIC_VECTOR(63 DOWNTO 0) := (others => '0');
		pci_dma_ready            : in STD_LOGIC;
		dma_cfg_address_set      : out  STD_LOGIC;
		
		dma_cmd                  : out STD_LOGIC_VECTOR(33 DOWNTO 0);
		dma_cmd_rd_en            : in  STD_LOGIC;
		dma_cmd_empty            : out STD_LOGIC;
		
		dma_data                 : out STD_LOGIC_VECTOR(31 DOWNTO 0);
		dma_data_rd_en           : in  STD_LOGIC;
		dma_data_rd_data_count   : out STD_LOGIC_VECTOR(10 DOWNTO 0);
		
		dma_gnt                  : in  STD_LOGIC;
		dma_req                  : out STD_LOGIC;
		
		dma_clk                  : in STD_LOGIC;
		
		debug : out std_logic;
		debug2 : out std_logic
		);
	END COMPONENT;
	
	signal A708_DMA_ADDR_CLK : STD_LOGIC;
	signal A708_DMA_ADDR : STD_LOGIC_VECTOR (63 downto 0);
	
	signal A708_STATE : STD_LOGIC_VECTOR (31 downto 0);
	signal A708_STATE_CLK : STD_LOGIC;
	
	signal A708_CMD : STD_LOGIC_VECTOR (31 downto 0);
	signal A708_CMD_EN : STD_LOGIC;
	signal A708_CMD_CLK : STD_LOGIC;
	
	signal A708_DEBUG : std_logic;
	signal A708_DEBUG2 : std_logic;
	
	signal A708_dma_start_address        : STD_LOGIC_VECTOR(63 DOWNTO 0);
	signal A708_pci_dma_ready            : STD_LOGIC;
	signal A708_dma_cfg_address_set      : STD_LOGIC;
	
	signal A708_dma_cmd                  : STD_LOGIC_VECTOR(33 DOWNTO 0);
	signal A708_dma_cmd_rd_en            : STD_LOGIC;
	signal A708_dma_cmd_empty            : STD_LOGIC;
		
	signal A708_dma_data                 : STD_LOGIC_VECTOR(31 DOWNTO 0);
	signal A708_dma_data_rd_en           : STD_LOGIC;
	signal A708_dma_data_rd_data_count   : STD_LOGIC_VECTOR(10 DOWNTO 0);
	
	signal A708_dma_gnt                  : STD_LOGIC;
	signal A708_dma_req                  : STD_LOGIC;

	--------------------------------------------------------------------------------- GLOBAL RESET
	signal GLOBAL_RESET                : std_logic;

	component reset is
	port
	(
		CLK      : in  STD_LOGIC;
		RESET    : out  STD_LOGIC;
		PLL_LOCK : in  STD_LOGIC;
		PCI_RESET : in STD_LOGIC
	);
	end component;

	constant	WdtTime	: std_logic_vector (23 downto 0 ) := X"4C4B40";
	signal	WdtCount	: std_logic_vector (23 downto 0 ) := WdtTime;
	signal	WdtOut 	: std_logic;
	signal 	i_Wdt_Tick	:	STD_LOGIC;
	signal 	Wdt_Start:	STD_LOGIC := '0';
	
	signal	init_ph	: std_logic_vector (1 downto 0) := "11";
	signal	init_tmr	: std_logic_vector (7 downto 0) := X"C0";

	signal	 DBG1_SSEL			: STD_LOGIC;
	signal	 DBG2_SCK			: STD_LOGIC;
	signal	 DBG3_MOSI			: STD_LOGIC;
	signal	 DBG4_MISO			: STD_LOGIC;
--	signal	 DBG_CNT				: STD_LOGIC_VECTOR (3 downto 0) :="0000";

	--------------------------------------------------------------------------------- D OUTPUT
	COMPONENT DIO_DRV8804
	PORT(
		RESET : IN std_logic;
		RESET_CLK : in  STD_LOGIC;
		CLK : IN std_logic;
		DATA : IN std_logic_vector(23 downto 0);
		DATA_CLK : IN std_logic;
		DATA_EN : IN std_logic;          
		OUT_CS : OUT std_logic;
		OUT_SCK : OUT std_logic;
		OUT_MOSI : OUT std_logic;
		OUT_ENBL_1 : OUT std_logic;
		OUT_RESET_1 : OUT std_logic;
		OUT_ENBL_2 : OUT std_logic;
		OUT_RESET_2 : OUT std_logic;
		OUT_ENBL_3 : OUT std_logic;
		OUT_RESET_3 : OUT std_logic;
		OUT_ENBL_4 : OUT std_logic;
		OUT_RESET_4 : OUT std_logic;
		READY : OUT std_logic
		);
	END COMPONENT;
	
	signal i_OUT_CS         : STD_LOGIC;
	signal i_OUT_SCK        : STD_LOGIC;
	signal i_OUT_MOSI       : STD_LOGIC;
	
	--------------------------------------------------------------------------------- D INPUT
	COMPONENT DIO_HI8435
	PORT(
		RESET : IN std_logic;
		RESET_CLK                : in  STD_LOGIC;
		
		CLK : IN std_logic;
		
		DIN : IN std_logic_vector(31 downto 0);
		DIN_CLK : IN std_logic;
		DIN_EN : IN std_logic;
		
		DOUT_CLK : IN std_logic;          
		DOUT : OUT std_logic_vector(31 downto 0);
		DOUT_STATE : OUT std_logic_vector(31 downto 0);
		
		CS : out  STD_LOGIC;
		SCK : out  STD_LOGIC;
		MOSI : out  STD_LOGIC;
		MISO : in  STD_LOGIC;
		MRB : out  STD_LOGIC
		);
	END COMPONENT;
	
	signal i_IN_CS         : STD_LOGIC;
	signal i_IN_SCK        : STD_LOGIC;
	signal i_IN_MOSI       : STD_LOGIC;
	signal i_IN_MRB        : STD_LOGIC;
	
	signal i_A429_MISO_1   : STD_LOGIC;
	signal i_A429_MOSI_1   : STD_LOGIC;
	signal i_A429_SCK_1    : STD_LOGIC;
	signal i_A429_CS_1     : STD_LOGIC;
	
	signal i_IN_DOUT : std_logic_vector(31 downto 0);
	signal i_IN_DOUT_STATE : std_logic_vector(31 downto 0);
	signal i_IN_DOUT_CLK : std_logic;
	
	signal i_IN_DIN : std_logic_vector(31 downto 0);
	signal i_IN_DIN_EN : std_logic;
	signal i_IN_DIN_CLK : std_logic;
	
	--------------------------------------------------------------------------------- ARINC
	COMPONENT ARINC_HI3210
	PORT(
		RESET : IN std_logic;
		RESET_CLK : in STD_LOGIC;
		CLK : IN std_logic;
		CMD_IN : IN std_logic_vector(31 downto 0);
		CMD_IN_CLK : IN std_logic;
		CMD_IN_EN : IN std_logic;
		STATUS_OUT_CLK : IN std_logic;
		ARINC_FIFO_RX_CLK : IN std_logic;
		ARINC_FIFO_RX_READ : IN std_logic_vector(7 downto 0);
		ARINC_FIFO_TX_0 : IN std_logic_vector(31 downto 0);
		ARINC_FIFO_TX_1 : IN std_logic_vector(31 downto 0);
		ARINC_FIFO_TX_2 : IN std_logic_vector(31 downto 0);
		ARINC_FIFO_TX_3 : IN std_logic_vector(31 downto 0);
		ARINC_FIFO_TX_CLK : IN std_logic;
		ARINC_FIFO_TX_WRITE : IN std_logic_vector(3 downto 0);
		MISO : IN std_logic;
		AINT : IN std_logic;
		MINT : IN std_logic;
		READY : IN std_logic;          
		STATUS_OUT : OUT std_logic_vector(31 downto 0);
		ARINC_FIFO_RX_0 : OUT std_logic_vector(31 downto 0);
		ARINC_FIFO_RX_1 : OUT std_logic_vector(31 downto 0);
		ARINC_FIFO_RX_2 : OUT std_logic_vector(31 downto 0);
		ARINC_FIFO_RX_3 : OUT std_logic_vector(31 downto 0);
		ARINC_FIFO_RX_4 : OUT std_logic_vector(31 downto 0);
		ARINC_FIFO_RX_5 : OUT std_logic_vector(31 downto 0);
		ARINC_FIFO_RX_6 : OUT std_logic_vector(31 downto 0);
		ARINC_FIFO_RX_7 : OUT std_logic_vector(31 downto 0);
		ARINC_FIFO_RX_EMPTY : OUT std_logic_vector(7 downto 0);
		ARINC_FIFO_TX_FULL : OUT std_logic_vector(3 downto 0);
		MOSI : OUT std_logic;
		SCK : OUT std_logic;
		CS : OUT std_logic;
		AACK : OUT std_logic;
		MINTACK : OUT std_logic;
		MRST : OUT std_logic;
		ATXMSK : OUT std_logic;
		RUN : OUT std_logic
		);
	END COMPONENT;
	
	signal A429_CMD_IN_1 : std_logic_vector(31 downto 0);
	signal A429_CMD_IN_CLK_1 : std_logic;
	signal A429_CMD_IN_EN_1 : std_logic;
	signal A429_STATUS_OUT_CLK_1 : std_logic;
	signal A429_ARINC_FIFO_RX_CLK_1 : std_logic;
	signal A429_ARINC_FIFO_RX_READ_1 : std_logic_vector(7 downto 0);
	signal A429_ARINC_FIFO_TX_0_1 : std_logic_vector(31 downto 0);
	signal A429_ARINC_FIFO_TX_1_1 : std_logic_vector(31 downto 0);
	signal A429_ARINC_FIFO_TX_2_1 : std_logic_vector(31 downto 0);
	signal A429_ARINC_FIFO_TX_3_1 : std_logic_vector(31 downto 0);
	signal A429_ARINC_FIFO_TX_CLK_1 : std_logic;
	signal A429_ARINC_FIFO_TX_WRITE_1 : std_logic_vector(3 downto 0);     
	signal A429_STATUS_OUT_1 : std_logic_vector(31 downto 0);
	signal A429_ARINC_FIFO_RX_0_1 : std_logic_vector(31 downto 0);
	signal A429_ARINC_FIFO_RX_1_1 : std_logic_vector(31 downto 0);
	signal A429_ARINC_FIFO_RX_2_1 : std_logic_vector(31 downto 0);
	signal A429_ARINC_FIFO_RX_3_1 : std_logic_vector(31 downto 0);
	signal A429_ARINC_FIFO_RX_4_1 : std_logic_vector(31 downto 0);
	signal A429_ARINC_FIFO_RX_5_1 : std_logic_vector(31 downto 0);
	signal A429_ARINC_FIFO_RX_6_1 : std_logic_vector(31 downto 0);
	signal A429_ARINC_FIFO_RX_7_1 : std_logic_vector(31 downto 0);
	signal A429_ARINC_FIFO_RX_EMPTY_1 : std_logic_vector(7 downto 0);
	signal A429_ARINC_FIFO_TX_FULL_1 : std_logic_vector(3 downto 0);
	
	signal A429_CMD_IN_2 : std_logic_vector(31 downto 0);
	signal A429_CMD_IN_CLK_2 : std_logic;
	signal A429_CMD_IN_EN_2 : std_logic;
	signal A429_STATUS_OUT_CLK_2 : std_logic;
	signal A429_ARINC_FIFO_RX_CLK_2 : std_logic;
	signal A429_ARINC_FIFO_RX_READ_2 : std_logic_vector(7 downto 0);
	signal A429_ARINC_FIFO_TX_0_2 : std_logic_vector(31 downto 0);
	signal A429_ARINC_FIFO_TX_1_2 : std_logic_vector(31 downto 0);
	signal A429_ARINC_FIFO_TX_2_2 : std_logic_vector(31 downto 0);
	signal A429_ARINC_FIFO_TX_3_2 : std_logic_vector(31 downto 0);
	signal A429_ARINC_FIFO_TX_CLK_2 : std_logic;
	signal A429_ARINC_FIFO_TX_WRITE_2 : std_logic_vector(3 downto 0);     
	signal A429_STATUS_OUT_2 : std_logic_vector(31 downto 0);
	signal A429_ARINC_FIFO_RX_0_2 : std_logic_vector(31 downto 0);
	signal A429_ARINC_FIFO_RX_1_2 : std_logic_vector(31 downto 0);
	signal A429_ARINC_FIFO_RX_2_2 : std_logic_vector(31 downto 0);
	signal A429_ARINC_FIFO_RX_3_2 : std_logic_vector(31 downto 0);
	signal A429_ARINC_FIFO_RX_4_2 : std_logic_vector(31 downto 0);
	signal A429_ARINC_FIFO_RX_5_2 : std_logic_vector(31 downto 0);
	signal A429_ARINC_FIFO_RX_6_2 : std_logic_vector(31 downto 0);
	signal A429_ARINC_FIFO_RX_7_2 : std_logic_vector(31 downto 0);
	signal A429_ARINC_FIFO_RX_EMPTY_2 : std_logic_vector(7 downto 0);
	signal A429_ARINC_FIFO_TX_FULL_2 : std_logic_vector(3 downto 0);
	
	signal A429_CMD_IN_3 : std_logic_vector(31 downto 0);
	signal A429_CMD_IN_CLK_3 : std_logic;
	signal A429_CMD_IN_EN_3 : std_logic;
	signal A429_STATUS_OUT_CLK_3 : std_logic;
	signal A429_ARINC_FIFO_RX_CLK_3 : std_logic;
	signal A429_ARINC_FIFO_RX_READ_3 : std_logic_vector(7 downto 0);
	signal A429_ARINC_FIFO_TX_0_3 : std_logic_vector(31 downto 0);
	signal A429_ARINC_FIFO_TX_1_3 : std_logic_vector(31 downto 0);
	signal A429_ARINC_FIFO_TX_2_3 : std_logic_vector(31 downto 0);
	signal A429_ARINC_FIFO_TX_3_3 : std_logic_vector(31 downto 0);
	signal A429_ARINC_FIFO_TX_CLK_3 : std_logic;
	signal A429_ARINC_FIFO_TX_WRITE_3 : std_logic_vector(3 downto 0);     
	signal A429_STATUS_OUT_3 : std_logic_vector(31 downto 0);
	signal A429_ARINC_FIFO_RX_0_3 : std_logic_vector(31 downto 0);
	signal A429_ARINC_FIFO_RX_1_3 : std_logic_vector(31 downto 0);
	signal A429_ARINC_FIFO_RX_2_3 : std_logic_vector(31 downto 0);
	signal A429_ARINC_FIFO_RX_3_3 : std_logic_vector(31 downto 0);
	signal A429_ARINC_FIFO_RX_4_3 : std_logic_vector(31 downto 0);
	signal A429_ARINC_FIFO_RX_5_3 : std_logic_vector(31 downto 0);
	signal A429_ARINC_FIFO_RX_6_3 : std_logic_vector(31 downto 0);
	signal A429_ARINC_FIFO_RX_7_3 : std_logic_vector(31 downto 0);
	signal A429_ARINC_FIFO_RX_EMPTY_3 : std_logic_vector(7 downto 0);
	signal A429_ARINC_FIFO_TX_FULL_3 : std_logic_vector(3 downto 0);
	
	signal A429_CMD_IN_4 : std_logic_vector(31 downto 0);
	signal A429_CMD_IN_CLK_4 : std_logic;
	signal A429_CMD_IN_EN_4 : std_logic;
	signal A429_STATUS_OUT_CLK_4 : std_logic;
	signal A429_ARINC_FIFO_RX_CLK_4 : std_logic;
	signal A429_ARINC_FIFO_RX_READ_4 : std_logic_vector(7 downto 0);
	signal A429_ARINC_FIFO_TX_0_4 : std_logic_vector(31 downto 0);
	signal A429_ARINC_FIFO_TX_1_4 : std_logic_vector(31 downto 0);
	signal A429_ARINC_FIFO_TX_2_4 : std_logic_vector(31 downto 0);
	signal A429_ARINC_FIFO_TX_3_4 : std_logic_vector(31 downto 0);
	signal A429_ARINC_FIFO_TX_CLK_4 : std_logic;
	signal A429_ARINC_FIFO_TX_WRITE_4 : std_logic_vector(3 downto 0);     
	signal A429_STATUS_OUT_4 : std_logic_vector(31 downto 0);
	signal A429_ARINC_FIFO_RX_0_4 : std_logic_vector(31 downto 0);
	signal A429_ARINC_FIFO_RX_1_4 : std_logic_vector(31 downto 0);
	signal A429_ARINC_FIFO_RX_2_4 : std_logic_vector(31 downto 0);
	signal A429_ARINC_FIFO_RX_3_4 : std_logic_vector(31 downto 0);
	signal A429_ARINC_FIFO_RX_4_4 : std_logic_vector(31 downto 0);
	signal A429_ARINC_FIFO_RX_5_4 : std_logic_vector(31 downto 0);
	signal A429_ARINC_FIFO_RX_6_4 : std_logic_vector(31 downto 0);
	signal A429_ARINC_FIFO_RX_7_4 : std_logic_vector(31 downto 0);
	signal A429_ARINC_FIFO_RX_EMPTY_4 : std_logic_vector(7 downto 0);
	signal A429_ARINC_FIFO_TX_FULL_4 : std_logic_vector(3 downto 0);
	
	------------------------------------------------------------------------------Serial Port
component Serial_Port
PORT	(
		  Clk 		:	in  STD_LOGIC;
		  ClkInn		:	in  STD_LOGIC;
		  Reset 		:	in  STD_LOGIC;
		  PortIn 	:	in  STD_LOGIC_VECTOR (31 downto 0);
		  PortOut 	:	out STD_LOGIC_VECTOR (31 downto 0);
		  CfgIn	 	:	in  STD_LOGIC_VECTOR (31 downto 0);
		  CfgOut 	:	out STD_LOGIC_VECTOR (31 downto 0);
		  RxData		:	in	 STD_LOGIC;
		  TxData		:	out STD_LOGIC;
		  RxEnable	:	out	STD_LOGIC;
		  TxEnable	:	out	STD_LOGIC;
		  CmdDataRd	:	in	 STD_LOGIC;
		  CmdDataWr	:	in	 STD_LOGIC;
		  CmdCfgRd	:	in	 STD_LOGIC;
		  CmdCfgWr	:	in	 STD_LOGIC
		  );
end component;

	signal RsSysClk_1	:  std_logic;
	
	signal PortIn_1	:	std_logic_vector(31 downto 0);
	signal PortOut_1	:	std_logic_vector(31 downto 0);
	signal CfgIn_1		:	std_logic_vector(31 downto 0);
	signal CfgOut_1	:	std_logic_vector(31 downto 0);
	signal CmdDataRd_1:	STD_LOGIC;
	signal CmdDataWr_1:	STD_LOGIC;
	signal CmdCfgRd_1	:	STD_LOGIC;
	signal CmdCfgWr_1	:	STD_LOGIC;
	signal i_UART1_RX	:	STD_LOGIC;
	signal i_UART1_TX	:	STD_LOGIC;
	signal i_RxEn_1	:	STD_LOGIC;
	signal i_TxEn_1	:	STD_LOGIC;

	signal PortIn_2	:	std_logic_vector(31 downto 0);
	signal PortOut_2	:	std_logic_vector(31 downto 0);
	signal CfgIn_2		:	std_logic_vector(31 downto 0);
	signal CfgOut_2	:	std_logic_vector(31 downto 0);
	signal CmdDataRd_2:	STD_LOGIC;
	signal CmdDataWr_2:	STD_LOGIC;
	signal CmdCfgRd_2	:	STD_LOGIC;
	signal CmdCfgWr_2	:	STD_LOGIC;
	signal i_UART2_RX	:	STD_LOGIC;
	signal i_UART2_TX	:	STD_LOGIC;
	signal i_RxEn_2	:	STD_LOGIC;
	signal i_TxEn_2	:	STD_LOGIC;

	signal PortIn_3	:	std_logic_vector(31 downto 0);
	signal PortOut_3	:	std_logic_vector(31 downto 0);
	signal CfgIn_3		:	std_logic_vector(31 downto 0);
	signal CfgOut_3	:	std_logic_vector(31 downto 0);
	signal CmdDataRd_3:	STD_LOGIC;
	signal CmdDataWr_3:	STD_LOGIC;
	signal CmdCfgRd_3	:	STD_LOGIC;
	signal CmdCfgWr_3	:	STD_LOGIC;
	signal i_UART3_RX	:	STD_LOGIC;
	signal i_UART3_TX	:	STD_LOGIC;
	signal i_RxEn_3	:	STD_LOGIC;
	signal i_TxEn_3	:	STD_LOGIC;

	signal PortIn_4	:	std_logic_vector(31 downto 0);
	signal PortOut_4	:	std_logic_vector(31 downto 0);
	signal CfgIn_4		:	std_logic_vector(31 downto 0);
	signal CfgOut_4	:	std_logic_vector(31 downto 0);
	signal CmdDataRd_4:	STD_LOGIC;
	signal CmdDataWr_4:	STD_LOGIC;
	signal CmdCfgRd_4	:	STD_LOGIC;
	signal CmdCfgWr_4	:	STD_LOGIC;
	signal i_UART4_RX	:	STD_LOGIC;
	signal i_UART4_TX	:	STD_LOGIC;
	signal i_RxEn_4	:	STD_LOGIC;
	signal i_TxEn_4	:	STD_LOGIC;
	
	----------------------------------------------------------------------------------DIMM_SPI
component ADS8665
PORT(
	Clk 			: in  STD_LOGIC;
   Reset 		: in  STD_LOGIC;
   AdcData 		: out  STD_LOGIC_VECTOR (31 downto 0);
   AdcControl 	: in  STD_LOGIC_VECTOR (31 downto 0);
   CmdDataRead : in  STD_LOGIC;
   CmdCfgWrite : in  STD_LOGIC;
   CmdRawRd 	: in  STD_LOGIC;
   CmdRawWr 	: in  STD_LOGIC;
   Ads8665_SCK : out  STD_LOGIC;
   Ads8665_MOSI: out  STD_LOGIC;
   Ads8665_MISO: in  STD_LOGIC;
   Ads8665_CS 	: out  STD_LOGIC
  );
end component;
	signal DIMM_ADC_DATA	: STD_LOGIC_VECTOR(31 downto 0);
	signal DIMM_ADC_CFG	: STD_LOGIC_VECTOR(31 downto 0);
	signal i_CmdAdcRead	: STD_LOGIC;
	signal i_CmdAdcCfg	: STD_LOGIC;
	signal i_RawAdcRead	: STD_LOGIC;
	signal i_RawAdcWrite	: STD_LOGIC;



		
	--------------------------------------------------------------------------------- PCIe DMA
	
	
begin
	--------------------------------------------------------------------------------- PCIe DMA
	dma_arbiter_i: dma_arbiter PORT MAP (
		RESET => GLOBAL_RESET,	
		RESET_CLK => CLK_16,
		DMA_CLK => pci_clk,
		
		-- to slaves
		REQ(0) => A708_dma_req,
		GNT(0) => A708_dma_gnt,
		IN_CLK(0) => CLK_16,
		
		-- to PCIe app core
		start_address => dma_start_address,
		pci_dma_ready => pci_dma_ready,
		dma_cfg_address_set => dma_cfg_address_set,
		
		dma_cmd => dma_cmd,
		dma_cmd_rd_en => dma_cmd_rd_en,
		dma_cmd_empty => dma_cmd_empty,
		
		dma_data => dma_data,
		dma_data_rd_en => dma_data_rd_en,
		dma_data_rd_data_count => dma_data_rd_data_count,
		
		-- to slaves		
		s_start_address(0) => A708_dma_start_address,
		s_dma_cfg_address_set(0) => A708_dma_cfg_address_set,
		s_pci_dma_ready(0) => A708_pci_dma_ready,
		
		dma_data_a(0) => a708_dma_data,
		dma_data_rd_en_a(0) => a708_dma_data_rd_en,
		dma_data_rd_data_count_a(0) => a708_dma_data_rd_data_count,
		
		dma_cmd_a(0) => A708_dma_cmd,
		dma_cmd_rd_en_a(0) => A708_dma_cmd_rd_en,
		dma_cmd_empty_a(0) => A708_dma_cmd_empty
	);

	--------------------------------------------------------------------------------- Global RESET
	u_reset : reset
	port map (
		CLK           => CLK_16,
		RESET         => GLOBAL_RESET,
		PLL_LOCK      => DCM_LOCKED,
		PCI_RESET     => pcie_rst
	);

	pcie_app_s6_i : pcie_app_s6
	generic map (
		enable_arinc_429  => enable_arinc_429,
		enable_dio_input  => enable_dio_input,
		enable_dio_output => enable_dio_output
	)
	port map
	(
		-- PCI Express (PCI_EXP) Fabric Interface
		pci_exp_txp                        => pci_exp_txp,
		pci_exp_txn                        => pci_exp_txn,
		pci_exp_rxp                        => pci_exp_rxp,
		pci_exp_rxn                        => pci_exp_rxn,
		sys_clk_p                          => sys_clk_p,
		sys_clk_n                          => sys_clk_n,
		
		sys_reset                          => pci_sys_reset,
		
		-- Transaction (TRN) Interface
		-- Common lock & reset
		user_clk                           => pci_clk,
		user_reset                         => pci_reset,
		user_lnk_up                        => pci_lnk_up,
		
		-- Interrupt command input FIFO
		int_cmd_wr_clk                     => int_cmd_wr_clk,
		int_cmd_wr_en                      => int_cmd_wr_en,
		int_cmd                            => int_cmd,
		int_cmd_num                        => int_cmd_num,
		int_cmd_full                       => int_cmd_full,
		
		-- Interrupt state signals (PCIe clock domain)
		cfg_interrupt_mmenable             => pci_cfg_interrupt_mmenable,
		cfg_interrupt_msienable            => pci_cfg_interrupt_msienable,
		
		-- DMA
		pci_dma_ready                      => pci_dma_ready,
		start_address                      => dma_start_address,
		dma_cfg_address_set                => dma_cfg_address_set,
		
		dma_cmd                            => dma_cmd,
		dma_cmd_rd_en                      => dma_cmd_rd_en,
		dma_cmd_empty                      => dma_cmd_empty,
		
		dma_data                           => dma_data,
		dma_data_rd_en                     => dma_data_rd_en,
		dma_data_rd_data_count             => dma_data_rd_data_count,
		
		-- Discrete output
		OUT_FAULT_1    => OUT_FAULT_1,
		OUT_FAULT_2    => OUT_FAULT_2,
		OUT_FAULT_3    => OUT_FAULT_3,
		OUT_FAULT_4    => OUT_FAULT_4,
		OUT_READY      => i_OUT_READY,
		OUT_DATA       => i_OUT_DATA,
		OUT_DATA_CLK   => i_OUT_DATA_CLK,
		OUT_DATA_EN    => i_OUT_DATA_EN,
		
		-- Discrete inputs
		IN_DIN  => i_IN_DIN,
		IN_DIN_CLK  => i_IN_DIN_CLK,
		IN_DIN_EN  => i_IN_DIN_EN,
		
		IN_DOUT  => i_IN_DOUT,
		IN_DOUT_STATE  => i_IN_DOUT_STATE,
		IN_DOUT_CLK  => i_IN_DOUT_CLK,
		
		-- A429
		A429_CMD_IN_1 => A429_CMD_IN_1,
		A429_CMD_IN_CLK_1 => A429_CMD_IN_CLK_1,
		A429_CMD_IN_EN_1 => A429_CMD_IN_EN_1,
		A429_STATUS_OUT_1 => A429_STATUS_OUT_1,
		A429_STATUS_OUT_CLK_1 => A429_STATUS_OUT_CLK_1,
		A429_ARINC_FIFO_RX_CLK_1 => A429_ARINC_FIFO_RX_CLK_1,
		A429_ARINC_FIFO_RX_READ_1 => A429_ARINC_FIFO_RX_READ_1,
		A429_ARINC_FIFO_TX_0_1 => A429_ARINC_FIFO_TX_0_1,
		A429_ARINC_FIFO_TX_1_1 => A429_ARINC_FIFO_TX_1_1,
		A429_ARINC_FIFO_TX_2_1 => A429_ARINC_FIFO_TX_2_1,
		A429_ARINC_FIFO_TX_3_1 => A429_ARINC_FIFO_TX_3_1,
		A429_ARINC_FIFO_TX_CLK_1 => A429_ARINC_FIFO_TX_CLK_1,
		A429_ARINC_FIFO_RX_0_1 => A429_ARINC_FIFO_RX_0_1,
		A429_ARINC_FIFO_RX_1_1 => A429_ARINC_FIFO_RX_1_1,
		A429_ARINC_FIFO_RX_2_1 => A429_ARINC_FIFO_RX_2_1,
		A429_ARINC_FIFO_RX_3_1 => A429_ARINC_FIFO_RX_3_1,
		A429_ARINC_FIFO_RX_4_1 => A429_ARINC_FIFO_RX_4_1,
		A429_ARINC_FIFO_RX_5_1 => A429_ARINC_FIFO_RX_5_1,
		A429_ARINC_FIFO_RX_6_1 => A429_ARINC_FIFO_RX_6_1,
		A429_ARINC_FIFO_RX_7_1 => A429_ARINC_FIFO_RX_7_1,
		A429_ARINC_FIFO_RX_EMPTY_1 => A429_ARINC_FIFO_RX_EMPTY_1,
		A429_ARINC_FIFO_TX_FULL_1 => A429_ARINC_FIFO_TX_FULL_1,
		A429_ARINC_FIFO_TX_WRITE_1 => A429_ARINC_FIFO_TX_WRITE_1,

		A429_CMD_IN_2 => A429_CMD_IN_2,
		A429_CMD_IN_CLK_2 => A429_CMD_IN_CLK_2,
		A429_CMD_IN_EN_2 => A429_CMD_IN_EN_2,
		A429_STATUS_OUT_2 => A429_STATUS_OUT_2,
		A429_STATUS_OUT_CLK_2 => A429_STATUS_OUT_CLK_2,
		A429_ARINC_FIFO_RX_CLK_2 => A429_ARINC_FIFO_RX_CLK_2,
		A429_ARINC_FIFO_RX_READ_2 => A429_ARINC_FIFO_RX_READ_2,
		A429_ARINC_FIFO_TX_0_2 => A429_ARINC_FIFO_TX_0_2,
		A429_ARINC_FIFO_TX_1_2 => A429_ARINC_FIFO_TX_1_2,
		A429_ARINC_FIFO_TX_2_2 => A429_ARINC_FIFO_TX_2_2,
		A429_ARINC_FIFO_TX_3_2 => A429_ARINC_FIFO_TX_3_2,
		A429_ARINC_FIFO_TX_CLK_2 => A429_ARINC_FIFO_TX_CLK_2,
		A429_ARINC_FIFO_RX_0_2 => A429_ARINC_FIFO_RX_0_2,
		A429_ARINC_FIFO_RX_1_2 => A429_ARINC_FIFO_RX_1_2,
		A429_ARINC_FIFO_RX_2_2 => A429_ARINC_FIFO_RX_2_2,
		A429_ARINC_FIFO_RX_3_2 => A429_ARINC_FIFO_RX_3_2,
		A429_ARINC_FIFO_RX_4_2 => A429_ARINC_FIFO_RX_4_2,
		A429_ARINC_FIFO_RX_5_2 => A429_ARINC_FIFO_RX_5_2,
		A429_ARINC_FIFO_RX_6_2 => A429_ARINC_FIFO_RX_6_2,
		A429_ARINC_FIFO_RX_7_2 => A429_ARINC_FIFO_RX_7_2,
		A429_ARINC_FIFO_RX_EMPTY_2 => A429_ARINC_FIFO_RX_EMPTY_2,
		A429_ARINC_FIFO_TX_FULL_2 => A429_ARINC_FIFO_TX_FULL_2,
		A429_ARINC_FIFO_TX_WRITE_2 => A429_ARINC_FIFO_TX_WRITE_2,
		
		A429_CMD_IN_3 => A429_CMD_IN_3,
		A429_CMD_IN_CLK_3 => A429_CMD_IN_CLK_3,
		A429_CMD_IN_EN_3 => A429_CMD_IN_EN_3,
		A429_STATUS_OUT_3 => A429_STATUS_OUT_3,
		A429_STATUS_OUT_CLK_3 => A429_STATUS_OUT_CLK_3,
		A429_ARINC_FIFO_RX_CLK_3 => A429_ARINC_FIFO_RX_CLK_3,
		A429_ARINC_FIFO_RX_READ_3 => A429_ARINC_FIFO_RX_READ_3,
		A429_ARINC_FIFO_TX_0_3 => A429_ARINC_FIFO_TX_0_3,
		A429_ARINC_FIFO_TX_1_3 => A429_ARINC_FIFO_TX_1_3,
		A429_ARINC_FIFO_TX_2_3 => A429_ARINC_FIFO_TX_2_3,
		A429_ARINC_FIFO_TX_3_3 => A429_ARINC_FIFO_TX_3_3,
		A429_ARINC_FIFO_TX_CLK_3 => A429_ARINC_FIFO_TX_CLK_3,
		A429_ARINC_FIFO_RX_0_3 => A429_ARINC_FIFO_RX_0_3,
		A429_ARINC_FIFO_RX_1_3 => A429_ARINC_FIFO_RX_1_3,
		A429_ARINC_FIFO_RX_2_3 => A429_ARINC_FIFO_RX_2_3,
		A429_ARINC_FIFO_RX_3_3 => A429_ARINC_FIFO_RX_3_3,
		A429_ARINC_FIFO_RX_4_3 => A429_ARINC_FIFO_RX_4_3,
		A429_ARINC_FIFO_RX_5_3 => A429_ARINC_FIFO_RX_5_3,
		A429_ARINC_FIFO_RX_6_3 => A429_ARINC_FIFO_RX_6_3,
		A429_ARINC_FIFO_RX_7_3 => A429_ARINC_FIFO_RX_7_3,
		A429_ARINC_FIFO_RX_EMPTY_3 => A429_ARINC_FIFO_RX_EMPTY_3,
		A429_ARINC_FIFO_TX_FULL_3 => A429_ARINC_FIFO_TX_FULL_3,
		A429_ARINC_FIFO_TX_WRITE_3 => A429_ARINC_FIFO_TX_WRITE_3,
		
		A429_CMD_IN_4 => A429_CMD_IN_4,
		A429_CMD_IN_CLK_4 => A429_CMD_IN_CLK_4,
		A429_CMD_IN_EN_4 => A429_CMD_IN_EN_4,
		A429_STATUS_OUT_4 => A429_STATUS_OUT_4,
		A429_STATUS_OUT_CLK_4 => A429_STATUS_OUT_CLK_4,
		A429_ARINC_FIFO_RX_CLK_4 => A429_ARINC_FIFO_RX_CLK_4,
		A429_ARINC_FIFO_RX_READ_4 => A429_ARINC_FIFO_RX_READ_4,
		A429_ARINC_FIFO_TX_0_4 => A429_ARINC_FIFO_TX_0_4,
		A429_ARINC_FIFO_TX_1_4 => A429_ARINC_FIFO_TX_1_4,
		A429_ARINC_FIFO_TX_2_4 => A429_ARINC_FIFO_TX_2_4,
		A429_ARINC_FIFO_TX_3_4 => A429_ARINC_FIFO_TX_3_4,
		A429_ARINC_FIFO_TX_CLK_4 => A429_ARINC_FIFO_TX_CLK_4,
		A429_ARINC_FIFO_RX_0_4 => A429_ARINC_FIFO_RX_0_4,
		A429_ARINC_FIFO_RX_1_4 => A429_ARINC_FIFO_RX_1_4,
		A429_ARINC_FIFO_RX_2_4 => A429_ARINC_FIFO_RX_2_4,
		A429_ARINC_FIFO_RX_3_4 => A429_ARINC_FIFO_RX_3_4,
		A429_ARINC_FIFO_RX_4_4 => A429_ARINC_FIFO_RX_4_4,
		A429_ARINC_FIFO_RX_5_4 => A429_ARINC_FIFO_RX_5_4,
		A429_ARINC_FIFO_RX_6_4 => A429_ARINC_FIFO_RX_6_4,
		A429_ARINC_FIFO_RX_7_4 => A429_ARINC_FIFO_RX_7_4,
		A429_ARINC_FIFO_RX_EMPTY_4 => A429_ARINC_FIFO_RX_EMPTY_4,
		A429_ARINC_FIFO_TX_FULL_4 => A429_ARINC_FIFO_TX_FULL_4,
		A429_ARINC_FIFO_TX_WRITE_4 => A429_ARINC_FIFO_TX_WRITE_4,
		
		A708_DMA_ADDR_CLK => A708_DMA_ADDR_CLK,
		A708_DMA_ADDR => A708_DMA_ADDR,
		
		A708_STATE => A708_STATE,
		A708_STATE_CLK => A708_STATE_CLK,
		A708_CMD => A708_CMD,
		A708_CMD_EN => A708_CMD_EN,
		A708_CMD_CLK => A708_CMD_CLK,

		RsSysClk		=>		RsSysClk_1,
 	   PortIn_1 	=> 	PortIn_1,
	   PortOut_1	=> 	PortOut_1,
 	   CfgIn_1 		=> 	CfgIn_1,
	   CfgOut_1 	=> 	CfgOut_1,
		CmdDataRd_1	=>		CmdDataRd_1,
		CmdDataWr_1	=> 	CmdDataWr_1,
		CmdCfgRd_1	=> 	CmdCfgRd_1,
		CmdCfgWr_1	=>		CmdCfgWr_1,		

 	   PortIn_2 	=> 	PortIn_2,
	   PortOut_2	=> 	PortOut_2,
 	   CfgIn_2 		=> 	CfgIn_2,
	   CfgOut_2 	=> 	CfgOut_2,
		CmdDataRd_2	=>		CmdDataRd_2,
		CmdDataWr_2	=> 	CmdDataWr_2,
		CmdCfgRd_2	=> 	CmdCfgRd_2,
		CmdCfgWr_2	=>		CmdCfgWr_2,		

 	   PortIn_3 	=> 	PortIn_3,
	   PortOut_3	=> 	PortOut_3,
 	   CfgIn_3 		=> 	CfgIn_3,
	   CfgOut_3 	=> 	CfgOut_3,
		CmdDataRd_3	=>		CmdDataRd_3,
		CmdDataWr_3	=> 	CmdDataWr_3,
		CmdCfgRd_3	=> 	CmdCfgRd_3,
		CmdCfgWr_3	=>		CmdCfgWr_3,		

 	   PortIn_4 	=> 	PortIn_4,
	   PortOut_4	=> 	PortOut_4,
 	   CfgIn_4 		=> 	CfgIn_4,
	   CfgOut_4 	=> 	CfgOut_4,
		CmdDataRd_4	=>		CmdDataRd_4,
		CmdDataWr_4	=> 	CmdDataWr_4,
		CmdCfgRd_4	=> 	CmdCfgRd_4,
		CmdCfgWr_4	=>		CmdCfgWr_4,

		Wdt_Tick		=>		i_Wdt_Tick,

		DIMM_ADC_DATA	=>	DIMM_ADC_DATA,
		DIMM_ADC_CFG	=> DIMM_ADC_CFG,
	   CmdAdcRead		=>	i_CmdAdcRead,
	   CmdAdcCfg		=>	i_CmdAdcCfg,
	   RawAdcRead		=>	i_RawAdcRead,
	   RawAdcWrite		=>	i_RawAdcWrite
	);
	
	pci_sys_reset <= GLOBAL_RESET;

	--------------------------------------------------------------------------------- Main Clock
	clknetwork : main_dcm
	port map
	(
		-- Clock in ports
		CLK_IN_50             => GCLK,
		-- Clock out ports
		CLK_OUT_16            => CLK_16,
		CLK_OUT_25            => CLK_25,
		CLK_OUT_40            => CLK_40,
		-- Status and control signals
		RESET                 => DCM_RESET,
		LOCKED                => DCM_LOCKED
	);
	
	DCM_RESET <= '0';
	
	--------------------------------------------------------------------------------- D OUTPUT
enable_dio_input_generate:
if enable_dio_input generate
	DIO_DRV8804_i: DIO_DRV8804 PORT MAP(
		RESET => GLOBAL_RESET,
		RESET_CLK => CLK_16,
		CLK => CLK_25,
		DATA => i_OUT_DATA,
		DATA_CLK => i_OUT_DATA_CLK,
		DATA_EN => i_OUT_DATA_EN,
		OUT_CS => i_OUT_CS,
		OUT_SCK => i_OUT_SCK,
		OUT_MOSI => i_OUT_MOSI,
		OUT_ENBL_1 => OUT_ENBL_1,
		OUT_RESET_1 => OUT_RESET_1,
		OUT_ENBL_2 => OUT_ENBL_2,
		OUT_RESET_2 => OUT_RESET_2,
		OUT_ENBL_3 => OUT_ENBL_3,
		OUT_RESET_3 => OUT_RESET_3,
		OUT_ENBL_4 => OUT_ENBL_4,
		OUT_RESET_4 => OUT_RESET_4,
		READY => i_OUT_READY
	);
end generate;
	
	--------------------------------------------------------------------------------- D OUTPUT
enable_dio_output_generate:
if enable_dio_output generate
	DIO_HI8435_i: DIO_HI8435 PORT MAP(
		RESET => GLOBAL_RESET,
		RESET_CLK => CLK_16,
		CLK => CLK_25,
		
		DIN => i_IN_DIN,
		DIN_CLK => i_IN_DIN_CLK,
		DIN_EN => i_IN_DIN_EN,
		
		DOUT => i_IN_DOUT,
		DOUT_STATE => i_IN_DOUT_STATE,
		DOUT_CLK => i_IN_DOUT_CLK,
		
		CS => i_IN_CS,
		SCK => i_IN_SCK,
		MOSI => i_IN_MOSI,
		MISO => IN_MISO,
		MRB => i_IN_MRB
	);
end generate;
	
	--------------------------------------------------------------------------------- ARINC
enable_arinc_429_generate:
if enable_arinc_429 generate
	ARINC_HI3210_i1: ARINC_HI3210 PORT MAP(
		RESET => GLOBAL_RESET,
		RESET_CLK => CLK_16,
		CLK => CLK_40,
		
		CMD_IN => A429_CMD_IN_1,
		CMD_IN_CLK => A429_CMD_IN_CLK_1,
		CMD_IN_EN => A429_CMD_IN_EN_1,
		STATUS_OUT => A429_STATUS_OUT_1,
		STATUS_OUT_CLK => A429_STATUS_OUT_CLK_1,
		ARINC_FIFO_RX_0 => A429_ARINC_FIFO_RX_0_1,
		ARINC_FIFO_RX_1 => A429_ARINC_FIFO_RX_1_1,
		ARINC_FIFO_RX_2 => A429_ARINC_FIFO_RX_2_1,
		ARINC_FIFO_RX_3 => A429_ARINC_FIFO_RX_3_1,
		ARINC_FIFO_RX_4 => A429_ARINC_FIFO_RX_4_1,
		ARINC_FIFO_RX_5 => A429_ARINC_FIFO_RX_5_1,
		ARINC_FIFO_RX_6 => A429_ARINC_FIFO_RX_6_1,
		ARINC_FIFO_RX_7 => A429_ARINC_FIFO_RX_7_1,
		ARINC_FIFO_RX_CLK => A429_ARINC_FIFO_RX_CLK_1,
		ARINC_FIFO_RX_READ => A429_ARINC_FIFO_RX_READ_1,
		ARINC_FIFO_RX_EMPTY => A429_ARINC_FIFO_RX_EMPTY_1,
		ARINC_FIFO_TX_0 => A429_ARINC_FIFO_TX_0_1,
		ARINC_FIFO_TX_1 => A429_ARINC_FIFO_TX_1_1,
		ARINC_FIFO_TX_2 => A429_ARINC_FIFO_TX_2_1,
		ARINC_FIFO_TX_3 => A429_ARINC_FIFO_TX_3_1,
		ARINC_FIFO_TX_CLK => A429_ARINC_FIFO_TX_CLK_1,
		ARINC_FIFO_TX_WRITE => A429_ARINC_FIFO_TX_WRITE_1,
		ARINC_FIFO_TX_FULL => A429_ARINC_FIFO_TX_FULL_1,
		
		MISO => i_A429_MISO_1,
		MOSI => i_A429_MOSI_1,
		SCK => i_A429_SCK_1,
		CS => i_A429_CS_1,
		AACK => A429_AACK_1,
		AINT => A429_AINT_1,
		MINT => A429_MINT_1,
		MINTACK => A429_MINTACK_1,
		MRST => A429_RESET_1,
		ATXMSK => A429_TXMSK_1,
		RUN => A429_RUN_1,
		READY => A429_READY_1
	);

	ARINC_HI3210_i2: ARINC_HI3210 PORT MAP(
		RESET => GLOBAL_RESET,
		RESET_CLK => CLK_16,
		CLK => CLK_40,
		
		CMD_IN => A429_CMD_IN_2,
		CMD_IN_CLK => A429_CMD_IN_CLK_2,
		CMD_IN_EN => A429_CMD_IN_EN_2,
		STATUS_OUT => A429_STATUS_OUT_2,
		STATUS_OUT_CLK => A429_STATUS_OUT_CLK_2,
		ARINC_FIFO_RX_0 => A429_ARINC_FIFO_RX_0_2,
		ARINC_FIFO_RX_1 => A429_ARINC_FIFO_RX_1_2,
		ARINC_FIFO_RX_2 => A429_ARINC_FIFO_RX_2_2,
		ARINC_FIFO_RX_3 => A429_ARINC_FIFO_RX_3_2,
		ARINC_FIFO_RX_4 => A429_ARINC_FIFO_RX_4_2,
		ARINC_FIFO_RX_5 => A429_ARINC_FIFO_RX_5_2,
		ARINC_FIFO_RX_6 => A429_ARINC_FIFO_RX_6_2,
		ARINC_FIFO_RX_7 => A429_ARINC_FIFO_RX_7_2,
		ARINC_FIFO_RX_CLK => A429_ARINC_FIFO_RX_CLK_2,
		ARINC_FIFO_RX_READ => A429_ARINC_FIFO_RX_READ_2,
		ARINC_FIFO_RX_EMPTY => A429_ARINC_FIFO_RX_EMPTY_2,
		ARINC_FIFO_TX_0 => A429_ARINC_FIFO_TX_0_2,
		ARINC_FIFO_TX_1 => A429_ARINC_FIFO_TX_1_2,
		ARINC_FIFO_TX_2 => A429_ARINC_FIFO_TX_2_2,
		ARINC_FIFO_TX_3 => A429_ARINC_FIFO_TX_3_2,
		ARINC_FIFO_TX_CLK => A429_ARINC_FIFO_TX_CLK_2,
		ARINC_FIFO_TX_WRITE => A429_ARINC_FIFO_TX_WRITE_2,
		ARINC_FIFO_TX_FULL => A429_ARINC_FIFO_TX_FULL_2,
		
		MISO => A429_MISO_2,
		MOSI => A429_MOSI_2,
		SCK => A429_SCK_2,
		CS => A429_CS_2,
		AACK => A429_AACK_2,
		AINT => A429_AINT_2,
		MINT => A429_MINT_2,
		MINTACK => A429_MINTACK_2,
		MRST => A429_RESET_2,
		ATXMSK => A429_TXMSK_2,
		RUN => A429_RUN_2,
		READY => A429_READY_2
	);

	ARINC_HI3210_i3: ARINC_HI3210 PORT MAP(
		RESET => GLOBAL_RESET,
		RESET_CLK => CLK_16,
		CLK => CLK_40,
		
		CMD_IN => A429_CMD_IN_3,
		CMD_IN_CLK => A429_CMD_IN_CLK_3,
		CMD_IN_EN => A429_CMD_IN_EN_3,
		STATUS_OUT => A429_STATUS_OUT_3,
		STATUS_OUT_CLK => A429_STATUS_OUT_CLK_3,
		ARINC_FIFO_RX_0 => A429_ARINC_FIFO_RX_0_3,
		ARINC_FIFO_RX_1 => A429_ARINC_FIFO_RX_1_3,
		ARINC_FIFO_RX_2 => A429_ARINC_FIFO_RX_2_3,
		ARINC_FIFO_RX_3 => A429_ARINC_FIFO_RX_3_3,
		ARINC_FIFO_RX_4 => A429_ARINC_FIFO_RX_4_3,
		ARINC_FIFO_RX_5 => A429_ARINC_FIFO_RX_5_3,
		ARINC_FIFO_RX_6 => A429_ARINC_FIFO_RX_6_3,
		ARINC_FIFO_RX_7 => A429_ARINC_FIFO_RX_7_3,
		ARINC_FIFO_RX_CLK => A429_ARINC_FIFO_RX_CLK_3,
		ARINC_FIFO_RX_READ => A429_ARINC_FIFO_RX_READ_3,
		ARINC_FIFO_RX_EMPTY => A429_ARINC_FIFO_RX_EMPTY_3,
		ARINC_FIFO_TX_0 => A429_ARINC_FIFO_TX_0_3,
		ARINC_FIFO_TX_1 => A429_ARINC_FIFO_TX_1_3,
		ARINC_FIFO_TX_2 => A429_ARINC_FIFO_TX_2_3,
		ARINC_FIFO_TX_3 => A429_ARINC_FIFO_TX_3_3,
		ARINC_FIFO_TX_CLK => A429_ARINC_FIFO_TX_CLK_3,
		ARINC_FIFO_TX_WRITE => A429_ARINC_FIFO_TX_WRITE_3,
		ARINC_FIFO_TX_FULL => A429_ARINC_FIFO_TX_FULL_3,
		
		MISO => A429_MISO_3,
		MOSI => A429_MOSI_3,
		SCK => A429_SCK_3,
		CS => A429_CS_3,
		AACK => A429_AACK_3,
		AINT => A429_AINT_3,
		MINT => A429_MINT_3,
		MINTACK => A429_MINTACK_3,
		MRST => A429_RESET_3,
		ATXMSK => A429_TXMSK_3,
		RUN => A429_RUN_3,
		READY => A429_READY_3
	);
	
	ARINC_HI3210_i4: ARINC_HI3210 PORT MAP(
		RESET => GLOBAL_RESET,
		RESET_CLK => CLK_16,
		CLK => CLK_40,
		
		CMD_IN => A429_CMD_IN_4,
		CMD_IN_CLK => A429_CMD_IN_CLK_4,
		CMD_IN_EN => A429_CMD_IN_EN_4,
		STATUS_OUT => A429_STATUS_OUT_4,
		STATUS_OUT_CLK => A429_STATUS_OUT_CLK_4,
		ARINC_FIFO_RX_0 => A429_ARINC_FIFO_RX_0_4,
		ARINC_FIFO_RX_1 => A429_ARINC_FIFO_RX_1_4,
		ARINC_FIFO_RX_2 => A429_ARINC_FIFO_RX_2_4,
		ARINC_FIFO_RX_3 => A429_ARINC_FIFO_RX_3_4,
		ARINC_FIFO_RX_4 => A429_ARINC_FIFO_RX_4_4,
		ARINC_FIFO_RX_5 => A429_ARINC_FIFO_RX_5_4,
		ARINC_FIFO_RX_6 => A429_ARINC_FIFO_RX_6_4,
		ARINC_FIFO_RX_7 => A429_ARINC_FIFO_RX_7_4,
		ARINC_FIFO_RX_CLK => A429_ARINC_FIFO_RX_CLK_4,
		ARINC_FIFO_RX_READ => A429_ARINC_FIFO_RX_READ_4,
		ARINC_FIFO_RX_EMPTY => A429_ARINC_FIFO_RX_EMPTY_4,
		ARINC_FIFO_TX_0 => A429_ARINC_FIFO_TX_0_4,
		ARINC_FIFO_TX_1 => A429_ARINC_FIFO_TX_1_4,
		ARINC_FIFO_TX_2 => A429_ARINC_FIFO_TX_2_4,
		ARINC_FIFO_TX_3 => A429_ARINC_FIFO_TX_3_4,
		ARINC_FIFO_TX_CLK => A429_ARINC_FIFO_TX_CLK_4,
		ARINC_FIFO_TX_WRITE => A429_ARINC_FIFO_TX_WRITE_4,
		ARINC_FIFO_TX_FULL => A429_ARINC_FIFO_TX_FULL_4,
		
		MISO => A429_MISO_4,
		MOSI => A429_MOSI_4,
		SCK => A429_SCK_4,
		CS => A429_CS_4,
		AACK => A429_AACK_4,
		AINT => A429_AINT_4,
		MINT => A429_MINT_4,
		MINTACK => A429_MINTACK_4,
		MRST => A429_RESET_4,
		ATXMSK => A429_TXMSK_4,
		RUN => A429_RUN_4,
		READY => A429_READY_4
	);
end generate;

--------------------------------------------------------------------------------- ARINC 708
enable_arinc_708_generate:
if enable_arinc_708 generate
	ARINC_708_i: ARINC_708 PORT MAP(
		RESET => GLOBAL_RESET,
		RESET_CLK => CLK_16,
		
		CLK16 => CLK_16,
		
		TXINHA => A708_TXINHA,
		TXA_P => A708_TXA_PLUS,
		TXA_N => A708_TXA_MINUS,
		CLKA => A708_CLKA,
		ENCLKA => A708_ENCLKA,
		TOC0A => A708_TOC0A,
		TOC1A => A708_TOC1A,
		TOC2A => A708_TOC2A,
		RXA_P => A708_RXA_PLUS,
		RXA_N => A708_RXA_MINUS,
		RXENA => A708_RXENA,
		ENPEXTA => A708_ENPEXTA,
		
		TXINHB => A708_TXINHB,
		TXB_P => A708_TXB_PLUS,
		TXB_N => A708_TXB_MINUS,
		CLKB => A708_CLKB,
		ENCLKB => A708_ENCLKB,
		TOC0B => A708_TOC0B,
		TOC1B => A708_TOC1B,
		TOC2B => A708_TOC2B,
		RXB_P => A708_RXB_PLUS,
		RXB_N => A708_RXB_MINUS,
		RXENB => A708_RXENB,
		ENPEXTB => A708_ENPEXTB,
		debug => A708_DEBUG,
		debug2 => A708_DEBUG2,
		
		A708_DMA_ADDR_CLK => A708_DMA_ADDR_CLK,
		A708_DMA_ADDR => A708_DMA_ADDR,
		
		A708_STATE => A708_STATE,
		A708_STATE_CLK => A708_STATE_CLK,
		
		A708_CMD => A708_CMD,
		A708_CMD_EN => A708_CMD_EN,
		A708_CMD_CLK => A708_CMD_CLK,
		
		-- DMA
		dma_start_address => A708_dma_start_address,
		pci_dma_ready => A708_pci_dma_ready,
		dma_cfg_address_set => A708_dma_cfg_address_set,
		
		dma_cmd => A708_dma_cmd,
		dma_cmd_rd_en => A708_dma_cmd_rd_en,
		dma_cmd_empty => A708_dma_cmd_empty,
		
		dma_data => A708_dma_data,
		dma_data_rd_en => A708_dma_data_rd_en,
		dma_data_rd_data_count => A708_dma_data_rd_data_count,
		
		dma_gnt => A708_dma_gnt,
		dma_req => A708_dma_req,
		
		dma_clk => pci_clk
	);
end generate;

RS232_1:	Serial_Port
PORT	MAP(
		  Clk 		=>	RsSysClk_1,
		  ClkInn		=>	CLK_25, -- CLK_SERIAL
		  Reset 		=>	GLOBAL_RESET,
		  PortIn 	=>	PortIn_1,
		  PortOut 	=>	PortOut_1,
		  CfgIn 		=>	CfgIn_1,
		  CfgOut 	=>	CfgOut_1,
		  RxData		=>	i_UART1_RX,
		  TxData		=>	i_UART1_TX,
		  RxEnable	=>	i_RxEn_1,
		  TxEnable	=> i_TxEn_1,
		  CmdDataRd	=>	CmdDataRd_1,
		  CmdDataWr =>	CmdDataWr_1,
		  CmdCfgRd  =>	CmdCfgRd_1,
		  CmdCfgWr  =>	CmdCfgWr_1	
		  );
		  i_UART1_RX <= UART_RX_1;
		  UART_TX_1 <= i_UART1_TX;
		  
RS232_2:	Serial_Port
PORT	MAP(
		  Clk 		=>	RsSysClk_1,
		  ClkInn		=>	CLK_25, -- CLK_SERIAL
		  Reset 		=>	GLOBAL_RESET,
		  PortIn 	=>	PortIn_2,
		  PortOut 	=>	PortOut_2,
		  CfgIn 		=>	CfgIn_2,
		  CfgOut 	=>	CfgOut_2,
		  RxData		=>	i_UART2_RX,
		  TxData		=>	i_UART2_TX,
		  RxEnable	=>	i_RxEn_2,
		  TxEnable	=> i_TxEn_2,
		  CmdDataRd	=>	CmdDataRd_2,
		  CmdDataWr =>	CmdDataWr_2,
		  CmdCfgRd  =>	CmdCfgRd_2,
		  CmdCfgWr  =>	CmdCfgWr_2	
		  );
 		  i_UART2_RX <= UART_RX_2;
		  UART_TX_2 <= i_UART2_TX;
		
RS232_3:	Serial_Port
PORT	MAP(
		  Clk 		=>	RsSysClk_1,
		  ClkInn		=>	CLK_25, -- CLK_SERIAL
		  Reset 		=>	GLOBAL_RESET,
		  PortIn 	=>	PortIn_3,
		  PortOut 	=>	PortOut_3,
		  CfgIn 		=>	CfgIn_3,
		  CfgOut 	=>	CfgOut_3,
		  RxData		=>	i_UART3_RX,
		  TxData		=>	i_UART3_TX,
		  RxEnable	=>	i_RxEn_3,
		  TxEnable	=> i_TxEn_3,
		  CmdDataRd	=>	CmdDataRd_3,
		  CmdDataWr =>	CmdDataWr_3,
		  CmdCfgRd  =>	CmdCfgRd_3,
		  CmdCfgWr  =>	CmdCfgWr_3	
		  );
 		  i_UART3_RX <= SERIAL_RX_1;
		  SERIAL_TX_1 <= i_UART3_TX;
		  SERIAL_RX_EN_1 <= i_RxEn_3;
		  SERIAL_TX_EN_1 <= i_TxEn_3;

RS232_4:	Serial_Port
PORT	MAP(
		  Clk 		=>	RsSysClk_1,
		  ClkInn		=>	CLK_25, -- CLK_SERIAL
		  Reset 		=>	GLOBAL_RESET,
		  PortIn 	=>	PortIn_4,
		  PortOut 	=>	PortOut_4,
		  CfgIn 		=>	CfgIn_4,
		  CfgOut 	=>	CfgOut_4,
		  RxData		=>	i_UART4_RX,
		  TxData		=>	i_UART4_TX,
		  RxEnable	=>	i_RxEn_4,
		  TxEnable	=> i_TxEn_4,
		  CmdDataRd	=>	CmdDataRd_4,
		  CmdDataWr =>	CmdDataWr_4,
		  CmdCfgRd  =>	CmdCfgRd_4,
		  CmdCfgWr  =>	CmdCfgWr_4	
	  );
 		  i_UART4_RX <= SERIAL_RX_2;
		  SERIAL_TX_2 <= i_UART4_TX;
		  SERIAL_RX_EN_2 <= i_RxEn_4;
		  SERIAL_TX_EN_2 <= i_TxEn_4;

i_ADS8665: ADS8665
PORT MAP(
		  Clk 			=>	RsSysClk_1,
		  Reset 			=>	GLOBAL_RESET,
        AdcData 		=>	DIMM_ADC_DATA,
        AdcControl 	=>	DIMM_ADC_CFG,
        CmdDataRead 	=> i_CmdAdcRead,
        CmdCfgWrite 	=> i_CmdAdcCfg,
        CmdRawRd 		=> i_RawAdcRead,
        CmdRawWr 		=> i_RawAdcWrite,
        Ads8665_SCK 	=> DIMM_SCK,
        Ads8665_MOSI => DIMM_MOSI,
        Ads8665_MISO => DIMM_MISO,
        Ads8665_CS 	=> DIMM_CS
);

		DBG1 <= i_UART3_RX;
		DBG2 <= i_UART3_TX;
		
		DBG3 <= i_RxEn_3;
		DBG4 <= i_TxEn_3;

--DBG1 <= A708_DEBUG;
--DBG2 <= A708_DEBUG2;
--DBG3 <= A708_RXA_PLUS;
--DBG4 <= A708_RXA_MINUS;

A429_CS_1 <= i_A429_CS_1;
A429_SCK_1 <= i_A429_SCK_1;
i_A429_MISO_1 <= A429_MISO_1;
A429_MOSI_1 <= i_A429_MOSI_1;

OUT_CS   <= i_OUT_CS;
OUT_SCK  <= i_OUT_SCK;
OUT_MOSI <= i_OUT_MOSI;

IN_CS <= i_IN_CS;
IN_SCK <= i_IN_SCK;
IN_MOSI <= i_IN_MOSI;
IN_MRB <= i_IN_MRB;


POWER: process (RsSysClk_1)
begin
 if RsSysClk_1'event and RsSysClk_1 = '1' then
	if Wdt_Start = '0' then
		if i_Wdt_Tick = '1' then
			Wdt_Start <= '1';
		end if;
		if WdtCount = X"0000" then
			WdtOut <= not WdtOut;
			WdtCount <= WdtTime;
		else
			WdtCount <= WdtCount - X"0001";
			case init_ph is
				when "00" =>
					BAT_CHRG <= '1';
					if BAT_STATE = '0' then
	--					DBG3 <= '1';
						init_tmr <= X"38";
					else
	--					DBG3 <= '0';	
						init_ph <= "01";
					end if;
				when "01" =>
					if WdtCount = X"0001" then
						init_tmr <= init_tmr - X"01";
						if init_tmr = X"00" then
							init_ph <= "10";
						end if;
					end if;
				when "10" =>
					Q7_PWREN <= '1';
				when "11" =>
					Q7_PWREN <= '0';
					init_tmr <= init_tmr - X"01";
					if init_tmr = X"00" then
						init_ph <= "00";
						init_tmr <= X"02";
					end if;
				when others =>
			end case;
		end if;
	else
		if i_Wdt_Tick = '1' then
			WdtOut <= not WdtOut;
--			i_Wdt_Tick <= '0';
		end if;
	end if;
	WATCHDOG_IN <= WdtOut;
	WATCHDOG_VM <= '0';
 end if;
end process POWER;

-- TODO:
ETH_RESET <= GLOBAL_RESET;

end Behavioral;
