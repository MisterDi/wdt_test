----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    09:38:11 03/17/2018 
-- Design Name: 
-- Module Name:    ARINC_HI3210 - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use IEEE.STD_LOGIC_unsigned.ALL;

library UNISIM;
use UNISIM.Vcomponents.ALL;

entity ARINC_HI3210 is
	Port ( 
		RESET : in STD_LOGIC;
		RESET_CLK : in STD_LOGIC;
		
		CLK : in STD_LOGIC;
		
		CMD_IN : in std_logic_vector(31 downto 0);
		CMD_IN_CLK : in std_logic;
		CMD_IN_EN : in std_logic;
		
		STATUS_OUT : out std_logic_vector(31 downto 0);
		STATUS_OUT_CLK : in  STD_LOGIC;
		
		ARINC_FIFO_RX_0 : out std_logic_vector(31 downto 0);
		ARINC_FIFO_RX_1 : out std_logic_vector(31 downto 0);
		ARINC_FIFO_RX_2 : out std_logic_vector(31 downto 0);
		ARINC_FIFO_RX_3 : out std_logic_vector(31 downto 0);
		ARINC_FIFO_RX_4 : out std_logic_vector(31 downto 0);
		ARINC_FIFO_RX_5 : out std_logic_vector(31 downto 0);
		ARINC_FIFO_RX_6 : out std_logic_vector(31 downto 0);
		ARINC_FIFO_RX_7 : out std_logic_vector(31 downto 0);
		ARINC_FIFO_RX_CLK : in STD_LOGIC;
		ARINC_FIFO_RX_READ : in std_logic_vector(7 downto 0);
		ARINC_FIFO_RX_EMPTY : out std_logic_vector(7 downto 0);
		
		ARINC_FIFO_TX_0 : in std_logic_vector(31 downto 0);
		ARINC_FIFO_TX_1 : in std_logic_vector(31 downto 0);
		ARINC_FIFO_TX_2 : in std_logic_vector(31 downto 0);
		ARINC_FIFO_TX_3 : in std_logic_vector(31 downto 0);
		ARINC_FIFO_TX_CLK : in STD_LOGIC;
		ARINC_FIFO_TX_WRITE : in std_logic_vector(3 downto 0);
		ARINC_FIFO_TX_FULL : out std_logic_vector(3 downto 0);
		
		-- Pins connections
		MISO : in STD_LOGIC;
		MOSI : out STD_LOGIC;
		SCK : out STD_LOGIC;
		CS : out STD_LOGIC;
		AACK : out STD_LOGIC;
		AINT : in STD_LOGIC;
		MINT : in STD_LOGIC;
		MINTACK : out STD_LOGIC;
		MRST : out STD_LOGIC;
		ATXMSK : out STD_LOGIC;
		RUN : out STD_LOGIC;
		READY : in STD_LOGIC
	);
end ARINC_HI3210;

architecture Behavioral of ARINC_HI3210 is

COMPONENT SPI_HI3210
	PORT(
		RESET : IN std_logic;
		CLK : IN std_logic;
		ADDR : IN std_logic_vector(15 downto 0);
		READ_EN : IN std_logic;
		WRITE_EN : IN std_logic;
		READ_ARINC_EN : IN std_logic;
		WRITE_ARINC_EN : IN std_logic;
		READ_ARINC_LABEL_EN : IN std_logic;
		READ_ARINC_BLOCK_EN : IN std_logic;
		DATA_IN : IN std_logic_vector(7 downto 0);
		ARINC_IN : IN std_logic_vector(31 downto 0);
		MISO : IN std_logic;          
		DONE : OUT std_logic;
		DATA_OUT : OUT std_logic_vector(7 downto 0);
		ARINC_OUT : OUT std_logic_vector(31 downto 0);
		CS : OUT std_logic;
		SCK : OUT std_logic;
		MOSI : OUT std_logic
		);
	END COMPONENT;

COMPONENT cdc_32_reg
	PORT(
		reset : IN std_logic;
		clk_in : IN std_logic;
		data_in : IN std_logic_vector(31 downto 0);
		clk_out : IN std_logic;          
		data_out : OUT std_logic_vector(31 downto 0)
	);
	END COMPONENT;

COMPONENT fifo_32_long
	PORT (
		rst            : IN STD_LOGIC;

		wr_clk         : IN STD_LOGIC;
		din            : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
		wr_en          : IN STD_LOGIC;
		full           : OUT STD_LOGIC;
		prog_full      : OUT STD_LOGIC;

		rd_clk         : IN STD_LOGIC;
		rd_en          : IN STD_LOGIC;
		dout           : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
		empty          : OUT STD_LOGIC
	);
	END COMPONENT;
	
signal i_CMD_IN : std_logic_vector(31 downto 0);
signal i_CMD_IN_EN : std_logic;
signal i_CMD_IN_EMPTY : std_logic;

signal spi_addr : std_logic_vector(15 downto 0);
signal spi_read_en : std_logic;
signal spi_write_en : std_logic;
signal spi_read_arinc_en : std_logic;
signal spi_write_arinc_en : std_logic;
signal spi_read_arinc_label_en : std_logic;
signal spi_read_arinc_block_en : std_logic;
signal spi_done : std_logic;
signal spi_data_in : std_logic_vector(7 downto 0);
signal spi_data_out : std_logic_vector(7 downto 0);
signal spi_arinc_in : std_logic_vector(31 downto 0);
signal spi_arinc_out : std_logic_vector(31 downto 0);

signal i_RUN : std_logic;
signal i_RUN_old : std_logic;
signal i_ATXMSK : std_logic;
signal i_MRST : std_logic;
signal i_status_out : std_logic_vector(31 downto 0);

signal fifo_reset : std_logic;

---------------------------------------------------------------------------- MASTER CONTROL REGISTER
type arinc_master_ctrl_type is record
		A429RX : std_logic;
		A429TX  : std_logic;
		AFLIP : std_logic;
	end record arinc_master_ctrl_type;

function vect_to_arinc_master_ctrl_type(
	constant din : std_logic_vector(7 downto 0))
	return arinc_master_ctrl_type is
	variable res : arinc_master_ctrl_type;
begin
	res.A429RX := din(7);
	res.A429TX := din(6);
	res.AFLIP := din(3);
	return res;
end vect_to_arinc_master_ctrl_type;

function arinc_master_ctrl_type_to_vect(
	constant din : arinc_master_ctrl_type)
	return std_logic_vector is
	variable res : std_logic_vector(7 downto 0);
begin
	res(7) := din.A429RX;
	res(6) := din.A429TX;
	res(3) := din.AFLIP;
	return res;
end arinc_master_ctrl_type_to_vect;
	
---------------------------------------------------------------------------- MASTER STATUS REGISTER
type arinc_master_status_type is record
		READY : std_logic;
		ACTIVE : std_logic;
		SAFE : std_logic;
		RAMBUSY : std_logic;
		PROG : std_logic;
		AUTOINIT : std_logic;
	end record arinc_master_status_type;

function vect_to_arinc_master_status_type(
	constant din : std_logic_vector(7 downto 0))
	return arinc_master_status_type is
	variable res : arinc_master_status_type;
begin
	res.READY := din(7);
	res.ACTIVE := din(6);
	res.SAFE := din(5);
	res.RAMBUSY := din(4);
	res.PROG := din(3);
	res.AUTOINIT := din(2);
	return res;
end vect_to_arinc_master_status_type;

---------------------------------------------------------------------------- ARINC 429 RX CONTROL REGISTER 0 - 7
type arinc_rx_ctrl_type is record
		ENABLE : std_logic;
		HI_LO : std_logic;
		PARITYEN : std_logic;
		DECODER : std_logic;
		SD10 : std_logic;
		SD9 : std_logic;
		FFS : std_logic_vector(1 downto 0);
	end record arinc_rx_ctrl_type;
	
function vect_to_arinc_rx_ctrl_type(
	constant din : std_logic_vector(7 downto 0))
	return arinc_rx_ctrl_type is
	variable res : arinc_rx_ctrl_type;
begin
	res.ENABLE := din(7);
	res.HI_LO := din(6);
	res.PARITYEN := din(5);
	res.DECODER := din(4);
	res.SD10 := din(3);
	res.SD9 := din(2);
	res.FFS := din(1 downto 0);
	return res;
end vect_to_arinc_rx_ctrl_type;

function arinc_rx_ctrl_type_to_vect(
	constant din : arinc_rx_ctrl_type)
	return std_logic_vector is
	variable res : std_logic_vector(7 downto 0);
begin
	res(7) := din.ENABLE;
	res(6) := din.HI_LO;
	res(5) := din.PARITYEN;
	res(4) := din.DECODER;
	res(3) := din.SD10;
	res(2) := din.SD9;
	res(1 downto 0) := din.FFS;
	return res;
end arinc_rx_ctrl_type_to_vect;

---------------------------------------------------------------------------- ARINC 429 TX CONTROL REGISTER 0 - 3
type arinc_tx_ctrl_type is record
		RUN_STOP : std_logic;
		HI_LO : std_logic;
		PARITY_DATA : std_logic;
		EVEN_ODD : std_logic;
		SKIP : std_logic;
	end record arinc_tx_ctrl_type;

function vect_to_arinc_tx_ctrl_type(
	constant din : std_logic_vector(7 downto 0))
	return arinc_tx_ctrl_type is
	variable res : arinc_tx_ctrl_type;
begin
	res.RUN_STOP := din(7);
	res.HI_LO := din(6);
	res.PARITY_DATA := din(5);
	res.EVEN_ODD := din(4);
	res.SKIP := din(3);
	return res;
end vect_to_arinc_tx_ctrl_type;

function arinc_tx_ctrl_type_to_vect(
	constant din : arinc_tx_ctrl_type)
	return std_logic_vector is
	variable res : std_logic_vector(7 downto 0);
begin
	res(7) := din.RUN_STOP;
	res(6) := din.HI_LO;
	res(5) := din.PARITY_DATA;
	res(4) := din.EVEN_ODD;
	res(3) := din.SKIP;
	return res;
end arinc_tx_ctrl_type_to_vect;

---------------------------------------------------------------------------- PENDING INTERRUPT REGISTER
type arinc_pend_int_type is record
		COPYERR : std_logic;
		AUTOERR : std_logic;
		CHKERR : std_logic;
		RAMFAIL : std_logic;
		FLAG : std_logic;
		ATXRDY : std_logic;
	end record arinc_pend_int_type;

function vect_to_arinc_pend_int_type(
	constant din : std_logic_vector(7 downto 0))
	return arinc_pend_int_type is
	variable res : arinc_pend_int_type;
begin
	res.COPYERR := din(7);
	res.AUTOERR := din(6);
	res.CHKERR := din(5);
	res.RAMFAIL := din(4);
	res.FLAG := din(3);
	res.ATXRDY := din(2);
	return res;
end vect_to_arinc_pend_int_type;
	
type array_arinc_rx_ctrl_type is array (7 downto 0) of arinc_rx_ctrl_type;
type array_arinc_tx_ctrl_type is array (3 downto 0) of arinc_tx_ctrl_type;

---------------------------------------------------------------------------- REGISTERS
type arinc_reg_type is record
		MASTER_CTRL : arinc_master_ctrl_type;
		MASTER_STATUS : arinc_master_status_type;
		
		RX_CTRL : array_arinc_rx_ctrl_type;
		RX_FIFO_NE : std_logic_vector(7 downto 0);
		RX_FIFO_THR : std_logic_vector(7 downto 0);
		RX_FIFO_FULL : std_logic_vector(7 downto 0);
		RX_FIFO_MUX : std_logic_vector(7 downto 0);
		RX_FIFO_THR_VAL : std_logic_vector(4 downto 0);
		
		LOOPBACK : std_logic_vector(7 downto 0);
		ARXBIT : std_logic_vector(7 downto 0);
		
		TX_CTRL : array_arinc_tx_ctrl_type;
		TX_RDY : std_logic_vector(3 downto 0);
		
		APIR : std_logic_vector(7 downto 0);
		AIMR : std_logic_vector(7 downto 0);
		
		PEND_INT : arinc_pend_int_type;
		PEND_INT_EN : arinc_pend_int_type;
		
		TX_READY_INT_EN : std_logic_vector(3 downto 0);
	end record arinc_reg_type;

---------------------------------------------------------------------------- REGISTERS UPDATE
type arinc_read_reg_type is record
		MASTER_CTRL : std_logic;
		MASTER_STATUS : std_logic;
		RX_CTRL : std_logic_vector(7 downto 0);
		RX_FIFO_NE : std_logic;
		RX_FIFO_THR : std_logic;
		RX_FIFO_FULL : std_logic;
		RX_FIFO_MUX  : std_logic;
		RX_FIFO_THR_VAL : std_logic;
		LOOPBACK : std_logic;
		TX_CTRL : std_logic_vector(3 downto 0);
		TX_RDY : std_logic;
		APIR : std_logic;
		AIMR : std_logic;
		PEND_INT : std_logic;
		PEND_INT_EN : std_logic;
		TX_READY_INT_EN : std_logic;
	end record arinc_read_reg_type;
	
function vect_to_arinc_read_reg_type(
	constant din : std_logic_vector(7 downto 0))
	return arinc_read_reg_type is
	variable res : arinc_read_reg_type;
begin
	res.MASTER_CTRL := din(0);
	res.MASTER_STATUS := din(1);
	res.RX_CTRL := din(9 downto 2);
	res.RX_FIFO_NE := din(10);
	res.RX_FIFO_THR := din(11);
	res.RX_FIFO_FULL := din(12);
	res.RX_FIFO_MUX := din(13);
	res.RX_FIFO_THR_VAL := din(14);
	res.LOOPBACK := din(15);
	res.TX_CTRL := din(19 downto 16);
	res.TX_RDY := din(20);
	res.APIR := din(21);
	res.AIMR := din(22);
	res.PEND_INT := din(23);
	res.PEND_INT_EN := din(24);
	res.TX_READY_INT_EN := din(25);
	return res;
end vect_to_arinc_read_reg_type;

signal update_flags : arinc_read_reg_type;

signal registers : arinc_reg_type;
signal registers_state : arinc_reg_type;

signal s_rcv_fifo_ne : std_logic_vector(7 downto 0);

signal core_ready : std_logic;
signal reset_fail : std_logic;

signal i_arinc_fifo_rx_0 : std_logic_vector(31 downto 0);
signal i_arinc_fifo_rx_1 : std_logic_vector(31 downto 0);
signal i_arinc_fifo_rx_2 : std_logic_vector(31 downto 0);
signal i_arinc_fifo_rx_3 : std_logic_vector(31 downto 0);
signal i_arinc_fifo_rx_4 : std_logic_vector(31 downto 0);
signal i_arinc_fifo_rx_5 : std_logic_vector(31 downto 0);
signal i_arinc_fifo_rx_6 : std_logic_vector(31 downto 0);
signal i_arinc_fifo_rx_7 : std_logic_vector(31 downto 0);
signal i_arinc_fifo_rx_wr_en : std_logic_vector(7 downto 0);
signal i_arinc_fifo_rx_full : std_logic_vector(7 downto 0);

signal i_arinc_fifo_tx_0 : std_logic_vector(31 downto 0);
signal i_arinc_fifo_tx_1 : std_logic_vector(31 downto 0);
signal i_arinc_fifo_tx_2 : std_logic_vector(31 downto 0);
signal i_arinc_fifo_tx_3 : std_logic_vector(31 downto 0);
signal i_arinc_fifo_tx_rd_en : std_logic_vector(3 downto 0);
signal i_arinc_fifo_tx_empty : std_logic_vector(3 downto 0);

signal i_arinc_cnt_0 : integer range 0 to 128000 := 0;
signal i_arinc_cnt_1 : integer range 0 to 128000 := 0;
signal i_arinc_cnt_2 : integer range 0 to 128000 := 0;
signal i_arinc_cnt_3 : integer range 0 to 128000 := 0;

signal i_cnt : integer range 0 to 160000 := 0 ;
signal rde_cnt : integer range 0 to 256 := 0 ;

---------------------------------------------------------------------------- STATE MACHINE
type arinc_state_type is (
	ARINC_WAIT,
	
	ARINC_WRITE_IC_SETT_2,
	ARINC_WRITE_IC_SETT_DONE,
	ARINC_RX_CTRL_DONE,
	ARINC_TX_CTRL_DONE,
	ARINC_CHECK_RX_STATE,
	ARINC_CHECK_RX_STATE_AN,
	ARINC_CHECK_TX_STATE,
	ARINC_CHECK_TX_DONE,
	
	ARINC_RESET_START,
	ARINC_RESET_BIST_CLEAR,
	ARINC_RESET_MRST_HIGH,
	ARINC_RESET_MRST_LOW,
	ARINC_RESET_WAIT,
	ARINC_RESET_CHECK_RAMFAIL,
	ARINC_RESET_WAIT_READY,
	ARINC_FILL_RDE_LOOKUP,
	ARINC_FILL_RDE_LOOKUP_AN,
	
	ARINC_MASTER_CTRL_WRITE,
	ARINC_MASTER_CTRL_WRITE_AN,
	ARINC_RX_CTRL_WRITE,
	ARINC_RX_CTRL_WRITE_AN,
	ARINC_TX_CTRL_WRITE,
	ARINC_TX_CTRL_WRITE_AN,
	ARINC_LOOPBACK_WRITE,
	ARINC_LOOPBACK_WRITE_AN,
	
	ARINC_MASTER_CTRL_READ,
	ARINC_MASTER_CTRL_READ_AN,
	ARINC_MASTER_STATUS_READ,
	ARINC_MASTER_STATUS_READ_AN,
	ARINC_RX_FIFO_NE_READ,
	ARINC_RX_FIFO_NE_READ_AN,
	ARINC_RX_FIFO_THR_READ,
	ARINC_RX_FIFO_THR_READ_AN,
	ARINC_RX_FIFO_FULL_READ,
	ARINC_RX_FIFO_FULL_READ_AN,
	ARINC_RX_FIFO_MUX_READ,
	ARINC_RX_FIFO_MUX_READ_AN,
	ARINC_RX_FIFO_THR_VAL_READ,
	ARINC_RX_FIFO_THR_VAL_READ_AN,
	ARINC_LOOPBACK_READ,
	ARINC_LOOPBACK_READ_AN,
	ARINC_TX_RDY_READ,
	ARINC_TX_RDY_READ_AN,
	ARINC_APIR_READ,
	ARINC_APIR_READ_AN,
	ARINC_AIMR_READ,
	ARINC_AIMR_READ_AN,
	ARINC_PEND_INT_READ,
	ARINC_PEND_INT_READ_AN,
	ARINC_PEND_INT_EN_READ,
	ARINC_PEND_INT_EN_READ_AN,
	ARINC_TX_READY_INT_EN_READ,
	ARINC_TX_READY_INT_EN_READ_AN,
	ARINC_RX_CTRL_READ,
	ARINC_RX_CTRL_READ_AN,
	ARINC_TX_CTRL_READ,
	ARINC_TX_CTRL_READ_AN
);
signal arinc_state : arinc_state_type;
signal arinc_state_old : arinc_state_type;
signal reg_id_rw : integer range 0 to 7 := 0;
signal reg_out_temp : std_logic_vector(7 downto 0);

COMPONENT cdc_bit
	PORT(
		CLK_IN : IN std_logic;
		D_IN : IN std_logic;
		CLK_OUT : IN std_logic;          
		D_OUT : OUT std_logic
	);
	END COMPONENT;
signal t_reset : std_logic;

begin
	reset_cdc_i: cdc_bit PORT MAP(
		CLK_IN => RESET_CLK,
		D_IN => RESET,
		CLK_OUT => CLK,
		D_OUT => t_reset
	);
	
	SPI_HI3210_i: SPI_HI3210 PORT MAP(
		RESET => t_reset,
		CLK => CLK,
		ADDR => spi_addr,
		READ_EN => spi_read_en,
		WRITE_EN => spi_write_en,
		READ_ARINC_EN => spi_read_arinc_en,
		WRITE_ARINC_EN => spi_write_arinc_en,
		READ_ARINC_LABEL_EN => spi_read_arinc_label_en,
		READ_ARINC_BLOCK_EN => spi_read_arinc_block_en,
		DONE => spi_done,
		DATA_IN => spi_data_in,
		DATA_OUT => spi_data_out,
		ARINC_IN => spi_arinc_in,
		ARINC_OUT => spi_arinc_out,
		CS => CS,
		SCK => SCK,
		MISO => MISO,
		MOSI => MOSI
	);
	
	status_cdc_32_reg: cdc_32_reg PORT MAP(
		reset => fifo_reset,
		clk_in => CLK,
		data_in => i_status_out,
		clk_out => STATUS_OUT_CLK,
		data_out => STATUS_OUT
	);
	
	fifo_32_cmd_i : fifo_32_long
	PORT MAP (
		rst           => fifo_reset,

		wr_clk        => CMD_IN_CLK,
		din           => CMD_IN,
		wr_en         => CMD_IN_EN,

		rd_clk        => CLK,
		rd_en         => i_CMD_IN_EN,
		dout          => i_CMD_IN,
		empty         => i_CMD_IN_EMPTY
	);
	
	FIFO_RX_0_i : fifo_32_long PORT MAP ( rst => t_reset,
			wr_clk => CLK, wr_en => i_arinc_fifo_rx_wr_en(0), din => i_arinc_fifo_rx_0, full => i_arinc_fifo_rx_full(0),
			rd_clk => ARINC_FIFO_RX_CLK, rd_en => ARINC_FIFO_RX_READ(0), dout => ARINC_FIFO_RX_0, empty => ARINC_FIFO_RX_EMPTY(0) );
	FIFO_RX_1_i : fifo_32_long PORT MAP ( rst => t_reset,
			wr_clk => CLK, wr_en => i_arinc_fifo_rx_wr_en(1), din => i_arinc_fifo_rx_1, full => i_arinc_fifo_rx_full(1),
			rd_clk => ARINC_FIFO_RX_CLK, rd_en => ARINC_FIFO_RX_READ(1), dout => ARINC_FIFO_RX_1, empty => ARINC_FIFO_RX_EMPTY(1) );
	FIFO_RX_2_i : fifo_32_long PORT MAP ( rst => t_reset,
			wr_clk => CLK, wr_en => i_arinc_fifo_rx_wr_en(2), din => i_arinc_fifo_rx_2, full => i_arinc_fifo_rx_full(2),
			rd_clk => ARINC_FIFO_RX_CLK, rd_en => ARINC_FIFO_RX_READ(2), dout => ARINC_FIFO_RX_2, empty => ARINC_FIFO_RX_EMPTY(2) );
	FIFO_RX_3_i : fifo_32_long PORT MAP ( rst => t_reset,
			wr_clk => CLK, wr_en => i_arinc_fifo_rx_wr_en(3), din => i_arinc_fifo_rx_3, full => i_arinc_fifo_rx_full(3),
			rd_clk => ARINC_FIFO_RX_CLK, rd_en => ARINC_FIFO_RX_READ(3), dout => ARINC_FIFO_RX_3, empty => ARINC_FIFO_RX_EMPTY(3) );
	FIFO_RX_4_i : fifo_32_long PORT MAP ( rst => t_reset,
			wr_clk => CLK, wr_en => i_arinc_fifo_rx_wr_en(4), din => i_arinc_fifo_rx_4, full => i_arinc_fifo_rx_full(4),
			rd_clk => ARINC_FIFO_RX_CLK, rd_en => ARINC_FIFO_RX_READ(4), dout => ARINC_FIFO_RX_4, empty => ARINC_FIFO_RX_EMPTY(4) );
	FIFO_RX_5_i : fifo_32_long PORT MAP ( rst => t_reset,
			wr_clk => CLK, wr_en => i_arinc_fifo_rx_wr_en(5), din => i_arinc_fifo_rx_5, full => i_arinc_fifo_rx_full(5),
			rd_clk => ARINC_FIFO_RX_CLK, rd_en => ARINC_FIFO_RX_READ(5), dout => ARINC_FIFO_RX_5, empty => ARINC_FIFO_RX_EMPTY(5) );
	FIFO_RX_6_i : fifo_32_long PORT MAP ( rst => t_reset,
			wr_clk => CLK, wr_en => i_arinc_fifo_rx_wr_en(6), din => i_arinc_fifo_rx_6, full => i_arinc_fifo_rx_full(6),
			rd_clk => ARINC_FIFO_RX_CLK, rd_en => ARINC_FIFO_RX_READ(6), dout => ARINC_FIFO_RX_6, empty => ARINC_FIFO_RX_EMPTY(6) );
	FIFO_RX_7_i : fifo_32_long PORT MAP ( rst => t_reset,
			wr_clk => CLK, wr_en => i_arinc_fifo_rx_wr_en(7), din => i_arinc_fifo_rx_7, full => i_arinc_fifo_rx_full(7),
			rd_clk => ARINC_FIFO_RX_CLK, rd_en => ARINC_FIFO_RX_READ(7), dout => ARINC_FIFO_RX_7, empty => ARINC_FIFO_RX_EMPTY(7) );

	FIFO_TX_0_i : fifo_32_long PORT MAP ( rst => t_reset,
			wr_clk => ARINC_FIFO_TX_CLK, wr_en => ARINC_FIFO_TX_WRITE(0), din => ARINC_FIFO_TX_0, prog_full => ARINC_FIFO_TX_FULL(0),
			rd_clk => CLK, rd_en => i_arinc_fifo_tx_rd_en(0), dout => i_arinc_fifo_tx_0, empty => i_arinc_fifo_tx_empty(0) );
	FIFO_TX_1_i : fifo_32_long PORT MAP ( rst => t_reset,
			wr_clk => ARINC_FIFO_TX_CLK, wr_en => ARINC_FIFO_TX_WRITE(1), din => ARINC_FIFO_TX_1, prog_full => ARINC_FIFO_TX_FULL(1),
			rd_clk => CLK, rd_en => i_arinc_fifo_tx_rd_en(1), dout => i_arinc_fifo_tx_1, empty => i_arinc_fifo_tx_empty(1) );
	FIFO_TX_2_i : fifo_32_long PORT MAP ( rst => t_reset,
			wr_clk => ARINC_FIFO_TX_CLK, wr_en => ARINC_FIFO_TX_WRITE(2), din => ARINC_FIFO_TX_2, prog_full => ARINC_FIFO_TX_FULL(2),
			rd_clk => CLK, rd_en => i_arinc_fifo_tx_rd_en(2), dout => i_arinc_fifo_tx_2, empty => i_arinc_fifo_tx_empty(2) );
	FIFO_TX_3_i : fifo_32_long PORT MAP ( rst => t_reset,
			wr_clk => ARINC_FIFO_TX_CLK, wr_en => ARINC_FIFO_TX_WRITE(3), din => ARINC_FIFO_TX_3, prog_full => ARINC_FIFO_TX_FULL(3),
			rd_clk => CLK, rd_en => i_arinc_fifo_tx_rd_en(3), dout => i_arinc_fifo_tx_3, empty => i_arinc_fifo_tx_empty(3) );
		
i_status_out(31 downto 24) <= registers_state.RX_FIFO_NE;
i_status_out(23 downto 20) <= registers_state.TX_RDY;
i_status_out(19 downto 15) <= (others => '0');
i_status_out(14) <= reset_fail;
i_status_out(13) <= registers_state.PEND_INT.RAMFAIL;
i_status_out(12) <= registers_state.PEND_INT.CHKERR;
i_status_out(11) <= registers_state.PEND_INT.AUTOERR;
i_status_out(10) <= registers_state.PEND_INT.COPYERR;
i_status_out(9)  <= i_ATXMSK;
i_status_out(8)  <= i_RUN;
i_status_out(7)  <= READY;
i_status_out(6)  <= registers_state.MASTER_STATUS.AUTOINIT;
i_status_out(5)  <= registers_state.MASTER_STATUS.PROG;
i_status_out(4)  <= registers_state.MASTER_STATUS.RAMBUSY;
i_status_out(3)  <= registers_state.MASTER_STATUS.SAFE;
i_status_out(2)  <= registers_state.MASTER_STATUS.ACTIVE;
i_status_out(1)  <= registers_state.MASTER_STATUS.READY;
i_status_out(0)  <= core_ready;

RUN <= i_RUN;
ATXMSK <= i_ATXMSK;
MRST <= i_MRST;

DRV: process (CLK, t_reset)
begin
	if t_reset = '1' then
		arinc_state <= ARINC_WAIT;
		core_ready <= '0';
		
		AACK <= '0';
		MINTACK <= '0';
		i_MRST <= '1';
		i_ATXMSK <= '0';
		i_RUN <= '0';
		i_RUN_old <= '0';
		
		spi_read_en <= '0';
		spi_write_en <= '0';
		spi_read_arinc_en <= '0';
		spi_write_arinc_en <= '0';
		spi_read_arinc_label_en <= '0';
		spi_read_arinc_block_en <= '0';
		fifo_reset <= '1';
		
		registers <= (('0', '0', '0'),
						('0', '0', '0', '0', '0', '0'),
						(others => ('0', '0', '0', '0', '0', '0', "00")),
						(others => '0'),
						(others => '0'),
						(others => '0'),
						(others => '0'),
						(others => '0'),
						(others => '0'),
						(others => '0'),
						(others => ('0', '0', '0', '0', '0')),
						(others => '0'),
						(others => '0'),
						(others => '0'),
						('0', '0', '0', '0', '0', '0'),
						('0', '0', '0', '0', '0', '0'),
						(others => '0'));
		registers_state <= (('0', '0', '0'),
						('0', '0', '0', '0', '0', '0'),
						(others => ('0', '0', '0', '0', '0', '0', "00")),
						(others => '0'),
						(others => '0'),
						(others => '0'),
						(others => '0'),
						(others => '0'),
						(others => '0'),
						(others => '0'),
						(others => ('0', '0', '0', '0', '0')),
						(others => '0'),
						(others => '0'),
						(others => '0'),
						('0', '0', '0', '0', '0', '0'),
						('0', '0', '0', '0', '0', '0'),
						(others => '0'));
		reset_fail <= '0';
		update_flags <= ('0', '0', (others => '0'), '0', '0', '0', '0', '0', '0', (others => '0'), '0', '0', '0', '0', '0', '0');
		
		i_arinc_cnt_0 <= 0;
		i_arinc_cnt_1 <= 0;
		i_arinc_cnt_2 <= 0;
		i_arinc_cnt_3 <= 0;
		
	elsif CLK'event and CLK = '1' then
		i_CMD_IN_EN <= '0';
		i_arinc_fifo_tx_rd_en <= (others => '0');
		i_arinc_fifo_rx_wr_en <= (others => '0');
		fifo_reset <= '0';
		
		if i_arinc_cnt_0 > 0 then
			i_arinc_cnt_0 <= i_arinc_cnt_0 - 1;
		end if;
		
		if i_arinc_cnt_1 > 0 then
			i_arinc_cnt_1 <= i_arinc_cnt_1 - 1;
		end if;
		
		if i_arinc_cnt_2 > 0 then
			i_arinc_cnt_2 <= i_arinc_cnt_2 - 1;
		end if;
		
		if i_arinc_cnt_3 > 0 then
			i_arinc_cnt_3 <= i_arinc_cnt_3 - 1;
		end if;
		
		case (arinc_state) is
			when ARINC_WAIT =>
				------------------------------------------------------------- UPDATE REGISTERS
				if    update_flags.MASTER_CTRL = '1' then
					arinc_state_old <= ARINC_WAIT;
					arinc_state     <= ARINC_MASTER_CTRL_READ;
					update_flags.MASTER_CTRL <= '0';
				elsif update_flags.MASTER_STATUS = '1' then
					arinc_state_old <= ARINC_WAIT;
					arinc_state     <= ARINC_MASTER_STATUS_READ;
					update_flags.MASTER_STATUS <= '0';
				elsif update_flags.RX_CTRL /= "00000000" then
					arinc_state_old <= ARINC_WAIT;
					arinc_state     <= ARINC_RX_CTRL_READ ;
					
					if    update_flags.RX_CTRL(0) = '1' then
						reg_id_rw    <= 0;
						update_flags.RX_CTRL(0) <= '0';
						
					elsif update_flags.RX_CTRL(1) = '1' then
						reg_id_rw    <= 1;
						update_flags.RX_CTRL(1) <= '0';
						
					elsif update_flags.RX_CTRL(2) = '1' then
						reg_id_rw    <= 2;
						update_flags.RX_CTRL(2) <= '0';
						
					elsif update_flags.RX_CTRL(3) = '1' then
						reg_id_rw    <= 3;
						update_flags.RX_CTRL(3) <= '0';
						
					elsif update_flags.RX_CTRL(4) = '1' then
						reg_id_rw    <= 4;
						update_flags.RX_CTRL(4) <= '0';
						
					elsif update_flags.RX_CTRL(5) = '1' then
						reg_id_rw    <= 5;
						update_flags.RX_CTRL(5) <= '0';
						
					elsif update_flags.RX_CTRL(6) = '1' then
						reg_id_rw    <= 6;
						update_flags.RX_CTRL(6) <= '0';
						
					elsif update_flags.RX_CTRL(7) = '1' then
						reg_id_rw    <= 7;
						update_flags.RX_CTRL(7) <= '0';
					end if;
					
				elsif update_flags.RX_FIFO_NE = '1' then
					update_flags.RX_FIFO_NE <= '0';
					arinc_state_old <= ARINC_WAIT;
					arinc_state     <= ARINC_RX_FIFO_NE_READ;
					
				elsif update_flags.RX_FIFO_THR = '1' then
					update_flags.RX_FIFO_THR <= '0';
					arinc_state_old <= ARINC_WAIT;
					arinc_state     <= ARINC_RX_FIFO_THR_READ;
					
				elsif update_flags.RX_FIFO_FULL = '1' then
					update_flags.RX_FIFO_FULL <= '0';
					arinc_state_old <= ARINC_WAIT;
					arinc_state     <= ARINC_RX_FIFO_FULL_READ;
					
				elsif update_flags.RX_FIFO_MUX = '1' then
					update_flags.RX_FIFO_MUX <= '0';
					arinc_state_old <= ARINC_WAIT;
					arinc_state     <= ARINC_RX_FIFO_MUX_READ;
					
				elsif update_flags.RX_FIFO_THR_VAL = '1' then
					update_flags.RX_FIFO_THR_VAL <= '0';
					arinc_state_old <= ARINC_WAIT;
					arinc_state     <= ARINC_RX_FIFO_THR_VAL_READ;
					
				elsif update_flags.LOOPBACK = '1' then
					update_flags.LOOPBACK <= '0';
					arinc_state_old <= ARINC_WAIT;
					arinc_state     <= ARINC_LOOPBACK_READ;
					
				elsif update_flags.TX_CTRL /= "0000" then
					arinc_state_old <= ARINC_WAIT;
					arinc_state     <= ARINC_TX_CTRL_READ;
					
					if    update_flags.TX_CTRL(0) = '1' then
						reg_id_rw    <= 0;
						update_flags.TX_CTRL(0) <= '0';
						
					elsif update_flags.TX_CTRL(1) = '1' then
						reg_id_rw    <= 1;
						update_flags.TX_CTRL(1) <= '0';
						
					elsif update_flags.TX_CTRL(2) = '1' then
						reg_id_rw    <= 2;
						update_flags.TX_CTRL(2) <= '0';
						
					elsif update_flags.TX_CTRL(3) = '1' then
						reg_id_rw    <= 3;
						update_flags.TX_CTRL(3) <= '0';
					end if;
					
				elsif update_flags.TX_RDY = '1' then
					update_flags.TX_RDY <= '0';
					arinc_state_old <= ARINC_WAIT;
					arinc_state     <= ARINC_TX_RDY_READ;
					
				elsif update_flags.APIR = '1' then
					update_flags.APIR <= '0';
					arinc_state_old <= ARINC_WAIT;
					arinc_state     <= ARINC_APIR_READ;
					
				elsif update_flags.AIMR = '1' then
					update_flags.AIMR <= '0';
					arinc_state_old <= ARINC_WAIT;
					arinc_state     <= ARINC_AIMR_READ;
					
				elsif update_flags.PEND_INT = '1' then
					update_flags.PEND_INT <= '0';
					arinc_state_old <= ARINC_WAIT;
					arinc_state     <= ARINC_PEND_INT_READ;
					
				elsif update_flags.PEND_INT_EN = '1' then
					update_flags.PEND_INT_EN <= '0';
					arinc_state_old <= ARINC_WAIT;
					arinc_state     <= ARINC_PEND_INT_EN_READ;
					
				elsif update_flags.TX_READY_INT_EN = '1' then
					update_flags.TX_READY_INT_EN <= '0';
					arinc_state_old <= ARINC_WAIT;
					arinc_state     <= ARINC_TX_READY_INT_EN_READ;
				
				------------------------------------------------------------- COMMANDS
				elsif i_CMD_IN_EMPTY = '0' then
					i_CMD_IN_EN <= '1';
					core_ready <= '0';
					
					if    i_CMD_IN(3 downto 0) = "0000" then
						-- WRITE Reset
						arinc_state <= ARINC_RESET_START;
							
						i_RUN_old <= i_RUN;
						i_RUN <= '0';
						
					elsif i_CMD_IN(3 downto 0) = "0001" then
						-- WRITE Set IC Settings
						i_ATXMSK <= i_CMD_IN(8);
						i_RUN_old <= i_CMD_IN(7);
						i_RUN <= i_CMD_IN(7);
						
						registers.MASTER_CTRL.A429RX <= i_CMD_IN(6);
						registers.MASTER_CTRL.A429TX <= i_CMD_IN(5);
						registers.MASTER_CTRL.AFLIP <= i_CMD_IN(4);
						registers.LOOPBACK <= i_CMD_IN(16 downto 9);
						
						arinc_state <= ARINC_MASTER_CTRL_WRITE;
						arinc_state_old <= ARINC_WRITE_IC_SETT_2;
						
					elsif i_CMD_IN(3 downto 0) = "0010" then
						-- WRITE RX Settings
						arinc_state <= ARINC_RX_CTRL_WRITE;
						arinc_state_old <= ARINC_RX_CTRL_DONE;
						
						reg_id_rw <= conv_integer(i_CMD_IN(6 downto 4));
						
						registers.RX_CTRL(conv_integer(i_CMD_IN(6 downto 4))).ENABLE <= i_CMD_IN(14);
						registers.RX_CTRL(conv_integer(i_CMD_IN(6 downto 4))).HI_LO <= i_CMD_IN(13);
						registers.RX_CTRL(conv_integer(i_CMD_IN(6 downto 4))).PARITYEN <= i_CMD_IN(12);
						registers.RX_CTRL(conv_integer(i_CMD_IN(6 downto 4))).DECODER <= i_CMD_IN(11);
						registers.RX_CTRL(conv_integer(i_CMD_IN(6 downto 4))).SD10 <= i_CMD_IN(10);
						registers.RX_CTRL(conv_integer(i_CMD_IN(6 downto 4))).SD9 <= i_CMD_IN(9);
						registers.RX_CTRL(conv_integer(i_CMD_IN(6 downto 4))).FFS <= i_CMD_IN(8 downto 7);
						
						i_RUN_old <= i_RUN;
						i_RUN <= '0';
						
					elsif i_CMD_IN(3 downto 0) = "0011" then
						-- WRITE TX Settings
						arinc_state <= ARINC_TX_CTRL_WRITE;
						arinc_state_old <= ARINC_TX_CTRL_DONE;
						
						reg_id_rw <= conv_integer(i_CMD_IN(6 downto 4));
						
						registers.TX_CTRL(conv_integer(i_CMD_IN(6 downto 4))).RUN_STOP <= i_CMD_IN(10);
						registers.TX_CTRL(conv_integer(i_CMD_IN(6 downto 4))).HI_LO  <= i_CMD_IN(9);
						registers.TX_CTRL(conv_integer(i_CMD_IN(6 downto 4))).PARITY_DATA  <= i_CMD_IN(8);
						registers.TX_CTRL(conv_integer(i_CMD_IN(6 downto 4))).EVEN_ODD  <= i_CMD_IN(7);
						registers.TX_CTRL(conv_integer(i_CMD_IN(6 downto 4))).SKIP  <= '0';
						
--						i_RUN_old <= i_RUN;
--						i_RUN <= '0';
					
					elsif i_CMD_IN(3 downto 0) = "0100" then
						-- WRITE UPDATE Status
						update_flags.MASTER_CTRL     <= update_flags.MASTER_CTRL     or i_CMD_IN(4);
						update_flags.MASTER_STATUS   <= update_flags.MASTER_STATUS   or i_CMD_IN(5);
						update_flags.RX_CTRL         <= update_flags.RX_CTRL         or i_CMD_IN(13 downto 6);
						update_flags.RX_FIFO_NE      <= update_flags.RX_FIFO_NE      or i_CMD_IN(14);
						update_flags.RX_FIFO_THR     <= update_flags.RX_FIFO_THR     or i_CMD_IN(15);
						update_flags.RX_FIFO_FULL    <= update_flags.RX_FIFO_FULL    or i_CMD_IN(16);
						update_flags.RX_FIFO_MUX     <= update_flags.RX_FIFO_MUX     or i_CMD_IN(17);
						update_flags.RX_FIFO_THR_VAL <= update_flags.RX_FIFO_THR_VAL or i_CMD_IN(18);
						update_flags.LOOPBACK        <= update_flags.LOOPBACK        or i_CMD_IN(19);
						update_flags.TX_CTRL         <= update_flags.TX_CTRL         or i_CMD_IN(23 downto 20);
						update_flags.TX_RDY          <= update_flags.TX_RDY          or i_CMD_IN(24);
						update_flags.APIR            <= update_flags.APIR            or i_CMD_IN(25);
						update_flags.AIMR            <= update_flags.AIMR            or i_CMD_IN(26);
						update_flags.PEND_INT        <= update_flags.PEND_INT        or i_CMD_IN(27);
						update_flags.PEND_INT_EN     <= update_flags.PEND_INT_EN     or i_CMD_IN(28);
						update_flags.TX_READY_INT_EN <= update_flags.TX_READY_INT_EN or i_CMD_IN(29);
						
					end if;
				else
					core_ready <= '1';
					
					if i_MRST = '0' and i_RUN = '1' then
						arinc_state_old <= ARINC_CHECK_TX_STATE;
						arinc_state     <= ARINC_TX_RDY_READ;
					end if;
				end if;
					
			when ARINC_CHECK_TX_STATE =>
				if    i_arinc_fifo_tx_empty(0) = '0' and registers_state.TX_RDY(0) = '1' and i_arinc_cnt_0 = 0 then
					if registers.TX_CTRL(0).HI_LO = '0' then
						i_arinc_cnt_0 <= 16000;
					else
						i_arinc_cnt_0 <= 128000;
					end if;
					i_arinc_fifo_tx_rd_en(0) <= '1';
					registers_state.TX_RDY(0) <= '0';
					spi_arinc_in <= i_arinc_fifo_tx_0;
					spi_write_arinc_en <= '1';
					spi_addr <= X"0000";
					arinc_state <= ARINC_CHECK_TX_DONE;
					
				elsif i_arinc_fifo_tx_empty(1) = '0' and registers_state.TX_RDY(1) = '1' and i_arinc_cnt_1 = 0 then
					if registers.TX_CTRL(1).HI_LO = '0' then
						i_arinc_cnt_1 <= 16000;
					else
						i_arinc_cnt_1 <= 128000;
					end if;
					i_arinc_fifo_tx_rd_en(1) <= '1';
					registers_state.TX_RDY(1) <= '0';
					spi_arinc_in <= i_arinc_fifo_tx_1;
					spi_write_arinc_en <= '1';
					spi_addr <= X"0001";
					arinc_state <= ARINC_CHECK_TX_DONE;
					
				elsif i_arinc_fifo_tx_empty(2) = '0' and registers_state.TX_RDY(2) = '1' and i_arinc_cnt_2 = 0  then
					if registers.TX_CTRL(2).HI_LO = '0' then
						i_arinc_cnt_2 <= 16000;
					else
						i_arinc_cnt_2 <= 128000;
					end if;
					i_arinc_fifo_tx_rd_en(2) <= '1';
					registers_state.TX_RDY(2) <= '0';
					spi_arinc_in <= i_arinc_fifo_tx_2;
					spi_write_arinc_en <= '1';
					spi_addr <= X"0002";
					arinc_state <= ARINC_CHECK_TX_DONE;
					
				elsif i_arinc_fifo_tx_empty(3) = '0' and registers_state.TX_RDY(3) = '1' and i_arinc_cnt_3 = 0  then
					if registers.TX_CTRL(3).HI_LO = '0' then
						i_arinc_cnt_3 <= 16000;
					else
						i_arinc_cnt_3 <= 128000;
					end if;
					i_arinc_fifo_tx_rd_en(3) <= '1';
					registers_state.TX_RDY(3) <= '0';
					spi_arinc_in <= i_arinc_fifo_tx_3;
					spi_write_arinc_en <= '1';
					spi_addr <= X"0003";
					arinc_state <= ARINC_CHECK_TX_DONE;
					
				else
					------------------------------------------------------------- CHECK RX FIFOS
					arinc_state <= ARINC_RX_FIFO_NE_READ;
					arinc_state_old <= ARINC_CHECK_RX_STATE;
				end if;
			
			when ARINC_CHECK_TX_DONE =>
				if spi_done = '1' then
					spi_write_arinc_en <= '0';
					arinc_state <= ARINC_CHECK_TX_STATE;
				end if;
			
			when ARINC_CHECK_RX_STATE =>
				if registers_state.RX_FIFO_NE /= "00000000" then
					spi_read_arinc_en <= '1';
					
					if    registers_state.RX_FIFO_NE(0) = '1' then
						spi_addr <= X"0000";
						
					elsif registers_state.RX_FIFO_NE(1) = '1' then
						spi_addr <= X"0001";
						
					elsif registers_state.RX_FIFO_NE(2) = '1' then
						spi_addr <= X"0002";
						
					elsif registers_state.RX_FIFO_NE(3) = '1' then
						spi_addr <= X"0003";
						
					elsif registers_state.RX_FIFO_NE(4) = '1' then
						spi_addr <= X"0004";
						
					elsif registers_state.RX_FIFO_NE(5) = '1' then
						spi_addr <= X"0005";
						
					elsif registers_state.RX_FIFO_NE(6) = '1' then
						spi_addr <= X"0006";
						
					elsif registers_state.RX_FIFO_NE(7) = '1' then
						spi_addr <= X"0007";
					end if;
					
					arinc_state <= ARINC_CHECK_RX_STATE_AN;
				else
					arinc_state <= ARINC_WAIT;
				end if;
				
			when ARINC_CHECK_RX_STATE_AN =>
				if spi_done = '1' then
					spi_read_arinc_en <= '0';
					
					if    registers_state.RX_FIFO_NE(0) = '1' then
						registers_state.RX_FIFO_NE(0) <= '0';
						i_arinc_fifo_rx_wr_en(0) <= '1';
						i_arinc_fifo_rx_0 <= spi_arinc_out;
						
					elsif registers_state.RX_FIFO_NE(1) = '1' then
						registers_state.RX_FIFO_NE(1) <= '0';
						i_arinc_fifo_rx_wr_en(1) <= '1';
						i_arinc_fifo_rx_1 <= spi_arinc_out;
						
					elsif registers_state.RX_FIFO_NE(2) = '1' then
						registers_state.RX_FIFO_NE(2) <= '0';
						i_arinc_fifo_rx_wr_en(2) <= '1';
						i_arinc_fifo_rx_2 <= spi_arinc_out;
						
					elsif registers_state.RX_FIFO_NE(3) = '1' then
						registers_state.RX_FIFO_NE(3) <= '0';
						i_arinc_fifo_rx_wr_en(3) <= '1';
						i_arinc_fifo_rx_3 <= spi_arinc_out;
						
					elsif registers_state.RX_FIFO_NE(4) = '1' then
						registers_state.RX_FIFO_NE(4) <= '0';
						i_arinc_fifo_rx_wr_en(4) <= '1';
						i_arinc_fifo_rx_4 <= spi_arinc_out;
						
					elsif registers_state.RX_FIFO_NE(5) = '1' then
						registers_state.RX_FIFO_NE(5) <= '0';
						i_arinc_fifo_rx_wr_en(5) <= '1';
						i_arinc_fifo_rx_5 <= spi_arinc_out;
						
					elsif registers_state.RX_FIFO_NE(6) = '1' then
						registers_state.RX_FIFO_NE(6) <= '0';
						i_arinc_fifo_rx_wr_en(6) <= '1';
						i_arinc_fifo_rx_6 <= spi_arinc_out;
						
					elsif registers_state.RX_FIFO_NE(7) = '1' then
						registers_state.RX_FIFO_NE(7) <= '0';
						i_arinc_fifo_rx_wr_en(7) <= '1';
						i_arinc_fifo_rx_7 <= spi_arinc_out;
					end if;
					
					arinc_state <= ARINC_CHECK_RX_STATE;
				end if;
			
			when ARINC_WRITE_IC_SETT_2 =>
				arinc_state <= ARINC_LOOPBACK_WRITE;
				arinc_state_old <= ARINC_WRITE_IC_SETT_DONE;
			
			when ARINC_WRITE_IC_SETT_DONE =>
				i_RUN <= i_RUN_old;
				update_flags.MASTER_CTRL <= '1';
				update_flags.MASTER_STATUS <= '1';
				arinc_state <= ARINC_WAIT;
				
			when ARINC_RX_CTRL_DONE =>
				i_RUN <= i_RUN_old;
				update_flags.RX_CTRL(reg_id_rw) <= '1';
				arinc_state <= ARINC_WAIT;
				
			when ARINC_TX_CTRL_DONE =>
				i_RUN <= i_RUN_old;
				update_flags.TX_CTRL(reg_id_rw) <= '1';
				arinc_state <= ARINC_WAIT;
				
			------------------------------------------------------------- RESET SEQUENCE
			when ARINC_RESET_START =>
				core_ready <= '0';
				AACK <= '0';
				MINTACK <= '0';
				--i_MRST <= '1';
				i_ATXMSK <= '0';
				i_RUN <= '0';
				i_RUN_old <= '0';
		
				if i_MRST = '0' then
					-- For Modes 2 and 3, the RBFFAIL bit-7 should be
					-- cleared to 0 by writing to the BIST Control/Status
					-- register (Address 0x8070) BEFORE taking MRST
					-- high.
					
					arinc_state <= ARINC_RESET_BIST_CLEAR;
					spi_addr <= X"8070";
					spi_data_in <= (others => '0');
					spi_write_en <= '1';
				else
					arinc_state <= ARINC_RESET_MRST_HIGH;
					i_cnt <= 0;
				end if;
				
			when ARINC_RESET_BIST_CLEAR =>
				if spi_done = '1' then
					spi_write_en <= '0';
					arinc_state <= ARINC_RESET_MRST_HIGH;
					i_cnt <= 0;
				end if;
				
			when ARINC_RESET_MRST_HIGH =>
				i_MRST <= '1';
				fifo_reset <= '1';
				i_arinc_cnt_0 <= 0;
				i_arinc_cnt_1 <= 0;
				i_arinc_cnt_2 <= 0;
				i_arinc_cnt_3 <= 0;
				
				if i_cnt >= 10000*16 then
					arinc_state <= ARINC_RESET_MRST_LOW;
				else
					i_cnt <= i_cnt + 1;
				end if;
			
			when ARINC_RESET_MRST_LOW =>
				i_MRST <= '0';
				fifo_reset <= '0';
				
				i_cnt <= 0;
				arinc_state <= ARINC_RESET_WAIT;
				
			when ARINC_RESET_WAIT =>
				-- wait 2 mS
				if i_cnt >= 10000*16 then
					if READY = '1' then
						arinc_state_old <= ARINC_RESET_CHECK_RAMFAIL;
						arinc_state <= ARINC_PEND_INT_READ;
					end if;
				else
					i_cnt <= i_cnt + 1;
				end if;
			
			when ARINC_RESET_CHECK_RAMFAIL =>
				update_flags <= ('1', '1', (others => '1'), '1', '1', '1', '1', '1', '1', (others => '1'), '1', '1', '1', '1', '1', '1');
				
				if registers_state.PEND_INT.RAMFAIL = '1' or registers_state.PEND_INT.COPYERR = '1' then
					reset_fail <= '1';
					arinc_state <= ARINC_WAIT;
				else
					reset_fail <= '0';
					arinc_state <= ARINC_RESET_WAIT_READY;
				end if;
				
			when ARINC_RESET_WAIT_READY =>
				if READY = '1' then
					arinc_state <= ARINC_FILL_RDE_LOOKUP;
					rde_cnt <= 0;
				end if;
				
			when ARINC_FILL_RDE_LOOKUP =>
				spi_addr <= X"7A00" + rde_cnt;
				spi_data_in <= (others => '1');
				spi_write_en <= '1';
				arinc_state <= ARINC_FILL_RDE_LOOKUP_AN;
				rde_cnt <= rde_cnt + 1;
				
			when ARINC_FILL_RDE_LOOKUP_AN =>
				if spi_done = '1' then
					spi_write_en <= '0';
					
					if rde_cnt > 255 then
						arinc_state <= ARINC_WAIT;
					else
						arinc_state <= ARINC_FILL_RDE_LOOKUP;
					end if;
				end if;
				
			------------------------------------------------------------- READ REGISTERS
			when ARINC_MASTER_CTRL_READ =>
				arinc_state <= ARINC_MASTER_CTRL_READ_AN;
				spi_addr <= X"800F";
				spi_read_en <= '1';
			when ARINC_MASTER_CTRL_READ_AN =>
				if spi_done = '1' then
					spi_read_en <= '0';
					registers_state.MASTER_CTRL <= vect_to_arinc_master_ctrl_type(spi_data_out);
					arinc_state <= arinc_state_old;
				end if;
				
			when ARINC_MASTER_STATUS_READ =>
				arinc_state <= ARINC_MASTER_STATUS_READ_AN;
				spi_addr <= X"800E";
				spi_read_en <= '1';
			when ARINC_MASTER_STATUS_READ_AN =>
				if spi_done = '1' then
					spi_read_en <= '0';
					registers_state.MASTER_STATUS <= vect_to_arinc_master_status_type(spi_data_out);
					arinc_state <= arinc_state_old;
				end if;
				
			when ARINC_RX_FIFO_NE_READ =>
				arinc_state <= ARINC_RX_FIFO_NE_READ_AN;
				spi_addr <= X"802B";
				spi_read_en <= '1';
			when ARINC_RX_FIFO_NE_READ_AN =>
				if spi_done = '1' then
					spi_read_en <= '0';
					registers_state.RX_FIFO_NE <= spi_data_out;
					arinc_state <= arinc_state_old;
				end if;
				
			when ARINC_RX_FIFO_THR_READ =>
				arinc_state <= ARINC_RX_FIFO_THR_READ_AN;
				spi_addr <= X"802A";
				spi_read_en <= '1';
			when ARINC_RX_FIFO_THR_READ_AN =>
				if spi_done = '1' then
					spi_read_en <= '0';
					registers_state.RX_FIFO_THR <= spi_data_out;
					arinc_state <= arinc_state_old;
				end if;
				
			when ARINC_RX_FIFO_FULL_READ =>
				arinc_state <= ARINC_RX_FIFO_FULL_READ_AN;
				spi_addr <= X"8029";
				spi_read_en <= '1';
			when ARINC_RX_FIFO_FULL_READ_AN =>
				if spi_done = '1' then
					spi_read_en <= '0';
					registers_state.RX_FIFO_FULL <= spi_data_out;
					arinc_state <= arinc_state_old;
				end if;
				
			when ARINC_RX_FIFO_MUX_READ =>
				arinc_state <= ARINC_RX_FIFO_MUX_READ_AN;
				spi_addr <= X"800C";
				spi_read_en <= '1';
			when ARINC_RX_FIFO_MUX_READ_AN =>
				if spi_done = '1' then
					spi_read_en <= '0';
					registers_state.RX_FIFO_MUX <= spi_data_out;
					arinc_state <= arinc_state_old;
				end if;
				
			when ARINC_RX_FIFO_THR_VAL_READ =>
				arinc_state <= ARINC_RX_FIFO_THR_VAL_READ_AN;
				spi_addr <= X"8021";
				spi_read_en <= '1';
			when ARINC_RX_FIFO_THR_VAL_READ_AN =>
				if spi_done = '1' then
					spi_read_en <= '0';
					registers_state.RX_FIFO_THR_VAL <= spi_data_out(4 downto 0);
					arinc_state <= arinc_state_old;
				end if;
				
			when ARINC_LOOPBACK_READ =>
				arinc_state <= ARINC_LOOPBACK_READ_AN;
				spi_addr <= X"8022";
				spi_read_en <= '1';
			when ARINC_LOOPBACK_READ_AN =>
				if spi_done = '1' then
					spi_read_en <= '0';
					registers_state.LOOPBACK <= spi_data_out;
					arinc_state <= arinc_state_old;
				end if;
				
			when ARINC_TX_RDY_READ =>
				arinc_state <= ARINC_TX_RDY_READ_AN;
				spi_addr <= X"800D";
				spi_read_en <= '1';
			when ARINC_TX_RDY_READ_AN =>
				if spi_done = '1' then
					spi_read_en <= '0';
					registers_state.TX_RDY <= spi_data_out(3 downto 0);
					arinc_state <= arinc_state_old;
				end if;
				
			when ARINC_APIR_READ =>
				arinc_state <= ARINC_APIR_READ_AN;
				spi_addr <= X"8000";
				spi_read_en <= '1';
			when ARINC_APIR_READ_AN =>
				if spi_done = '1' then
					spi_read_en <= '0';
					registers_state.APIR <= spi_data_out;
					arinc_state <= arinc_state_old;
				end if;
				
			when ARINC_AIMR_READ =>
				arinc_state <= ARINC_AIMR_READ_AN;
				spi_addr <= X"8020";
				spi_read_en <= '1';
			when ARINC_AIMR_READ_AN =>
				if spi_done = '1' then
					spi_read_en <= '0';
					registers_state.AIMR <= spi_data_out;
					arinc_state <= arinc_state_old;
				end if;
				
			when ARINC_PEND_INT_READ =>
				arinc_state <= ARINC_PEND_INT_READ_AN;
				spi_addr <= X"800A";
				spi_read_en <= '1';
			when ARINC_PEND_INT_READ_AN =>
				if spi_done = '1' then
					spi_read_en <= '0';
					registers_state.PEND_INT <= vect_to_arinc_pend_int_type(spi_data_out);
					arinc_state <= arinc_state_old;
				end if;
				
			when ARINC_PEND_INT_EN_READ =>
				arinc_state <= ARINC_PEND_INT_EN_READ_AN;
				spi_addr <= X"8034";
				spi_read_en <= '1';
			when ARINC_PEND_INT_EN_READ_AN =>
				if spi_done = '1' then
					spi_read_en <= '0';
					registers_state.PEND_INT_EN <= vect_to_arinc_pend_int_type(spi_data_out);
					arinc_state <= arinc_state_old;
				end if;
				
			when ARINC_TX_READY_INT_EN_READ =>
				arinc_state <= ARINC_TX_READY_INT_EN_READ_AN;
				spi_addr <= X"8035";
				spi_read_en <= '1';
			when ARINC_TX_READY_INT_EN_READ_AN =>
				if spi_done = '1' then
					spi_read_en <= '0';
					registers_state.TX_READY_INT_EN <= spi_data_out(3 downto 0);
					arinc_state <= arinc_state_old;
				end if;
				
			when ARINC_RX_CTRL_READ =>
				arinc_state <= ARINC_RX_CTRL_READ_AN;
				spi_addr <= X"8010" + reg_id_rw;
				spi_read_en <= '1';
			when ARINC_RX_CTRL_READ_AN =>
				if spi_done = '1' then
					spi_read_en <= '0';
					registers_state.RX_CTRL(reg_id_rw) <= vect_to_arinc_rx_ctrl_type(spi_data_out);
					arinc_state <= arinc_state_old;
				end if;
				
			when ARINC_TX_CTRL_READ =>
				arinc_state <= ARINC_TX_CTRL_READ_AN;
				spi_addr <= X"8018" + reg_id_rw;
				spi_read_en <= '1';
			when ARINC_TX_CTRL_READ_AN =>
				if spi_done = '1' then
					spi_read_en <= '0';
					registers_state.TX_CTRL(reg_id_rw) <= vect_to_arinc_tx_ctrl_type(spi_data_out);
					arinc_state <= arinc_state_old;
				end if;
			
			------------------------------------------------------------- WRITE REGISTERS
			when ARINC_MASTER_CTRL_WRITE =>
				arinc_state <= ARINC_MASTER_CTRL_WRITE_AN;
				spi_addr <= X"800F";
				spi_data_in <= arinc_master_ctrl_type_to_vect(registers.MASTER_CTRL);
				spi_write_en <= '1';
			when ARINC_MASTER_CTRL_WRITE_AN =>
				if spi_done = '1' then
					spi_write_en <= '0';
					arinc_state <= arinc_state_old;
				end if;
			
			when ARINC_RX_CTRL_WRITE =>
				arinc_state <= ARINC_RX_CTRL_WRITE_AN;
				spi_addr <= X"8010" + reg_id_rw;
				spi_data_in <= arinc_rx_ctrl_type_to_vect(registers.RX_CTRL(reg_id_rw));
				spi_write_en <= '1';
			when ARINC_RX_CTRL_WRITE_AN =>
				if spi_done = '1' then
					spi_write_en <= '0';
					arinc_state <= arinc_state_old;
				end if;
			
			when ARINC_TX_CTRL_WRITE =>
				arinc_state <= ARINC_TX_CTRL_WRITE_AN;
				spi_addr <= X"8018" + reg_id_rw;
				spi_data_in <= arinc_tx_ctrl_type_to_vect(registers.TX_CTRL(reg_id_rw));
				spi_write_en <= '1';
			when ARINC_TX_CTRL_WRITE_AN =>
				if spi_done = '1' then
					spi_write_en <= '0';
					arinc_state <= arinc_state_old;
				end if;
			
			when ARINC_LOOPBACK_WRITE =>
				arinc_state <= ARINC_LOOPBACK_WRITE_AN;
				spi_addr <= X"8022";
				spi_data_in <= registers.LOOPBACK;
				spi_write_en <= '1';
			when ARINC_LOOPBACK_WRITE_AN =>
				if spi_done = '1' then
					spi_write_en <= '0';
					arinc_state <= arinc_state_old;
				end if;
			
			when others =>
		end case;
	end if;
end process;

end Behavioral;
