-------------------------------------------------------------------------------
-- Project    : Spartan-6 Integrated Block for PCI Express
-- File       : PIO_32_TX_ENGINE.vhd
-- Description: 32 bit LocalLink Transmit Unit.
-------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity PIO_32_TX_ENGINE is
	port (
		clk                   : in  std_logic;
		rst_n                 : in  std_logic;

		-- AXIS
		s_axis_tx_tready      : in  std_logic;
		s_axis_tx_tdata       : out std_logic_vector(31 downto 0);
		s_axis_tx_tkeep       : out std_logic_vector(3 downto 0);
		s_axis_tx_tlast       : out std_logic;
		s_axis_tx_tvalid      : out std_logic;
		tx_src_dsc            : out std_logic;

		req_compl_i           : in  std_logic;
		req_compl_with_data_i : in  std_logic;
		compl_done_o          : out std_logic;

		req_tc_i              : in  std_logic_vector(2 downto 0);
		req_td_i              : in  std_logic;
		req_ep_i              : in  std_logic;
		req_attr_i            : in  std_logic_vector(1 downto 0);
		req_len_i             : in  std_logic_vector(9 downto 0);
		req_rid_i             : in  std_logic_vector(15 downto 0);
		req_tag_i             : in  std_logic_vector(7 downto 0);
		req_be_i              : in  std_logic_vector(7 downto 0);
		req_addr_i            : in  std_logic_vector(29 downto 0);
		req_bar_i             : in  std_logic_vector(1 downto 0);

		rd_bar_o              : out std_logic_vector(1 downto 0);
		rd_addr_o             : out std_logic_vector(29 downto 0);
		rd_be_o               : out std_logic_vector(3 downto 0);
		rd_data_i             : in  std_logic_vector(31 downto 0);

		completer_id_i        : in  std_logic_vector(15 downto 0);
		cfg_bus_mstr_enable_i : in  std_logic;

		-- Connection with PCIe DMA
		pio_tx_address        : IN  STD_LOGIC_VECTOR(63 DOWNTO 0);
		pio_tx_length         : IN  STD_LOGIC_VECTOR(7 DOWNTO 0);
		pio_tx_dout           : IN  STD_LOGIC_VECTOR(31 DOWNTO 0);
		pio_tx_rd_en          : OUT STD_LOGIC;
		pio_tx_start          : IN  STD_LOGIC;
		pio_tx_ack            : OUT STD_LOGIC;
		
		tx_busy               : out std_logic;
		rx_busy               : in  std_logic
	);
end pio_32_tx_engine;

architecture rtl of pio_32_tx_engine is
	-- Clock-to-out delay
	constant TCQ : time := 1 ns;

	-- TLP Header format/type values
	constant PIO_32_CPLD_FMT_TYPE : std_logic_vector(6 downto 0) := "1001010";
	constant PIO_32_CPL_FMT_TYPE  : std_logic_vector(6 downto 0) := "0001010";

	constant PIO_32_DMA_FMT_TYPE  : std_logic_vector(6 downto 0) := "1100000";

	-- States
	type state_type is (
		PIO_32_TX_RST_STATE,
		PIO_32_TX_CPL_CPLD_DW1,
		PIO_32_TX_CPL_CPLD_DW2,
		PIO_32_TX_CPLD_DW3,
		PIO_32_TX_WAIT_STATE,

		PIO_32_TX_DMA_DW1,
		PIO_32_TX_DMA_DW2,
		PIO_32_TX_DMA_DW3,
		PIO_32_TX_DMA_DW4,
		PIO_32_TX_DMA_WAIT,
		PIO_32_TX_DMA_FINISH
	);
	signal state                 : state_type;

	-- Local signals
	signal byte_count            : std_logic_vector(11 downto 0);
	signal lower_addr            : std_logic_vector(6 downto 0);
	signal cpl_w_data            : std_logic;
	signal req_compl_q           : std_logic;
	signal req_compl_with_data_q : std_logic;
	signal rd_be_o_int           : std_logic_vector(3 downto 0);

	signal dma_cnt               : STD_LOGIC_VECTOR(7 DOWNTO 0) := (others => '0');
	signal dma_ready_i           : std_logic := '0';

	signal dma_tag               : std_logic_vector(7 downto 0) := (others => '0');
	
	signal pio_tx_dout_i         : STD_LOGIC_VECTOR(31 DOWNTO 0);
  
begin
  
	-- Unused discontinue signal
	tx_src_dsc <= '0';

	-- Assign byte enables to output bus
	rd_be_o <= rd_be_o_int;

	-- Present address and byte enable to memory module
	rd_addr_o    <= req_addr_i(29 downto 0);
	rd_be_o_int  <= req_be_i(3 downto 0);

-- Calculate byte count based on byte enable
process(rd_be_o_int) begin
	case rd_be_o_int(3 downto 0) is
		when "1001" => byte_count <= X"004";
		when "1011" => byte_count <= X"004";
		when "1101" => byte_count <= X"004";
		when "1111" => byte_count <= X"004";
		when "0101" => byte_count <= X"003";
		when "0111" => byte_count <= X"003";
		when "1010" => byte_count <= X"003";
		when "1110" => byte_count <= X"003";
		when "0011" => byte_count <= X"002";
		when "0110" => byte_count <= X"002";
		when "1100" => byte_count <= X"002";
		when "0001" => byte_count <= X"001";
		when "0010" => byte_count <= X"001";
		when "0100" => byte_count <= X"001";
		when "1000" => byte_count <= X"001";
		when others => byte_count <= X"001"; -- "0000"
	end case;
end process;

-- Calculate lower address based on  byte enable
process(rd_be_o_int, req_addr_i) begin
	case (rd_be_o_int(3 downto 0)) is
		when "0000" => lower_addr <= req_addr_i(4 downto 0) & "00";
		when "0001" => lower_addr <= req_addr_i(4 downto 0) & "00";
		when "0011" => lower_addr <= req_addr_i(4 downto 0) & "00";
		when "0101" => lower_addr <= req_addr_i(4 downto 0) & "00";
		when "0111" => lower_addr <= req_addr_i(4 downto 0) & "00";
		when "1001" => lower_addr <= req_addr_i(4 downto 0) & "00";
		when "1011" => lower_addr <= req_addr_i(4 downto 0) & "00";
		when "1101" => lower_addr <= req_addr_i(4 downto 0) & "00";
		when "1111" => lower_addr <= req_addr_i(4 downto 0) & "00";
		when "0010" => lower_addr <= req_addr_i(4 downto 0) & "01";
		when "0110" => lower_addr <= req_addr_i(4 downto 0) & "01";
		when "1010" => lower_addr <= req_addr_i(4 downto 0) & "01";
		when "1110" => lower_addr <= req_addr_i(4 downto 0) & "01";
		when "0100" => lower_addr <= req_addr_i(4 downto 0) & "10";
		when "1100" => lower_addr <= req_addr_i(4 downto 0) & "10";
		when others => lower_addr <= req_addr_i(4 downto 0) & "11"; -- "1000"
	end case;
end process;

process begin
	wait until rising_edge(clk);
	if (rst_n = '0') then
		req_compl_q            <= '0' after TCQ;
		req_compl_with_data_q  <= '1' after TCQ;
	else
		req_compl_q            <= req_compl_i after TCQ;
		req_compl_with_data_q  <= req_compl_with_data_i after TCQ;
	end if;
end process;

--  Generate Completion with 1 DW Payload or Completion with no data
process begin
	wait until rising_edge(clk);
	if (rst_n = '0') then
		s_axis_tx_tlast   <= '0' after TCQ;
		s_axis_tx_tvalid  <= '0' after TCQ;
		s_axis_tx_tdata   <= (others => '0') after TCQ;
		s_axis_tx_tkeep   <= x"F" after TCQ;

		compl_done_o      <= '0' after TCQ;

		state             <= PIO_32_TX_RST_STATE after TCQ;
		pio_tx_ack        <= '0';
		pio_tx_rd_en      <= '0';
		tx_busy           <= '0';
		dma_tag           <= (others => '0');
	else
		compl_done_o      <= '0' after TCQ;
		pio_tx_rd_en      <= '0';
		pio_tx_ack        <= '0';

		case (state) is
			when PIO_32_TX_RST_STATE =>
				tx_busy           <= '0';

				if rx_busy = '0' and dma_ready_i = '1' and pio_tx_start = '1' and cfg_bus_mstr_enable_i = '1' then
					tx_busy           <= '1';
					s_axis_tx_tlast   <= '0' after TCQ;
					s_axis_tx_tvalid  <= '1' after TCQ;
					s_axis_tx_tdata   <= '0' &
												PIO_32_DMA_FMT_TYPE &
												'0' &
												"000" &
												"0000" &
												'0' &
												'0' &
												"00" &
												"00" &
												"00" & pio_tx_length after TCQ;
					state             <= PIO_32_TX_DMA_DW1 after TCQ;
					dma_cnt           <= "00000001";
					dma_ready_i       <= '0';

				elsif ((req_compl_q = '1') and (req_compl_with_data_q = '1'))  then
					-- Begin a CplD TLP
					s_axis_tx_tlast   <= '0' after TCQ;
					s_axis_tx_tvalid  <= '1' after TCQ;
					s_axis_tx_tdata   <= '0' &
												PIO_32_CPLD_FMT_TYPE &
												'0' &
												req_tc_i &
												"0000" &
												req_td_i &
												req_ep_i &
												req_attr_i &
												"00" &
												req_len_i after TCQ;
					cpl_w_data        <= req_compl_with_data_q after TCQ;
					state             <= PIO_32_TX_CPL_CPLD_DW1 after TCQ;
					dma_ready_i         <= '0';
				elsif ((req_compl_q = '1') and (req_compl_with_data_q = '0')) then
					-- Begin a Cpl TLP
					s_axis_tx_tlast   <= '0' after TCQ;
					s_axis_tx_tvalid  <= '1' after TCQ;
					s_axis_tx_tdata   <= '0' &
												PIO_32_CPL_FMT_TYPE &
												'0' &
												req_tc_i &
												"0000" &
												req_td_i &
												req_ep_i &
												req_attr_i &
												"00" &
											  req_len_i after TCQ;
					cpl_w_data        <= req_compl_with_data_q after TCQ;
					state             <= PIO_32_TX_CPL_CPLD_DW1 after TCQ;

					dma_ready_i         <= '0';
				else
					s_axis_tx_tlast   <= '0' after TCQ;
					s_axis_tx_tvalid  <= '0' after TCQ;
					s_axis_tx_tdata   <= (others => '0') after TCQ; -- 32-bit
					s_axis_tx_tkeep   <= x"F" after TCQ;
					state             <= PIO_32_TX_RST_STATE after TCQ;

					dma_ready_i         <= cfg_bus_mstr_enable_i and s_axis_tx_tready;
				end if;
			-- end of PIO_32_TX_RST_STATE

			when PIO_32_TX_CPL_CPLD_DW1 =>
				if (s_axis_tx_tready = '1') then
					-- Output next DW of TLP
					s_axis_tx_tlast   <= '0' after TCQ;
					s_axis_tx_tvalid  <= '1' after TCQ;

					s_axis_tx_tdata   <= completer_id_i &
												"000" &
												'0' &
												byte_count after TCQ;
					state            <= PIO_32_TX_CPL_CPLD_DW2 after TCQ;
				else
					-- Wait for core to accept previous DW
					state            <= PIO_32_TX_CPL_CPLD_DW1 after TCQ;
				end if;
			-- end of PIO_32_TX_CPL_CPLD_DW1

			when PIO_32_TX_CPL_CPLD_DW2 =>
				if (s_axis_tx_tready = '1') then
					-- Output next DW of TLP
					s_axis_tx_tlast   <= '0' after TCQ;
					s_axis_tx_tvalid  <= '1' after TCQ;
					s_axis_tx_tdata   <= req_rid_i &
												req_tag_i &
												'0' &
												lower_addr after TCQ;
					if (cpl_w_data = '1') then
						-- For a CplD, there is one more DW
						s_axis_tx_tlast <= '0' after TCQ;
						state           <= PIO_32_TX_CPLD_DW3 after TCQ;
					else
						-- For a Cpl, this is the final DW
						s_axis_tx_tlast     <= '1' after TCQ;
						state           <= PIO_32_TX_WAIT_STATE after TCQ;
					end if;
				else
					-- Wait for core to accept previous DW
					state              <= PIO_32_TX_CPL_CPLD_DW2 after TCQ;
				end if;
			-- end of PIO_32_TX_CPL_CPLD_DW2

			when PIO_32_TX_CPLD_DW3 =>
				if (s_axis_tx_tready = '1') then
					-- Output next DW of TLP
					s_axis_tx_tlast  <= '1' after TCQ;
					s_axis_tx_tvalid <= '1' after TCQ;
					s_axis_tx_tdata  <= rd_data_i(7 downto 0) & rd_data_i(15 downto 8) & rd_data_i(23 downto 16) & rd_data_i(31 downto 24) after TCQ;
					state            <= PIO_32_TX_WAIT_STATE after TCQ;
				else
					-- Wait for core to accept previous DW
					state            <= PIO_32_TX_CPLD_DW3 after TCQ;
			end if;

			when PIO_32_TX_WAIT_STATE =>
				if (s_axis_tx_tready = '1') then
					-- Core has accepted final DW of TLP
					s_axis_tx_tlast  <= '0' after TCQ;
					s_axis_tx_tvalid <= '0' after TCQ;
					compl_done_o     <= '1' after TCQ;
					s_axis_tx_tdata  <= (others => '0') after TCQ;
					state            <= PIO_32_TX_RST_STATE after TCQ;
				else
					-- Wait for core to accept previous DW
					state            <= PIO_32_TX_WAIT_STATE after TCQ;
				end if;
			-- end of PIO_32_TX_WAIT_STATE
	  
			when PIO_32_TX_DMA_DW1 =>
				if (s_axis_tx_tready = '1') then
					-- Output next DW of TLP
					s_axis_tx_tlast   <= '0' after TCQ;
					s_axis_tx_tvalid  <= '1' after TCQ;

					if pio_tx_length > 1 then
						s_axis_tx_tdata   <= completer_id_i &
													dma_tag &
													X"F" &
													X"F" after TCQ;
					else
						s_axis_tx_tdata   <= completer_id_i &
													dma_tag &
													X"0" &
													X"F" after TCQ;
					end if;

					state             <= PIO_32_TX_DMA_DW2 after TCQ;
					dma_tag           <= dma_tag + 1;
				else
					-- Wait for core to accept previous DW
					state             <= PIO_32_TX_DMA_DW1 after TCQ;
				end if;
			-- end of PIO_32_TX_DMA_DW1
		 
			when PIO_32_TX_DMA_DW2 =>
				if (s_axis_tx_tready = '1') then
					-- Output next DW of TLP
					s_axis_tx_tlast   <= '0' after TCQ;
					s_axis_tx_tvalid  <= '1' after TCQ;

					s_axis_tx_tdata   <= pio_tx_address(63 downto 32) after TCQ;
					state             <= PIO_32_TX_DMA_DW3 after TCQ;
				else
					-- Wait for core to accept previous DW
					state             <= PIO_32_TX_DMA_DW2 after TCQ;
				end if;
			-- end of PIO_32_TX_DMA_DW2
	
			when PIO_32_TX_DMA_DW3 =>
				if (s_axis_tx_tready = '1') then
					-- Output next DW of TLP
					s_axis_tx_tlast   <= '0' after TCQ;
					s_axis_tx_tvalid  <= '1' after TCQ;

					s_axis_tx_tdata   <= pio_tx_address(31 downto 0) after TCQ;
					state             <= PIO_32_TX_DMA_DW4 after TCQ;
					pio_tx_rd_en    <= '1';
				else
					-- Wait for core to accept previous DW
					state             <= PIO_32_TX_DMA_DW3 after TCQ;
				end if;
			-- end of PIO_32_TX_DMA_DW3

			when PIO_32_TX_DMA_DW4 =>
				if (s_axis_tx_tready = '1') then
					-- Output next DW of TLP
					if dma_cnt >= pio_tx_length then
						s_axis_tx_tlast <= '1' after TCQ;
						state           <= PIO_32_TX_DMA_WAIT after TCQ;
					else
						s_axis_tx_tlast <= '0' after TCQ;
						pio_tx_rd_en    <= '1';
					end if;
					
					s_axis_tx_tvalid   <= '1' after TCQ;
					s_axis_tx_tdata    <= pio_tx_dout(7 downto 0) & pio_tx_dout(15 downto 8) & pio_tx_dout(23 downto 16) & pio_tx_dout(31 downto 24) after TCQ;
					
					dma_cnt            <= dma_cnt + 1;
				else
					-- Wait for core to accept previous DW
					state              <= PIO_32_TX_DMA_DW4 after TCQ;
				end if;
			-- end of PIO_32_TX_DMA_DW4
		 
			when PIO_32_TX_DMA_WAIT =>
				if (s_axis_tx_tready = '1') then
					-- Core has accepted final DW of TLP
					s_axis_tx_tlast  <= '0' after TCQ;
					s_axis_tx_tvalid <= '0' after TCQ;
					s_axis_tx_tdata  <= (others => '0') after TCQ;
					state            <= PIO_32_TX_DMA_FINISH;
					pio_tx_ack       <= '1';
				else
					-- Wait for core to accept previous DW
					state            <= PIO_32_TX_DMA_WAIT after TCQ;
				end if;
			-- end of PIO_32_TX_DMA_WAIT
			
			when PIO_32_TX_DMA_FINISH =>
				if pio_tx_start = '0' then
					state            <= PIO_32_TX_RST_STATE after TCQ;
				else
					pio_tx_ack       <= '1';
				end if;
			when others =>
				state               <= PIO_32_TX_RST_STATE after TCQ;
		end case;
	end if;
end process;
  
--	dma_ready <= dma_ready_i;

end rtl;

