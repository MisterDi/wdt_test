-------------------------------------------------------------------------------
-- Project    : Spartan-6 Integrated Block for PCI Express
-- File       : PIO_EP.vhd
-- Description: Endpoint Programmed I/O module.
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.numeric_std.all;

entity PIO_EP is
	port (
		-- Common
		clk                    : in  std_logic;
		rst_n                  : in  std_logic;

		-- AXIS TX
		s_axis_tx_tready       : in  std_logic;
		s_axis_tx_tdata        : out std_logic_vector(31 downto 0);
		s_axis_tx_tkeep        : out std_logic_vector(3 downto 0);
		s_axis_tx_tlast        : out std_logic;
		s_axis_tx_tvalid       : out std_logic;
		tx_src_dsc             : out std_logic;

		-- AXIS RX
		m_axis_rx_tdata        : in  std_logic_vector(31 downto 0);
		m_axis_rx_tkeep        : in  std_logic_vector(3 downto 0);
		m_axis_rx_tlast        : in  std_logic;
		m_axis_rx_tvalid       : in  std_logic;
		m_axis_rx_tready       : out std_logic;
		m_axis_rx_tuser        : in  std_logic_vector(21 downto 0);

		-- Turn-off signaling
		req_compl_o            : out std_logic;
		compl_done_o           : out std_logic;

		-- Configuration
		cfg_completer_id       : in  std_logic_vector(15 downto 0);
		cfg_bus_mstr_enable    : in  std_logic;

		--  Read Port
		rd_addr    : out std_logic_vector(29 downto 0);
		rd_bar     : out std_logic_vector(1 downto 0);
		rd_be      : out std_logic_vector(3 downto 0);
		rd_data    : in  std_logic_vector(31 downto 0);

		--  Write Port
		wr_addr    : out std_logic_vector(29 downto 0);
		wr_bar     : out std_logic_vector(1 downto 0);
		wr_be      : out std_logic_vector(7 downto 0);
		wr_data    : out std_logic_vector(31 downto 0);
		wr_en      : out std_logic;
		wr_busy    : in  std_logic;

		pio_tx_address        : IN  STD_LOGIC_VECTOR(63 DOWNTO 0);
		pio_tx_length         : IN  STD_LOGIC_VECTOR(7 DOWNTO 0);
		pio_tx_dout           : IN  STD_LOGIC_VECTOR(31 DOWNTO 0);
		pio_tx_rd_en          : OUT STD_LOGIC;
		pio_tx_start          : IN  STD_LOGIC;
		pio_tx_ack            : OUT STD_LOGIC
	);
end PIO_EP;

architecture rtl of PIO_EP is

	-- Local signals
	signal req_compl     : std_logic;
	signal req_compl_with_data : std_logic;
	signal compl_done    : std_logic;

	signal req_tc        : std_logic_vector(2 downto 0);
	signal req_td        : std_logic;
	signal req_ep        : std_logic;
	signal req_attr      : std_logic_vector(1 downto 0);
	signal req_len       : std_logic_vector(9 downto 0);
	signal req_rid       : std_logic_vector(15 downto 0);
	signal req_tag       : std_logic_vector(7 downto 0);
	signal req_be        : std_logic_vector(7 downto 0);
	signal req_addr      : std_logic_vector(29 downto 0);
	signal req_bar       : std_logic_vector(1 downto 0);

	signal tx_busy       : std_logic;
	signal rx_busy       : std_logic;

	component PIO_32_RX_ENGINE is
		port (
			clk                   : in  std_logic;
			rst_n                 : in  std_logic;

			m_axis_rx_tdata       : in  std_logic_vector(31 downto 0);
			m_axis_rx_tkeep       : in  std_logic_vector(3 downto 0);
			m_axis_rx_tlast       : in  std_logic;
			m_axis_rx_tvalid      : in  std_logic;
			m_axis_rx_tready      : out std_logic;
			m_axis_rx_tuser       : in  std_logic_vector(21 downto 0);

			req_compl_o           : out std_logic;
			req_compl_with_data_o : out std_logic;
			compl_done_i          : in  std_logic;

			req_tc_o              : out std_logic_vector(2 downto 0);  -- Memory Read TC
			req_td_o              : out std_logic;                     -- Memory Read TD
			req_ep_o              : out std_logic;                     -- Memory Read EP
			req_attr_o            : out std_logic_vector(1 downto 0);  -- Memory Read Attribute
			req_len_o             : out std_logic_vector(9 downto 0);  -- Memory Read Length (1DW)
			req_rid_o             : out std_logic_vector(15 downto 0); -- Memory Read Requestor ID
			req_tag_o             : out std_logic_vector(7 downto 0);  -- Memory Read Tag
			req_be_o              : out std_logic_vector(7 downto 0);  -- Memory Read Byte Enables
			req_addr_o            : out std_logic_vector(29 downto 0); -- Memory Read Address
			req_bar_o             : out std_logic_vector(1 downto 0);  -- Memory Read Address

			wr_addr_o             : out std_logic_vector(29 downto 0); -- Memory Write Address
			wr_be_o               : out std_logic_vector(7 downto 0);  -- Memory Write Byte Enable
			wr_data_o             : out std_logic_vector(31 downto 0); -- Memory Write Data
			wr_en_o               : out std_logic;                     -- Memory Write Enable
			wr_busy_i             : in std_logic;                      -- Memory Write Busy
			wr_bar_o              : out std_logic_vector(1 downto 0);  -- bar ID
			
			tx_busy               : in std_logic;
			rx_busy               : out std_logic
		);
	end component;

	component PIO_32_TX_ENGINE is
		port (
			clk                   : in  std_logic;
			rst_n                 : in  std_logic;

			-- AXIS
			s_axis_tx_tready      : in  std_logic;
			s_axis_tx_tdata       : out  std_logic_vector(31 downto 0);
			s_axis_tx_tkeep       : out std_logic_vector(3 downto 0);
			s_axis_tx_tlast       : out std_logic;
			s_axis_tx_tvalid      : out std_logic;
			tx_src_dsc            : out std_logic;

			req_compl_i           : in  std_logic;
			req_compl_with_data_i : in  std_logic;
			compl_done_o          : out  std_logic;

			req_tc_i              : in std_logic_vector(2 downto 0);
			req_td_i              : in  std_logic;
			req_ep_i              : in  std_logic;
			req_attr_i            : in std_logic_vector(1 downto 0);
			req_len_i             : in std_logic_vector(9 downto 0);
			req_rid_i             : in std_logic_vector(15 downto 0);
			req_tag_i             : in std_logic_vector(7 downto 0);
			req_be_i              : in std_logic_vector(7 downto 0);
			req_addr_i            : in std_logic_vector(29 downto 0);
			req_bar_i             : in std_logic_vector(1 downto 0);

			rd_addr_o             : out std_logic_vector(29 downto 0);
			rd_be_o               : out std_logic_vector(3 downto 0);
			rd_data_i             : in std_logic_vector(31 downto 0);
			rd_bar_o              : out std_logic_vector(1 downto 0);

			completer_id_i        : in std_logic_vector(15 downto 0);
			cfg_bus_mstr_enable_i : in  std_logic;

			pio_tx_address        : IN  STD_LOGIC_VECTOR(63 DOWNTO 0);
			pio_tx_length         : IN  STD_LOGIC_VECTOR(7 DOWNTO 0);
			pio_tx_dout           : IN  STD_LOGIC_VECTOR(31 DOWNTO 0);
			pio_tx_rd_en          : OUT STD_LOGIC;
			pio_tx_start          : IN  STD_LOGIC;
			pio_tx_ack            : OUT STD_LOGIC;
			
			tx_busy               : out std_logic;
			rx_busy               : in std_logic
		);
	end component;
	
begin
	req_compl_o     <= req_compl;
	compl_done_o    <= compl_done;

	--
	-- Local-Link Receive Controller
	--
	EP_RX : PIO_32_RX_ENGINE
	port map (
		clk                   => clk,                 -- I
		rst_n                 => rst_n,               -- I

		--AXIS RX
		m_axis_rx_tdata       => m_axis_rx_tdata,     -- I
		m_axis_rx_tkeep       => m_axis_rx_tkeep,     -- I
		m_axis_rx_tlast       => m_axis_rx_tlast,     -- I
		m_axis_rx_tvalid      => m_axis_rx_tvalid,    -- I
		m_axis_rx_tready      => m_axis_rx_tready,    -- O
		m_axis_rx_tuser       => m_axis_rx_tuser,     -- I


		-- Handshake with Tx engine
		req_compl_o           => req_compl,           -- O
		req_compl_with_data_o => req_compl_with_data, -- O
		compl_done_i          => compl_done,          -- I

		req_tc_o              => req_tc,              -- O [2:0]
		req_td_o              => req_td,              -- O
		req_ep_o              => req_ep,              -- O
		req_attr_o            => req_attr,            -- O [1:0]
		req_len_o             => req_len,             -- O [9:0]
		req_rid_o             => req_rid,             -- O [15:0]
		req_tag_o             => req_tag,             -- O [7:0]
		req_be_o              => req_be,              -- O [7:0]
		req_addr_o            => req_addr,            -- O [32:0]
		req_bar_o             => req_bar,             -- O [32:0]

		-- Memory Write Port
		wr_addr_o             => wr_addr,             -- O [30:0]
		wr_be_o               => wr_be,               -- O [7:0]
		wr_data_o             => wr_data,             -- O [31:0]
		wr_en_o               => wr_en,               -- O
		wr_busy_i             => wr_busy,             -- I
		wr_bar_o              => wr_bar,               -- O [30:0]
		
		tx_busy               => tx_busy,
		rx_busy               => rx_busy
	);

	--
	-- Local-Link Transmit Controller
	--
	EP_TX : PIO_32_TX_ENGINE
	port map (
		clk                    => clk,                 -- I
		rst_n                  => rst_n,               -- I

		-- AXIS Tx
		s_axis_tx_tready       => s_axis_tx_tready,     -- I
		s_axis_tx_tdata        => s_axis_tx_tdata,      -- O
		s_axis_tx_tkeep        => s_axis_tx_tkeep,      -- O
		s_axis_tx_tlast        => s_axis_tx_tlast,      -- O
		s_axis_tx_tvalid       => s_axis_tx_tvalid,     -- O
		tx_src_dsc             => tx_src_dsc,           -- O


		-- Handshake with Rx engine
		req_compl_i            => req_compl,           -- I
		req_compl_with_data_i  => req_compl_with_data, -- I
		compl_done_o           => compl_done,          -- 0

		req_tc_i               => req_tc,              -- I [2:0]
		req_td_i               => req_td,              -- I
		req_ep_i               => req_ep,              -- I
		req_attr_i             => req_attr,            -- I [1:0]
		req_len_i              => req_len,             -- I [9:0]
		req_rid_i              => req_rid,             -- I [15:0]
		req_tag_i              => req_tag,             -- I [7:0]
		req_be_i               => req_be,              -- I [7:0]
		req_addr_i             => req_addr,            -- I [30:0]
		req_bar_i              => req_bar,             -- I [30:0]

		-- Read Port
		rd_addr_o              => rd_addr,             -- O [30:0]
		rd_be_o                => rd_be,               -- O [3:0]
		rd_data_i              => rd_data,             -- I [31:0]
		rd_bar_o               => rd_bar,              -- I [31:0]

		completer_id_i         => cfg_completer_id,    -- I [15:0]
		cfg_bus_mstr_enable_i  => cfg_bus_mstr_enable, -- I

		pio_tx_address         => pio_tx_address,
		pio_tx_length          => pio_tx_length,
		pio_tx_dout            => pio_tx_dout,
		pio_tx_rd_en           => pio_tx_rd_en,
		pio_tx_start           => pio_tx_start,
		pio_tx_ack             => pio_tx_ack,
		
		tx_busy                => tx_busy,
		rx_busy                => rx_busy
	);

end rtl;
