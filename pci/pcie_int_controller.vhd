----------------------------------------------------------------------------------
-- Company: Aerotech LTD, LLC
-- Engineer: Eugene Shamaev
-- 
-- Create Date:    11:53:29 01/02/2018 
-- Design Name: 
-- Module Name:    pcie_int_controller - Behavioral 
-- Project Name:   Video10D
-- Target Devices: Spartan-6 45T
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

library UNISIM;
use UNISIM.Vcomponents.ALL;

entity pcie_int_controller is
	Port (
		clk                     : in  STD_LOGIC;
		reset                   : in  STD_LOGIC;
		
		-- INT core is ready for trasnfer (or finished last transfer)
		pci_int_ready           : OUT STD_LOGIC;
		
		-- connection to PCIe modules
		cfg_interrupt           : out std_logic;
		cfg_interrupt_rdy       : in  std_logic;
		cfg_interrupt_assert    : out std_logic;
		cfg_interrupt_do        : in  std_logic_vector(7 downto 0);
		cfg_interrupt_di        : out std_logic_vector(7 downto 0);
		cfg_interrupt_mmenable  : in  std_logic_vector(2 downto 0);
		cfg_interrupt_msienable : in  std_logic;
		
		-- connection for writing
		int_cmd_wr_clk          : IN  STD_LOGIC;
		int_cmd_wr_en           : IN  STD_LOGIC;
		int_cmd                 : IN  STD_LOGIC_VECTOR(1 DOWNTO 0);
		int_cmd_num             : IN  STD_LOGIC_VECTOR(1 DOWNTO 0);
		int_cmd_full            : OUT STD_LOGIC
	);
end pcie_int_controller;

architecture Behavioral of pcie_int_controller is
	
	-- command FIFO input
	signal rd_en              : STD_LOGIC;
	signal cmd                : STD_LOGIC_VECTOR(1 DOWNTO 0);
	signal int_num            : STD_LOGIC_VECTOR(1 DOWNTO 0);
	signal empty              : STD_LOGIC;

	COMPONENT int_cmd_fifo
	PORT (
		rst                    : IN  STD_LOGIC;
		
		wr_clk                 : IN  STD_LOGIC;
		wr_en                  : IN  STD_LOGIC;
		din                    : IN  STD_LOGIC_VECTOR(3 DOWNTO 0);
		full                   : OUT STD_LOGIC;

		rd_clk                 : IN  STD_LOGIC;
		rd_en                  : IN  STD_LOGIC;
		dout                   : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
		empty                  : OUT STD_LOGIC
	);
	END COMPONENT;

	-- States
	type int_state_type is (
		INT_WAIT_CMD,
		INT_WAIT_READY_CMD
	);
	signal int_state : int_state_type;
	
begin

int_cmd_fifo_i : int_cmd_fifo
	PORT MAP (
		rst              => reset,
		
		wr_clk           => int_cmd_wr_clk,
		wr_en            => int_cmd_wr_en,
		din(1 downto 0)  => int_cmd,
		din(3 downto 2)  => int_cmd_num,
		full             => int_cmd_full,
		
		rd_clk           => clk,
		rd_en            => rd_en,
		dout(1 downto 0) => cmd,
		dout(3 downto 2) => int_num,
		empty            => empty
	);

-- Typically 1 interrupt takes 300 nS
process (reset, clk)
begin
	if reset = '1' then
		int_state     <= INT_WAIT_CMD;
		rd_en         <= '0';
		cfg_interrupt <= '0';
		pci_int_ready <= '0';
	elsif clk'event and clk = '1' then
		rd_en <= '0';
		pci_int_ready <= '0';
		
		case (int_state) is
			when INT_WAIT_CMD =>
				pci_int_ready <= '1';
				
				if empty = '0' then
					if cfg_interrupt_msienable <= '1' then
						if cmd = "00" then
							cfg_interrupt_di <= "000000" & int_num;
							cfg_interrupt    <= '1';
							int_state        <= INT_WAIT_READY_CMD;
						end if;
					end if;
					
					rd_en <= '1';
				end if;
				
			when INT_WAIT_READY_CMD =>
				if cfg_interrupt_rdy = '1' then
					int_state     <= INT_WAIT_CMD;
					cfg_interrupt <= '0';
				end if;
				
			when others =>
		end case;
	end if;
end process;

end Behavioral;

