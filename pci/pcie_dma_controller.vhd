----------------------------------------------------------------------------------
-- Company: Aerotech LTD, LLC
-- Engineer: Eugene Shamaev
-- 
-- Create Date:    15:51:44 01/03/2018 
-- Design Name: 
-- Module Name:    pcie_dma_controller - Behavioral 
-- Project Name:   Video10D
-- Target Devices: Spartan-6 45T
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
use ieee.numeric_std.all;

library UNISIM;
use UNISIM.Vcomponents.ALL;

entity pcie_dma_controller is
	generic (
		-- PCI
		MAX_TRANSFER           : integer := 16
	);
	
	Port (
		clk                    : in  STD_LOGIC;
		reset                  : in  STD_LOGIC;
		
		-- Control signals
		-- 64 bit adress of memory at host
		start_address          : IN  STD_LOGIC_VECTOR(63 DOWNTO 0);
		-- DMA core is ready for trasnfer (or finished last transfer)
		pci_dma_ready          : OUT STD_LOGIC;
		-- DMA command is processed only if address regsters are filled
		dma_cfg_address_set    : IN  STD_LOGIC;
		
		-- Connection with PCIe TX module
		pio_tx_address         : OUT STD_LOGIC_VECTOR(63 DOWNTO 0);
		pio_tx_length          : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
		pio_tx_start           : OUT STD_LOGIC;
		pio_tx_ack             : IN  STD_LOGIC;
		
		pio_dma_data_dout      : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
		pio_dma_data_rd_en     : IN  STD_LOGIC;
		
		-- DMA FIFO
		dma_cmd                : in  STD_LOGIC_VECTOR(33 DOWNTO 0);
		dma_cmd_rd_en          : out STD_LOGIC;
		dma_cmd_empty          : in  STD_LOGIC;
		
		dma_data               : in  STD_LOGIC_VECTOR(31 DOWNTO 0);
		dma_data_rd_en         : out STD_LOGIC;
		dma_data_rd_data_count : in  STD_LOGIC_VECTOR(10 DOWNTO 0);
		
		debug                  : out std_logic := '0'
	);
	
end pcie_dma_controller;

architecture Behavioral of pcie_dma_controller is

	signal dma_cmd_o        : STD_LOGIC_VECTOR(1 DOWNTO 0);
	signal dma_cmd_length_o : STD_LOGIC_VECTOR(31 DOWNTO 0);

	signal curr_addr       : STD_LOGIC_VECTOR(63 DOWNTO 0);
	signal total_length    : STD_LOGIC_VECTOR(31 DOWNTO 0);
	signal pio_tx_length_i : STD_LOGIC_VECTOR(7 DOWNTO 0);

	type dma_state_type is (
		DMA_WAIT_CMD,
		DMA_PREPARE_CMD,
		DMA_WAIT_FOR_DATA_CMD,
		DMA_WAIT_FOR_TRANSFER_CMD,
		DMA_LOOP_CMD
	);
	signal dma_state : dma_state_type;

begin

dma_cmd_o         <= dma_cmd(1 downto 0);
dma_cmd_length_o  <= dma_cmd(33 downto 2);
pio_dma_data_dout <= dma_data;
dma_data_rd_en     <= pio_dma_data_rd_en;

process (reset, clk)
begin
	if reset = '1' then
		dma_state         <= DMA_WAIT_CMD;
		total_length      <= (others => '0');
		dma_cmd_rd_en     <= '0';
		pci_dma_ready     <= '0';
	elsif clk'event and clk = '1' then
		dma_cmd_rd_en     <= '0';
		pci_dma_ready     <= '0';
		
		--------------------------------------------
		debug             <= '0';
		
		case (dma_state) is
			when DMA_WAIT_CMD =>
				pci_dma_ready <= '1';
				
				if dma_cmd_empty = '0' then
					if dma_cmd_o = "00" and dma_cfg_address_set = '1' then
						curr_addr    <= start_address;
						total_length <= dma_cmd_length_o;
						dma_state    <= DMA_PREPARE_CMD;
					end if;
					
					dma_cmd_rd_en <= '1';
				end if;
			
			when DMA_PREPARE_CMD =>
				if total_length <= MAX_TRANSFER then
					-- last transfer
						pio_tx_length_i <= total_length(7 downto 0);
				else
					-- ongoing transfer
						pio_tx_length_i <= std_logic_vector(to_unsigned(MAX_TRANSFER, 8));
				end if;
				
				pio_tx_address <= curr_addr;
				dma_state      <= DMA_WAIT_FOR_DATA_CMD;
				
			when DMA_WAIT_FOR_DATA_CMD =>
				if dma_data_rd_data_count >= pio_tx_length_i then
					-- all data is present in data FIFO
					pio_tx_start <= '1';
					dma_state    <= DMA_WAIT_FOR_TRANSFER_CMD;
				end if;
			
			when DMA_WAIT_FOR_TRANSFER_CMD =>
				if pio_tx_ack = '1' then
					pio_tx_start <= '0';
					curr_addr    <= curr_addr + (pio_tx_length_i & "00");
					total_length <= total_length - pio_tx_length_i;
					dma_state    <= DMA_LOOP_CMD;
					
					--------------------------------------------
					debug <= '1';
				end if;
				
			when DMA_LOOP_CMD =>
				if total_length = 0 then
					-- transfer finished
					dma_state <= DMA_WAIT_CMD;
				else
					-- more data to transfer
					dma_state <= DMA_PREPARE_CMD;
				end if;
				
			when others =>
		end case;
	end if;
end process;

pio_tx_length <= pio_tx_length_i;

end Behavioral;

