-------------------------------------------------------------------------------
-- Project    : Spartan-6 Integrated Block for PCI Express
-- File       : PIO.vhd
-- Description: Programmed I/O module. Design implements 8 KBytes of
--              programmable memory space. Host processor can access this
--              memory space using Memory Read 32 and Memory Write 32 TLPs.
--              Design accepts  1 Double Word (DW) payload length on Memory
--              Write 32 TLP and responds to 1 DW length Memory Read 32 TLPs
--              with a Completion with Data TLP (1DW payload).
--
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;

entity PIO is
	port (
		user_clk               : in  std_logic;
		user_reset             : in  std_logic;
		user_lnk_up            : in  std_logic;

		-- AXIS TX
		s_axis_tx_tready       : in  std_logic;
		s_axis_tx_tdata        : out std_logic_vector(31 downto 0);
		s_axis_tx_tkeep        : out std_logic_vector(3 downto 0);
		s_axis_tx_tlast        : out std_logic;
		s_axis_tx_tvalid       : out std_logic;
		tx_src_dsc             : out std_logic;

		-- AXIS RX
		m_axis_rx_tdata        : in  std_logic_vector(31 downto 0);
		m_axis_rx_tkeep        : in  std_logic_vector(3 downto 0);
		m_axis_rx_tlast        : in  std_logic;
		m_axis_rx_tvalid       : in  std_logic;
		m_axis_rx_tready       : out std_logic;
		m_axis_rx_tuser        : in  std_logic_vector(21 downto 0);

		cfg_to_turnoff         : in  std_logic;
		cfg_turnoff_ok         : out std_logic;

		cfg_completer_id       : in  std_logic_vector(15 downto 0);
		cfg_bus_mstr_enable    : in  std_logic;

		--  Read Port
		rd_addr    : out  std_logic_vector(29 downto 0);
		rd_be      : out  std_logic_vector(3 downto 0);
		rd_data    : in std_logic_vector(31 downto 0);
		rd_bar     : out std_logic_vector(1 downto 0);

		--  Write Port
		wr_addr    : out  std_logic_vector(29 downto 0);
		wr_bar     : out std_logic_vector(1 downto 0);
		wr_be      : out  std_logic_vector(7 downto 0);
		wr_data    : out  std_logic_vector(31 downto 0);
		wr_en      : out  std_logic;
		wr_busy    : in std_logic;

		pio_tx_address        : IN  STD_LOGIC_VECTOR(63 DOWNTO 0);
		pio_tx_length         : IN  STD_LOGIC_VECTOR(7 DOWNTO 0);
		pio_tx_dout           : IN  STD_LOGIC_VECTOR(31 DOWNTO 0);
		pio_tx_rd_en          : OUT STD_LOGIC;
		pio_tx_start          : IN  STD_LOGIC;
		pio_tx_ack            : OUT STD_LOGIC;
		
		compl_done_o          : OUT STD_LOGIC
	);
end PIO;

architecture rtl of PIO is

	-- Local signals
	signal req_compl      : std_logic;
	signal compl_done     : std_logic;
	signal pio_reset_n    : std_logic;
	signal user_reset_inv : std_logic;

	component PIO_EP
		port (
			-- Common
			clk                    : in  std_logic;
			rst_n                  : in  std_logic;

			s_axis_tx_tready       : in  std_logic;
			s_axis_tx_tdata        : out std_logic_vector(31 downto 0);
			s_axis_tx_tkeep        : out std_logic_vector(3 downto 0);
			s_axis_tx_tlast        : out std_logic;
			s_axis_tx_tvalid       : out std_logic;
			tx_src_dsc             : out std_logic;

			m_axis_rx_tdata        : in std_logic_vector(31 downto 0);
			m_axis_rx_tkeep        : in std_logic_vector(3 downto 0);
			m_axis_rx_tlast        : in  std_logic;
			m_axis_rx_tvalid       : in  std_logic;
			m_axis_rx_tready       : out std_logic;
			m_axis_rx_tuser        : in std_logic_vector(21 downto 0);

			req_compl_o            : out std_logic;
			compl_done_o           : out std_logic;

			cfg_completer_id       : in std_logic_vector(15 downto 0);
			cfg_bus_mstr_enable    : in std_logic;

			--  Read Port
			rd_addr    : out std_logic_vector(29 downto 0);
			rd_be      : out std_logic_vector(3 downto 0);
			rd_data    : in std_logic_vector(31 downto 0);
			rd_bar     : out std_logic_vector(1 downto 0);

			--  Write Port
			wr_addr    : out std_logic_vector(29 downto 0);
			wr_bar     : out std_logic_vector(1 downto 0);
			wr_be      : out std_logic_vector(7 downto 0);
			wr_data    : out std_logic_vector(31 downto 0);
			wr_en      : out std_logic;
			wr_busy    : in std_logic;

			pio_tx_address        : IN  STD_LOGIC_VECTOR(63 DOWNTO 0);
			pio_tx_length         : IN  STD_LOGIC_VECTOR(7 DOWNTO 0);
			pio_tx_dout           : IN  STD_LOGIC_VECTOR(31 DOWNTO 0);
			pio_tx_rd_en          : OUT STD_LOGIC;
			pio_tx_start          : IN  STD_LOGIC;
			pio_tx_ack            : OUT STD_LOGIC
		);
	end component;

	component PIO_TO_CTRL
		port (
			clk              : in  std_logic;
			rst_n            : in  std_logic;

			req_compl_i      : in  std_logic;
			compl_done_i     : in  std_logic;

			cfg_to_turnoff : in  std_logic;
			cfg_turnoff_ok : out std_logic
		);
	end component;

begin

	pio_reset_n  <= user_lnk_up and not user_reset;
	compl_done_o   <= compl_done;

	--
	-- PIO instance
	--
	PIO_EP_i : PIO_EP
	port map (
		clk                 => user_clk,            -- I
		rst_n               => pio_reset_n,        -- I

		-- AXIS Tx
		s_axis_tx_tready       =>s_axis_tx_tready,     -- I
		s_axis_tx_tdata        =>s_axis_tx_tdata,      -- O
		s_axis_tx_tkeep        =>s_axis_tx_tkeep,      -- O
		s_axis_tx_tlast        =>s_axis_tx_tlast,      -- O
		s_axis_tx_tvalid       =>s_axis_tx_tvalid,     -- O
		tx_src_dsc             =>tx_src_dsc,           -- O

		--AXIS RX
		m_axis_rx_tdata      => m_axis_rx_tdata,    -- I
		m_axis_rx_tkeep      => m_axis_rx_tkeep,    -- I
		m_axis_rx_tlast      => m_axis_rx_tlast,    -- I
		m_axis_rx_tvalid     => m_axis_rx_tvalid,   -- I
		m_axis_rx_tready     => m_axis_rx_tready,   -- O
		m_axis_rx_tuser      => m_axis_rx_tuser,    -- I

		req_compl_o         => req_compl,          -- O
		compl_done_o        => compl_done,         -- O

		cfg_completer_id    => cfg_completer_id,   -- I [15:0]
		cfg_bus_mstr_enable => cfg_bus_mstr_enable, -- I

		rd_addr             => rd_addr,
		rd_bar              => rd_bar,
		rd_be               => rd_be,
		rd_data             => rd_data,
		wr_addr             => wr_addr,
		wr_bar              => wr_bar,
		wr_be               => wr_be,
		wr_data             => wr_data,
		wr_en               => wr_en,
		wr_busy             => wr_busy,

		pio_tx_address         => pio_tx_address,
		pio_tx_length          => pio_tx_length,
		pio_tx_dout            => pio_tx_dout,
		pio_tx_rd_en           => pio_tx_rd_en,
		pio_tx_start           => pio_tx_start,
		pio_tx_ack             => pio_tx_ack
	);

	--
	-- Turn-Off controller
	--

	user_reset_inv <= not user_reset;

	PIO_TO : PIO_TO_CTRL
	port map (
		clk              => user_clk,          -- I
		rst_n            => user_reset_inv,      -- I

		req_compl_i      => req_compl,        -- I
		compl_done_i     => compl_done,       -- I

		cfg_to_turnoff => cfg_to_turnoff, -- I
		cfg_turnoff_ok => cfg_turnoff_ok  -- O
	);

end rtl;  -- PIO
