-------------------------------------------------------------------------------
-- Project    : Spartan-6 Integrated Block for PCI Express
-- File       : pcie_app_s6.vhd
-- Description: PCI Express Endpoint sample application design.
-------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

library UNISIM;
use UNISIM.Vcomponents.ALL;

entity pcie_app_s6 is
	generic (
		-- PCI
		FAST_TRAIN              : boolean := FALSE;
		
		-- FPGA version and build codes
		FGPA_MAJOR              : integer := 0;
		FGPA_MINOR              : integer := 1;
		FGPA_BUILD              : integer := 1;
		
		-- 
		enable_arinc_429  : boolean := true;
		enable_dio_input  : boolean := true;
		enable_dio_output : boolean := true;
		enable_arinc_708  : boolean := true;
		enable_sound      : boolean := true
	);
	port (
		-- PCIe
		sys_clk_p                  : in  STD_LOGIC;
		sys_clk_n                  : in  STD_LOGIC;
		pci_exp_txp                : out STD_LOGIC;
		pci_exp_txn                : out STD_LOGIC;
		pci_exp_rxp                : in  STD_LOGIC;
		pci_exp_rxn                : in  STD_LOGIC;
	
		-- Asynchronous reset signal
		sys_reset                  : in  std_logic;
	
		-- 1-lane Integrated Endpoint Block Frequency: 62.5 MHz
		user_clk                   : out std_logic;
		user_reset                 : out std_logic;
		user_lnk_up                : out std_logic;
		
		-- Interrupt command input FIFO
		int_cmd_wr_clk             : IN  STD_LOGIC;
		int_cmd_wr_en              : IN  STD_LOGIC;
		int_cmd                    : IN  STD_LOGIC_VECTOR(1 DOWNTO 0);
		int_cmd_num                : IN  STD_LOGIC_VECTOR(1 DOWNTO 0);
		int_cmd_full               : OUT STD_LOGIC;
		
		-- INT core is ready for trasnfer (or finished last transfer)
		-- PCIe CLOCK DOMAIN
		pci_int_ready              : OUT STD_LOGIC;
		
		-- Interrupt state signals
		-- PCIe CLOCK DOMAIN
		cfg_interrupt_mmenable     : OUT std_logic_vector(2 downto 0);
		cfg_interrupt_msienable    : OUT std_logic;
		
		-- DMA control signals
		start_address              : IN  STD_LOGIC_VECTOR(63 DOWNTO 0);
		pci_dma_ready              : OUT STD_LOGIC;
		dma_cfg_address_set        : IN  STD_LOGIC;
		
		-- DMA FIFO
		dma_cmd                : in  STD_LOGIC_VECTOR(33 DOWNTO 0);
		dma_cmd_rd_en          : out STD_LOGIC;
		dma_cmd_empty          : in  STD_LOGIC;
		
		dma_data               : in  STD_LOGIC_VECTOR(31 DOWNTO 0);
		dma_data_rd_en         : out STD_LOGIC;
		dma_data_rd_data_count : in  STD_LOGIC_VECTOR(10 DOWNTO 0);
		
		debug : out std_logic := '0';

		A429_CMD_IN_1 : OUT std_logic_vector(31 downto 0);
		A429_CMD_IN_CLK_1 : OUT std_logic;
		A429_CMD_IN_EN_1 : OUT std_logic;
		A429_STATUS_OUT_1 : IN std_logic_vector(31 downto 0);
		A429_STATUS_OUT_CLK_1 : OUT std_logic;
		A429_ARINC_FIFO_RX_CLK_1 : OUT std_logic;
		A429_ARINC_FIFO_RX_READ_1 : OUT std_logic_vector(7 downto 0);
		A429_ARINC_FIFO_TX_0_1 : OUT std_logic_vector(31 downto 0);
		A429_ARINC_FIFO_TX_1_1 : OUT std_logic_vector(31 downto 0);
		A429_ARINC_FIFO_TX_2_1 : OUT std_logic_vector(31 downto 0);
		A429_ARINC_FIFO_TX_3_1 : OUT std_logic_vector(31 downto 0);
		A429_ARINC_FIFO_TX_CLK_1 : OUT std_logic;
		A429_ARINC_FIFO_RX_0_1 : IN std_logic_vector(31 downto 0);
		A429_ARINC_FIFO_RX_1_1 : IN std_logic_vector(31 downto 0);
		A429_ARINC_FIFO_RX_2_1 : IN std_logic_vector(31 downto 0);
		A429_ARINC_FIFO_RX_3_1 : IN std_logic_vector(31 downto 0);
		A429_ARINC_FIFO_RX_4_1 : IN std_logic_vector(31 downto 0);
		A429_ARINC_FIFO_RX_5_1 : IN std_logic_vector(31 downto 0);
		A429_ARINC_FIFO_RX_6_1 : IN std_logic_vector(31 downto 0);
		A429_ARINC_FIFO_RX_7_1 : IN std_logic_vector(31 downto 0);
		A429_ARINC_FIFO_RX_EMPTY_1 : IN std_logic_vector(7 downto 0);
		A429_ARINC_FIFO_TX_FULL_1 : IN std_logic_vector(3 downto 0);
		A429_ARINC_FIFO_TX_WRITE_1 : OUT std_logic_vector(3 downto 0);

		A429_CMD_IN_2 : OUT std_logic_vector(31 downto 0);
		A429_CMD_IN_CLK_2 : OUT std_logic;
		A429_CMD_IN_EN_2 : OUT std_logic;
		A429_STATUS_OUT_2 : IN std_logic_vector(31 downto 0);
		A429_STATUS_OUT_CLK_2 : OUT std_logic;
		A429_ARINC_FIFO_RX_CLK_2 : OUT std_logic;
		A429_ARINC_FIFO_RX_READ_2 : OUT std_logic_vector(7 downto 0);
		A429_ARINC_FIFO_TX_0_2 : OUT std_logic_vector(31 downto 0);
		A429_ARINC_FIFO_TX_1_2 : OUT std_logic_vector(31 downto 0);
		A429_ARINC_FIFO_TX_2_2 : OUT std_logic_vector(31 downto 0);
		A429_ARINC_FIFO_TX_3_2 : OUT std_logic_vector(31 downto 0);
		A429_ARINC_FIFO_TX_CLK_2 : OUT std_logic;
		A429_ARINC_FIFO_RX_0_2 : IN std_logic_vector(31 downto 0);
		A429_ARINC_FIFO_RX_1_2 : IN std_logic_vector(31 downto 0);
		A429_ARINC_FIFO_RX_2_2 : IN std_logic_vector(31 downto 0);
		A429_ARINC_FIFO_RX_3_2 : IN std_logic_vector(31 downto 0);
		A429_ARINC_FIFO_RX_4_2 : IN std_logic_vector(31 downto 0);
		A429_ARINC_FIFO_RX_5_2 : IN std_logic_vector(31 downto 0);
		A429_ARINC_FIFO_RX_6_2 : IN std_logic_vector(31 downto 0);
		A429_ARINC_FIFO_RX_7_2 : IN std_logic_vector(31 downto 0);
		A429_ARINC_FIFO_RX_EMPTY_2 : IN std_logic_vector(7 downto 0);
		A429_ARINC_FIFO_TX_FULL_2 : IN std_logic_vector(3 downto 0);
		A429_ARINC_FIFO_TX_WRITE_2 : OUT std_logic_vector(3 downto 0);
		
		A429_CMD_IN_3 : OUT std_logic_vector(31 downto 0);
		A429_CMD_IN_CLK_3 : OUT std_logic;
		A429_CMD_IN_EN_3 : OUT std_logic;
		A429_STATUS_OUT_3 : IN std_logic_vector(31 downto 0);
		A429_STATUS_OUT_CLK_3 : OUT std_logic;
		A429_ARINC_FIFO_RX_CLK_3 : OUT std_logic;
		A429_ARINC_FIFO_RX_READ_3 : OUT std_logic_vector(7 downto 0);
		A429_ARINC_FIFO_TX_0_3 : OUT std_logic_vector(31 downto 0);
		A429_ARINC_FIFO_TX_1_3 : OUT std_logic_vector(31 downto 0);
		A429_ARINC_FIFO_TX_2_3 : OUT std_logic_vector(31 downto 0);
		A429_ARINC_FIFO_TX_3_3 : OUT std_logic_vector(31 downto 0);
		A429_ARINC_FIFO_TX_CLK_3 : OUT std_logic;
		A429_ARINC_FIFO_RX_0_3 : IN std_logic_vector(31 downto 0);
		A429_ARINC_FIFO_RX_1_3 : IN std_logic_vector(31 downto 0);
		A429_ARINC_FIFO_RX_2_3 : IN std_logic_vector(31 downto 0);
		A429_ARINC_FIFO_RX_3_3 : IN std_logic_vector(31 downto 0);
		A429_ARINC_FIFO_RX_4_3 : IN std_logic_vector(31 downto 0);
		A429_ARINC_FIFO_RX_5_3 : IN std_logic_vector(31 downto 0);
		A429_ARINC_FIFO_RX_6_3 : IN std_logic_vector(31 downto 0);
		A429_ARINC_FIFO_RX_7_3 : IN std_logic_vector(31 downto 0);
		A429_ARINC_FIFO_RX_EMPTY_3 : IN std_logic_vector(7 downto 0);
		A429_ARINC_FIFO_TX_FULL_3 : IN std_logic_vector(3 downto 0);
		A429_ARINC_FIFO_TX_WRITE_3 : OUT std_logic_vector(3 downto 0);
		
		A429_CMD_IN_4 : OUT std_logic_vector(31 downto 0);
		A429_CMD_IN_CLK_4 : OUT std_logic;
		A429_CMD_IN_EN_4 : OUT std_logic;
		A429_STATUS_OUT_4 : IN std_logic_vector(31 downto 0);
		A429_STATUS_OUT_CLK_4 : OUT std_logic;
		A429_ARINC_FIFO_RX_CLK_4 : OUT std_logic;
		A429_ARINC_FIFO_RX_READ_4 : OUT std_logic_vector(7 downto 0);
		A429_ARINC_FIFO_TX_0_4 : OUT std_logic_vector(31 downto 0);
		A429_ARINC_FIFO_TX_1_4 : OUT std_logic_vector(31 downto 0);
		A429_ARINC_FIFO_TX_2_4 : OUT std_logic_vector(31 downto 0);
		A429_ARINC_FIFO_TX_3_4 : OUT std_logic_vector(31 downto 0);
		A429_ARINC_FIFO_TX_CLK_4 : OUT std_logic;
		A429_ARINC_FIFO_RX_0_4 : IN std_logic_vector(31 downto 0);
		A429_ARINC_FIFO_RX_1_4 : IN std_logic_vector(31 downto 0);
		A429_ARINC_FIFO_RX_2_4 : IN std_logic_vector(31 downto 0);
		A429_ARINC_FIFO_RX_3_4 : IN std_logic_vector(31 downto 0);
		A429_ARINC_FIFO_RX_4_4 : IN std_logic_vector(31 downto 0);
		A429_ARINC_FIFO_RX_5_4 : IN std_logic_vector(31 downto 0);
		A429_ARINC_FIFO_RX_6_4 : IN std_logic_vector(31 downto 0);
		A429_ARINC_FIFO_RX_7_4 : IN std_logic_vector(31 downto 0);
		A429_ARINC_FIFO_RX_EMPTY_4 : IN std_logic_vector(7 downto 0);
		A429_ARINC_FIFO_TX_FULL_4 : IN std_logic_vector(3 downto 0);
		A429_ARINC_FIFO_TX_WRITE_4 : OUT std_logic_vector(3 downto 0);
		
		-- Discrete output
		OUT_FAULT_1    : in  STD_LOGIC;
		OUT_FAULT_2    : in  STD_LOGIC;
		OUT_FAULT_3    : in  STD_LOGIC;
		OUT_FAULT_4    : in  STD_LOGIC;
		
		OUT_READY      : in  STD_LOGIC;
		OUT_DATA       : out STD_LOGIC_VECTOR (23 downto 0);
		OUT_DATA_CLK   : out STD_LOGIC;
		OUT_DATA_EN    : out STD_LOGIC;
		
		-- Discrete input
		IN_DIN : out STD_LOGIC_VECTOR (31 downto 0);
		IN_DIN_CLK : out STD_LOGIC;
		IN_DIN_EN : out STD_LOGIC;
		
		IN_DOUT : in  STD_LOGIC_VECTOR (31 downto 0);
		IN_DOUT_STATE : in  STD_LOGIC_VECTOR (31 downto 0);
		IN_DOUT_CLK : out STD_LOGIC;
		
		-- 708
		A708_DMA_ADDR_CLK : out STD_LOGIC;
		A708_DMA_ADDR : out STD_LOGIC_VECTOR (63 downto 0);
		
		A708_STATE : in STD_LOGIC_VECTOR (31 downto 0);
		A708_STATE_CLK : out STD_LOGIC;
		A708_CMD : out STD_LOGIC_VECTOR (31 downto 0);
		A708_CMD_EN : out STD_LOGIC;
		A708_CMD_CLK : out STD_LOGIC;

		-- Serial port's
		RsSysClk		: out	std_logic;
 	   PortIn_1 	: out	STD_LOGIC_VECTOR (31 downto 0);
	   PortOut_1 	: in	STD_LOGIC_VECTOR (31 downto 0);
 	   CfgIn_1 		: out	STD_LOGIC_VECTOR (31 downto 0);
	   CfgOut_1 	: in	STD_LOGIC_VECTOR (31 downto 0);
	   CmdDataRd_1	: out	STD_LOGIC;
	   CmdDataWr_1	: out	STD_LOGIC;
	   CmdCfgRd_1	: out	STD_LOGIC;
	   CmdCfgWr_1	: out	STD_LOGIC;

 	   PortIn_2 	: out	STD_LOGIC_VECTOR (31 downto 0);
	   PortOut_2 	: in	STD_LOGIC_VECTOR (31 downto 0);
 	   CfgIn_2 		: out	STD_LOGIC_VECTOR (31 downto 0);
	   CfgOut_2 	: in	STD_LOGIC_VECTOR (31 downto 0);
	   CmdDataRd_2	: out	STD_LOGIC;
	   CmdDataWr_2	: out	STD_LOGIC;
	   CmdCfgRd_2	: out	STD_LOGIC;
	   CmdCfgWr_2	: out	STD_LOGIC;

 	   PortIn_3 	: out	STD_LOGIC_VECTOR (31 downto 0);
	   PortOut_3 	: in	STD_LOGIC_VECTOR (31 downto 0);
 	   CfgIn_3 		: out	STD_LOGIC_VECTOR (31 downto 0);
	   CfgOut_3 	: in	STD_LOGIC_VECTOR (31 downto 0);
	   CmdDataRd_3	: out	STD_LOGIC;
	   CmdDataWr_3	: out	STD_LOGIC;
	   CmdCfgRd_3	: out	STD_LOGIC;
	   CmdCfgWr_3	: out	STD_LOGIC;

 	   PortIn_4 	: out	STD_LOGIC_VECTOR (31 downto 0);
	   PortOut_4 	: in	STD_LOGIC_VECTOR (31 downto 0);
 	   CfgIn_4 		: out	STD_LOGIC_VECTOR (31 downto 0);
	   CfgOut_4 	: in	STD_LOGIC_VECTOR (31 downto 0);
	   CmdDataRd_4	: out	STD_LOGIC;
	   CmdDataWr_4	: out	STD_LOGIC;
	   CmdCfgRd_4	: out	STD_LOGIC;
	   CmdCfgWr_4	: out	STD_LOGIC;
		
		Wdt_Tick		: out STD_LOGIC;

		DIMM_ADC_DATA	: out	STD_LOGIC_VECTOR(31 downto 0);
		DIMM_ADC_CFG	: in	STD_LOGIC_VECTOR(31 downto 0);
	   CmdAdcRead		: out	STD_LOGIC;
	   CmdAdcCfg		: out	STD_LOGIC;
	   RawAdcRead		: out	STD_LOGIC;
	   RawAdcWrite		: out	STD_LOGIC
	
	);
end pcie_app_s6;

architecture rtl of pcie_app_s6 is

	signal pio_tx_address         : STD_LOGIC_VECTOR(63 DOWNTO 0);
	signal pio_tx_length          : STD_LOGIC_VECTOR(7 DOWNTO 0);
	signal pio_tx_start           : STD_LOGIC;
	signal pio_tx_ack             : STD_LOGIC;

	COMPONENT pcie_dma_controller is
	Port (
		clk                    : in  STD_LOGIC;
		reset                  : in  STD_LOGIC;
		
		-- Control signals
		start_address          : IN  STD_LOGIC_VECTOR(63 DOWNTO 0);
		pci_dma_ready          : OUT STD_LOGIC;
		dma_cfg_address_set    : IN  STD_LOGIC;
		
		-- Connection with PCIe TX module
		pio_tx_address         : OUT STD_LOGIC_VECTOR(63 DOWNTO 0);
		pio_tx_length          : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
		pio_tx_start           : OUT STD_LOGIC;
		pio_tx_ack             : IN  STD_LOGIC;
		
		pio_dma_data_dout      : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
		pio_dma_data_rd_en     : IN  STD_LOGIC;
		
		-- DMA FIFO
		dma_cmd                : in  STD_LOGIC_VECTOR(33 DOWNTO 0);
		dma_cmd_rd_en          : out STD_LOGIC;
		dma_cmd_empty          : in  STD_LOGIC;
		
		dma_data               : in  STD_LOGIC_VECTOR(31 DOWNTO 0);
		dma_data_rd_en         : out STD_LOGIC;
		dma_data_rd_data_count : in  STD_LOGIC_VECTOR(10 DOWNTO 0);
		
		debug : out std_logic := '0'
	);
	end COMPONENT;

	signal pio_dma_data_dout          : STD_LOGIC_VECTOR(31 DOWNTO 0);
	signal pio_dma_data_rd_en         : STD_LOGIC;

	COMPONENT pcie_int_controller
	Port (
		clk                     : in  STD_LOGIC;
		reset                   : in  STD_LOGIC;
		
		-- INT core is ready for trasnfer (or finished last transfer)
		pci_int_ready           : OUT STD_LOGIC;
		
		-- connection to PCIe modules
		cfg_interrupt           : out std_logic;
		cfg_interrupt_rdy       : in  std_logic;
		cfg_interrupt_assert    : out std_logic;
		cfg_interrupt_do        : in  std_logic_vector(7 downto 0);
		cfg_interrupt_di        : out std_logic_vector(7 downto 0);
		cfg_interrupt_mmenable  : in  std_logic_vector(2 downto 0);
		cfg_interrupt_msienable : in  std_logic;
		
		int_cmd_wr_clk          : IN  STD_LOGIC;
		int_cmd_wr_en           : IN  STD_LOGIC;
		int_cmd                 : IN  STD_LOGIC_VECTOR(1 DOWNTO 0);
		int_cmd_num             : IN  STD_LOGIC_VECTOR(1 DOWNTO 0);
		int_cmd_full            : OUT STD_LOGIC
	);
	end COMPONENT;

	component PIO is
	port (
		user_clk               : in  std_logic;
		user_reset             : in  std_logic;
		user_lnk_up            : in  std_logic;

		-- AXIS TX
		s_axis_tx_tready       : in  std_logic;
		s_axis_tx_tdata        : out std_logic_vector(31 downto 0);
		s_axis_tx_tkeep        : out std_logic_vector(3 downto 0);
		s_axis_tx_tlast        : out std_logic;
		s_axis_tx_tvalid       : out std_logic;
		tx_src_dsc             : out std_logic;

		-- AXIS RX
		m_axis_rx_tdata        : in  std_logic_vector(31 downto 0);
		m_axis_rx_tkeep        : in  std_logic_vector(3 downto 0);
		m_axis_rx_tlast        : in  std_logic;
		m_axis_rx_tvalid       : in  std_logic;
		m_axis_rx_tready       : out std_logic;
		m_axis_rx_tuser        : in  std_logic_vector(21 downto 0);

		cfg_to_turnoff         : in  std_logic;
		cfg_turnoff_ok         : out std_logic;
		cfg_completer_id       : in  std_logic_vector(15 downto 0);
		cfg_bus_mstr_enable    : in  std_logic;

		--  Read Port
		rd_addr    : out std_logic_vector(29 downto 0);
		rd_bar     : out std_logic_vector(1 downto 0);
		rd_be      : out std_logic_vector(3 downto 0);
		rd_data    : in  std_logic_vector(31 downto 0);

		--  Write Port
		wr_addr    : out std_logic_vector(29 downto 0);
		wr_bar     : out std_logic_vector(1 downto 0);
		wr_be      : out std_logic_vector(7 downto 0);
		wr_data    : out std_logic_vector(31 downto 0);
		wr_en      : out std_logic;
		wr_busy    : in  std_logic;

		pio_tx_address        : IN  STD_LOGIC_VECTOR(63 DOWNTO 0);
		pio_tx_length         : IN  STD_LOGIC_VECTOR(7 DOWNTO 0);
		pio_tx_dout           : IN  STD_LOGIC_VECTOR(31 DOWNTO 0);
		pio_tx_rd_en          : OUT STD_LOGIC;
		pio_tx_start          : IN  STD_LOGIC;
		pio_tx_ack            : OUT STD_LOGIC;
		
		compl_done_o          : OUT STD_LOGIC
	);
	end component PIO;

   -----------------------------------------------------------------------------ARINC_SPI
	COMPONENT Arnic429_master PORT(
			  SCK 		: out  STD_LOGIC;
           MOSI 		: out  STD_LOGIC;
           MISO 		: in  STD_LOGIC;
			  SSEL		: out	STD_LOGIC;
           DataIn 	: in  STD_LOGIC_VECTOR (31 downto 0);
           DataOut 	: out  STD_LOGIC_VECTOR (31 downto 0);
           ReadDev 	: in  STD_LOGIC;
           WriteDev	: in  STD_LOGIC;
           ClkTop		: in  STD_LOGIC;
			  ClkInn		: in  STD_LOGIC;
           Reset 		: in  STD_LOGIC;
           InpFull	: out  STD_LOGIC;
           InpEmpty	: out  STD_LOGIC;
           OutFull	: out  STD_LOGIC;
           OutEmpty	: out  STD_LOGIC;
			  Cmd			: in  STD_LOGIC_VECTOR(0 downto 0);
			  DevStatus	: out STD_LOGIC_VECTOR (3 downto 0)
--			  AINT		: in STD_LOGIC;
--			  AACK		: out  STD_LOGIC;
--			  MINT		: in STD_LOGIC;
--			  MINTACK	: out  STD_LOGIC;
--			  RST_PIN	: out  STD_LOGIC;
--			  TXMSK		: out  STD_LOGIC;
--			  RUN			: out  STD_LOGIC;
--			  READY		: in STD_LOGIC
		);
	end COMPONENT;
	
	-- Memory Access
	--  Read Port
	signal rd_addr    : std_logic_vector(29 downto 0);
	signal rd_bar     : std_logic_vector(1 downto 0);
	signal rd_be      : std_logic_vector(3 downto 0);
	signal rd_data    : std_logic_vector(31 downto 0);

	--  Write Port
	signal wr_addr    : std_logic_vector(29 downto 0);
	signal wr_bar     : std_logic_vector(1 downto 0);
	signal wr_be      : std_logic_vector(7 downto 0);
	signal wr_data    : std_logic_vector(31 downto 0);
	signal wr_en      : std_logic;
	signal wr_busy    : std_logic;

	-- Tx
	signal tx_buf_av                   : std_logic_vector(5 downto 0);
	signal tx_cfg_req                  : std_logic;
	signal tx_err_drop                 : std_logic;
	signal s_axis_tx_tready            : std_logic;
	signal s_axis_tx_tdata             : std_logic_vector(31 downto 0);
	signal s_axis_tx_tuser_i           : std_logic_vector(3 downto 0);
	signal s_axis_tx_tlast             : std_logic;
	signal s_axis_tx_tvalid            : std_logic;
	signal tx_cfg_gnt                  : std_logic;
	signal s_axis_tx_tkeep             : std_logic_vector(3 downto 0);

	-- Rx
	signal m_axis_rx_tdata             : std_logic_vector(31 downto 0);
	signal m_axis_rx_tkeep             : std_logic_vector (3 downto 0);
	signal m_axis_rx_tlast             : std_logic;
	signal m_axis_rx_tvalid            : std_logic;
	signal m_axis_rx_tready            : std_logic;
	signal m_axis_rx_tuser             : std_logic_vector(21 downto 0);
	signal rx_np_ok                    : std_logic;

	-- Flow Control
	signal fc_cpld                     : std_logic_vector(11 downto 0);
	signal fc_cplh                     : std_logic_vector(7 downto 0);
	signal fc_npd                      : std_logic_vector(11 downto 0);
	signal fc_nph                      : std_logic_vector(7 downto 0);
	signal fc_pd                       : std_logic_vector(11 downto 0);
	signal fc_ph                       : std_logic_vector(7 downto 0);
	signal fc_sel                      : std_logic_vector(2 downto 0);

	-- Config
	signal cfg_dsn                     : std_logic_vector(63 downto 0);
	signal cfg_do                      : std_logic_vector(31 downto 0);
	signal cfg_rd_wr_done              : std_logic;
	signal cfg_dwaddr                  : std_logic_vector(9 downto 0);
	signal cfg_rd_en                   : std_logic;

	-- Error signaling
	signal cfg_err_cor                 : std_logic;
	signal cfg_err_ur                  : std_logic;
	signal cfg_err_ecrc                : std_logic;
	signal cfg_err_cpl_timeout         : std_logic;
	signal cfg_err_cpl_abort           : std_logic;
	signal cfg_err_posted              : std_logic;
	signal cfg_err_locked              : std_logic;
	signal cfg_err_tlp_cpl_header      : std_logic_vector(47 downto 0);
	signal cfg_err_cpl_rdy             : std_logic;

	-- Interrupt signaling
	signal cfg_interrupt               : std_logic;
	signal cfg_interrupt_rdy           : std_logic;
	signal cfg_interrupt_assert        : std_logic;
	signal cfg_interrupt_di            : std_logic_vector(7 downto 0);
	signal cfg_interrupt_do            : std_logic_vector(7 downto 0);
	signal cfg_interrupt_mmenable_i    : std_logic_vector(2 downto 0);
	signal cfg_interrupt_msienable_i   : std_logic;

	-- Power management signaling
	signal cfg_turnoff_ok              : std_logic;
	signal cfg_to_turnoff              : std_logic;
	signal cfg_trn_pending             : std_logic;
	signal cfg_pm_wake                 : std_logic;

	-- System configuration and status
	signal cfg_bus_number              : std_logic_vector(7 downto 0);
	signal cfg_device_number           : std_logic_vector(4 downto 0);
	signal cfg_function_number         : std_logic_vector(2 downto 0);
	signal cfg_status                  : std_logic_vector(15 downto 0);
	signal cfg_command                 : std_logic_vector(15 downto 0);
	signal cfg_dstatus                 : std_logic_vector(15 downto 0);
	signal cfg_dcommand                : std_logic_vector(15 downto 0);
	signal cfg_lstatus                 : std_logic_vector(15 downto 0);
	signal cfg_lcommand                : std_logic_vector(15 downto 0);
	signal cfg_pcie_link_state         : std_logic_vector(2 downto 0);

	-- System (SYS) Interface
	signal sys_clk_c                   : std_logic;
	
	signal compl_done_o          : STD_LOGIC;
	
	component s6_pcie_v2_4 is
	generic (
		TL_TX_RAM_RADDR_LATENCY           : integer    := 1;
		TL_TX_RAM_RDATA_LATENCY           : integer    := 3;
		TL_RX_RAM_RADDR_LATENCY           : integer    := 1;
		TL_RX_RAM_RDATA_LATENCY           : integer    := 3;
		TL_RX_RAM_WRITE_LATENCY           : integer    := 1;
		VC0_TX_LASTPACKET                 : integer    := 29;
		VC0_RX_RAM_LIMIT                  : bit_vector := x"FFF";
		VC0_TOTAL_CREDITS_PH              : integer    := 32;
		VC0_TOTAL_CREDITS_PD              : integer    := 467;
		VC0_TOTAL_CREDITS_NPH             : integer    := 8;
		VC0_TOTAL_CREDITS_CH              : integer    := 40;
		VC0_TOTAL_CREDITS_CD              : integer    := 467;
		VC0_CPL_INFINITE                  : boolean    := TRUE;
		BAR0                              : bit_vector := x"FFFFC004";
		BAR1                              : bit_vector := x"FFFFFFFF";
		BAR2                              : bit_vector := x"FFFFC004";
		BAR3                              : bit_vector := x"FFFFFFFF";
		BAR4                              : bit_vector := x"00000000";
		BAR5                              : bit_vector := x"00000000";
		EXPANSION_ROM                     : bit_vector := "0000000000000000000000";
		DISABLE_BAR_FILTERING             : boolean    := FALSE;
		DISABLE_ID_CHECK                  : boolean    := FALSE;
		TL_TFC_DISABLE                    : boolean    := FALSE;
		TL_TX_CHECKS_DISABLE              : boolean    := FALSE;
		USR_CFG                           : boolean    := FALSE;
		USR_EXT_CFG                       : boolean    := FALSE;
		DEV_CAP_MAX_PAYLOAD_SUPPORTED     : integer    := 2;
		CLASS_CODE                        : bit_vector := x"040000";
		CARDBUS_CIS_POINTER               : bit_vector := x"00000000";
		PCIE_CAP_CAPABILITY_VERSION       : bit_vector := x"1";
		PCIE_CAP_DEVICE_PORT_TYPE         : bit_vector := x"0";
		PCIE_CAP_SLOT_IMPLEMENTED         : boolean    := FALSE;
		PCIE_CAP_INT_MSG_NUM              : bit_vector := "00000";
		DEV_CAP_PHANTOM_FUNCTIONS_SUPPORT : integer    := 0;
		DEV_CAP_EXT_TAG_SUPPORTED         : boolean    := FALSE;
		DEV_CAP_ENDPOINT_L0S_LATENCY      : integer    := 7;
		DEV_CAP_ENDPOINT_L1_LATENCY       : integer    := 7;
		SLOT_CAP_ATT_BUTTON_PRESENT       : boolean    := FALSE;
		SLOT_CAP_ATT_INDICATOR_PRESENT    : boolean    := FALSE;
		SLOT_CAP_POWER_INDICATOR_PRESENT  : boolean    := FALSE;
		DEV_CAP_ROLE_BASED_ERROR          : boolean    := TRUE;
		LINK_CAP_ASPM_SUPPORT             : integer    := 3;
		LINK_CAP_L0S_EXIT_LATENCY         : integer    := 7;
		LINK_CAP_L1_EXIT_LATENCY          : integer    := 7;
		LL_ACK_TIMEOUT                    : bit_vector := x"00B7";
		LL_ACK_TIMEOUT_EN                 : boolean    := FALSE;
		LL_REPLAY_TIMEOUT                 : bit_vector := x"00FF";
		LL_REPLAY_TIMEOUT_EN              : boolean    := TRUE;
		MSI_CAP_MULTIMSGCAP               : integer    := 1;
		MSI_CAP_MULTIMSG_EXTENSION        : integer    := 0;
		LINK_STATUS_SLOT_CLOCK_CONFIG     : boolean    := TRUE;
		PLM_AUTO_CONFIG                   : boolean    := FALSE;
		FAST_TRAIN                        : boolean    := FALSE;
		ENABLE_RX_TD_ECRC_TRIM            : boolean    := TRUE;
		DISABLE_SCRAMBLING                : boolean    := FALSE;
		PM_CAP_VERSION                    : integer    := 3;
		PM_CAP_PME_CLOCK                  : boolean    := FALSE;
		PM_CAP_DSI                        : boolean    := FALSE;
		PM_CAP_AUXCURRENT                 : integer    := 0;
		PM_CAP_D1SUPPORT                  : boolean    := TRUE;
		PM_CAP_D2SUPPORT                  : boolean    := TRUE;
		PM_CAP_PMESUPPORT                 : bit_vector := x"0F";
		PM_DATA0                          : bit_vector := x"00";
		PM_DATA_SCALE0                    : bit_vector := x"0";
		PM_DATA1                          : bit_vector := x"00";
		PM_DATA_SCALE1                    : bit_vector := x"0";
		PM_DATA2                          : bit_vector := x"00";
		PM_DATA_SCALE2                    : bit_vector := x"0";
		PM_DATA3                          : bit_vector := x"00";
		PM_DATA_SCALE3                    : bit_vector := x"0";
		PM_DATA4                          : bit_vector := x"00";
		PM_DATA_SCALE4                    : bit_vector := x"0";
		PM_DATA5                          : bit_vector := x"00";
		PM_DATA_SCALE5                    : bit_vector := x"0";
		PM_DATA6                          : bit_vector := x"00";
		PM_DATA_SCALE6                    : bit_vector := x"0";
		PM_DATA7                          : bit_vector := x"00";
		PM_DATA_SCALE7                    : bit_vector := x"0";
		PCIE_GENERIC                      : bit_vector := "000001101111";
		GTP_SEL                           : integer    := 0;
		CFG_VEN_ID                        : std_logic_vector(15 downto 0) := x"1D7C";
		CFG_DEV_ID                        : std_logic_vector(15 downto 0) := x"0100";
		CFG_REV_ID                        : std_logic_vector(7 downto 0)  := x"01";
		CFG_SUBSYS_VEN_ID                 : std_logic_vector(15 downto 0) := x"1D7C";
		CFG_SUBSYS_ID                     : std_logic_vector(15 downto 0) := x"0100";
		REF_CLK_FREQ                      : integer    := 0
	);
	port (
		-- PCI Express Fabric Interface
		pci_exp_txp             : out std_logic;
		pci_exp_txn             : out std_logic;
		pci_exp_rxp             : in  std_logic;
		pci_exp_rxn             : in  std_logic;

		user_lnk_up             : out std_logic;

		-- Tx
		s_axis_tx_tdata         : in  std_logic_vector(31 downto 0);
		s_axis_tx_tlast         : in  std_logic;
		s_axis_tx_tvalid        : in  std_logic;
		s_axis_tx_tready        : out std_logic;
		s_axis_tx_tkeep         : in  std_logic_vector(3 downto 0);
		s_axis_tx_tuser         : in  std_logic_vector(3 downto 0);
		
		tx_err_drop             : out std_logic;
		tx_buf_av               : out std_logic_vector(5 downto 0);
		tx_cfg_req              : out std_logic;
		tx_cfg_gnt              : in  std_logic;

		-- Rx
		m_axis_rx_tdata         : out std_logic_vector(31 downto 0);
		m_axis_rx_tlast         : out std_logic;
		m_axis_rx_tvalid        : out std_logic;
		m_axis_rx_tkeep         : out std_logic_vector(3 downto 0);
		m_axis_rx_tready        : in  std_logic;
		m_axis_rx_tuser         : out std_logic_vector(21 downto 0);
		rx_np_ok                : in  std_logic;

		fc_sel                  : in  std_logic_vector(2 downto 0);
		fc_nph                  : out std_logic_vector(7 downto 0);
		fc_npd                  : out std_logic_vector(11 downto 0);
		fc_ph                   : out std_logic_vector(7 downto 0);
		fc_pd                   : out std_logic_vector(11 downto 0);
		fc_cplh                 : out std_logic_vector(7 downto 0);
		fc_cpld                 : out std_logic_vector(11 downto 0);

		-- Host (CFG) Interface
		cfg_do                  : out std_logic_vector(31 downto 0);
		cfg_rd_wr_done          : out std_logic;
		cfg_dwaddr              : in  std_logic_vector(9 downto 0);
		cfg_rd_en               : in  std_logic;
		cfg_err_ur              : in  std_logic;
		cfg_err_cor             : in  std_logic;
		cfg_err_ecrc            : in  std_logic;
		cfg_err_cpl_timeout     : in  std_logic;
		cfg_err_cpl_abort       : in  std_logic;
		cfg_err_posted          : in  std_logic;
		cfg_err_locked          : in  std_logic;
		cfg_err_tlp_cpl_header  : in  std_logic_vector(47 downto 0);
		cfg_err_cpl_rdy         : out std_logic;
		
		cfg_interrupt           : in  std_logic;
		cfg_interrupt_rdy       : out std_logic;
		cfg_interrupt_assert    : in  std_logic;
		cfg_interrupt_do        : out std_logic_vector(7 downto 0);
		cfg_interrupt_di        : in  std_logic_vector(7 downto 0);
		cfg_interrupt_mmenable  : out std_logic_vector(2 downto 0);
		cfg_interrupt_msienable : out std_logic;
		
		cfg_turnoff_ok          : in  std_logic;
		cfg_to_turnoff          : out std_logic;
		cfg_pm_wake             : in  std_logic;
		cfg_pcie_link_state     : out std_logic_vector(2 downto 0);
		cfg_trn_pending         : in  std_logic;
		cfg_dsn                 : in  std_logic_vector(63 downto 0);
		cfg_bus_number          : out std_logic_vector(7 downto 0);
		cfg_device_number       : out std_logic_vector(4 downto 0);
		cfg_function_number     : out std_logic_vector(2 downto 0);
		cfg_status              : out std_logic_vector(15 downto 0);
		cfg_command             : out std_logic_vector(15 downto 0);
		cfg_dstatus             : out std_logic_vector(15 downto 0);
		cfg_dcommand            : out std_logic_vector(15 downto 0);
		cfg_lstatus             : out std_logic_vector(15 downto 0);
		cfg_lcommand            : out std_logic_vector(15 downto 0);

		-- System Interface
		sys_clk                 : in  std_logic;
		sys_reset               : in  std_logic;
		user_clk_out            : out std_logic;
		user_reset_out          : out std_logic;
		received_hot_reset      : out std_logic
	);
	end component s6_pcie_v2_4;

	constant PCI_EXP_EP_OUI      : std_logic_vector(23 downto 0) := x"000A35";
	constant PCI_EXP_EP_DSN_1    : std_logic_vector(31 downto 0) := x"01" & PCI_EXP_EP_OUI;
	constant PCI_EXP_EP_DSN_2    : std_logic_vector(31 downto 0) := x"00000001";

	signal   cfg_completer_id    : std_logic_vector(15 downto 0);
	signal   cfg_bus_mstr_enable : std_logic;
	
	signal   user_lnk_up_out     : std_logic;
	signal   user_reset_out      : std_logic;
	signal   user_clk_out        : std_logic;
	
	--------------------------------------------------------------------------------- AUX SIGNALS
	signal temp_32 : std_logic_vector (31 downto 0);
	
	signal test_32 : std_logic_vector (31 downto 0) := X"55AAFF00";
	
	signal	  SysDataIn_1		: STD_LOGIC_VECTOR(31 downto 0);
	signal	  SysDataOut_1		: STD_LOGIC_VECTOR(31 downto 0);
	signal	  ReadDev_1			: STD_LOGIC;
	signal	  WriteDev_1		: STD_LOGIC;
	signal	  DevRdy_1			: STD_LOGIC;
	signal	  Cmd_1				: STD_LOGIC_VECTOR(0 downto 0);
	signal	  DevStatus_1		: STD_LOGIC_VECTOR(3 downto 0);
	signal     InpFull_1			: STD_LOGIC;
	signal	  InpEmpty_1		: STD_LOGIC;
   signal     OutFull_1			: STD_LOGIC;
   signal     OutEmpty_1		: STD_LOGIC;
	
	signal     pio_tx_ack_old  : std_logic;
	signal     pio_tx_ack_front: std_logic;
	
begin

pcie_dma_controller_i: pcie_dma_controller
	Port map(
		clk                    => user_clk_out,
		reset                  => user_reset_out,
		
		-- Control signals
		start_address          => start_address,
		pci_dma_ready          => pci_dma_ready,
		dma_cfg_address_set    => dma_cfg_address_set, 
		
		-- Connection with PCIe TX module
		pio_tx_address         => pio_tx_address,
		pio_tx_length          => pio_tx_length,
		pio_tx_start           => pio_tx_start,
		pio_tx_ack             => pio_tx_ack,
		
		dma_cmd                => dma_cmd,
		dma_cmd_rd_en          => dma_cmd_rd_en,
		dma_cmd_empty          => dma_cmd_empty,
		
		dma_data               => dma_data,
		dma_data_rd_en         => dma_data_rd_en,
		dma_data_rd_data_count => dma_data_rd_data_count,
		
		pio_dma_data_dout           => pio_dma_data_dout,
		pio_dma_data_rd_en          => pio_dma_data_rd_en,
		
		debug => debug
	);

pcie_int_controller_i: pcie_int_controller
	Port map (
		clk                     => user_clk_out,
		reset                   => user_reset_out,
		
		-- coneection to PCIe modules
		cfg_interrupt           => cfg_interrupt,
		cfg_interrupt_rdy       => cfg_interrupt_rdy,
		cfg_interrupt_assert    => cfg_interrupt_assert,
		cfg_interrupt_do        => cfg_interrupt_do,
		cfg_interrupt_di        => cfg_interrupt_di,
		cfg_interrupt_mmenable  => cfg_interrupt_mmenable_i,
		cfg_interrupt_msienable => cfg_interrupt_msienable_i,
		
		-- connection for writing
		int_cmd_wr_clk          => int_cmd_wr_clk,
		int_cmd_wr_en           => int_cmd_wr_en,
		int_cmd                 => int_cmd,
		int_cmd_num             => int_cmd_num,
		int_cmd_full            => int_cmd_full,
		
		pci_int_ready           => pci_int_ready
	);

	-- Core input tie-offs
	fc_sel                 <= "000";
	rx_np_ok               <= '1';
	tx_cfg_gnt             <= '1';

	cfg_err_cor            <= '0';
	cfg_err_ur             <= '0';
	cfg_err_ecrc           <= '0';
	cfg_err_cpl_timeout    <= '0';
	cfg_err_cpl_abort      <= '0';
	cfg_err_posted         <= '0';
	cfg_err_locked         <= '0';
	
	cfg_pm_wake            <= '0';
	cfg_trn_pending        <= '0';

	s_axis_tx_tuser_i(0)     <= '0'; -- Unused for S6
	s_axis_tx_tuser_i(1)     <= '0'; -- Error forward packet
	s_axis_tx_tuser_i(2)     <= '0'; -- Stream packet

	cfg_err_tlp_cpl_header <= (OTHERS => '0');
	cfg_dwaddr             <= (OTHERS => '0');
	cfg_rd_en              <= '0';
	cfg_dsn                <= PCI_EXP_EP_DSN_2 & PCI_EXP_EP_DSN_1;

	-- Programmed I/O Module
	cfg_completer_id       <= cfg_bus_number & cfg_device_number & cfg_function_number;
	cfg_bus_mstr_enable    <= cfg_command(2);

	PIO_i : PIO
	port map (
		user_clk            => user_clk_out,            -- I
		user_reset          => user_reset_out,        -- I
		user_lnk_up         => user_lnk_up_out,       -- I

		s_axis_tx_tready    => s_axis_tx_tready ,       -- I
		s_axis_tx_tdata     => s_axis_tx_tdata ,        -- O
		s_axis_tx_tkeep     => s_axis_tx_tkeep ,        -- O
		s_axis_tx_tlast     => s_axis_tx_tlast ,        -- O
		s_axis_tx_tvalid    => s_axis_tx_tvalid ,       -- O
		tx_src_dsc          => s_axis_tx_tuser_i(3) ,     -- O

		m_axis_rx_tdata     => m_axis_rx_tdata ,        -- I
		m_axis_rx_tkeep     => m_axis_rx_tkeep ,        -- I
		m_axis_rx_tlast     => m_axis_rx_tlast ,        -- I
		m_axis_rx_tvalid    => m_axis_rx_tvalid ,       -- I
		m_axis_rx_tready    => m_axis_rx_tready ,       -- O
		m_axis_rx_tuser     => m_axis_rx_tuser ,        -- I

		cfg_to_turnoff      => cfg_to_turnoff,   -- I
		cfg_turnoff_ok      => cfg_turnoff_ok,   -- O

		cfg_completer_id    => cfg_completer_id,   -- I [15:0]
		cfg_bus_mstr_enable => cfg_bus_mstr_enable, -- I

		rd_addr             => rd_addr,
		rd_be               => rd_be,
		rd_data             => rd_data,
		rd_bar              => rd_bar,
		wr_addr             => wr_addr,
		wr_be               => wr_be,
		wr_data             => wr_data,
		wr_en               => wr_en,
		wr_busy             => wr_busy,
		wr_bar              => wr_bar,
		
		pio_tx_address         => pio_tx_address,
		pio_tx_length          => pio_tx_length,
		pio_tx_dout            => pio_dma_data_dout,
		pio_tx_rd_en           => pio_dma_data_rd_en,
		pio_tx_start           => pio_tx_start,
		pio_tx_ack             => pio_tx_ack,
		
		compl_done_o           => compl_done_o
	);
	
s6_pcie_v2_4_i : s6_pcie_v2_4
	generic map (
		FAST_TRAIN                        => FAST_TRAIN
	)
	port map (
		-- PCI Express (PCI_EXP) Fabric Interface
		pci_exp_txp                        => pci_exp_txp,
		pci_exp_txn                        => pci_exp_txn,
		pci_exp_rxp                        => pci_exp_rxp,
		pci_exp_rxn                        => pci_exp_rxn,

		-- Transaction (TRN) Interface
		-- Common clock & reset
		user_lnk_up                        => user_lnk_up_out,
		user_clk_out                       => user_clk_out,
		user_reset_out                     => user_reset_out,
		-- Common flow control
		fc_sel                             => fc_sel,
		fc_nph                             => fc_nph,
		fc_npd                             => fc_npd,
		fc_ph                              => fc_ph,
		fc_pd                              => fc_pd,
		fc_cplh                            => fc_cplh,
		fc_cpld                            => fc_cpld,
		-- Transaction Tx
		s_axis_tx_tready                   => s_axis_tx_tready,
		s_axis_tx_tdata                    => s_axis_tx_tdata,
		s_axis_tx_tkeep                    => s_axis_tx_tkeep,
		s_axis_tx_tuser                    => s_axis_tx_tuser_i,
		s_axis_tx_tlast                    => s_axis_tx_tlast,
		s_axis_tx_tvalid                   => s_axis_tx_tvalid,
		tx_err_drop                        => tx_err_drop,
		tx_buf_av                          => tx_buf_av,
		tx_cfg_req                         => tx_cfg_req,
		tx_cfg_gnt                         => tx_cfg_gnt,
		-- Transaction Rx
		m_axis_rx_tdata                    => m_axis_rx_tdata,
		m_axis_rx_tkeep                    => m_axis_rx_tkeep,
		m_axis_rx_tlast                    => m_axis_rx_tlast,
		m_axis_rx_tvalid                   => m_axis_rx_tvalid,
		m_axis_rx_tready                   => m_axis_rx_tready,
		m_axis_rx_tuser                    => m_axis_rx_tuser,
		rx_np_ok                           => rx_np_ok,

		-- Configuration (CFG) Interface
		-- Configuration space access
		cfg_do                             => cfg_do,
		cfg_rd_wr_done                     => cfg_rd_wr_done,
		cfg_dwaddr                         => cfg_dwaddr,
		cfg_rd_en                          => cfg_rd_en,
		-- Error reporting
		cfg_err_ur                         => cfg_err_ur,
		cfg_err_cor                        => cfg_err_cor,
		cfg_err_ecrc                       => cfg_err_ecrc,
		cfg_err_cpl_timeout                => cfg_err_cpl_timeout,
		cfg_err_cpl_abort                  => cfg_err_cpl_abort,
		cfg_err_posted                     => cfg_err_posted,
		cfg_err_locked                     => cfg_err_locked,
		cfg_err_tlp_cpl_header             => cfg_err_tlp_cpl_header,
		cfg_err_cpl_rdy                    => cfg_err_cpl_rdy,
		-- Interrupt generation
		cfg_interrupt                      => cfg_interrupt,
		cfg_interrupt_rdy                  => cfg_interrupt_rdy,
		cfg_interrupt_assert               => cfg_interrupt_assert,
		cfg_interrupt_do                   => cfg_interrupt_do,
		cfg_interrupt_di                   => cfg_interrupt_di,
		cfg_interrupt_mmenable             => cfg_interrupt_mmenable_i,
		cfg_interrupt_msienable            => cfg_interrupt_msienable_i,
		-- Power management signaling
		cfg_turnoff_ok                     => cfg_turnoff_ok,
		cfg_to_turnoff                     => cfg_to_turnoff,
		cfg_pm_wake                        => cfg_pm_wake,
		cfg_pcie_link_state                => cfg_pcie_link_state,
		cfg_trn_pending                    => cfg_trn_pending,
		-- System configuration and status
		cfg_dsn                            => cfg_dsn,
		cfg_bus_number                     => cfg_bus_number,
		cfg_device_number                  => cfg_device_number,
		cfg_function_number                => cfg_function_number,
		cfg_status                         => cfg_status,
		cfg_command                        => cfg_command,
		cfg_dstatus                        => cfg_dstatus,
		cfg_dcommand                       => cfg_dcommand,
		cfg_lstatus                        => cfg_lstatus,
		cfg_lcommand                       => cfg_lcommand,

		-- System (SYS) Interface
		sys_clk                            => sys_clk_c,
		sys_reset                          => sys_reset,
		
		received_hot_reset                 => OPEN
	);
	
--------------------------------------------------------------------------------- PCIe
	refclk_ibuf : IBUFDS
	port map
	(
		O  => sys_clk_c,
		I  => sys_clk_p,
		IB => sys_clk_n
	);
	
user_reset  <= user_reset_out;
user_lnk_up <= user_lnk_up_out;
user_clk    <= user_clk_out;

enable_dio_output_generate:
if enable_dio_output generate
	OUT_DATA_CLK <= user_clk_out;
end generate;

enable_dio_input_generate:
if enable_dio_input generate
	IN_DIN_CLK <= user_clk_out;
	IN_DOUT_CLK <= user_clk_out;
end generate;

enable_arinc_429_generate:
if enable_arinc_429 generate
	A429_CMD_IN_CLK_1 <= user_clk_out;
	A429_STATUS_OUT_CLK_1 <= user_clk_out;
	A429_ARINC_FIFO_RX_CLK_1 <= user_clk_out;
	A429_ARINC_FIFO_TX_CLK_1 <= user_clk_out;

	A429_CMD_IN_CLK_2 <= user_clk_out;
	A429_STATUS_OUT_CLK_2 <= user_clk_out;
	A429_ARINC_FIFO_RX_CLK_2 <= user_clk_out;
	A429_ARINC_FIFO_TX_CLK_2 <= user_clk_out;

	A429_CMD_IN_CLK_3 <= user_clk_out;
	A429_STATUS_OUT_CLK_3 <= user_clk_out;
	A429_ARINC_FIFO_RX_CLK_3 <= user_clk_out;
	A429_ARINC_FIFO_TX_CLK_3 <= user_clk_out;

	A429_CMD_IN_CLK_4 <= user_clk_out;
	A429_STATUS_OUT_CLK_4 <= user_clk_out;
	A429_ARINC_FIFO_RX_CLK_4 <= user_clk_out;
	A429_ARINC_FIFO_TX_CLK_4 <= user_clk_out;
end generate;

enable_arinc_708_generate:
if enable_arinc_708 generate
	A708_STATE_CLK <= user_clk_out;
	A708_CMD_CLK <= user_clk_out;
	A708_DMA_ADDR_CLK <= user_clk_out;
end generate;

RsSysClk <= user_clk_out;

process (sys_reset, user_clk_out)
begin
	if sys_reset = '1' then
		
	elsif user_clk_out'event and user_clk_out = '1' then
		if enable_dio_output then
			OUT_DATA_EN    <= '0';
		end if;
			
		if enable_dio_input then
			IN_DIN_EN      <= '0';
		end if;
		
		if enable_arinc_429 then
			A429_ARINC_FIFO_RX_READ_1 <= (others => '0');
			A429_ARINC_FIFO_RX_READ_2 <= (others => '0');
			A429_ARINC_FIFO_RX_READ_3 <= (others => '0');
			A429_ARINC_FIFO_RX_READ_4 <= (others => '0');
			
			A429_ARINC_FIFO_TX_WRITE_1 <= (others => '0');
			A429_ARINC_FIFO_TX_WRITE_2 <= (others => '0');
			A429_ARINC_FIFO_TX_WRITE_3 <= (others => '0');
			A429_ARINC_FIFO_TX_WRITE_4 <= (others => '0');
			
			A429_CMD_IN_EN_1 <= '0';
			A429_CMD_IN_EN_2 <= '0';
			A429_CMD_IN_EN_3 <= '0';
			A429_CMD_IN_EN_4 <= '0';
			
			A429_ARINC_FIFO_TX_WRITE_1 <= (others => '0');
			A429_ARINC_FIFO_TX_WRITE_2 <= (others => '0');
			A429_ARINC_FIFO_TX_WRITE_3 <= (others => '0');
			A429_ARINC_FIFO_TX_WRITE_4 <= (others => '0');
		end if;
		
		if enable_arinc_708 then
			A708_CMD_EN <= '0';
		end if;
		
		CmdCfgRd_1 <= '0';
		CmdDataRd_1 <= '0';
		CmdCfgWr_1 <= '0';
		CmdDataWr_1 <= '0';
		
		CmdCfgRd_2 <= '0';
		CmdDataRd_2 <= '0';
		CmdCfgWr_2 <= '0';
		CmdDataWr_2 <= '0';
		
		CmdCfgRd_3 <= '0';
		CmdDataRd_3 <= '0';
		CmdCfgWr_3 <= '0';
		CmdDataWr_3 <= '0';
		
		CmdCfgRd_4 <= '0';
		CmdDataRd_4 <= '0';
		CmdCfgWr_4 <= '0';
		CmdDataWr_4 <= '0';
		
		Wdt_Tick <= '0';
		
	   CmdAdcRead		<= '0';
	   CmdAdcCfg		<= '0';
	   RawAdcRead		<= '0';
	   RawAdcWrite		<= '0';		

		if ReadDev_1 = '1' then ReadDev_1 <= '0'; end if;
		if WriteDev_1 = '1' then WriteDev_1 <= '0'; end if;
		
--		pio_tx_ack_old <= pio_tx_ack;
--		if pio_tx_ack_old = '0' and pio_tx_ack = '1' then
--			pio_tx_ack_front <= '1';
--		else
--			pio_tx_ack_front <= '0';
--		end if;
		
		pio_tx_ack_front <= pio_tx_ack;
		
		if rd_bar = "00" then
		
			-- pio_tx_ack_front - one impulse following read success
		
			case (rd_addr(15 downto 0)) is
				when X"0000" =>
					rd_data <= std_logic_vector(to_unsigned(FGPA_MAJOR, 8)) &
					           std_logic_vector(to_unsigned(FGPA_MINOR, 8)) &
								  std_logic_vector(to_unsigned(FGPA_BUILD, 16));
								  
				when X"0001" =>
					rd_data <= X"F57E519F";
					
				when X"0008" =>	
					rd_data <= test_32;
					
				when X"0010" =>
					rd_data <= SysDataOut_1;
					ReadDev_1 <= '1';
					
--				when X"0012" =>
--					rd_data <= X"00000" &
--								InpFull_1 & InpEmpty_1 & OutFull_1 & OutEmpty_1 & "0"  &
--								A429_AINT_1 & A429_MINT_1 & A429_READY_1 & DevStatus_1;
				
				when X"0020" =>
					rd_data(8 downto 0) <= OUT_READY & X"0" & (not OUT_FAULT_4) & (not OUT_FAULT_3) & (not OUT_FAULT_2) & (not OUT_FAULT_1);
					
				when X"0021" =>
					rd_data <= IN_DOUT_STATE;
				
				when X"0022" =>
					rd_data <= IN_DOUT;
				
				when X"0024" =>
					rd_data <= A429_STATUS_OUT_1;
					
				when X"0025" =>
					rd_data(7 downto 0) <= A429_ARINC_FIFO_RX_EMPTY_1;
					rd_data(11 downto 8) <= not A429_ARINC_FIFO_TX_FULL_1;
					rd_data(31 downto 12) <= (others => '0');
				
				when X"0026" =>
					if A429_ARINC_FIFO_RX_EMPTY_1(0) = '0' then
						rd_data <= A429_ARINC_FIFO_RX_0_1;
						if compl_done_o = '1' then
							A429_ARINC_FIFO_RX_READ_1(0) <= '1';
						end if;
					else
						rd_data <= (others => '1');
					end if;
				when X"0027" =>
					if A429_ARINC_FIFO_RX_EMPTY_1(1) = '0' then
						rd_data <= A429_ARINC_FIFO_RX_1_1;
						if compl_done_o = '1' then
							A429_ARINC_FIFO_RX_READ_1(1) <= '1';
						end if;
					else
						rd_data <= (others => '1');
					end if;
				when X"0028" =>
					if A429_ARINC_FIFO_RX_EMPTY_1(2) = '0' then
						rd_data <= A429_ARINC_FIFO_RX_2_1;
						if compl_done_o = '1' then
							A429_ARINC_FIFO_RX_READ_1(2) <= '1';
						end if;
					else
						rd_data <= (others => '1');
					end if;
				when X"0029" =>
					if A429_ARINC_FIFO_RX_EMPTY_1(3) = '0' then
						rd_data <= A429_ARINC_FIFO_RX_3_1;
						if compl_done_o = '1' then
							A429_ARINC_FIFO_RX_READ_1(3) <= '1';
						end if;
					else
						rd_data <= (others => '1');
					end if;
				when X"002A" =>
					if A429_ARINC_FIFO_RX_EMPTY_1(4) = '0' then
						rd_data <= A429_ARINC_FIFO_RX_4_1;
						if compl_done_o = '1' then
							A429_ARINC_FIFO_RX_READ_1(4) <= '1';
						end if;
					else
						rd_data <= (others => '1');
					end if;
				when X"002B" =>
					if A429_ARINC_FIFO_RX_EMPTY_1(5) = '0' then
						rd_data <= A429_ARINC_FIFO_RX_5_1;
						if compl_done_o = '1' then
							A429_ARINC_FIFO_RX_READ_1(5) <= '1';
						end if;
					else
						rd_data <= (others => '1');
					end if;
				when X"002C" =>
					if A429_ARINC_FIFO_RX_EMPTY_1(6) = '0' then
						rd_data <= A429_ARINC_FIFO_RX_6_1;
						if compl_done_o = '1' then
							A429_ARINC_FIFO_RX_READ_1(6) <= '1';
						end if;
					else
						rd_data <= (others => '1');
					end if;
				when X"002D" =>
					if A429_ARINC_FIFO_RX_EMPTY_1(7) = '0' then
						rd_data <= A429_ARINC_FIFO_RX_7_1;
						if compl_done_o = '1' then
							A429_ARINC_FIFO_RX_READ_1(7) <= '1';
						end if;
					else
						rd_data <= (others => '1');
					end if;
					
				when X"0030" =>
					rd_data <= A429_STATUS_OUT_2;
					
				when X"0031" =>
					rd_data(7 downto 0) <= A429_ARINC_FIFO_RX_EMPTY_2;
					rd_data(11 downto 8) <= not A429_ARINC_FIFO_TX_FULL_2;
				
				when X"0032" =>
					if A429_ARINC_FIFO_RX_EMPTY_2(0) = '0' then
						rd_data <= A429_ARINC_FIFO_RX_0_2;
						if compl_done_o = '1' then
							A429_ARINC_FIFO_RX_READ_2(0) <= '1';
						end if;
					else
						rd_data <= (others => '1');
					end if;
				when X"0033" =>
					if A429_ARINC_FIFO_RX_EMPTY_2(1) = '0' then
						rd_data <= A429_ARINC_FIFO_RX_1_2;
						if compl_done_o = '1' then
							A429_ARINC_FIFO_RX_READ_2(1) <= '1';
						end if;
					else
						rd_data <= (others => '1');
					end if;
				when X"0034" =>
					if A429_ARINC_FIFO_RX_EMPTY_2(2) = '0' then
						rd_data <= A429_ARINC_FIFO_RX_2_2;
						if compl_done_o = '1' then
							A429_ARINC_FIFO_RX_READ_2(2) <= '1';
						end if;
					else
						rd_data <= (others => '1');
					end if;
				when X"0035" =>
					if A429_ARINC_FIFO_RX_EMPTY_2(3) = '0' then
						rd_data <= A429_ARINC_FIFO_RX_3_2;
						if compl_done_o = '1' then
							A429_ARINC_FIFO_RX_READ_2(3) <= '1';
						end if;
					else
						rd_data <= (others => '1');
					end if;
				when X"0036" =>
					if A429_ARINC_FIFO_RX_EMPTY_2(4) = '0' then
						rd_data <= A429_ARINC_FIFO_RX_4_2;
						if compl_done_o = '1' then
							A429_ARINC_FIFO_RX_READ_2(4) <= '1';
						end if;
					else
						rd_data <= (others => '1');
					end if;
				when X"0037" =>
					if A429_ARINC_FIFO_RX_EMPTY_2(5) = '0' then
						rd_data <= A429_ARINC_FIFO_RX_5_2;
						if compl_done_o = '1' then
							A429_ARINC_FIFO_RX_READ_2(5) <= '1';
						end if;
					else
						rd_data <= (others => '1');
					end if;
				when X"0038" =>
					if A429_ARINC_FIFO_RX_EMPTY_2(6) = '0' then
						rd_data <= A429_ARINC_FIFO_RX_6_2;
						if compl_done_o = '1' then
							A429_ARINC_FIFO_RX_READ_2(6) <= '1';
						end if;
					else
						rd_data <= (others => '1');
					end if;
				when X"0039" =>
					if A429_ARINC_FIFO_RX_EMPTY_2(7) = '0' then
						rd_data <= A429_ARINC_FIFO_RX_7_2;
						if compl_done_o = '1' then
							A429_ARINC_FIFO_RX_READ_2(7) <= '1';
						end if;
					else
						rd_data <= (others => '1');
					end if;
				
				when X"003C" =>
					rd_data <= A429_STATUS_OUT_3;
					
				when X"003D" =>
					rd_data(7 downto 0) <= A429_ARINC_FIFO_RX_EMPTY_3;
					rd_data(11 downto 8) <= not A429_ARINC_FIFO_TX_FULL_3;
				
				when X"003E" =>
					if A429_ARINC_FIFO_RX_EMPTY_3(0) = '0' then
						rd_data <= A429_ARINC_FIFO_RX_0_3;
						if compl_done_o = '1' then
							A429_ARINC_FIFO_RX_READ_3(0) <= '1';
						end if;
					else
						rd_data <= (others => '1');
					end if;
				when X"003F" =>
					if A429_ARINC_FIFO_RX_EMPTY_3(1) = '0' then
						rd_data <= A429_ARINC_FIFO_RX_1_3;
						if compl_done_o = '1' then
							A429_ARINC_FIFO_RX_READ_3(1) <= '1';
						end if;
					else
						rd_data <= (others => '1');
					end if;
				when X"0040" =>
					if A429_ARINC_FIFO_RX_EMPTY_3(2) = '0' then
						rd_data <= A429_ARINC_FIFO_RX_2_3;
						if compl_done_o = '1' then
							A429_ARINC_FIFO_RX_READ_3(2) <= '1';
						end if;
					else
						rd_data <= (others => '1');
					end if;
				when X"0041" =>
					if A429_ARINC_FIFO_RX_EMPTY_3(3) = '0' then
						rd_data <= A429_ARINC_FIFO_RX_3_3;
						if compl_done_o = '1' then
							A429_ARINC_FIFO_RX_READ_3(3) <= '1';
						end if;
					else
						rd_data <= (others => '1');
					end if;
				when X"0042" =>
					if A429_ARINC_FIFO_RX_EMPTY_3(4) = '0' then
						rd_data <= A429_ARINC_FIFO_RX_4_3;
						if compl_done_o = '1' then
							A429_ARINC_FIFO_RX_READ_3(4) <= '1';
						end if;
					else
						rd_data <= (others => '1');
					end if;
				when X"0043" =>
					if A429_ARINC_FIFO_RX_EMPTY_3(5) = '0' then
						rd_data <= A429_ARINC_FIFO_RX_5_3;
						if compl_done_o = '1' then
							A429_ARINC_FIFO_RX_READ_3(5) <= '1';
						end if;
					else
						rd_data <= (others => '1');
					end if;
				when X"0044" =>
					if A429_ARINC_FIFO_RX_EMPTY_3(6) = '0' then
						rd_data <= A429_ARINC_FIFO_RX_6_3;
						if compl_done_o = '1' then
							A429_ARINC_FIFO_RX_READ_3(6) <= '1';
						end if;
					else
						rd_data <= (others => '1');
					end if;
				when X"0045" =>
					if A429_ARINC_FIFO_RX_EMPTY_3(7) = '0' then
						rd_data <= A429_ARINC_FIFO_RX_7_3;
						if compl_done_o = '1' then
							A429_ARINC_FIFO_RX_READ_3(7) <= '1';
						end if;
					else
						rd_data <= (others => '1');
					end if;
					
				when X"0048" =>
					rd_data <= A429_STATUS_OUT_4;
					
				when X"0049" =>
					rd_data(7 downto 0) <= A429_ARINC_FIFO_RX_EMPTY_4;
					rd_data(11 downto 8) <= not A429_ARINC_FIFO_TX_FULL_4;
				
				when X"004A" =>
					if A429_ARINC_FIFO_RX_EMPTY_4(0) = '0' then
						rd_data <= A429_ARINC_FIFO_RX_0_4;
						if compl_done_o = '1' then
							A429_ARINC_FIFO_RX_READ_4(0) <= '1';
						end if;
					else
						rd_data <= (others => '1');
					end if;
				when X"004B" =>
					if A429_ARINC_FIFO_RX_EMPTY_4(1) = '0' then
						rd_data <= A429_ARINC_FIFO_RX_1_4;
						if compl_done_o = '1' then
							A429_ARINC_FIFO_RX_READ_4(1) <= '1';
						end if;
					else
						rd_data <= (others => '1');
					end if;
				when X"004C" =>
					if A429_ARINC_FIFO_RX_EMPTY_4(2) = '0' then
						rd_data <= A429_ARINC_FIFO_RX_2_4;
						if compl_done_o = '1' then
							A429_ARINC_FIFO_RX_READ_4(2) <= '1';
						end if;
					else
						rd_data <= (others => '1');
					end if;
				when X"004D" =>
					if A429_ARINC_FIFO_RX_EMPTY_4(3) = '0' then
						rd_data <= A429_ARINC_FIFO_RX_3_4;
						if compl_done_o = '1' then
							A429_ARINC_FIFO_RX_READ_4(3) <= '1';
						end if;
					else
						rd_data <= (others => '1');
					end if;
				when X"004E" =>
					if A429_ARINC_FIFO_RX_EMPTY_4(4) = '0' then
						rd_data <= A429_ARINC_FIFO_RX_4_4;
						if compl_done_o = '1' then
							A429_ARINC_FIFO_RX_READ_4(4) <= '1';
						end if;
					else
						rd_data <= (others => '1');
					end if;
				when X"004F" =>
					if A429_ARINC_FIFO_RX_EMPTY_4(5) = '0' then
						rd_data <= A429_ARINC_FIFO_RX_5_4;
						if compl_done_o = '1' then
							A429_ARINC_FIFO_RX_READ_4(5) <= '1';
						end if;
					else
						rd_data <= (others => '1');
					end if;
				when X"0050" =>
					if A429_ARINC_FIFO_RX_EMPTY_4(6) = '0' then
						rd_data <= A429_ARINC_FIFO_RX_6_4;
						if compl_done_o = '1' then
							A429_ARINC_FIFO_RX_READ_4(6) <= '1';
						end if;
					else
						rd_data <= (others => '1');
					end if;
				when X"0051" =>
					if A429_ARINC_FIFO_RX_EMPTY_4(7) = '0' then
						rd_data <= A429_ARINC_FIFO_RX_7_4;
						if compl_done_o = '1' then
							A429_ARINC_FIFO_RX_READ_4(7) <= '1';
						end if;
					else
						rd_data <= (others => '1');
					end if;
				
				when X"0056" =>
					rd_data <= A708_STATE;

				------------------------------SERIAL_PORT_1
				when X"0060" =>
					rd_data <= PortOut_1;
					if compl_done_o = '1' then
						CmdDataRd_1<= '1';
					end if;
				when X"0061" =>
					rd_data <= CfgOut_1;
					if compl_done_o = '1' then
						CmdCfgRd_1<= '1';
					end if;
				
				------------------------------SERIAL_PORT_2
				when X"0062" =>
					rd_data <= PortOut_2;
					if compl_done_o = '1' then
						CmdDataRd_2<= '1';
					end if;
				when X"0063" =>
					rd_data <= CfgOut_2;
					if compl_done_o = '1' then
						CmdCfgRd_2<= '1';
					end if;

				------------------------------SERIAL_PORT_3
				when X"0064" =>
					rd_data <= PortOut_3;
					if compl_done_o = '1' then
						CmdDataRd_3<= '1';
					end if;
				when X"0065" =>
					rd_data <= CfgOut_3;
					if compl_done_o = '1' then
						CmdCfgRd_3<= '1';
					end if;

				------------------------------SERIAL_PORT_4
				when X"0066" =>
					rd_data <= PortOut_4;
					if compl_done_o = '1' then
						CmdDataRd_4<= '1';
					end if;
				when X"0067" =>
					rd_data <= CfgOut_4;
					if compl_done_o = '1' then
						CmdCfgRd_4<= '1';
					end if;
				
				-----------------------------------DIMM_SPI
				when X"0068" =>
					rd_data <= DIMM_ADC_CFG;
					if compl_done_o = '1' then
						CmdAdcRead <= '1';
					end if;
					
				when X"0069" =>
					rd_data <= DIMM_ADC_CFG;
					if compl_done_o = '1' then
						RawAdcRead <= '1';
					end if;
					

				when others =>
					rd_data <= (others => '0');
			end case;

			if wr_en = '1' then
				case (wr_addr(15 downto 0)) is
						
					when X"0008" =>	
						test_32 <= wr_data;
					
					when X"0010" =>
						SysDataIn_1 <= wr_data;
						Cmd_1 <= "1";
						WriteDev_1 <= '1';

					when X"0011" =>
						SysDataIn_1 <= wr_data;
						Cmd_1 <= "0";
						WriteDev_1 <= '1';
					
--					when X"0012" =>
--						A429_AACK_1		<= wr_data(4);
--						A429_MINTACK_1	<= wr_data(3);
--						A429_RESET_1	<= wr_data(2);
--						A429_TXMSK_1	<= wr_data(1);
--						A429_RUN_1 		<= wr_data(0);
						
					when X"0020" =>
						OUT_DATA       <= wr_data(27 downto 24) & wr_data(19 downto 16) & wr_data(15 downto 0);
						OUT_DATA_EN    <= '1';
						
					when X"0021" =>
						IN_DIN        <= wr_data;
						IN_DIN_EN    <= '1';
						
					when X"0024" =>
						A429_CMD_IN_1 <= wr_data;
						A429_CMD_IN_EN_1 <= '1';
					
					when X"0026" =>
						A429_ARINC_FIFO_TX_0_1 <= wr_data;
						A429_ARINC_FIFO_TX_WRITE_1(0) <= '1';
					when X"0027" =>
						A429_ARINC_FIFO_TX_1_1 <= wr_data;
						A429_ARINC_FIFO_TX_WRITE_1(1) <= '1';
					when X"0028" =>
						A429_ARINC_FIFO_TX_2_1 <= wr_data;
						A429_ARINC_FIFO_TX_WRITE_1(2) <= '1';
					when X"0029" =>
						A429_ARINC_FIFO_TX_3_1 <= wr_data;
						A429_ARINC_FIFO_TX_WRITE_1(3) <= '1';
					
					when X"0030" =>
						A429_CMD_IN_2 <= wr_data;
						A429_CMD_IN_EN_2 <= '1';
					
					when X"0032" =>
						A429_ARINC_FIFO_TX_0_2 <= wr_data;
						A429_ARINC_FIFO_TX_WRITE_2(0) <= '1';
					when X"0033" =>
						A429_ARINC_FIFO_TX_1_2 <= wr_data;
						A429_ARINC_FIFO_TX_WRITE_2(1) <= '1';
					when X"0034" =>
						A429_ARINC_FIFO_TX_2_2 <= wr_data;
						A429_ARINC_FIFO_TX_WRITE_2(2) <= '1';
					when X"0035" =>
						A429_ARINC_FIFO_TX_3_2 <= wr_data;
						A429_ARINC_FIFO_TX_WRITE_2(3) <= '1';
						
					when X"003C" =>
						A429_CMD_IN_3 <= wr_data;
						A429_CMD_IN_EN_3 <= '1';
					
					when X"003E" =>
						A429_ARINC_FIFO_TX_0_3 <= wr_data;
						A429_ARINC_FIFO_TX_WRITE_3(0) <= '1';
					when X"003F" =>
						A429_ARINC_FIFO_TX_1_3 <= wr_data;
						A429_ARINC_FIFO_TX_WRITE_3(1) <= '1';
					when X"0040" =>
						A429_ARINC_FIFO_TX_2_3 <= wr_data;
						A429_ARINC_FIFO_TX_WRITE_3(2) <= '1';
					when X"0041" =>
						A429_ARINC_FIFO_TX_3_3 <= wr_data;
						A429_ARINC_FIFO_TX_WRITE_3(3) <= '1';
						
					when X"0048" =>
						A429_CMD_IN_4 <= wr_data;
						A429_CMD_IN_EN_4 <= '1';
					
					when X"004A" =>
						A429_ARINC_FIFO_TX_0_4 <= wr_data;
						A429_ARINC_FIFO_TX_WRITE_4(0) <= '1';
					when X"004B" =>
						A429_ARINC_FIFO_TX_1_4 <= wr_data;
						A429_ARINC_FIFO_TX_WRITE_4(1) <= '1';
					when X"004C" =>
						A429_ARINC_FIFO_TX_2_4 <= wr_data;
						A429_ARINC_FIFO_TX_WRITE_4(2) <= '1';
					when X"004D" =>
						A429_ARINC_FIFO_TX_3_4 <= wr_data;
						A429_ARINC_FIFO_TX_WRITE_4(3) <= '1';
						
					----------------------------------------------- A708
					when X"0056" =>
						A708_CMD <= wr_data;
						A708_CMD_EN <= '1';
					when X"0057" =>
						A708_DMA_ADDR(31 downto 0) <= wr_data;
					when X"0058" =>
						A708_DMA_ADDR(63 downto 32) <= wr_data;

					--------------------------------------SERIAL_PORT_1
					when X"0060" =>
						PortIn_1 <= wr_data;
						CmdDataWr_1 <= '1';
					when X"0061" =>
						CfgIn_1 <= wr_data;
						CmdCfgWr_1 <= '1';				
		
					--------------------------------------SERIAL_PORT_2
					when X"0062" =>
						PortIn_2 <= wr_data;
						CmdDataWr_2 <= '1';
					when X"0063" =>
						CfgIn_2 <= wr_data;
						CmdCfgWr_2 <= '1';				
		
					--------------------------------------SERIAL_PORT_3
					when X"0064" =>
						PortIn_3 <= wr_data;
						CmdDataWr_3 <= '1';
					when X"0065" =>
						CfgIn_3 <= wr_data;
						CmdCfgWr_3 <= '1';				
		
					--------------------------------------SERIAL_PORT_4
					when X"0066" =>
						PortIn_4 <= wr_data;
						CmdDataWr_4 <= '1';
					when X"0067" =>
						CfgIn_4 <= wr_data;
						CmdCfgWr_4 <= '1';
						
					-------------------------------------------DIMM_SPI
					when X"0068" =>
						DIMM_ADC_DATA <= wr_data;
						CmdAdcCfg <= '1';
					when X"0069" =>
						DIMM_ADC_DATA <= wr_data;
						RawAdcWrite <= '1';
						
					-------------------------------------WATCHDOG PULSE
					when	X"0100" =>
						Wdt_Tick <= '1';
		
					when others =>
				end case;
			end if;
		end if;
	end if;
end process;

-- output interrupt capability
cfg_interrupt_mmenable  <= cfg_interrupt_mmenable_i;
cfg_interrupt_msienable <= cfg_interrupt_msienable_i;

end rtl;
