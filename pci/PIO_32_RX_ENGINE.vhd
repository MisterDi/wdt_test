-------------------------------------------------------------------------------
-- Project    : Spartan-6 Integrated Block for PCI Express
-- File       : PIO_32_RX_ENGINE.vhd
-- Description: 32-bit Local-Link Receive Unit.
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

entity PIO_32_RX_ENGINE is
port (
	clk                   : in  std_logic;
	rst_n                 : in  std_logic;

	--
	-- Receive AXI-S interface from PCIe core
	--
	m_axis_rx_tdata       : in  std_logic_vector(31 downto 0);
	m_axis_rx_tkeep       : in  std_logic_vector(3 downto 0);
	m_axis_rx_tlast       : in  std_logic;
	m_axis_rx_tvalid      : in  std_logic;
	m_axis_rx_tready      : out std_logic;
	m_axis_rx_tuser       : in  std_logic_vector(21 downto 0);

	--
	-- Memory Read data handshake with Completion
	-- transmit unit. Transmit unit reponds to
	-- req_compl assertion and responds with compl_done
	-- assertion when a Completion w/ data is transmitted.
	--
	req_compl_o           : out std_logic;
	req_compl_with_data_o : out std_logic;
	compl_done_i          : in  std_logic;

	req_tc_o              : out std_logic_vector(2 downto 0);  -- Memory Read TC
	req_td_o              : out std_logic;                     -- Memory Read TD
	req_ep_o              : out std_logic;                     -- Memory Read EP
	req_attr_o            : out std_logic_vector(1 downto 0);  -- Memory Read Attribute
	req_len_o             : out std_logic_vector(9 downto 0);  -- Memory Read Length (1DW)
	req_rid_o             : out std_logic_vector(15 downto 0); -- Memory Read Requestor ID
	req_tag_o             : out std_logic_vector(7 downto 0);  -- Memory Read Tag
	req_be_o              : out std_logic_vector(7 downto 0);  -- Memory Read Byte Enables
	req_addr_o            : out std_logic_vector(29 downto 0); -- Memory Read Address
	req_bar_o             : out std_logic_vector(1 downto 0);  -- bar ID

	--
	-- Memory interface used to save 1 DW data received
	-- on Memory Write 32 TLP. Data extracted from
	-- inbound TLP is presented to the Endpoint memory
	-- unit. Endpoint memory unit reacts to wr_en_o
	-- assertion and asserts wr_busy_i when it is
	-- processing written information.
	--
	wr_addr_o             : out std_logic_vector(29 downto 0); -- Memory Write Address
	wr_be_o               : out std_logic_vector(7 downto 0);  -- Memory Write Byte Enable
	wr_data_o             : out std_logic_vector(31 downto 0); -- Memory Write Data
	wr_en_o               : out std_logic;                     -- Memory Write Enable
	wr_busy_i             : in  std_logic;                      -- Memory Write Busy
	wr_bar_o              : out std_logic_vector(1 downto 0);  -- bar ID
	
	tx_busy               : in  std_logic;
	rx_busy               : out std_logic
);
end PIO_32_RX_ENGINE;

architecture rtl of PIO_32_RX_ENGINE is

	-- Clock-to-out delay
	constant TCQ : time := 1 ns;

	-- TLP Header format/type values
	constant PIO_32_RX_MEM_RD32_FMT_TYPE   : std_logic_vector(6 downto 0) := "0000000";
	constant PIO_32_RX_MEM_WR32_FMT_TYPE   : std_logic_vector(6 downto 0) := "1000000";
	constant PIO_32_RX_MEM_RD64_FMT_TYPE   : std_logic_vector(6 downto 0) := "0100000";
	constant PIO_32_RX_MEM_WR64_FMT_TYPE   : std_logic_vector(6 downto 0) := "1100000";
	constant PIO_32_RX_IO_RD32_FMT_TYPE    : std_logic_vector(6 downto 0) := "0000010";
	constant PIO_32_RX_IO_WR32_FMT_TYPE    : std_logic_vector(6 downto 0) := "1000010";

	constant PIO_32_RX_MEM_CPL_FMT_TYPE  : std_logic_vector(6 downto 0) := "0001010";
   constant PIO_32_RX_MEM_CPLD_FMT_TYPE : std_logic_vector(6 downto 0) := "1001010";

	-- States
	type state_type is (
		PIO_32_RX_RST_STATE,
		
		PIO_32_RX_MEM_RD32_DW1,
		PIO_32_RX_MEM_RD32_DW2,
		
		PIO_32_RX_IO_MEM_WR32_DW1,
		PIO_32_RX_IO_MEM_WR32_DW2,
		PIO_32_RX_IO_MEM_WR32_DW3,
		PIO_32_RX_IO_MEM_WR32_DW4,
		
		PIO_32_RX_MEM_RD64_DW1,
		PIO_32_RX_MEM_RD64_DW2,
		PIO_32_RX_MEM_RD64_DW3,
		
		PIO_32_RX_MEM_WR64_DW1,
		PIO_32_RX_MEM_WR64_DW2,
		PIO_32_RX_MEM_WR64_DW3,
		PIO_32_RX_MEM_WR64_DW4,
		
		PIO_32_RX_MEM_CPL_DW1,
		PIO_32_RX_MEM_CPL_DW2,
		
		PIO_32_RX_WAIT_STATE
	);

	signal state              : state_type;
	signal tlp_type           : std_logic_vector(6 downto 0);

	signal m_axis_rx_tready_int : std_logic;

	signal bar_select         : std_logic_vector(1 downto 0);
	signal bar_mux            : std_logic_vector(3 downto 0);

	signal in_packet_q        : std_logic;
	signal sop                : std_logic;

begin

	m_axis_rx_tready <= m_axis_rx_tready_int and (not tx_busy);

	-- Create logic to determine when a packet starts
	sop <= not(in_packet_q) and m_axis_rx_tvalid;

process begin
	wait until rising_edge(clk);
	if (rst_n = '0') then
		in_packet_q    <= '0' after TCQ;
	elsif (m_axis_rx_tvalid = '1' and m_axis_rx_tready_int = '1' and m_axis_rx_tlast = '1') then
		in_packet_q    <= '0' after TCQ;
	elsif (sop = '1' and m_axis_rx_tready_int = '1') then
		in_packet_q    <= '1' after TCQ;
	end if;
end process;


-- RX receiver state machine and logic
process begin
	wait until rising_edge(clk);
	if (rst_n = '0') then
		m_axis_rx_tready_int  <= '0' after TCQ;

		req_compl_o           <= '0' after TCQ;
		req_compl_with_data_o <= '1' after TCQ;

		req_tc_o              <= (others => '0') after TCQ;
		req_td_o              <= '0' after TCQ;
		req_ep_o              <= '0' after TCQ;
		req_attr_o            <= (others => '0') after TCQ;
		req_len_o             <= (others => '0') after TCQ;
		req_rid_o             <= (others => '0') after TCQ;
		req_tag_o             <= (others => '0') after TCQ;
		req_be_o              <= (others => '0') after TCQ;
		req_addr_o            <= (others => '0') after TCQ;

		wr_be_o               <= (others => '0') after TCQ;
		wr_addr_o             <= (others => '0') after TCQ;
		wr_data_o             <= (others => '0') after TCQ;
		wr_en_o               <= '0' after TCQ;
		wr_bar_o              <= (others => '0') after TCQ;

		state                 <= PIO_32_RX_RST_STATE after TCQ;
		tlp_type              <= (others => '0') after TCQ;
	else
		wr_en_o               <= '0' after TCQ;
		req_compl_o           <= '0' after TCQ;
		req_compl_with_data_o <= '1' after TCQ;

		case (state) is
			when PIO_32_RX_RST_STATE =>
				m_axis_rx_tready_int <= '1' after TCQ;
				tlp_type             <= m_axis_rx_tdata(30 downto 24) after TCQ;
				req_tc_o             <= m_axis_rx_tdata(22 downto 20) after TCQ;
				req_td_o             <= m_axis_rx_tdata(15) after TCQ;
				req_ep_o             <= m_axis_rx_tdata(14) after TCQ;
				req_attr_o           <= m_axis_rx_tdata(13 downto 12) after TCQ;
				req_len_o            <= m_axis_rx_tdata(9 downto 0) after TCQ;
				
				rx_busy              <= m_axis_rx_tvalid;
				
				if ((sop = '1') and (m_axis_rx_tvalid = '1') and (m_axis_rx_tready_int = '1')) then
					case (m_axis_rx_tdata(30 downto 24)) is
						when PIO_32_RX_MEM_RD32_FMT_TYPE => -- HANGS!!!!!!
							if (m_axis_rx_tdata(9 downto 0) = "0000000001") then
								rx_busy      <= '1';
								state        <= PIO_32_RX_MEM_RD32_DW1 after TCQ;
							else
								state        <= PIO_32_RX_RST_STATE after TCQ;
							end if;

						when PIO_32_RX_MEM_WR32_FMT_TYPE =>
							if (m_axis_rx_tdata(9 downto 0) = "0000000001") then
								rx_busy      <= '1';
								state        <= PIO_32_RX_IO_MEM_WR32_DW1 after TCQ;
							else
								state        <= PIO_32_RX_RST_STATE after TCQ;
							end if;

						when PIO_32_RX_MEM_RD64_FMT_TYPE =>
							if (m_axis_rx_tdata(9 downto 0) = "0000000001") then
								rx_busy      <= '1';
								state        <= PIO_32_RX_MEM_RD64_DW1 after TCQ;
							else
								state        <= PIO_32_RX_RST_STATE after TCQ;
							end if;

						when PIO_32_RX_MEM_WR64_FMT_TYPE =>
							if (m_axis_rx_tdata(9 downto 0) = "0000000001") then
								rx_busy      <= '1';
								state        <= PIO_32_RX_MEM_WR64_DW1 after TCQ;
							else
								state        <= PIO_32_RX_RST_STATE after TCQ;
							end if;

						when PIO_32_RX_IO_RD32_FMT_TYPE =>
							if (m_axis_rx_tdata(9 downto 0) = "0000000001") then
								rx_busy      <= '1';
								state        <= PIO_32_RX_MEM_RD32_DW1 after TCQ;
							else
								state        <= PIO_32_RX_RST_STATE after TCQ;
							end if;

						when PIO_32_RX_IO_WR32_FMT_TYPE =>
							if (m_axis_rx_tdata(9 downto 0) = "0000000001") then
								rx_busy      <= '1';
								state        <= PIO_32_RX_IO_MEM_WR32_DW1 after TCQ;
							else
								state        <= PIO_32_RX_RST_STATE after TCQ;
							end if;
							
						-- MEM WRITE COMPLETION (USUALLY OUR DMA WRITE)
--						when PIO_32_RX_MEM_CPL_FMT_TYPE =>
--								state        <= PIO_32_RX_MEM_CPL_DW1 after TCQ;
						when others => -- other TLPs
								state        <= PIO_32_RX_RST_STATE after TCQ;
					end case; -- m_axis_rx_tdata(30 downto 24)
				else -- ((trn_rsof_n = '0') and (trn_rsrc_rdy_n = '0') and (trn_rdst_rdy_n_int = '0'))
					state <= PIO_32_RX_RST_STATE after TCQ;
				end if;
			-- end of PIO_32_RX_RST_STATE

			when PIO_32_RX_MEM_RD32_DW1 =>
				if ((m_axis_rx_tvalid = '1') and (m_axis_rx_tready_int ='1')) then
					req_rid_o    <= m_axis_rx_tdata(31 downto 16) after TCQ;
					req_tag_o    <= m_axis_rx_tdata(15 downto 8) after TCQ;
					req_be_o     <= m_axis_rx_tdata(7 downto 0) after TCQ;
					state        <= PIO_32_RX_MEM_RD32_DW2 after TCQ;
				else
					state        <= PIO_32_RX_MEM_RD32_DW1 after TCQ;
				end if;
			-- end of PIO_32_RX_MEM_RD32_DW1

			when PIO_32_RX_MEM_RD32_DW2 =>
				if ((m_axis_rx_tvalid = '1') and (m_axis_rx_tready_int = '1')) then
					req_addr_o           <= m_axis_rx_tdata(31 downto 2) after TCQ;
					req_bar_o            <= bar_select;
					req_compl_o          <= '1' after TCQ;
					m_axis_rx_tready_int <= '0' after TCQ;
					state                <= PIO_32_RX_WAIT_STATE after TCQ;
				else
					state                <= PIO_32_RX_MEM_RD32_DW2 after TCQ;
				end if;
			-- end of PIO_32_RX_MEM_RD32_DW2

			when PIO_32_RX_IO_MEM_WR32_DW1 =>
				if ((m_axis_rx_tvalid = '1') and (m_axis_rx_tready_int = '1')) then
					req_rid_o    <= m_axis_rx_tdata(31 downto 16) after TCQ;
					req_tag_o    <= m_axis_rx_tdata(15 downto 8) after TCQ;
					wr_be_o      <= m_axis_rx_tdata(7 downto 0) after TCQ;
					state        <= PIO_32_RX_IO_MEM_WR32_DW2 after TCQ;
				else
					state        <= PIO_32_RX_IO_MEM_WR32_DW1 after TCQ;
				end if;
			-- end of PIO_32_RX_IO_MEM_WR32_DW1

			when PIO_32_RX_IO_MEM_WR32_DW2 =>
				if ((m_axis_rx_tvalid = '1') and (m_axis_rx_tready_int = '1')) then
					wr_addr_o    <= m_axis_rx_tdata(31 downto 2) after TCQ;
					wr_bar_o     <= bar_select;
					state        <= PIO_32_RX_IO_MEM_WR32_DW3 after TCQ;
				else
					state        <= PIO_32_RX_IO_MEM_WR32_DW2 after TCQ;
				end if;
			-- end of PIO_32_RX_IO_MEM_WR32_DW2

			when PIO_32_RX_IO_MEM_WR32_DW3 =>
				if ((m_axis_rx_tvalid = '1') and (m_axis_rx_tready_int = '1')) then
					wr_data_o               <= m_axis_rx_tdata(7 downto 0) & m_axis_rx_tdata(15 downto 8) & m_axis_rx_tdata(23 downto 16) & m_axis_rx_tdata(31 downto 24) after TCQ;
					wr_en_o                 <= '1' after TCQ;
					if (tlp_type = PIO_32_RX_IO_WR32_FMT_TYPE) then
						req_compl_o          <= '1' after TCQ;
					else
						req_compl_o          <= '0' after TCQ;
					end if;
					req_compl_with_data_o   <= '0' after TCQ;
					m_axis_rx_tready_int    <= '0' after TCQ;
					state                   <= PIO_32_RX_WAIT_STATE after TCQ;
				else
					state                   <= PIO_32_RX_IO_MEM_WR32_DW3 after TCQ;
				end if;
			-- end of PIO_32_RX_IO_MEM_WR32_DW3

			when PIO_32_RX_MEM_RD64_DW1 =>
				if ((m_axis_rx_tvalid = '1') and (m_axis_rx_tready_int ='1')) then
					req_rid_o    <= m_axis_rx_tdata(31 downto 16) after TCQ;
					req_tag_o    <= m_axis_rx_tdata(15 downto 8) after TCQ;
					req_be_o     <= m_axis_rx_tdata(7 downto 0) after TCQ;
					state        <= PIO_32_RX_MEM_RD64_DW2 after TCQ;
				else
					state        <= PIO_32_RX_MEM_RD64_DW1 after TCQ;
				end if;
			-- end of PIO_32_RX_MEM_RD64_DW1

			when PIO_32_RX_MEM_RD64_DW2 =>
				if ((m_axis_rx_tvalid = '1') and (m_axis_rx_tready_int = '1')) then
					state        <= PIO_32_RX_MEM_RD64_DW3 after TCQ;
				else
					state        <= PIO_32_RX_MEM_RD64_DW2 after TCQ;
				end if;
			-- end of PIO_32_RX_MEM_RD64_DW2

			when PIO_32_RX_MEM_RD64_DW3 =>
				if ((m_axis_rx_tvalid = '1') and (m_axis_rx_tready_int = '1')) then
					req_addr_o           <= m_axis_rx_tdata(31 downto 2) after TCQ;
					req_bar_o            <= bar_select;
					req_compl_o          <= '1' after TCQ;
					m_axis_rx_tready_int <= '0' after TCQ;
					state                <= PIO_32_RX_WAIT_STATE after TCQ;
				else
					state                <= PIO_32_RX_MEM_RD64_DW3 after TCQ;
				end if;
			-- end of PIO_32_RX_MEM_RD64_DW3

			when PIO_32_RX_MEM_WR64_DW1 =>
				if ((m_axis_rx_tvalid = '1') and (m_axis_rx_tready_int = '1')) then
					wr_be_o      <= m_axis_rx_tdata(7 downto 0) after TCQ;
					state        <= PIO_32_RX_MEM_WR64_DW2 after TCQ;
				else
					state        <= PIO_32_RX_MEM_WR64_DW1 after TCQ;
				end if;
			-- end of PIO_32_RX_MEM_WR64_DW1

			when PIO_32_RX_MEM_WR64_DW2 =>
				if ((m_axis_rx_tvalid = '1') and (m_axis_rx_tready_int = '1')) then
					state        <= PIO_32_RX_MEM_WR64_DW3 after TCQ;
				else
					state        <= PIO_32_RX_MEM_WR64_DW2 after TCQ;
				end if;
			-- end of PIO_32_RX_MEM_WR64_DW2

			when PIO_32_RX_MEM_WR64_DW3 =>
				if ((m_axis_rx_tvalid = '1') and (m_axis_rx_tready_int = '1')) then
					wr_addr_o  <= m_axis_rx_tdata(31 downto 2) after TCQ;
					wr_bar_o   <= bar_select;
					state      <= PIO_32_RX_MEM_WR64_DW4 after TCQ;
				else
					state      <= PIO_32_RX_MEM_WR64_DW3 after TCQ;
				end if;
			-- end of PIO_32_RX_MEM_WR64_DW3

			when PIO_32_RX_MEM_WR64_DW4 =>
				if ((m_axis_rx_tvalid = '1') and (m_axis_rx_tready_int = '1')) then
					wr_data_o            <= m_axis_rx_tdata(7 downto 0) & m_axis_rx_tdata(15 downto 8) & m_axis_rx_tdata(23 downto 16) & m_axis_rx_tdata(31 downto 24) after TCQ;
					wr_en_o              <= '1' after TCQ;
					m_axis_rx_tready_int <= '0' after TCQ;
					state                <= PIO_32_RX_WAIT_STATE after TCQ;
				else
					state                <= PIO_32_RX_MEM_WR64_DW4 after TCQ;
				end if;
			-- end of PIO_32_RX_MEM_WR64_DW4
			
			when PIO_32_RX_MEM_CPL_DW1 =>
				if ((m_axis_rx_tvalid = '1') and (m_axis_rx_tready_int = '1')) then
					state                <= PIO_32_RX_MEM_CPL_DW2 after TCQ;
				else
					state                <= PIO_32_RX_MEM_CPL_DW1 after TCQ;
				end if;
			-- end of PIO_32_RX_MEM_CPL_DW1

			when PIO_32_RX_MEM_CPL_DW2 =>
				if ((m_axis_rx_tvalid = '1') and (m_axis_rx_tready_int = '1')) then
					state                <= PIO_32_RX_WAIT_STATE after TCQ;
					m_axis_rx_tready_int <= '0' after TCQ;
				else
					state                <= PIO_32_RX_MEM_CPL_DW2 after TCQ;
				end if;
			-- end of PIO_32_RX_MEM_CPL_DW2
			
			when PIO_32_RX_WAIT_STATE =>			
				wr_en_o                 <= '0' after TCQ;
				
				if ((tlp_type = PIO_32_RX_MEM_WR32_FMT_TYPE) and (wr_busy_i = '0')) then
					m_axis_rx_tready_int <= '1' after TCQ;
					state                <= PIO_32_RX_RST_STATE after TCQ;
				elsif ((tlp_type = PIO_32_RX_IO_WR32_FMT_TYPE) and (wr_busy_i = '0')) then
					m_axis_rx_tready_int <= '1' after TCQ;
					state                <= PIO_32_RX_RST_STATE after TCQ;
				elsif ((tlp_type = PIO_32_RX_MEM_WR64_FMT_TYPE) and (wr_busy_i = '0')) then
					m_axis_rx_tready_int <= '1' after TCQ;
					state                <= PIO_32_RX_RST_STATE after TCQ;
				elsif ((tlp_type = PIO_32_RX_MEM_RD32_FMT_TYPE) and (compl_done_i = '1')) then
					m_axis_rx_tready_int <= '1' after TCQ;
					state                <= PIO_32_RX_RST_STATE after TCQ;
				elsif ((tlp_type = PIO_32_RX_IO_RD32_FMT_TYPE) and (compl_done_i = '1')) then
					m_axis_rx_tready_int <= '1' after TCQ;
					state                <= PIO_32_RX_RST_STATE after TCQ;
				elsif ((tlp_type = PIO_32_RX_MEM_RD64_FMT_TYPE) and (compl_done_i = '1')) then
					m_axis_rx_tready_int <= '1' after TCQ;
					state                <= PIO_32_RX_RST_STATE after TCQ;
				elsif tlp_type = PIO_32_RX_MEM_CPL_FMT_TYPE then
					m_axis_rx_tready_int <= '1' after TCQ;
					state                <= PIO_32_RX_RST_STATE after TCQ;
				else
					state                <= PIO_32_RX_WAIT_STATE after TCQ;
				end if;
			-- end of PIO_32_RX_WAIT_STATE

			when others =>
				state <= PIO_32_RX_WAIT_STATE after TCQ;
		end  case;
	end if;
end process;

	-- m_axis_rx_tuser[9:2] bar_hit[6:0]
	--	Receive BAR Hit: Indicates BAR(s) targeted by the current receive transaction. Asserted throughout the packet, from beginning of the packet to m_axis_rx_tlast.
	--	� m_axis_rx_tuser[2] => BAR0
	--	� m_axis_rx_tuser[3] => BAR1
	--	� m_axis_rx_tuser[4] => BAR2
	--	� m_axis_rx_tuser[5] => BAR3
	--	� m_axis_rx_tuser[6] => BAR4
	--	� m_axis_rx_tuser[7] => BAR5
	--	� m_axis_rx_tuser[8] => Expansion ROM Address.
	--	If two BARs are configured into a single 64-bit address, both corresponding m_axis_rx_tuser bits are asserted.
	--	m_axis_rx_tuser[9] is reserved for future use.
	bar_mux <= (m_axis_rx_tuser(2) and m_axis_rx_tuser(3)) & (m_axis_rx_tuser(4) and m_axis_rx_tuser(5)) & (m_axis_rx_tuser(6) and m_axis_rx_tuser(7)) & m_axis_rx_tuser(8);
	
process (bar_mux) begin
	case (bar_mux) is
		when "1000" =>
			bar_select <= "00";
		when "0100" =>
			bar_select <= "01";
		when "0010" =>
			bar_select <= "10";
		when "0001" =>
			bar_select <= "11";
		when others =>
			bar_select <= "00";
	end case;
end process;

end; -- PIO_32_RX_ENGINE

