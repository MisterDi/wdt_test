----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    19:20:40 03/16/2018 
-- Design Name: 
-- Module Name:    DIO_HI8435 - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

library UNISIM;
use UNISIM.Vcomponents.ALL;

entity DIO_HI8435 is
	Port (
		RESET : in  STD_LOGIC;
		RESET_CLK                : in  STD_LOGIC;
		
		CLK : in  STD_LOGIC;
		
		DIN : in  STD_LOGIC_VECTOR (31 downto 0);
		DIN_CLK : in  STD_LOGIC;
		DIN_EN : in  STD_LOGIC;
		
		DOUT : out  STD_LOGIC_VECTOR (31 downto 0);
		DOUT_STATE : out  STD_LOGIC_VECTOR (31 downto 0);
		DOUT_CLK : in  STD_LOGIC;
		
		CS : out  STD_LOGIC;
		SCK : out  STD_LOGIC;
		MOSI : out  STD_LOGIC;
		MISO : in  STD_LOGIC;
		MRB : out  STD_LOGIC
	);
end DIO_HI8435;

architecture Behavioral of DIO_HI8435 is

COMPONENT cdc_32_reg
	PORT(
		reset : IN std_logic;
		clk_in : IN std_logic;
		data_in : IN std_logic_vector(31 downto 0);
		clk_out : IN std_logic;          
		data_out : OUT std_logic_vector(31 downto 0)
	);
	END COMPONENT;
	
	COMPONENT fifo_32_long
	PORT (
		rst            : IN STD_LOGIC;

		wr_clk         : IN STD_LOGIC;
		din            : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
		wr_en          : IN STD_LOGIC;
		full           : OUT STD_LOGIC;

		rd_clk         : IN STD_LOGIC;
		rd_en          : IN STD_LOGIC;
		dout           : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
		empty          : OUT STD_LOGIC
	);
	END COMPONENT;
	
	COMPONENT SPI_HI8435
	PORT(
		RESET : IN std_logic;
		CLK : IN std_logic;
		DIN : IN std_logic_vector(39 downto 0);
		BITS : IN std_logic_vector(6 downto 0);
		EN : IN std_logic;
		MISO : IN std_logic;          
		DOUT : OUT std_logic_vector(31 downto 0);
		DONE : OUT std_logic;
		SCK : OUT std_logic;
		MOSI : OUT std_logic
		);
	END COMPONENT;

signal i_DOUT : STD_LOGIC_VECTOR (31 downto 0) := (others => '0');
signal i_DOUT_STATE : STD_LOGIC_VECTOR (31 downto 0) := (others => '0');

signal update_precnt  : integer range 0 to 250000 := 0;
signal update_cnt  : STD_LOGIC_VECTOR (7 downto 0) := (others => '0');
signal update_rate : STD_LOGIC_VECTOR (7 downto 0) := (others => '0');
signal update_flag : STD_LOGIC := '0';

signal i_DIN_RD_EN : STD_LOGIC := '0';
signal i_DIN : STD_LOGIC_VECTOR (31 downto 0) := (others => '0');
signal i_DIN_EMPTY : STD_LOGIC := '0';
signal i_MRB : STD_LOGIC := '0';

signal READY : STD_LOGIC := '0';

signal WRITE_CTRL : STD_LOGIC := '0';
signal WRITE_SENS : STD_LOGIC := '0';
signal WRITE_GOTHR : STD_LOGIC := '0';
signal WRITE_SOTHR : STD_LOGIC := '0';
signal WRITE_TMDATA : STD_LOGIC := '0';

signal READ_CTRL : STD_LOGIC := '0';
signal READ_SENS : STD_LOGIC := '0';
signal READ_GOTHR : STD_LOGIC := '0';
signal READ_SOTHR : STD_LOGIC := '0';
signal READ_TMDATA : STD_LOGIC := '0';

signal SPI_DIN : STD_LOGIC_VECTOR (31 downto 0) := (others => '0');
signal SPI_DOUT : STD_LOGIC_VECTOR (39 downto 0) := (others => '0');
signal SPI_BITS : STD_LOGIC_VECTOR (6 downto 0) := (others => '0');
signal SPI_EN : STD_LOGIC := '0';
signal SPI_DONE : STD_LOGIC := '0';

signal ctrl_soft_reset : STD_LOGIC := '0';
signal ctrl_test : STD_LOGIC := '0';
signal s_ctrl_soft_reset : STD_LOGIC := '0';
signal s_ctrl_test : STD_LOGIC := '0';

signal sense_bank : STD_LOGIC_VECTOR (3 downto 0) := (others => '0');
signal s_sense_bank : STD_LOGIC_VECTOR (3 downto 0) := (others => '0');

signal tmdata_odd1 : STD_LOGIC := '0';
signal tmdata_odd0 : STD_LOGIC := '0';
signal tmdata_all1 : STD_LOGIC := '0';
signal tmdata_all0 : STD_LOGIC := '0';
signal s_tmdata_odd1 : STD_LOGIC := '0';
signal s_tmdata_odd0 : STD_LOGIC := '0';
signal s_tmdata_all1 : STD_LOGIC := '0';
signal s_tmdata_all0 : STD_LOGIC := '0';

signal go_hys : STD_LOGIC_VECTOR (5 downto 0) := (others => '0');
signal go_cval : STD_LOGIC_VECTOR (5 downto 0) := (others => '0');
signal s_go_cval : STD_LOGIC_VECTOR (5 downto 0) := (others => '0');

signal so_hys : STD_LOGIC_VECTOR (5 downto 0) := (others => '0');
signal so_cval : STD_LOGIC_VECTOR (5 downto 0) := (others => '0');
signal s_so_cval : STD_LOGIC_VECTOR (5 downto 0) := (others => '0');

signal reset_delay : integer range 0 to 50000 := 0;

type din_state_type is (
	DIN_WAIT,
	
	DIN_UPDATE_START,
	DIN_UPDATE_ACK,
	
	DIN_WRITE_CTRL_START,
	DIN_WRITE_SENS_START,
	DIN_WRITE_TMDATA_START,
	DIN_WRITE_GOTHR_START,
	DIN_WRITE_SOTHR_START,
	
	DIN_WRITE_ACK,
	
	DIN_READ_CTRL_START,
	DIN_READ_CTRL_ACK,
	
	DIN_READ_SENS_START,
	DIN_READ_SENS_ACK,
	
	DIN_READ_TMDATA_START,
	DIN_READ_TMDATA_ACK,
	
	DIN_READ_GOTHR_START,
	DIN_READ_GOTHR_ACK,
	
	DIN_READ_SOTHR_START,
	DIN_READ_SOTHR_ACK
);
signal din_state : din_state_type;

COMPONENT cdc_bit
	PORT(
		CLK_IN : IN std_logic;
		D_IN : IN std_logic;
		CLK_OUT : IN std_logic;          
		D_OUT : OUT std_logic
	);
	END COMPONENT;
signal t_reset : std_logic;
	
begin
	reset_cdc_i: cdc_bit PORT MAP(
		CLK_IN => RESET_CLK,
		D_IN => RESET,
		CLK_OUT => CLK,
		D_OUT => t_reset
	);
	
	dout_cdc_32_reg: cdc_32_reg PORT MAP(
		reset => t_reset,
		clk_in => CLK,
		data_in => i_DOUT,
		clk_out => DOUT_CLK,
		data_out => DOUT
	);

	dout_state_cdc_32_reg: cdc_32_reg PORT MAP(
		reset => t_reset,
		clk_in => CLK,
		data_in => i_DOUT_STATE,
		clk_out => DOUT_CLK,
		data_out => DOUT_STATE
	);
	
	fifo_32_cmd_i : fifo_32_long
	PORT MAP (
		rst           => t_reset,

		wr_clk        => DIN_CLK,
		din           => DIN,
		wr_en         => DIN_EN,

		rd_clk        => CLK,
		rd_en         => i_DIN_RD_EN,
		dout          => i_DIN,
		empty         => i_DIN_EMPTY
	);
	
	SPI_HI8435_i: SPI_HI8435 PORT MAP(
		RESET => t_reset,
		CLK => CLK,
		DIN => SPI_DOUT,
		DOUT => SPI_DIN,
		BITS => SPI_BITS,
		EN => SPI_EN,
		DONE => SPI_DONE,
		SCK => SCK,
		MISO => MISO,
		MOSI => MOSI 
	);

i_DOUT_STATE(0) <= READY;
i_DOUT_STATE(1) <= '0';
i_DOUT_STATE(2) <= s_ctrl_test;
i_DOUT_STATE(3) <= s_ctrl_soft_reset;
i_DOUT_STATE(4) <= s_tmdata_all0;
i_DOUT_STATE(5) <= s_tmdata_all1;
i_DOUT_STATE(6) <= s_tmdata_odd0;
i_DOUT_STATE(7) <= s_tmdata_odd1;
i_DOUT_STATE(15 downto 8) <= update_rate;
i_DOUT_STATE(21 downto 16) <= s_go_cval;
i_DOUT_STATE(27 downto 22) <= s_so_cval;
i_DOUT_STATE(31 downto 28) <= s_sense_bank;

MRB <= i_MRB;

DRV: process (CLK, t_reset)
begin
	if CLK'event and CLK = '1' then
		if t_reset = '1' then
			din_state <= DIN_WAIT;
			
			update_flag <= '0';
			update_precnt <= 0;
			update_cnt <= (others => '0');
			update_rate <= (others => '0');
			
			i_DIN_RD_EN <= '0';
			
			WRITE_CTRL <= '0';
			WRITE_SENS <= '0';
			WRITE_GOTHR <= '0';
			WRITE_SOTHR <= '0';
			WRITE_TMDATA <= '0';

			READ_CTRL <= '0';
			READ_SENS <= '0';
			READ_GOTHR <= '0';
			READ_SOTHR <= '0';
			READ_TMDATA <= '0';
			
			i_MRB <= '0';
			READY <= '0';
			
			--SPI_DOUT <= (others => '0');
			--SPI_BITS <= (others => '0');
			SPI_EN <= '0';
			CS <= '1';
			
			ctrl_soft_reset <= '0';
			ctrl_test <= '0';
			s_ctrl_soft_reset <= '0';
			s_ctrl_test <= '0';
			
			reset_delay <= 0;
		else
			i_DIN_RD_EN <= '0';
			READY       <= '0';
			
			if reset_delay > 0 then
				reset_delay <= reset_delay - 1;
			else
				if i_MRB = '1' then 
					if update_rate /= X"00" then
						update_precnt <= update_precnt + 1;
						
						if update_precnt >= 250000 then
							update_precnt <= 0;
							update_cnt    <= update_cnt + 1;
							
							if update_cnt >= update_rate then
								update_cnt  <= "00000001";
								update_flag <= '1';
							end if;
						end if;
					end if;
				end if;
				
				case (din_state) is
					when DIN_WAIT =>
						if update_flag = '1' then
							update_flag <= '0';
							din_state <= DIN_UPDATE_START;
						
						elsif WRITE_CTRL = '1' then
							WRITE_CTRL <= '0';
							din_state <= DIN_WRITE_CTRL_START;
						
						elsif WRITE_SENS = '1' then
							WRITE_SENS <= '0';
							din_state <= DIN_WRITE_SENS_START;
							
						elsif WRITE_TMDATA = '1' then
							WRITE_TMDATA <= '0';
							din_state <= DIN_WRITE_TMDATA_START;
						
						elsif WRITE_GOTHR = '1' then
							WRITE_GOTHR <= '0';
							din_state <= DIN_WRITE_GOTHR_START;
						
						elsif WRITE_SOTHR = '1' then
							WRITE_SOTHR <= '0';
							din_state <= DIN_WRITE_SOTHR_START;
							
						elsif READ_CTRL = '1' then
							READ_CTRL <= '0';
							din_state <= DIN_READ_CTRL_START;
						
						elsif READ_SENS = '1' then
							READ_SENS <= '0';
							din_state <= DIN_READ_SENS_START;
							
						elsif READ_TMDATA = '1' then
							READ_TMDATA <= '0';
							din_state <= DIN_READ_TMDATA_START;
						
						elsif READ_GOTHR = '1' then
							READ_GOTHR <= '0';
							din_state <= DIN_READ_GOTHR_START;
						
						elsif READ_SOTHR = '1' then
							READ_SOTHR <= '0';
							din_state <= DIN_READ_SOTHR_START;
							
						elsif i_DIN_EMPTY = '0' then
							i_DIN_RD_EN <= '1';
							
							case (i_DIN(3 downto 0)) is
								when "0000" =>
									update_rate <= (others => '0');
									
									-- RESET
									if i_DIN(5) = '1' then
										-- HARDWARE
										i_MRB <= '0';
										reset_delay <= 50000;
										
									elsif i_DIN(4) = '1' then
										-- SOFTWARE
										ctrl_soft_reset <= '1';
										ctrl_test <= '0';
										WRITE_CTRL <= '1';
										i_MRB <= '1';
										reset_delay <= 50000;
										
									else
										ctrl_soft_reset <= '0';
										WRITE_CTRL <= '1';
										i_MRB <= '1';
										reset_delay <= 50000;
										
									end if;
								
								when "0001" =>
									-- Set operational
									update_rate <= i_DIN(15 downto 8);
									
									i_MRB <= '1';
									
									ctrl_test <= '0';
									ctrl_soft_reset <= '0';
									WRITE_CTRL <= '1';
									
									if i_DIN(7 downto 4) /= "0000" then
										ctrl_test <= '1';
										
										tmdata_odd1 <= i_DIN(7);
										tmdata_odd0 <= i_DIN(6);
										tmdata_all1 <= i_DIN(5);
										tmdata_all0 <= i_DIN(4);
										WRITE_TMDATA <= '1';
									end if;
								
								when "0010" =>
									-- Set levels
									sense_bank <= i_DIN(31 downto 28);
									WRITE_SENS <= '1';
									
									go_hys <= i_DIN(15 downto 10);
									go_cval <= i_DIN(9 downto 4);
									so_hys <= i_DIN(27 downto 22);
									so_cval <= i_DIN(21 downto 16);
									WRITE_GOTHR <= '1';
									WRITE_SOTHR <= '1';
								
								when "0011" =>
									-- Force update
									update_flag <= '1';
									
									if i_DIN(4) = '1' then
										READ_CTRL <= '1';
										READ_SENS <= '1';
										READ_TMDATA <= '1';
										READ_GOTHR <= '1';
										READ_SOTHR <= '1';
									end if;
								
								when others =>
							end case;
						
						else
							READY <= '1';
							
						end if;
					
					when DIN_WRITE_CTRL_START =>
						SPI_DOUT(39 downto 32) <= X"02";
						SPI_DOUT(31 downto 24) <= "000000" & ctrl_soft_reset & ctrl_test;
						SPI_BITS <= std_logic_vector(to_unsigned(16, SPI_BITS'length));
						SPI_EN <= '1';
						CS <= '0';
						din_state <= DIN_WRITE_ACK;
					
					when DIN_WRITE_SENS_START =>
						SPI_DOUT(39 downto 32) <= X"04";
						SPI_DOUT(31 downto 24) <= "0000" & sense_bank;
						SPI_BITS <= std_logic_vector(to_unsigned(16, SPI_BITS'length));
						SPI_EN <= '1';
						CS <= '0';
						din_state <= DIN_WRITE_ACK;
					
					when DIN_WRITE_TMDATA_START =>
						SPI_DOUT(39 downto 32) <= X"1E";
						SPI_DOUT(31 downto 24) <= "0000" & tmdata_odd1 & tmdata_odd0 & tmdata_all1 & tmdata_all0;
						SPI_BITS <= std_logic_vector(to_unsigned(16, SPI_BITS'length));
						SPI_EN <= '1';
						CS <= '0';
						din_state <= DIN_WRITE_ACK;
						
					when DIN_WRITE_GOTHR_START =>
						SPI_DOUT(39 downto 32) <= X"3A";
						SPI_DOUT(31 downto 24) <= "00" & go_hys;
						SPI_DOUT(23 downto 16) <= "00" & go_cval;
						SPI_BITS <= std_logic_vector(to_unsigned(24, SPI_BITS'length));
						SPI_EN <= '1';
						CS <= '0';
						din_state <= DIN_WRITE_ACK;
					
					when DIN_WRITE_SOTHR_START =>
						SPI_DOUT(39 downto 32) <= X"3C";
						SPI_DOUT(31 downto 24) <= "00" & so_hys;
						SPI_DOUT(23 downto 16) <= "00" & so_cval;
						SPI_BITS <= std_logic_vector(to_unsigned(24, SPI_BITS'length));
						SPI_EN <= '1';
						CS <= '0';
						din_state <= DIN_WRITE_ACK;
					
					when DIN_WRITE_ACK =>
						if SPI_DONE = '1'  then
							SPI_EN <= '0';
							CS <= '1';
							din_state <= DIN_WAIT;
						end if;
						
					when DIN_READ_CTRL_START =>
						SPI_DOUT(39 downto 32) <= X"82";
						SPI_BITS <= std_logic_vector(to_unsigned(16, SPI_BITS'length));
						SPI_EN <= '1';
						CS <= '0';
						din_state <= DIN_READ_CTRL_ACK;
					
					when DIN_READ_CTRL_ACK =>
						if SPI_DONE = '1'  then
							SPI_EN <= '0';
							CS <= '1';
							s_ctrl_soft_reset <= SPI_DIN(1);
							s_ctrl_test <= SPI_DIN(0);
							din_state <= DIN_WAIT;
						end if;
					
					when DIN_READ_SENS_START =>
						SPI_DOUT(39 downto 32) <= X"84";
						SPI_BITS <= std_logic_vector(to_unsigned(16, SPI_BITS'length));
						SPI_EN <= '1';
						CS <= '0';
						din_state <= DIN_READ_SENS_ACK;
					
					when DIN_READ_SENS_ACK =>
						if SPI_DONE = '1'  then
							SPI_EN <= '0';
							CS <= '1';
							s_sense_bank <= SPI_DIN(3 downto 0);
							din_state <= DIN_WAIT;
						end if;
						
					when DIN_READ_TMDATA_START =>
						SPI_DOUT(39 downto 32) <= X"9E";
						SPI_BITS <= std_logic_vector(to_unsigned(16, SPI_BITS'length));
						SPI_EN <= '1';
						CS <= '0';
						din_state <= DIN_READ_TMDATA_ACK;
					
					when DIN_READ_TMDATA_ACK =>
						if SPI_DONE = '1'  then
							SPI_EN <= '0';
							CS <= '1';
							s_tmdata_odd1 <= SPI_DIN(3);
							s_tmdata_odd0 <= SPI_DIN(2);
							s_tmdata_all1 <= SPI_DIN(1);
							s_tmdata_all0 <= SPI_DIN(0);
							din_state <= DIN_WAIT;
						end if;
						
					when DIN_READ_GOTHR_START =>
						SPI_DOUT(39 downto 32) <= X"BA";
						SPI_BITS <= std_logic_vector(to_unsigned(24, SPI_BITS'length));
						SPI_EN <= '1';
						CS <= '0';
						din_state <= DIN_READ_GOTHR_ACK;
					
					when DIN_READ_GOTHR_ACK =>
						if SPI_DONE = '1'  then
							SPI_EN <= '0';
							CS <= '1';
							s_go_cval <= SPI_DIN(5 downto 0);
							din_state <= DIN_WAIT;
						end if;
					
					when DIN_READ_SOTHR_START =>
						SPI_DOUT(39 downto 32) <= X"BC";
						SPI_BITS <= std_logic_vector(to_unsigned(24, SPI_BITS'length));
						SPI_EN <= '1';
						CS <= '0';
						din_state <= DIN_READ_SOTHR_ACK;
					
					when DIN_READ_SOTHR_ACK =>
						if SPI_DONE = '1'  then
							SPI_EN <= '0';
							CS <= '1';
							s_so_cval <= SPI_DIN(5 downto 0);
							din_state <= DIN_WAIT;
						end if;
					
					when DIN_UPDATE_START =>
						SPI_DOUT(39 downto 32) <= X"F8";
						SPI_DOUT(31 downto 0) <= (others => '0');
						SPI_BITS <= std_logic_vector(to_unsigned(40, SPI_BITS'length));
						SPI_EN <= '1';
						CS <= '0';
						din_state <= DIN_UPDATE_ACK;
					
					when DIN_UPDATE_ACK =>
						if SPI_DONE = '1'  then
							SPI_EN <= '0';
							CS <= '1';
							i_DOUT <= SPI_DIN;
							din_state <= DIN_WAIT;
						end if;
					
					when others =>
				end case;
			end if;
		end if;
	end if;
end process;

end Behavioral;

