/**********************************************************************/
/*   ____  ____                                                       */
/*  /   /\/   /                                                       */
/* /___/  \  /                                                        */
/* \   \   \/                                                       */
/*  \   \        Copyright (c) 2003-2009 Xilinx, Inc.                */
/*  /   /          All Right Reserved.                                 */
/* /---/   /\                                                         */
/* \   \  /  \                                                      */
/*  \___\/\___\                                                    */
/***********************************************************************/

#include "xsi.h"

struct XSI_INFO xsi_info;

char *UNISIM_P_0947159679;
char *IEEE_P_3620187407;
char *IEEE_P_2592010699;
char *STD_STANDARD;
char *IEEE_P_1242562249;
char *IEEE_P_3499444699;


int main(int argc, char **argv)
{
    xsi_init_design(argc, argv);
    xsi_register_info(&xsi_info);

    xsi_register_min_prec_unit(-12);
    ieee_p_2592010699_init();
    ieee_p_1242562249_init();
    ieee_p_3499444699_init();
    ieee_p_3620187407_init();
    unisim_p_0947159679_init();
    work_a_0358429577_3212880686_init();
    xilinxcorelib_a_0210886262_3212880686_init();
    xilinxcorelib_a_0572016320_3212880686_init();
    xilinxcorelib_a_1999898397_3212880686_init();
    xilinxcorelib_a_0303942097_3212880686_init();
    work_a_1136290278_1234945380_init();
    xilinxcorelib_a_4225244279_3212880686_init();
    xilinxcorelib_a_1800884735_3212880686_init();
    xilinxcorelib_a_0227836994_3212880686_init();
    xilinxcorelib_a_2891086109_3212880686_init();
    work_a_2505090880_2416129651_init();
    work_a_3622444780_3212880686_init();
    xilinxcorelib_a_3453796240_3212880686_init();
    xilinxcorelib_a_0123742296_3212880686_init();
    xilinxcorelib_a_3535798345_3212880686_init();
    work_a_0653227377_0084329324_init();
    work_a_1233405540_3212880686_init();
    xilinxcorelib_a_3952651998_3212880686_init();
    xilinxcorelib_a_1451380714_3212880686_init();
    xilinxcorelib_a_1745497601_3212880686_init();
    xilinxcorelib_a_2095249874_3212880686_init();
    work_a_3137097229_3781092905_init();
    work_a_3201820754_3212880686_init();
    work_a_3759598187_2372691052_init();


    xsi_register_tops("work_a_3759598187_2372691052");

    UNISIM_P_0947159679 = xsi_get_engine_memory("unisim_p_0947159679");
    IEEE_P_3620187407 = xsi_get_engine_memory("ieee_p_3620187407");
    IEEE_P_2592010699 = xsi_get_engine_memory("ieee_p_2592010699");
    xsi_register_ieee_std_logic_1164(IEEE_P_2592010699);
    STD_STANDARD = xsi_get_engine_memory("std_standard");
    IEEE_P_1242562249 = xsi_get_engine_memory("ieee_p_1242562249");
    IEEE_P_3499444699 = xsi_get_engine_memory("ieee_p_3499444699");

    return xsi_run_simulation(argc, argv);

}
