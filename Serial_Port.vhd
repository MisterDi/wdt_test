----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    12:48:56 03/21/2018 
-- Design Name: 
-- Module Name:    Serial_Port - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
library UNISIM;
use UNISIM.VComponents.all;

entity Serial_Port is
    Port ( Clk 		: in  STD_LOGIC;
			  ClkInn		: in  STD_LOGIC;
           Reset 		: in  STD_LOGIC;
           PortIn 	: in  STD_LOGIC_VECTOR (31 downto 0);
           PortOut 	: out  STD_LOGIC_VECTOR (31 downto 0);
           CfgIn		: in  STD_LOGIC_VECTOR (31 downto 0);
           CfgOut		: out  STD_LOGIC_VECTOR (31 downto 0);
			  RxData		:	in	STD_LOGIC;
			  TxData		:	out	STD_LOGIC;
			  RxEnable	:	out	STD_LOGIC;
			  TxEnable	:	out	STD_LOGIC;
			  CmdDataRd	: in	STD_LOGIC;
			  CmdDataWr	: in	STD_LOGIC;
			  CmdCfgRd	: in	STD_LOGIC;
			  CmdCfgWr	: in	STD_LOGIC
			  );
end Serial_Port;

architecture Behavioral of Serial_Port is

component RS_232_core
port (	  Clk 	 : in  STD_LOGIC;
           Reset 	 : in  STD_LOGIC;
           DataIn	 : in  STD_LOGIC_VECTOR (7 downto 0);
           DataOut : out  STD_LOGIC_VECTOR (7 downto 0);
           RdDat	 : in  STD_LOGIC;
           WrDat	 : in  STD_LOGIC;
           RX_Data : in  STD_LOGIC;
           TX_Data : out  STD_LOGIC;
           PreScale: in  STD_LOGIC_VECTOR (15 downto 0);
           Parity  : in  STD_LOGIC_VECTOR (1 downto 0);
           StopBit : in  STD_LOGIC_VECTOR (1 downto 0);
           DataBit : in  STD_LOGIC_VECTOR (3 downto 0);
           DevStatus : out  STD_LOGIC_VECTOR (7 downto 0)
			  );
end component;

component Fifo_RS
port(
			 rst 		: IN STD_LOGIC;
			 wr_clk 	: IN STD_LOGIC;
			 rd_clk 	: IN STD_LOGIC;
			 din 		: IN STD_LOGIC_VECTOR(7 DOWNTO 0);
			 wr_en 	: IN STD_LOGIC;
			 rd_en 	: IN STD_LOGIC;
			 dout 	: OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
			 full 	: OUT STD_LOGIC;
			 empty 	: OUT STD_LOGIC
);
end component;

type	fifo_fsm is (
			fifo_idle,
			fifo_read1,
			fifo_read2,
			fifo_read3,
			fifo_write1,
			fifo_write2,
			fifo_write3
);

signal	Reciver		:	fifo_fsm;
signal	Transmiter	:	fifo_fsm;
signal	PortGet		:	fifo_fsm;
signal	PortPut		:	fifo_fsm;

signal	RegPortIn	:	std_logic_vector(31 downto 0);
signal	RegPortOut	:	std_logic_vector(31 downto 0);
signal	RegCfgIn		:	std_logic_vector(31 downto 0);
signal	RegCfgOut	:	std_logic_vector(31 downto 0);

signal	RxFifoIn		:	std_logic_vector(7 downto 0);
signal	RxFifoOut	:	std_logic_vector(7 downto 0);

signal	CpuRdy		:	std_logic := '1';

signal	RxFifoFull	:	std_logic;
signal	RxFifoEmpty	:	std_logic;

signal	RxFifoRead	:	std_logic;
signal	RxFifoWrite	:	std_logic;

signal	TxFifoFull	:	std_logic;
signal	TxFifoEmpty	:	std_logic;

signal	TxFifoRead	:	std_logic;
signal	TxFifoWrite	:	std_logic;

signal	TxFifoIn:		std_logic_vector(7 downto 0);
signal	TxFifoOut:		std_logic_vector(7 downto 0);

signal	RS232_Read	:	std_logic;
signal	RS232_Write	:	std_logic;
signal	RS232_Status:	std_logic_vector(7 downto 0);
--	RS232_Status(0) RxBusy
-- RS232_Status(1) TxBusy
-- RS232_Status(2) FrameErr
-- RS232_Status(3) ParityErr
-- RS232_Status(4) RxRdy
-- RS232_Status(5) RxOwerflow

signal	PreScale		:	std_logic_vector(15 downto 0);
signal	DataBit		:	std_logic_vector(3 downto 0);
signal	StopBit		:	std_logic_vector(1 downto 0);
signal	ParityBit	:	std_logic_vector(1 downto 0);
signal	Duplex		:	std_logic;

signal	FlushFifo	: std_logic := '0';
			  			  
begin
rs232_i: RS_232_core
PORT MAP (
			Clk 		=>	ClkInn,
			Reset		=> Reset,
			DataIn	=> TxFifoOut,
			DataOut	=> RxFifoIn,
			RdDat		=> RS232_Read,
			WrDat		=> RS232_Write,
			RX_Data	=> RxData,
			TX_Data	=> TxData,
			PreScale	=> PreScale,
			Parity   => ParityBit,
			StopBit  =>	StopBit,
			DataBit  => DataBit,
			DevStatus=> RS232_Status
);

Fifo_RX:	Fifo_RS
port map (
		 rst 		=>	FlushFifo,
		 wr_clk 	=>	ClkInn,
		 rd_clk 	=> Clk,
		 din 		=>	RxFifoIn,
		 wr_en 	=> RxFifoWrite,
		 rd_en 	=> RxFifoRead,
		 dout 	=> RxFifoOut,
		 full 	=> RxFifoFull,
		 empty 	=> RxFifoEmpty
);

Fifo_TX:	Fifo_RS
port map (
		 rst 		=>	FlushFifo,
		 wr_clk 	=>	Clk,
		 rd_clk 	=> ClkInn,
		 din 		=>	TxFifoIn,
		 wr_en 	=> TxFifoWrite,
		 rd_en 	=> TxFifoRead,
		 dout 	=> TxFifoOut,
		 full 	=> TxFifoFull,
		 empty 	=> TxFifoEmpty
);

Rcv: process (Clk,Reset)
begin
	if Reset = '1' then
		Reciver	<= fifo_idle;
		CpuRdy <= '1';
--		FlushFifo <= '1';
	else
		if Clk'event and Clk = '1' then
			PortOut <= RegPortOut;
			if CmdDataRd = '1' then
				RegPortOut(26 downto 24) <= "000";
				RegPortOut(30 downto 29) <= "00";
				CpuRdy <= '1';
			end if;
			
			case Reciver is
				when fifo_idle =>
					if (RxFifoEmpty = '0') and (CpuRdy = '1') then
						RxFifoRead <= '1';
						Reciver <= fifo_read1;
					end if;
				when fifo_read1 =>
					RxFifoRead <= '0';
					Reciver <= fifo_read2;
				when fifo_read2 =>
					Reciver <= fifo_idle;
					if RegPortOut(26 downto 24) = "000" then 
						RegPortOut(7 downto 0) <= RxFifoOut;
						RegPortOut(23 downto 8) <= X"0000";
						RegPortOut(24) <= '1';
					end if;	
					if RegPortOut(26 downto 24) = "001" then 
						RegPortOut(15 downto 8) <= RxFifoOut;
						RegPortOut(25) <= '1';
					end if;	
					if RegPortOut(26 downto 24) = "011" then 
						RegPortOut(23 downto 16) <= RxFifoOut;
						RegPortOut(26) <= '1';
						CpuRdy <= '0';
					end if;
				when others =>
			end case;
			
			CfgOut(31 downto 16) <= PreScale;
			CfgOut(15 downto 12) <= DataBit;
			CfgOut(11 downto 10) <= StopBit;
			CfgOut(9 downto 8) <= ParityBit;
			CfgOut(7) <= RS232_Status(0);
			CfgOut(6) <= RxFifoEmpty;
			CfgOut(5) <= RxFifoFull;
			CfgOut(4) <= RS232_Status(1);
			CfgOut(3) <= TxFifoEmpty;
			CfgOut(2) <= TxFifoFull;
			CfgOut(1) <= RS232_Status(3);
			CfgOut(0) <= RS232_Status(2);
			if CmdCfgRd = '1' then
				CfgOut(1 downto 0) <= "00";
			end if;
		end if;
	end if;
end process;

Xmt: process (Clk,Reset)
begin
	if Reset = '1' then
		Transmiter <= fifo_idle;
	else
		if Clk'event and Clk = '1' then
			if CmdDataWr = '1' then
				RegPortIn <= PortIn;
				Transmiter <= fifo_write1;
			end if;
			if CmdCfgWr = '1' then
				PreScale <= CfgIn(31 downto 16);
				Databit <= CfgIn(15 downto 12);
				StopBit <= CfgIn(11 downto 10);
				ParityBit <= CfgIn(9 downto 8);
				Duplex <= CfgIn(0);
				FlushFifo <= '1';
			else
				FlushFifo <= '0';				
			end if;
			case Transmiter is
				when fifo_idle =>
					TxFifoWrite <= '0';
				when fifo_write1 =>
					TxFifoIn <= RegPortIn(7 downto 0);
					TxFifoWrite <= '1';
					if (RegPortIn(25) = '1' ) then
						Transmiter <= fifo_write2;
					else
						Transmiter <= fifo_idle;
					end if;
				when fifo_write2 =>
					TxFifoIn <= RegPortIn(15 downto 8);
					if (RegPortIn(26) = '1' ) then
						Transmiter <= fifo_write3;
					else
						Transmiter <= fifo_idle;
					end if;
				when fifo_write3 =>
					TxFifoIn <= RegPortIn(23 downto 16);
					Transmiter <= fifo_idle;
				when others =>
			end case;
		end if;	
	end if;
end process;

engine: process (ClkInn,Reset)
begin
	if Reset = '1' then
		PortGet <= fifo_idle;
		PortPut <= fifo_idle;
		RS232_Write <= '0';
		RS232_Read <= '0';
	else
		if ClkInn'event and ClkInn = '1' then
			RxEnable <= '0';
			case	PortGet is -- read from reciver 
				when fifo_idle =>
					if RS232_Status(4) = '1' then
						PortGet <= fifo_read1;
						RS232_Read <= '1';
					end if;
				when fifo_read1 =>
					PortGet <= fifo_read2;
					RxFifoWrite <= '1';
					RS232_Read <= '0';
				when fifo_read2 =>
					PortGet <= fifo_idle;
					RxFifoWrite <= '0';
				when others =>	
			end case;
--			if (Duplex = '1' or RS232_Status(0)= '0') then -- write to transmitter
			case PortPut is
				when fifo_idle =>
					if (TxFifoEmpty = '0') then
						if (RS232_Status(1)= '0') then
							PortPut <= fifo_write1;
							TxFifoRead <= '1';
							TxEnable <= '1';
						end if;
					else
						if (RS232_Status(1) = '0') then
							TxEnable <= '0';
						end if;	
					end if;
				when fifo_write1 =>
					PortPut <= fifo_write2;
					TxFifoRead <= '0';
					RS232_Write <= '1';
				when fifo_write2 =>
					if RS232_Status(1)= '1' then
						PortPut <= fifo_idle;
					end if;
					RS232_Write <= '0';
				when others =>
			end case;
--			end if;
--			if Duplex = '0' and RS232_Status(1)= '0' then
--				TxEnable <= '0';
--			end if;
		end if;	
	end if;
end process;

end Behavioral;

