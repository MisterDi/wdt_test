--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   07:11:25 03/23/2018
-- Design Name:   
-- Module Name:   D:/Work/dknp10_io/TB_708.vhd
-- Project Name:  IO_10D
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: ARINC_708
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY TB_708 IS
END TB_708;
 
ARCHITECTURE behavior OF TB_708 IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT ARINC_708
    PORT(
         RESET : IN  std_logic;
			RESET_CLK : IN  std_logic;
         CLK16 : IN  std_logic;
         TXINHA : OUT  std_logic;
         TXA_P : OUT  std_logic;
         TXA_N : OUT  std_logic;
         CLKA : OUT  std_logic;
         ENCLKA : OUT  std_logic;
         TOC0A : OUT  std_logic;
         TOC1A : OUT  std_logic;
         TOC2A : OUT  std_logic;
         RXA_P : IN  std_logic;
         RXA_N : IN  std_logic;
         RXENA : OUT  std_logic;
         ENPEXTA : OUT  std_logic;
         TXINHB : OUT  std_logic;
         TXB_P : OUT  std_logic;
         TXB_N : OUT  std_logic;
         CLKB : OUT  std_logic;
         ENCLKB : OUT  std_logic;
         TOC0B : OUT  std_logic;
         TOC1B : OUT  std_logic;
         TOC2B : OUT  std_logic;
         RXB_P : IN  std_logic;
         RXB_N : IN  std_logic;
         RXENB : OUT  std_logic;
         ENPEXTB : OUT  std_logic;
			
			A708_DMA_ADDR_CLK        : in STD_LOGIC := '0';
			A708_DMA_ADDR            : in STD_LOGIC_VECTOR (63 downto 0) := (others => '0');
			
			A708_STATE_CLK           : in STD_LOGIC := '0';
			
			A708_CMD                 : in STD_LOGIC_VECTOR (31 downto 0) := (others => '0');
			A708_CMD_EN              : in STD_LOGIC := '0';
			A708_CMD_CLK             : in STD_LOGIC := '0';
			
			-- DMA
			pci_dma_ready            : in STD_LOGIC := '0';
			
			dma_clk						 : in STD_LOGIC := '0';
			
			dma_cmd_rd_en            : in  STD_LOGIC := '0';
			
			dma_data_rd_en           : in  STD_LOGIC := '0';
			
			dma_gnt                  : in  STD_LOGIC := '0';
		
		debug                    : out std_logic;
		debug2                   : out std_logic
		
        );
    END COMPONENT;
    

   --Inputs
   signal RESET : std_logic := '0';
	signal RESET_CLK : std_logic := '0';
   signal CLK16 : std_logic := '0';
   signal RXA_P : std_logic := '0';
   signal RXA_N : std_logic := '0';
   signal RXB_P : std_logic := '0';
   signal RXB_N : std_logic := '0';

 	--Outputs
   signal TXINHA : std_logic;
   signal TXA_P : std_logic;
   signal TXA_N : std_logic;
   signal CLKA : std_logic;
   signal ENCLKA : std_logic;
   signal TOC0A : std_logic;
   signal TOC1A : std_logic;
   signal TOC2A : std_logic;
   signal RXENA : std_logic;
   signal ENPEXTA : std_logic;
   signal TXINHB : std_logic;
   signal TXB_P : std_logic;
   signal TXB_N : std_logic;
   signal CLKB : std_logic;
   signal ENCLKB : std_logic;
   signal TOC0B : std_logic;
   signal TOC1B : std_logic;
   signal TOC2B : std_logic;
   signal RXENB : std_logic;
   signal ENPEXTB : std_logic;
	
	signal debug : std_logic;
	signal debug2 : std_logic;

   -- Clock period definitions
   constant CLK16_period : time := 62.5 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: ARINC_708 PORT MAP (
          RESET => RESET,
			 RESET_CLK => RESET_CLK,
          CLK16 => CLK16,
          TXINHA => TXINHA,
          TXA_P => TXA_P,
          TXA_N => TXA_N,
          CLKA => CLKA,
          ENCLKA => ENCLKA,
          TOC0A => TOC0A,
          TOC1A => TOC1A,
          TOC2A => TOC2A,
          RXA_P => RXA_P,
          RXA_N => RXA_N,
          RXENA => RXENA,
          ENPEXTA => ENPEXTA,
          TXINHB => TXINHB,
          TXB_P => TXB_P,
          TXB_N => TXB_N,
          CLKB => CLKB,
          ENCLKB => ENCLKB,
          TOC0B => TOC0B,
          TOC1B => TOC1B,
          TOC2B => TOC2B,
          RXB_P => RXB_P,
          RXB_N => RXB_N,
          RXENB => RXENB,
          ENPEXTB => ENPEXTB,
			 
			 debug => debug,
			 debug2 => debug2
        );
	
	RXA_P <= TXA_P;
	RXA_N <= TXA_N;
	
	RESET_CLK <= CLK16;

   -- Clock process definitions
   CLK16_process :process
   begin
		CLK16 <= '0';
		wait for CLK16_period/2;
		CLK16 <= '1';
		wait for CLK16_period/2;
   end process;

   -- Stimulus process
   stim_proc: process
   begin		
      -- hold reset state for 100 ns.
		RESET <= '1';
      wait for 100 ns;	

      wait for CLK16_period*10;

      -- insert stimulus here 
		RESET <= '0';

      wait;
   end process;

END;
