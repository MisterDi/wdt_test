----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    08:34:27 03/17/2018 
-- Design Name: 
-- Module Name:    SPI_HI3210 - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

library UNISIM;
use UNISIM.Vcomponents.ALL;

entity SPI_HI3210 is
	Port ( 
		RESET : in  STD_LOGIC;
		CLK : in  STD_LOGIC;
		
		ADDR : in  STD_LOGIC_VECTOR (15 downto 0);
		
		READ_EN : in  STD_LOGIC;
		WRITE_EN : in  STD_LOGIC;
		READ_ARINC_EN : in  STD_LOGIC;
		WRITE_ARINC_EN : in  STD_LOGIC;
		READ_ARINC_LABEL_EN : in  STD_LOGIC;
		READ_ARINC_BLOCK_EN : in  STD_LOGIC;
		DONE : out  STD_LOGIC;
		
		DATA_IN : in  STD_LOGIC_VECTOR (7 downto 0);
		DATA_OUT : out  STD_LOGIC_VECTOR (7 downto 0);
		
		ARINC_IN : in  STD_LOGIC_VECTOR (31 downto 0);
		ARINC_OUT : out  STD_LOGIC_VECTOR (31 downto 0);
		
		CS : out  STD_LOGIC;
		SCK : out  STD_LOGIC;
		MISO : in  STD_LOGIC;
		MOSI : out  STD_LOGIC
	);
end SPI_HI3210;

architecture Behavioral of SPI_HI3210 is

signal spi_out : STD_LOGIC_VECTOR (39 downto 0);
signal spi_in : STD_LOGIC_VECTOR (31 downto 0);
signal bits_cnt : integer range 0 to 40;

type spi_state_type is (
	SPI_WAIT,
	
	SPI_READ_START,
	SPI_READ_DATA,
	SPI_READ_FINISH,
	
	SPI_WRITE_START,
	SPI_WRITE_DATA,
	SPI_WRITE_FINISH,
	
	SPI_READ_ARINC_START,
	SPI_READ_ARINC_LABEL_START,
	SPI_READ_ARINC_BLOCK_START,
	SPI_READ_ARINC_FINISH,
	
	SPI_WRITE_ARINC_START,
	SPI_WRITE_ARINC_FINISH,
	
	SPI_W_L,
	SPI_W_H,
	SPI_W_DONE
);
signal spi_state : spi_state_type;
signal spi_state_ret : spi_state_type;
signal delay : integer range 0 to 4;

begin

DRV: process (CLK, RESET)
begin
	if RESET = '1' then
		SCK <= '0';
		MOSI <= '0';
		DONE <= '0';
		CS <= '1';
		spi_state <= SPI_WAIT;
		delay <= 0;
	elsif CLK'event and CLK = '1' then
		case (spi_state) is
			when SPI_WAIT =>
				if    READ_EN = '1' then
					spi_state <= SPI_READ_START;
					
				elsif WRITE_EN = '1' then
					spi_state <= SPI_WRITE_START;
					
				elsif READ_ARINC_EN = '1' then
					spi_state <= SPI_READ_ARINC_START;
					
				elsif WRITE_ARINC_EN = '1' then
					spi_state <= SPI_WRITE_ARINC_START;
					
				elsif READ_ARINC_LABEL_EN = '1' then
					spi_state <= SPI_READ_ARINC_LABEL_START;
					
				elsif READ_ARINC_BLOCK_EN = '1' then
					spi_state <= SPI_READ_ARINC_BLOCK_START;
					
				end if;
			
			------------------------------------------------------ READ BYTE
			when SPI_READ_START =>
				spi_out(39 downto 32) <= X"8C";
				spi_out(31 downto 16) <= ADDR;
				bits_cnt <= 24;
				spi_state_ret <= SPI_READ_DATA;
				spi_state <= SPI_W_L;
				CS <= '0';
				delay <= 0;
				
			when SPI_READ_DATA =>
				if delay >= 4 then
					spi_out(39 downto 32) <= X"80";
					bits_cnt <= 16;
					spi_state_ret <= SPI_READ_FINISH;
					spi_state <= SPI_W_L;
					CS <= '0';
				else
					delay <= delay + 1;
				end if;
			
			when SPI_READ_FINISH =>
				CS <= '1';
				if READ_EN = '1' then
					DONE <= '1';
					DATA_OUT <= spi_in(7 downto 0);
				else
					DONE <= '0';
					spi_state <= SPI_WAIT;
				end if;
			
			------------------------------------------------------ WRITE BYTE
			when SPI_WRITE_START =>
				spi_out(39 downto 32) <= X"8C";
				spi_out(31 downto 16) <= ADDR;
				bits_cnt <= 24;
				spi_state_ret <= SPI_WRITE_DATA;
				spi_state <= SPI_W_L;
				CS <= '0';
				delay <= 0;
				
			when SPI_WRITE_DATA =>
				if delay >= 4 then
					spi_out(39 downto 32) <= X"84";
					spi_out(31 downto 24) <= DATA_IN;
					bits_cnt <= 16;
					spi_state_ret <= SPI_WRITE_FINISH;
					spi_state <= SPI_W_L;
					CS <= '0';
				else
					delay <= delay + 1;
				end if;
			
			when SPI_WRITE_FINISH =>
				CS <= '1';
				if WRITE_EN = '1' then
					DONE <= '1';
				else
					DONE <= '0';
					spi_state <= SPI_WAIT;
				end if;
			
			------------------------------------------------------ READ ARINC
			when SPI_READ_ARINC_START =>
				spi_out(39 downto 32) <= "101" & ADDR(2 downto 0) & "00";
				bits_cnt <= 40;
				spi_state_ret <= SPI_READ_ARINC_FINISH;
				spi_state <= SPI_W_L;
				CS <= '0';
			
			when SPI_READ_ARINC_LABEL_START =>
				spi_out(39 downto 32) <= "110" & ADDR(2 downto 0) & "00";
				bits_cnt <= 40;
				spi_state_ret <= SPI_READ_ARINC_FINISH;
				spi_state <= SPI_W_L;
				CS <= '0';
				
			when SPI_READ_ARINC_BLOCK_START =>
				spi_out(39 downto 32) <= "111" & ADDR(2 downto 0) & "00";
				bits_cnt <= 40;
				spi_state_ret <= SPI_READ_ARINC_FINISH;
				spi_state <= SPI_W_L;
				CS <= '0';
			
			when SPI_READ_ARINC_FINISH =>
				CS <= '1';
				if READ_ARINC_EN = '1' or READ_ARINC_LABEL_EN = '1' or READ_ARINC_BLOCK_EN = '1' then
					ARINC_OUT <= spi_in(31 downto 0);
					DONE <= '1';
				else
					DONE <= '0';
					spi_state <= SPI_WAIT;
				end if;
			
			------------------------------------------------------ WRITE ARINC
			when SPI_WRITE_ARINC_START =>
				spi_out(39 downto 32) <= "100101" & ADDR(1 downto 0);
				spi_out(31 downto 0) <= ARINC_IN;
				bits_cnt <= 40;
				spi_state_ret <= SPI_WRITE_ARINC_FINISH;
				spi_state <= SPI_W_L;
				CS <= '0';
			
			when SPI_WRITE_ARINC_FINISH =>
				CS <= '1';
				if WRITE_ARINC_EN = '1' then
					DONE <= '1';
				else
					DONE <= '0';
					spi_state <= SPI_WAIT;
				end if;
			
			------------------------------------------------------ SPI TRANSFER
			when SPI_W_L =>
				if bits_cnt = 0 then
					spi_state <= SPI_W_DONE;
				else
					SCK <= '0';
					MOSI <= spi_out(39);
					spi_out <= spi_out(38 downto 0) & '0';
					spi_state <= SPI_W_H;
					bits_cnt <= bits_cnt - 1;
				end if;
				
			when SPI_W_H =>
				SCK <= '1';
				spi_state <= SPI_W_L;
				spi_in <= spi_in(30 downto 0) & MISO;
				
			when SPI_W_DONE =>
				CS <= '1';
				spi_state <= spi_state_ret;
				
			when others =>
		end case;
	end if;
end process;

end Behavioral;

