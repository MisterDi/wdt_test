----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    17:59:09 03/17/2018 
-- Design Name: 
-- Module Name:    RS_232_core - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity RS_232_core is
    Port ( Clk 	 : in  STD_LOGIC;
           Reset 	 : in  STD_LOGIC;
           DataIn	 : in  STD_LOGIC_VECTOR (7 downto 0);
           DataOut : out  STD_LOGIC_VECTOR (7 downto 0);
           RdDat	 : in  STD_LOGIC;
           WrDat	 : in  STD_LOGIC;
           RX_Data : in  STD_LOGIC;
           TX_Data : out  STD_LOGIC;
           PreScale: in  STD_LOGIC_VECTOR (15 downto 0);
           Parity  : in  STD_LOGIC_VECTOR (1 downto 0);
           StopBit : in  STD_LOGIC_VECTOR (1 downto 0);
           DataBit : in  STD_LOGIC_VECTOR (3 downto 0);
           DevStatus : out  STD_LOGIC_VECTOR (7 downto 0));
end RS_232_core;

architecture Behavioral of RS_232_core is

type State is (
	st_idle,
	st_start_bit,
	st_data_bit,
	st_parity_bit,
	st_stop_bit
);

signal	RxFsm		: State := st_idle;
signal	TxFsm		: State := st_idle;

signal	RxBitCnt	:	std_logic_vector(3 downto 0);
signal	TxBitCnt	:	std_logic_vector(3 downto 0);

signal	RxTimer	:	std_logic_vector(4 downto 0);
signal	TxTimer	:	std_logic_vector(4 downto 0);

signal	DataClkRx:	std_logic;
signal	DataClkTx:	std_logic;

signal	SclCntRx	:	std_logic_vector(15 downto 0);
signal	SclCntTx	:	std_logic_vector(15 downto 0);

signal	RxInp1	:	std_logic;
signal	RxInp2	:	std_logic;

signal	TxInp1	:	std_logic;
signal	TxInp2	:	std_logic;

signal	RxReg		:	std_logic_vector(7 downto 0);
signal	TxReg		:	std_logic_vector(7 downto 0);

signal	ParityBitRx	:	std_logic;
signal	ParityBitTx	:	std_logic;

signal	RxBusy	: std_logic := '0';
signal	TxBusy	: std_logic := '0';
signal	FrameErr	: std_logic := '0';
signal	ParityErr: std_logic := '0';
signal	RxRdy		: std_logic := '0';
signal	RxOwf		: std_logic := '0';

begin

RS232RX: process (Clk,Reset)
begin
	if Reset = '1' then
		RxFsm <= st_idle;
		RxBitCnt <= "0000";
		SclCntRx <= X"0000";
		DataClkRx	<= '0';
		RxTimer <= "10000";
	else
		if Clk'event and Clk = '1' then
			RxInp1 <= RX_Data;
			RxInp2 <= RxInp1;
			if SclCntRx = X"0000" then
				SclCntRx <= PreScale;
				DataClkRx 	<= '1';
			else
				SclCntRx <= SclCntRx - X"0001";
			end if;
			if RdDat = '1' then
				RxRdy <= '0';
			end if;
			DevStatus(0) <=RxBusy;
			DevStatus(2) <=FrameErr;
			DevStatus(3) <=ParityErr;
			DevStatus(4) <=RxRdy;
			DevStatus(5) <=RxOwf;
			DevStatus(6) <= '0';
			DevStatus(7) <= '0';			
			case RxFsm is
				when st_idle =>
					if ((RxInp2 = '1') and (RxInp1 = '0')) then
						RxFsm <= st_start_bit;
						RxBusy <= '1';
						FrameErr <= '0';
						ParityErr <= '0';
						SclCntRx <= PreScale;
						DataClkRx <= '0';
						RxTimer <= "00111";
					end if;
				when st_start_bit =>
					if DataClkRx = '1' then
						DataClkRx <= '0';
						if RxTimer = "00000" then
							RxTimer <= "10001";
							RxBitCnt <= DataBit;
							RxReg <= X"00";
							ParityBitRx <= '0';
							RxFsm <= st_data_bit;
						else
							RxTimer <= RxTimer - "00001";
						end if;
					else
						if ((RxInp2 = '0') and (RxInp1 = '1')) then
							RxFsm <= st_idle;
							FrameErr <= '1';
						end if;
					end if;
				when st_data_bit =>
					if DataClkRx = '1' then
						DataClkRx <= '0';
						if RxTimer = "00000" then
							RxTimer <= "10000";
							RxBitCnt <= RxBitCnt - "0001";
							if RxInp2 = '1' then
								ParityBitRx <= not ParityBitRx;
							end if;
							case RxBitCnt is
								when "1000" =>
									RxReg(0) <= RxInp2;
								when "0111" =>
									RxReg(1) <= RxInp2;
								when "0110" =>
									RxReg(2) <= RxInp2;
								when "0101" =>
									RxReg(3) <= RxInp2;
								when "0100" =>
									RxReg(4) <= RxInp2;
								when "0011" =>
									RxReg(5) <= RxInp2;
								when "0010" =>
									RxReg(6) <= RxInp2;
								when "0001" =>
									RxReg(7) <= RxInp2;
								when others =>
							end case;
						else
							RxTimer <= RxTimer - "00001";
						end if;
					end if;
					if (RxBitCnt = "0000") then
						if	Parity = "00" then
							RxFsm <= st_stop_bit;
							case StopBit is
								when "00" =>
									RxTimer <= "10000"; 
								when "01" =>
									RxTimer <= "11000"; 
								when "10" =>
									RxTimer <= "11111";
								when others =>
							end case;
						else
							RxFsm <= st_parity_bit;
							RxTimer <= "10000"; 
						end if;	
					end if;
				when st_parity_bit =>
					if DataClkRx = '1' then
						DataClkRx <= '0';
						if RxTimer = "00000" then
							RxFsm <= st_stop_bit;
							case StopBit is
								when "00" =>
									RxTimer <= "10000"; 
								when "01" =>
									RxTimer <= "11000"; 
								when "10" =>
									RxTimer <= "11111";
								when others =>
									RxTimer <= "11111";
							end case;
							if RxInp2 = '1' then
								ParityBitRx <= not ParityBitRx;
							end if;
						else
							RxTimer <= RxTimer - "00001";
						end if ;
					end if;
				when st_stop_bit =>
					if DataClkRx = '1' then
						DataClkRx <= '0';
						if RxTimer = "00000" then
							RxFsm <= st_idle;
							RxBusy <= '0';
							if RxRdy = '0' then
								RxRdy <= '1';
								RxOwf <= '0';
							else
								RxOwf <= '1';
							end if;
							DataOut <= RxReg;
							if Parity = "00" then
								DevStatus(3) <= '0';
							else
								if Parity(1) = ParityBitRx then
									ParityErr <= '0';
								else
									ParityErr <= '1';
								end if;
							end if;
						else
							RxTimer <= RxTimer - "00001";
						end if ;
					end if;
				when others =>
			end case;
		end if;	
	end if;
end process;

RS232TX: process (Clk,Reset)
begin
	if Reset = '1' then
		TxFsm <= st_idle;
		TxBitCnt <= "0000";
		SclCntTx <= X"0000";
		DataClkTx	<= '0';
		Tx_Data <= '1';
		DevStatus(1) <= '0';
	else
		if Clk'event and Clk = '1' then
			TxInp1 <= WrDat;
			TxInp2 <= TxInp1;
			if SclCntTx = X"0000" then
				SclCntTx <= PreScale;
				DataClkTx 	<= '1';
			else
				SclCntTx <= SclCntTx - X"0001";
			end if;
			case TxFsm is
				when st_idle =>
					if ((TxInp2 = '0') and (TxInp1 = '1')) then
						TxFsm <= st_start_bit;
						DevStatus(1) <= '1';
						TxTimer <= "10000";
						SclCntTx <= PreScale;
						DataClkTx <= '0';
						TX_Data <= '0';
						DevStatus(1) <= '1';
					end if;
				when st_start_bit =>
					if DataClkTx = '1' then
						DataClkTx <= '0'; 
						if TxTimer = "00000" then
							TxTimer <= "10000";
							TxFsm <= st_data_bit;
							TxBitCnt <= DataBit - "0001";
							TxReg <= DataIn;
							ParityBitTx <= '0';
						else
							TxTimer <= TxTimer - "00001";
						end if;
					end if;
				when st_data_bit =>
					TX_Data <= TxReg(0);
					if DataClkTx = '1' then
						DataClkTx <= '0';
						if TxTimer = "00000" then
							TxTimer <= "10000";
							if TxReg(0) = '1' then
								ParityBitTx <= not ParityBitTx;
							end if;
							TxReg <= '0' & TxReg(7 downto 1);
							if TxBitCnt = "0000" then
								if Parity = "00" then
									TxFsm <= st_stop_bit;
									case StopBit is
										when "00" =>
											TxTimer <= "10000"; 
										when "01" =>
											TxTimer <= "11000"; 
										when "10" =>
											TxTimer <= "11111";
										when others =>
											TxTimer <= "11111";
									end case;
								else
									TxFsm <= st_parity_bit;
								end if;
							else
								TxBitCnt <= TxBitCnt - "0001";
							end if;
						else
							TxTimer <= TxTimer - "00001";
						end if;
					end if;
				when st_parity_bit =>
					if Parity(0) = '1' then
						TX_Data <= ParityBitTX;
					else
						TX_Data <= not ParityBitTX;
					end if;
			   when st_stop_bit =>
					TX_Data <= '1';
					if DataClkTx = '1' then
						DataClkTx <= '0';
						if TxTimer = "00000" then
							TxFsm <= st_idle;
							DevStatus(1) <= '0';
						else
							TxTimer <= TxTimer - "00001";
						end if;
					end if;
				when others =>			
			end case;
		end if;	
	end if;
end process;

end Behavioral;

