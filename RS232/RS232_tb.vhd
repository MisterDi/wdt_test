--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   12:04:36 03/19/2018
-- Design Name:   
-- Module Name:   D:/Design/DKNP10/RS232/RS_232/RS232_tb.vhd
-- Project Name:  RS_232
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: RS_232_core
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
USE ieee.numeric_std.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
 
ENTITY RS232_tb IS
END RS232_tb;
 
ARCHITECTURE behavior OF RS232_tb IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT RS_232_core
    PORT(
         Clk : IN  std_logic;
         Reset : IN  std_logic;
         DataIn : IN  std_logic_vector(7 downto 0);
         DataOut : OUT  std_logic_vector(7 downto 0);
         RdDat : IN  std_logic;
         WrDat : IN  std_logic;
         RX_Data : IN  std_logic;
         TX_Data : OUT  std_logic;
         PreScale : IN  std_logic_vector(15 downto 0);
         Parity : IN  std_logic_vector(1 downto 0);
         StopBit : IN  std_logic_vector(1 downto 0);
         DataBit : IN  std_logic_vector(3 downto 0);
         DevStatus : OUT  std_logic_vector(7 downto 0)
        );
    END COMPONENT;
    

   --Inputs
   signal Clk : std_logic := '0';
   signal Reset : std_logic := '0';
   signal DataIn : std_logic_vector(7 downto 0) := (others => '0');
   signal RdDat : std_logic := '0';
   signal WrDat : std_logic := '0';
   signal RX_Data : std_logic := '1';
   signal PreScale : std_logic_vector(15 downto 0) := (others => '0');
   signal Parity : std_logic_vector(1 downto 0) := (others => '0');
   signal StopBit : std_logic_vector(1 downto 0) := (others => '0');
   signal DataBit : std_logic_vector(3 downto 0) := (others => '0');

 	--Outputs
   signal DataOut : std_logic_vector(7 downto 0);
   signal TX_Data : std_logic;
   signal DevStatus : std_logic_vector(7 downto 0);

   -- Clock period definitions
   constant Clk_period : time := 10 ns;

	signal	TransmitLine1	:	std_logic;
	signal	TransmitLine2	:	std_logic;
 
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: RS_232_core PORT MAP (
          Clk => Clk,
          Reset => Reset,
          DataIn => DataIn,
          DataOut => DataOut,
          RdDat => RdDat,
          WrDat => WrDat,
          RX_Data => RX_Data,
          TX_Data => TX_Data,
          PreScale => PreScale,
          Parity => Parity,
          StopBit => StopBit,
          DataBit => DataBit,
          DevStatus => DevStatus
        );
		  
   -- Clock process definitions
   Clk_process :process
   begin
		Clk <= '0';
		wait for Clk_period/2;
		Clk <= '1';
		wait for Clk_period/2;
   end process;
	
	transmit: process(Clk)
	begin
		if Clk'event and Clk = '1' then
			TransmitLine1 <= TX_Data;
			TransmitLine2 <= TransmitLine1;
			RX_Data <= TransmitLine2;
		end if;
	end process;

   -- Stimulus process
   stim_proc: process

	variable OutData	: std_logic_vector(7 downto 0) := X"AA"; 
	
   begin		
      -- hold reset state for 100 ns.
		Reset <= '1';
      wait for 100 ns;	
		Reset <= '0';
      wait for Clk_period*10;

		PreScale <= X"0004";
		Parity <= "00";
		StopBit <= "00";
		DataBit <= "1000";
		DataIn <= OutData;
		wait for 50ns;
		WrDat <= '1';
		wait for 30ns;
		WrDat <= '0';
      wait;
   end process;

END;
