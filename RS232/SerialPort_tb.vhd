--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   07:50:05 03/26/2018
-- Design Name:   
-- Module Name:   D:/Design/DKNP10/RS232/SerialPort_tb.vhd
-- Project Name:  SerialPort
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: Serial_Port
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY SerialPort_tb IS
END SerialPort_tb;
 
ARCHITECTURE behavior OF SerialPort_tb IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT Serial_Port
    PORT(
         Clk : IN  std_logic;
         ClkInn : IN  std_logic;
         Reset : IN  std_logic;
         PortIn : IN  std_logic_vector(31 downto 0);
         PortOut : OUT  std_logic_vector(31 downto 0);
         CfgIn : IN  std_logic_vector(31 downto 0);
         CfgOut : OUT  std_logic_vector(31 downto 0);
         RxData : IN  std_logic;
         TxData : OUT  std_logic;
         CmdDataRd : IN  std_logic;
         CmdDataWr : IN  std_logic;
         CmdCfgRd : IN  std_logic;
         CmdCfgWr : IN  std_logic
        );
    END COMPONENT;
    

   --Inputs
   signal Clk : std_logic := '0';
   signal ClkInn : std_logic := '0';
   signal Reset : std_logic := '0';
   signal PortIn : std_logic_vector(31 downto 0) := (others => '0');
   signal CfgIn : std_logic_vector(31 downto 0) := (others => '0');
   signal RxData : std_logic := '0';
   signal CmdDataRd : std_logic := '0';
   signal CmdDataWr : std_logic := '0';
   signal CmdCfgRd : std_logic := '0';
   signal CmdCfgWr : std_logic := '0';

 	--Outputs
   signal PortOut : std_logic_vector(31 downto 0);
   signal CfgOut : std_logic_vector(31 downto 0);
   signal TxData : std_logic;

   -- Clock period definitions
   constant Clk_period : time := 10 ns;
   constant ClkInn_period : time := 40 ns;

	signal	TransmitLine1	:	std_logic;
	signal	TransmitLine2	:	std_logic;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: Serial_Port PORT MAP (
          Clk => Clk,
          ClkInn => ClkInn,
          Reset => Reset,
          PortIn => PortIn,
          PortOut => PortOut,
          CfgIn => CfgIn,
          CfgOut => CfgOut,
          RxData => RxData,
          TxData => TxData,
          CmdDataRd => CmdDataRd,
          CmdDataWr => CmdDataWr,
          CmdCfgRd => CmdCfgRd,
          CmdCfgWr => CmdCfgWr
        );

   -- Clock process definitions
   Clk_process :process
   begin
		Clk <= '0';
		wait for Clk_period/2;
		Clk <= '1';
		wait for Clk_period/2;
   end process;
 
   ClkInn_process :process
   begin
		ClkInn <= '0';
		wait for ClkInn_period/2;
		ClkInn <= '1';
		wait for ClkInn_period/2;
   end process;
 
	transmit: process(ClkInn)
	begin
		if ClkInn'event and ClkInn = '1' then
			TransmitLine1 <= TxData;
			TransmitLine2 <= TransmitLine1;
			RxData <= TransmitLine2;
		end if;
	end process;

   -- Stimulus process
   stim_proc: process
   begin	
		Reset <= '1';
      -- hold reset state for 100 ns.
      wait for 100 ns;	
		Reset <= '0';
      wait for Clk_period*10;
		CfgIn <= X"00048000";
      wait for Clk_period*2;
		CmdCfgWr <= '1';
      wait for Clk_period;
		CmdCfgWr <= '0';
      wait for Clk_period*5;
		CmdCfgRd <= '1';
      wait for Clk_period;
		CmdCfgRd <= '0';
		PortIn <= X"07F055AA";
		wait for Clk_period*5;
		CmdDataWr <= '1';
      wait for Clk_period;
		CmdDataWr <= '0';
      wait for ClkInn_period*5;
		CmdDataRd <= '1';
      wait for Clk_period;
		CmdDataRd <= '0';
      wait for Clk_period;

      wait;
   end process;

END;
