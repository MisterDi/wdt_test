/**********************************************************************/
/*   ____  ____                                                       */
/*  /   /\/   /                                                       */
/* /___/  \  /                                                        */
/* \   \   \/                                                       */
/*  \   \        Copyright (c) 2003-2009 Xilinx, Inc.                */
/*  /   /          All Right Reserved.                                 */
/* /---/   /\                                                         */
/* \   \  /  \                                                      */
/*  \___\/\___\                                                    */
/***********************************************************************/

/* This file is designed for use with ISim build 0x7708f090 */

#define XSI_HIDE_SYMBOL_SPEC true
#include "xsi.h"
#include <memory.h>
#ifdef __GNUC__
#include <stdlib.h>
#else
#include <malloc.h>
#define alloca _alloca
#endif
static const char *ng0 = "C:/Design/Dknp10_io/RS232/Serial_Port.vhd";
extern char *IEEE_P_3620187407;



static void work_a_0096048784_3212880686_p_0(char *t0)
{
    char t19[16];
    char t22[16];
    char *t1;
    char *t2;
    unsigned char t3;
    unsigned char t4;
    char *t5;
    char *t6;
    char *t7;
    char *t8;
    unsigned char t9;
    unsigned char t10;
    char *t11;
    char *t12;
    unsigned char t13;
    unsigned char t14;
    char *t15;
    unsigned int t16;
    unsigned int t17;
    unsigned int t18;
    int t20;
    unsigned int t21;
    int t23;
    char *t24;
    char *t25;
    char *t26;
    static char *nl0[] = {&&LAB19, &&LAB20, &&LAB21, &&LAB22, &&LAB22, &&LAB22, &&LAB22};

LAB0:    xsi_set_current_line(185, ng0);
    t1 = (t0 + 1352U);
    t2 = *((char **)t1);
    t3 = *((unsigned char *)t2);
    t4 = (t3 == (unsigned char)3);
    if (t4 != 0)
        goto LAB2;

LAB4:    xsi_set_current_line(189, ng0);
    t1 = (t0 + 992U);
    t4 = xsi_signal_has_event(t1);
    if (t4 == 1)
        goto LAB8;

LAB9:    t3 = (unsigned char)0;

LAB10:    if (t3 != 0)
        goto LAB5;

LAB7:
LAB6:
LAB3:    t1 = (t0 + 9568);
    *((int *)t1) = 1;

LAB1:    return;
LAB2:    xsi_set_current_line(186, ng0);
    t1 = (t0 + 9680);
    t5 = (t1 + 56U);
    t6 = *((char **)t5);
    t7 = (t6 + 56U);
    t8 = *((char **)t7);
    *((unsigned char *)t8) = (unsigned char)0;
    xsi_driver_first_trans_fast(t1);
    xsi_set_current_line(187, ng0);
    t1 = (t0 + 9744);
    t2 = (t1 + 56U);
    t5 = *((char **)t2);
    t6 = (t5 + 56U);
    t7 = *((char **)t6);
    *((unsigned char *)t7) = (unsigned char)3;
    xsi_driver_first_trans_fast(t1);
    goto LAB3;

LAB5:    xsi_set_current_line(190, ng0);
    t2 = (t0 + 3912U);
    t6 = *((char **)t2);
    t2 = (t0 + 9808);
    t7 = (t2 + 56U);
    t8 = *((char **)t7);
    t11 = (t8 + 56U);
    t12 = *((char **)t11);
    memcpy(t12, t6, 32U);
    xsi_driver_first_trans_fast_port(t2);
    xsi_set_current_line(191, ng0);
    t1 = (t0 + 2472U);
    t2 = *((char **)t1);
    t3 = *((unsigned char *)t2);
    t4 = (t3 == (unsigned char)3);
    if (t4 != 0)
        goto LAB11;

LAB13:
LAB12:    xsi_set_current_line(198, ng0);
    t1 = (t0 + 3112U);
    t2 = *((char **)t1);
    t3 = *((unsigned char *)t2);
    t1 = (char *)((nl0) + t3);
    goto **((char **)t1);

LAB8:    t2 = (t0 + 1032U);
    t5 = *((char **)t2);
    t9 = *((unsigned char *)t5);
    t10 = (t9 == (unsigned char)3);
    t3 = t10;
    goto LAB10;

LAB11:    xsi_set_current_line(192, ng0);
    t1 = (t0 + 16240);
    t9 = (3U != 3U);
    if (t9 == 1)
        goto LAB14;

LAB15:    t6 = (t0 + 9872);
    t7 = (t6 + 56U);
    t8 = *((char **)t7);
    t11 = (t8 + 56U);
    t12 = *((char **)t11);
    memcpy(t12, t1, 3U);
    xsi_driver_first_trans_delta(t6, 5U, 3U, 0LL);
    xsi_set_current_line(193, ng0);
    t1 = (t0 + 16243);
    t3 = (2U != 2U);
    if (t3 == 1)
        goto LAB16;

LAB17:    t5 = (t0 + 9872);
    t6 = (t5 + 56U);
    t7 = *((char **)t6);
    t8 = (t7 + 56U);
    t11 = *((char **)t8);
    memcpy(t11, t1, 2U);
    xsi_driver_first_trans_delta(t5, 1U, 2U, 0LL);
    xsi_set_current_line(194, ng0);
    t1 = (t0 + 9744);
    t2 = (t1 + 56U);
    t5 = *((char **)t2);
    t6 = (t5 + 56U);
    t7 = *((char **)t6);
    *((unsigned char *)t7) = (unsigned char)3;
    xsi_driver_first_trans_fast(t1);
    goto LAB12;

LAB14:    xsi_size_not_matching(3U, 3U, 0);
    goto LAB15;

LAB16:    xsi_size_not_matching(2U, 2U, 0);
    goto LAB17;

LAB18:    xsi_set_current_line(226, ng0);
    t1 = (t0 + 6952U);
    t2 = *((char **)t1);
    t1 = (t0 + 10000);
    t5 = (t1 + 56U);
    t6 = *((char **)t5);
    t7 = (t6 + 56U);
    t8 = *((char **)t7);
    memcpy(t8, t2, 16U);
    xsi_driver_first_trans_delta(t1, 0U, 16U, 0LL);
    xsi_set_current_line(227, ng0);
    t1 = (t0 + 7112U);
    t2 = *((char **)t1);
    t1 = (t0 + 10000);
    t5 = (t1 + 56U);
    t6 = *((char **)t5);
    t7 = (t6 + 56U);
    t8 = *((char **)t7);
    memcpy(t8, t2, 4U);
    xsi_driver_first_trans_delta(t1, 16U, 4U, 0LL);
    xsi_set_current_line(228, ng0);
    t1 = (t0 + 7272U);
    t2 = *((char **)t1);
    t1 = (t0 + 10000);
    t5 = (t1 + 56U);
    t6 = *((char **)t5);
    t7 = (t6 + 56U);
    t8 = *((char **)t7);
    memcpy(t8, t2, 2U);
    xsi_driver_first_trans_delta(t1, 20U, 2U, 0LL);
    xsi_set_current_line(229, ng0);
    t1 = (t0 + 7432U);
    t2 = *((char **)t1);
    t1 = (t0 + 10000);
    t5 = (t1 + 56U);
    t6 = *((char **)t5);
    t7 = (t6 + 56U);
    t8 = *((char **)t7);
    memcpy(t8, t2, 2U);
    xsi_driver_first_trans_delta(t1, 22U, 2U, 0LL);
    xsi_set_current_line(230, ng0);
    t1 = (t0 + 6792U);
    t2 = *((char **)t1);
    t20 = (0 - 7);
    t16 = (t20 * -1);
    t17 = (1U * t16);
    t18 = (0 + t17);
    t1 = (t2 + t18);
    t3 = *((unsigned char *)t1);
    t5 = (t0 + 10000);
    t6 = (t5 + 56U);
    t7 = *((char **)t6);
    t8 = (t7 + 56U);
    t11 = *((char **)t8);
    *((unsigned char *)t11) = t3;
    xsi_driver_first_trans_delta(t5, 24U, 1, 0LL);
    xsi_set_current_line(231, ng0);
    t1 = (t0 + 5032U);
    t2 = *((char **)t1);
    t3 = *((unsigned char *)t2);
    t1 = (t0 + 10000);
    t5 = (t1 + 56U);
    t6 = *((char **)t5);
    t7 = (t6 + 56U);
    t8 = *((char **)t7);
    *((unsigned char *)t8) = t3;
    xsi_driver_first_trans_delta(t1, 25U, 1, 0LL);
    xsi_set_current_line(232, ng0);
    t1 = (t0 + 4872U);
    t2 = *((char **)t1);
    t3 = *((unsigned char *)t2);
    t1 = (t0 + 10000);
    t5 = (t1 + 56U);
    t6 = *((char **)t5);
    t7 = (t6 + 56U);
    t8 = *((char **)t7);
    *((unsigned char *)t8) = t3;
    xsi_driver_first_trans_delta(t1, 26U, 1, 0LL);
    xsi_set_current_line(233, ng0);
    t1 = (t0 + 6792U);
    t2 = *((char **)t1);
    t20 = (1 - 7);
    t16 = (t20 * -1);
    t17 = (1U * t16);
    t18 = (0 + t17);
    t1 = (t2 + t18);
    t3 = *((unsigned char *)t1);
    t5 = (t0 + 10000);
    t6 = (t5 + 56U);
    t7 = *((char **)t6);
    t8 = (t7 + 56U);
    t11 = *((char **)t8);
    *((unsigned char *)t11) = t3;
    xsi_driver_first_trans_delta(t5, 27U, 1, 0LL);
    xsi_set_current_line(234, ng0);
    t1 = (t0 + 5672U);
    t2 = *((char **)t1);
    t3 = *((unsigned char *)t2);
    t1 = (t0 + 10000);
    t5 = (t1 + 56U);
    t6 = *((char **)t5);
    t7 = (t6 + 56U);
    t8 = *((char **)t7);
    *((unsigned char *)t8) = t3;
    xsi_driver_first_trans_delta(t1, 28U, 1, 0LL);
    xsi_set_current_line(235, ng0);
    t1 = (t0 + 5512U);
    t2 = *((char **)t1);
    t3 = *((unsigned char *)t2);
    t1 = (t0 + 10000);
    t5 = (t1 + 56U);
    t6 = *((char **)t5);
    t7 = (t6 + 56U);
    t8 = *((char **)t7);
    *((unsigned char *)t8) = t3;
    xsi_driver_first_trans_delta(t1, 29U, 1, 0LL);
    xsi_set_current_line(236, ng0);
    t1 = (t0 + 6792U);
    t2 = *((char **)t1);
    t20 = (3 - 7);
    t16 = (t20 * -1);
    t17 = (1U * t16);
    t18 = (0 + t17);
    t1 = (t2 + t18);
    t3 = *((unsigned char *)t1);
    t5 = (t0 + 10000);
    t6 = (t5 + 56U);
    t7 = *((char **)t6);
    t8 = (t7 + 56U);
    t11 = *((char **)t8);
    *((unsigned char *)t11) = t3;
    xsi_driver_first_trans_delta(t5, 30U, 1, 0LL);
    xsi_set_current_line(237, ng0);
    t1 = (t0 + 6792U);
    t2 = *((char **)t1);
    t20 = (2 - 7);
    t16 = (t20 * -1);
    t17 = (1U * t16);
    t18 = (0 + t17);
    t1 = (t2 + t18);
    t3 = *((unsigned char *)t1);
    t5 = (t0 + 10000);
    t6 = (t5 + 56U);
    t7 = *((char **)t6);
    t8 = (t7 + 56U);
    t11 = *((char **)t8);
    *((unsigned char *)t11) = t3;
    xsi_driver_first_trans_delta(t5, 31U, 1, 0LL);
    xsi_set_current_line(238, ng0);
    t1 = (t0 + 2792U);
    t2 = *((char **)t1);
    t3 = *((unsigned char *)t2);
    t4 = (t3 == (unsigned char)3);
    if (t4 != 0)
        goto LAB38;

LAB40:
LAB39:    goto LAB6;

LAB19:    xsi_set_current_line(200, ng0);
    t5 = (t0 + 5032U);
    t6 = *((char **)t5);
    t9 = *((unsigned char *)t6);
    t10 = (t9 == (unsigned char)2);
    if (t10 == 1)
        goto LAB26;

LAB27:    t4 = (unsigned char)0;

LAB28:    if (t4 != 0)
        goto LAB23;

LAB25:
LAB24:    goto LAB18;

LAB20:    xsi_set_current_line(205, ng0);
    t1 = (t0 + 9936);
    t2 = (t1 + 56U);
    t5 = *((char **)t2);
    t6 = (t5 + 56U);
    t7 = *((char **)t6);
    *((unsigned char *)t7) = (unsigned char)2;
    xsi_driver_first_trans_fast(t1);
    xsi_set_current_line(206, ng0);
    t1 = (t0 + 9680);
    t2 = (t1 + 56U);
    t5 = *((char **)t2);
    t6 = (t5 + 56U);
    t7 = *((char **)t6);
    *((unsigned char *)t7) = (unsigned char)2;
    xsi_driver_first_trans_fast(t1);
    goto LAB18;

LAB21:    xsi_set_current_line(208, ng0);
    t1 = (t0 + 9680);
    t2 = (t1 + 56U);
    t5 = *((char **)t2);
    t6 = (t5 + 56U);
    t7 = *((char **)t6);
    *((unsigned char *)t7) = (unsigned char)0;
    xsi_driver_first_trans_fast(t1);
    xsi_set_current_line(209, ng0);
    t1 = (t0 + 3912U);
    t2 = *((char **)t1);
    t16 = (31 - 26);
    t17 = (t16 * 1U);
    t18 = (0 + t17);
    t1 = (t2 + t18);
    t5 = (t19 + 0U);
    t6 = (t5 + 0U);
    *((int *)t6) = 26;
    t6 = (t5 + 4U);
    *((int *)t6) = 24;
    t6 = (t5 + 8U);
    *((int *)t6) = -1;
    t20 = (24 - 26);
    t21 = (t20 * -1);
    t21 = (t21 + 1);
    t6 = (t5 + 12U);
    *((unsigned int *)t6) = t21;
    t6 = (t0 + 16245);
    t8 = (t22 + 0U);
    t11 = (t8 + 0U);
    *((int *)t11) = 0;
    t11 = (t8 + 4U);
    *((int *)t11) = 2;
    t11 = (t8 + 8U);
    *((int *)t11) = 1;
    t23 = (2 - 0);
    t21 = (t23 * 1);
    t21 = (t21 + 1);
    t11 = (t8 + 12U);
    *((unsigned int *)t11) = t21;
    t3 = ieee_std_logic_unsigned_equal_stdv_stdv(IEEE_P_3620187407, t1, t19, t6, t22);
    if (t3 != 0)
        goto LAB29;

LAB31:
LAB30:    xsi_set_current_line(213, ng0);
    t1 = (t0 + 3912U);
    t2 = *((char **)t1);
    t16 = (31 - 26);
    t17 = (t16 * 1U);
    t18 = (0 + t17);
    t1 = (t2 + t18);
    t5 = (t19 + 0U);
    t6 = (t5 + 0U);
    *((int *)t6) = 26;
    t6 = (t5 + 4U);
    *((int *)t6) = 24;
    t6 = (t5 + 8U);
    *((int *)t6) = -1;
    t20 = (24 - 26);
    t21 = (t20 * -1);
    t21 = (t21 + 1);
    t6 = (t5 + 12U);
    *((unsigned int *)t6) = t21;
    t6 = (t0 + 16248);
    t8 = (t22 + 0U);
    t11 = (t8 + 0U);
    *((int *)t11) = 0;
    t11 = (t8 + 4U);
    *((int *)t11) = 2;
    t11 = (t8 + 8U);
    *((int *)t11) = 1;
    t23 = (2 - 0);
    t21 = (t23 * 1);
    t21 = (t21 + 1);
    t11 = (t8 + 12U);
    *((unsigned int *)t11) = t21;
    t3 = ieee_std_logic_unsigned_equal_stdv_stdv(IEEE_P_3620187407, t1, t19, t6, t22);
    if (t3 != 0)
        goto LAB32;

LAB34:
LAB33:    xsi_set_current_line(217, ng0);
    t1 = (t0 + 3912U);
    t2 = *((char **)t1);
    t16 = (31 - 26);
    t17 = (t16 * 1U);
    t18 = (0 + t17);
    t1 = (t2 + t18);
    t5 = (t19 + 0U);
    t6 = (t5 + 0U);
    *((int *)t6) = 26;
    t6 = (t5 + 4U);
    *((int *)t6) = 24;
    t6 = (t5 + 8U);
    *((int *)t6) = -1;
    t20 = (24 - 26);
    t21 = (t20 * -1);
    t21 = (t21 + 1);
    t6 = (t5 + 12U);
    *((unsigned int *)t6) = t21;
    t6 = (t0 + 16251);
    t8 = (t22 + 0U);
    t11 = (t8 + 0U);
    *((int *)t11) = 0;
    t11 = (t8 + 4U);
    *((int *)t11) = 2;
    t11 = (t8 + 8U);
    *((int *)t11) = 1;
    t23 = (2 - 0);
    t21 = (t23 * 1);
    t21 = (t21 + 1);
    t11 = (t8 + 12U);
    *((unsigned int *)t11) = t21;
    t3 = ieee_std_logic_unsigned_equal_stdv_stdv(IEEE_P_3620187407, t1, t19, t6, t22);
    if (t3 != 0)
        goto LAB35;

LAB37:
LAB36:    goto LAB18;

LAB22:    goto LAB18;

LAB23:    xsi_set_current_line(201, ng0);
    t5 = (t0 + 9936);
    t8 = (t5 + 56U);
    t11 = *((char **)t8);
    t12 = (t11 + 56U);
    t15 = *((char **)t12);
    *((unsigned char *)t15) = (unsigned char)3;
    xsi_driver_first_trans_fast(t5);
    xsi_set_current_line(202, ng0);
    t1 = (t0 + 9680);
    t2 = (t1 + 56U);
    t5 = *((char **)t2);
    t6 = (t5 + 56U);
    t7 = *((char **)t6);
    *((unsigned char *)t7) = (unsigned char)1;
    xsi_driver_first_trans_fast(t1);
    goto LAB24;

LAB26:    t5 = (t0 + 4712U);
    t7 = *((char **)t5);
    t13 = *((unsigned char *)t7);
    t14 = (t13 == (unsigned char)3);
    t4 = t14;
    goto LAB28;

LAB29:    xsi_set_current_line(210, ng0);
    t11 = (t0 + 4552U);
    t12 = *((char **)t11);
    t11 = (t0 + 9872);
    t15 = (t11 + 56U);
    t24 = *((char **)t15);
    t25 = (t24 + 56U);
    t26 = *((char **)t25);
    memcpy(t26, t12, 8U);
    xsi_driver_first_trans_delta(t11, 24U, 8U, 0LL);
    xsi_set_current_line(211, ng0);
    t1 = (t0 + 9872);
    t2 = (t1 + 56U);
    t5 = *((char **)t2);
    t6 = (t5 + 56U);
    t7 = *((char **)t6);
    *((unsigned char *)t7) = (unsigned char)3;
    xsi_driver_first_trans_delta(t1, 7U, 1, 0LL);
    goto LAB30;

LAB32:    xsi_set_current_line(214, ng0);
    t11 = (t0 + 4552U);
    t12 = *((char **)t11);
    t11 = (t0 + 9872);
    t15 = (t11 + 56U);
    t24 = *((char **)t15);
    t25 = (t24 + 56U);
    t26 = *((char **)t25);
    memcpy(t26, t12, 8U);
    xsi_driver_first_trans_delta(t11, 16U, 8U, 0LL);
    xsi_set_current_line(215, ng0);
    t1 = (t0 + 9872);
    t2 = (t1 + 56U);
    t5 = *((char **)t2);
    t6 = (t5 + 56U);
    t7 = *((char **)t6);
    *((unsigned char *)t7) = (unsigned char)3;
    xsi_driver_first_trans_delta(t1, 6U, 1, 0LL);
    goto LAB33;

LAB35:    xsi_set_current_line(218, ng0);
    t11 = (t0 + 4552U);
    t12 = *((char **)t11);
    t11 = (t0 + 9872);
    t15 = (t11 + 56U);
    t24 = *((char **)t15);
    t25 = (t24 + 56U);
    t26 = *((char **)t25);
    memcpy(t26, t12, 8U);
    xsi_driver_first_trans_delta(t11, 8U, 8U, 0LL);
    xsi_set_current_line(219, ng0);
    t1 = (t0 + 9872);
    t2 = (t1 + 56U);
    t5 = *((char **)t2);
    t6 = (t5 + 56U);
    t7 = *((char **)t6);
    *((unsigned char *)t7) = (unsigned char)3;
    xsi_driver_first_trans_delta(t1, 5U, 1, 0LL);
    xsi_set_current_line(220, ng0);
    t1 = (t0 + 9744);
    t2 = (t1 + 56U);
    t5 = *((char **)t2);
    t6 = (t5 + 56U);
    t7 = *((char **)t6);
    *((unsigned char *)t7) = (unsigned char)2;
    xsi_driver_first_trans_fast(t1);
    goto LAB36;

LAB38:    xsi_set_current_line(239, ng0);
    t1 = (t0 + 16254);
    t9 = (2U != 2U);
    if (t9 == 1)
        goto LAB41;

LAB42:    t6 = (t0 + 10000);
    t7 = (t6 + 56U);
    t8 = *((char **)t7);
    t11 = (t8 + 56U);
    t12 = *((char **)t11);
    memcpy(t12, t1, 2U);
    xsi_driver_first_trans_delta(t6, 30U, 2U, 0LL);
    goto LAB39;

LAB41:    xsi_size_not_matching(2U, 2U, 0);
    goto LAB42;

}

static void work_a_0096048784_3212880686_p_1(char *t0)
{
    char *t1;
    char *t2;
    unsigned char t3;
    unsigned char t4;
    char *t5;
    char *t6;
    char *t7;
    char *t8;
    unsigned char t9;
    unsigned char t10;
    unsigned char t11;
    unsigned char t12;
    char *t13;
    char *t14;
    char *t15;
    unsigned int t16;
    unsigned int t17;
    unsigned int t18;
    int t19;
    static char *nl0[] = {&&LAB18, &&LAB22, &&LAB22, &&LAB22, &&LAB19, &&LAB20, &&LAB21};

LAB0:    xsi_set_current_line(247, ng0);
    t1 = (t0 + 1352U);
    t2 = *((char **)t1);
    t3 = *((unsigned char *)t2);
    t4 = (t3 == (unsigned char)3);
    if (t4 != 0)
        goto LAB2;

LAB4:    xsi_set_current_line(250, ng0);
    t1 = (t0 + 992U);
    t4 = xsi_signal_has_event(t1);
    if (t4 == 1)
        goto LAB8;

LAB9:    t3 = (unsigned char)0;

LAB10:    if (t3 != 0)
        goto LAB5;

LAB7:
LAB6:
LAB3:    t1 = (t0 + 9584);
    *((int *)t1) = 1;

LAB1:    return;
LAB2:    xsi_set_current_line(248, ng0);
    t1 = (t0 + 10064);
    t5 = (t1 + 56U);
    t6 = *((char **)t5);
    t7 = (t6 + 56U);
    t8 = *((char **)t7);
    *((unsigned char *)t8) = (unsigned char)0;
    xsi_driver_first_trans_fast(t1);
    goto LAB3;

LAB5:    xsi_set_current_line(251, ng0);
    t2 = (t0 + 2632U);
    t6 = *((char **)t2);
    t11 = *((unsigned char *)t6);
    t12 = (t11 == (unsigned char)3);
    if (t12 != 0)
        goto LAB11;

LAB13:
LAB12:    xsi_set_current_line(255, ng0);
    t1 = (t0 + 2952U);
    t2 = *((char **)t1);
    t3 = *((unsigned char *)t2);
    t4 = (t3 == (unsigned char)3);
    if (t4 != 0)
        goto LAB14;

LAB16:
LAB15:    xsi_set_current_line(262, ng0);
    t1 = (t0 + 3272U);
    t2 = *((char **)t1);
    t3 = *((unsigned char *)t2);
    t1 = (char *)((nl0) + t3);
    goto **((char **)t1);

LAB8:    t2 = (t0 + 1032U);
    t5 = *((char **)t2);
    t9 = *((unsigned char *)t5);
    t10 = (t9 == (unsigned char)3);
    t3 = t10;
    goto LAB10;

LAB11:    xsi_set_current_line(252, ng0);
    t2 = (t0 + 1512U);
    t7 = *((char **)t2);
    t2 = (t0 + 10128);
    t8 = (t2 + 56U);
    t13 = *((char **)t8);
    t14 = (t13 + 56U);
    t15 = *((char **)t14);
    memcpy(t15, t7, 32U);
    xsi_driver_first_trans_fast(t2);
    xsi_set_current_line(253, ng0);
    t1 = (t0 + 10064);
    t2 = (t1 + 56U);
    t5 = *((char **)t2);
    t6 = (t5 + 56U);
    t7 = *((char **)t6);
    *((unsigned char *)t7) = (unsigned char)4;
    xsi_driver_first_trans_fast(t1);
    goto LAB12;

LAB14:    xsi_set_current_line(256, ng0);
    t1 = (t0 + 1832U);
    t5 = *((char **)t1);
    t16 = (31 - 31);
    t17 = (t16 * 1U);
    t18 = (0 + t17);
    t1 = (t5 + t18);
    t6 = (t0 + 10192);
    t7 = (t6 + 56U);
    t8 = *((char **)t7);
    t13 = (t8 + 56U);
    t14 = *((char **)t13);
    memcpy(t14, t1, 16U);
    xsi_driver_first_trans_fast(t6);
    xsi_set_current_line(257, ng0);
    t1 = (t0 + 1832U);
    t2 = *((char **)t1);
    t16 = (31 - 15);
    t17 = (t16 * 1U);
    t18 = (0 + t17);
    t1 = (t2 + t18);
    t5 = (t0 + 10256);
    t6 = (t5 + 56U);
    t7 = *((char **)t6);
    t8 = (t7 + 56U);
    t13 = *((char **)t8);
    memcpy(t13, t1, 4U);
    xsi_driver_first_trans_fast(t5);
    xsi_set_current_line(258, ng0);
    t1 = (t0 + 1832U);
    t2 = *((char **)t1);
    t16 = (31 - 11);
    t17 = (t16 * 1U);
    t18 = (0 + t17);
    t1 = (t2 + t18);
    t5 = (t0 + 10320);
    t6 = (t5 + 56U);
    t7 = *((char **)t6);
    t8 = (t7 + 56U);
    t13 = *((char **)t8);
    memcpy(t13, t1, 2U);
    xsi_driver_first_trans_fast(t5);
    xsi_set_current_line(259, ng0);
    t1 = (t0 + 1832U);
    t2 = *((char **)t1);
    t16 = (31 - 9);
    t17 = (t16 * 1U);
    t18 = (0 + t17);
    t1 = (t2 + t18);
    t5 = (t0 + 10384);
    t6 = (t5 + 56U);
    t7 = *((char **)t6);
    t8 = (t7 + 56U);
    t13 = *((char **)t8);
    memcpy(t13, t1, 2U);
    xsi_driver_first_trans_fast(t5);
    xsi_set_current_line(260, ng0);
    t1 = (t0 + 1832U);
    t2 = *((char **)t1);
    t19 = (0 - 31);
    t16 = (t19 * -1);
    t17 = (1U * t16);
    t18 = (0 + t17);
    t1 = (t2 + t18);
    t3 = *((unsigned char *)t1);
    t5 = (t0 + 10448);
    t6 = (t5 + 56U);
    t7 = *((char **)t6);
    t8 = (t7 + 56U);
    t13 = *((char **)t8);
    *((unsigned char *)t13) = t3;
    xsi_driver_first_trans_fast(t5);
    goto LAB15;

LAB17:    goto LAB6;

LAB18:    xsi_set_current_line(264, ng0);
    t5 = (t0 + 10512);
    t6 = (t5 + 56U);
    t7 = *((char **)t6);
    t8 = (t7 + 56U);
    t13 = *((char **)t8);
    *((unsigned char *)t13) = (unsigned char)2;
    xsi_driver_first_trans_fast(t5);
    goto LAB17;

LAB19:    xsi_set_current_line(266, ng0);
    t1 = (t0 + 3752U);
    t2 = *((char **)t1);
    t16 = (31 - 7);
    t17 = (t16 * 1U);
    t18 = (0 + t17);
    t1 = (t2 + t18);
    t5 = (t0 + 10576);
    t6 = (t5 + 56U);
    t7 = *((char **)t6);
    t8 = (t7 + 56U);
    t13 = *((char **)t8);
    memcpy(t13, t1, 8U);
    xsi_driver_first_trans_fast(t5);
    xsi_set_current_line(267, ng0);
    t1 = (t0 + 10512);
    t2 = (t1 + 56U);
    t5 = *((char **)t2);
    t6 = (t5 + 56U);
    t7 = *((char **)t6);
    *((unsigned char *)t7) = (unsigned char)3;
    xsi_driver_first_trans_fast(t1);
    xsi_set_current_line(268, ng0);
    t1 = (t0 + 3752U);
    t2 = *((char **)t1);
    t19 = (25 - 31);
    t16 = (t19 * -1);
    t17 = (1U * t16);
    t18 = (0 + t17);
    t1 = (t2 + t18);
    t3 = *((unsigned char *)t1);
    t4 = (t3 == (unsigned char)3);
    if (t4 != 0)
        goto LAB23;

LAB25:    xsi_set_current_line(271, ng0);
    t1 = (t0 + 10064);
    t2 = (t1 + 56U);
    t5 = *((char **)t2);
    t6 = (t5 + 56U);
    t7 = *((char **)t6);
    *((unsigned char *)t7) = (unsigned char)0;
    xsi_driver_first_trans_fast(t1);

LAB24:    goto LAB17;

LAB20:    xsi_set_current_line(274, ng0);
    t1 = (t0 + 3752U);
    t2 = *((char **)t1);
    t16 = (31 - 15);
    t17 = (t16 * 1U);
    t18 = (0 + t17);
    t1 = (t2 + t18);
    t5 = (t0 + 10576);
    t6 = (t5 + 56U);
    t7 = *((char **)t6);
    t8 = (t7 + 56U);
    t13 = *((char **)t8);
    memcpy(t13, t1, 8U);
    xsi_driver_first_trans_fast(t5);
    xsi_set_current_line(275, ng0);
    t1 = (t0 + 3752U);
    t2 = *((char **)t1);
    t19 = (26 - 31);
    t16 = (t19 * -1);
    t17 = (1U * t16);
    t18 = (0 + t17);
    t1 = (t2 + t18);
    t3 = *((unsigned char *)t1);
    t4 = (t3 == (unsigned char)3);
    if (t4 != 0)
        goto LAB26;

LAB28:    xsi_set_current_line(278, ng0);
    t1 = (t0 + 10064);
    t2 = (t1 + 56U);
    t5 = *((char **)t2);
    t6 = (t5 + 56U);
    t7 = *((char **)t6);
    *((unsigned char *)t7) = (unsigned char)0;
    xsi_driver_first_trans_fast(t1);

LAB27:    goto LAB17;

LAB21:    xsi_set_current_line(281, ng0);
    t1 = (t0 + 3752U);
    t2 = *((char **)t1);
    t16 = (31 - 23);
    t17 = (t16 * 1U);
    t18 = (0 + t17);
    t1 = (t2 + t18);
    t5 = (t0 + 10576);
    t6 = (t5 + 56U);
    t7 = *((char **)t6);
    t8 = (t7 + 56U);
    t13 = *((char **)t8);
    memcpy(t13, t1, 8U);
    xsi_driver_first_trans_fast(t5);
    xsi_set_current_line(282, ng0);
    t1 = (t0 + 10064);
    t2 = (t1 + 56U);
    t5 = *((char **)t2);
    t6 = (t5 + 56U);
    t7 = *((char **)t6);
    *((unsigned char *)t7) = (unsigned char)0;
    xsi_driver_first_trans_fast(t1);
    goto LAB17;

LAB22:    goto LAB17;

LAB23:    xsi_set_current_line(269, ng0);
    t5 = (t0 + 10064);
    t6 = (t5 + 56U);
    t7 = *((char **)t6);
    t8 = (t7 + 56U);
    t13 = *((char **)t8);
    *((unsigned char *)t13) = (unsigned char)5;
    xsi_driver_first_trans_fast(t5);
    goto LAB24;

LAB26:    xsi_set_current_line(276, ng0);
    t5 = (t0 + 10064);
    t6 = (t5 + 56U);
    t7 = *((char **)t6);
    t8 = (t7 + 56U);
    t13 = *((char **)t8);
    *((unsigned char *)t13) = (unsigned char)6;
    xsi_driver_first_trans_fast(t5);
    goto LAB27;

}

static void work_a_0096048784_3212880686_p_2(char *t0)
{
    char *t1;
    char *t2;
    unsigned char t3;
    unsigned char t4;
    char *t5;
    char *t6;
    char *t7;
    char *t8;
    unsigned char t9;
    unsigned char t10;
    unsigned char t11;
    int t12;
    unsigned int t13;
    unsigned int t14;
    unsigned int t15;
    unsigned char t16;
    unsigned char t17;
    char *t18;
    char *t19;
    char *t20;
    char *t21;
    char *t22;
    static char *nl0[] = {&&LAB12, &&LAB13, &&LAB14, &&LAB15, &&LAB15, &&LAB15, &&LAB15};
    static char *nl1[] = {&&LAB20, &&LAB23, &&LAB23, &&LAB23, &&LAB21, &&LAB22, &&LAB23};

LAB0:    xsi_set_current_line(291, ng0);
    t1 = (t0 + 1352U);
    t2 = *((char **)t1);
    t3 = *((unsigned char *)t2);
    t4 = (t3 == (unsigned char)3);
    if (t4 != 0)
        goto LAB2;

LAB4:    xsi_set_current_line(297, ng0);
    t1 = (t0 + 1152U);
    t4 = xsi_signal_has_event(t1);
    if (t4 == 1)
        goto LAB8;

LAB9:    t3 = (unsigned char)0;

LAB10:    if (t3 != 0)
        goto LAB5;

LAB7:
LAB6:
LAB3:    t1 = (t0 + 9600);
    *((int *)t1) = 1;

LAB1:    return;
LAB2:    xsi_set_current_line(292, ng0);
    t1 = (t0 + 10640);
    t5 = (t1 + 56U);
    t6 = *((char **)t5);
    t7 = (t6 + 56U);
    t8 = *((char **)t7);
    *((unsigned char *)t8) = (unsigned char)0;
    xsi_driver_first_trans_fast(t1);
    xsi_set_current_line(293, ng0);
    t1 = (t0 + 10704);
    t2 = (t1 + 56U);
    t5 = *((char **)t2);
    t6 = (t5 + 56U);
    t7 = *((char **)t6);
    *((unsigned char *)t7) = (unsigned char)0;
    xsi_driver_first_trans_fast(t1);
    xsi_set_current_line(294, ng0);
    t1 = (t0 + 10768);
    t2 = (t1 + 56U);
    t5 = *((char **)t2);
    t6 = (t5 + 56U);
    t7 = *((char **)t6);
    *((unsigned char *)t7) = (unsigned char)2;
    xsi_driver_first_trans_fast(t1);
    xsi_set_current_line(295, ng0);
    t1 = (t0 + 10832);
    t2 = (t1 + 56U);
    t5 = *((char **)t2);
    t6 = (t5 + 56U);
    t7 = *((char **)t6);
    *((unsigned char *)t7) = (unsigned char)2;
    xsi_driver_first_trans_fast(t1);
    goto LAB3;

LAB5:    xsi_set_current_line(298, ng0);
    t2 = (t0 + 3432U);
    t6 = *((char **)t2);
    t11 = *((unsigned char *)t6);
    t2 = (char *)((nl0) + t11);
    goto **((char **)t2);

LAB8:    t2 = (t0 + 1192U);
    t5 = *((char **)t2);
    t9 = *((unsigned char *)t5);
    t10 = (t9 == (unsigned char)3);
    t3 = t10;
    goto LAB10;

LAB11:    xsi_set_current_line(314, ng0);
    t1 = (t0 + 3592U);
    t2 = *((char **)t1);
    t3 = *((unsigned char *)t2);
    t1 = (char *)((nl1) + t3);
    goto **((char **)t1);

LAB12:    xsi_set_current_line(300, ng0);
    t7 = (t0 + 6792U);
    t8 = *((char **)t7);
    t12 = (4 - 7);
    t13 = (t12 * -1);
    t14 = (1U * t13);
    t15 = (0 + t14);
    t7 = (t8 + t15);
    t16 = *((unsigned char *)t7);
    t17 = (t16 == (unsigned char)3);
    if (t17 != 0)
        goto LAB16;

LAB18:
LAB17:    goto LAB11;

LAB13:    xsi_set_current_line(305, ng0);
    t1 = (t0 + 10640);
    t2 = (t1 + 56U);
    t5 = *((char **)t2);
    t6 = (t5 + 56U);
    t7 = *((char **)t6);
    *((unsigned char *)t7) = (unsigned char)2;
    xsi_driver_first_trans_fast(t1);
    xsi_set_current_line(306, ng0);
    t1 = (t0 + 10896);
    t2 = (t1 + 56U);
    t5 = *((char **)t2);
    t6 = (t5 + 56U);
    t7 = *((char **)t6);
    *((unsigned char *)t7) = (unsigned char)3;
    xsi_driver_first_trans_fast(t1);
    xsi_set_current_line(307, ng0);
    t1 = (t0 + 10832);
    t2 = (t1 + 56U);
    t5 = *((char **)t2);
    t6 = (t5 + 56U);
    t7 = *((char **)t6);
    *((unsigned char *)t7) = (unsigned char)2;
    xsi_driver_first_trans_fast(t1);
    goto LAB11;

LAB14:    xsi_set_current_line(309, ng0);
    t1 = (t0 + 10640);
    t2 = (t1 + 56U);
    t5 = *((char **)t2);
    t6 = (t5 + 56U);
    t7 = *((char **)t6);
    *((unsigned char *)t7) = (unsigned char)0;
    xsi_driver_first_trans_fast(t1);
    xsi_set_current_line(310, ng0);
    t1 = (t0 + 10896);
    t2 = (t1 + 56U);
    t5 = *((char **)t2);
    t6 = (t5 + 56U);
    t7 = *((char **)t6);
    *((unsigned char *)t7) = (unsigned char)2;
    xsi_driver_first_trans_fast(t1);
    goto LAB11;

LAB15:    goto LAB11;

LAB16:    xsi_set_current_line(301, ng0);
    t18 = (t0 + 10640);
    t19 = (t18 + 56U);
    t20 = *((char **)t19);
    t21 = (t20 + 56U);
    t22 = *((char **)t21);
    *((unsigned char *)t22) = (unsigned char)1;
    xsi_driver_first_trans_fast(t18);
    xsi_set_current_line(302, ng0);
    t1 = (t0 + 10832);
    t2 = (t1 + 56U);
    t5 = *((char **)t2);
    t6 = (t5 + 56U);
    t7 = *((char **)t6);
    *((unsigned char *)t7) = (unsigned char)3;
    xsi_driver_first_trans_fast(t1);
    goto LAB17;

LAB19:    goto LAB6;

LAB20:    xsi_set_current_line(316, ng0);
    t5 = (t0 + 5672U);
    t6 = *((char **)t5);
    t9 = *((unsigned char *)t6);
    t10 = (t9 == (unsigned char)2);
    if (t10 == 1)
        goto LAB27;

LAB28:    t4 = (unsigned char)0;

LAB29:    if (t4 != 0)
        goto LAB24;

LAB26:
LAB25:    goto LAB19;

LAB21:    xsi_set_current_line(321, ng0);
    t1 = (t0 + 10704);
    t2 = (t1 + 56U);
    t5 = *((char **)t2);
    t6 = (t5 + 56U);
    t7 = *((char **)t6);
    *((unsigned char *)t7) = (unsigned char)5;
    xsi_driver_first_trans_fast(t1);
    xsi_set_current_line(322, ng0);
    t1 = (t0 + 10960);
    t2 = (t1 + 56U);
    t5 = *((char **)t2);
    t6 = (t5 + 56U);
    t7 = *((char **)t6);
    *((unsigned char *)t7) = (unsigned char)2;
    xsi_driver_first_trans_fast(t1);
    xsi_set_current_line(323, ng0);
    t1 = (t0 + 10768);
    t2 = (t1 + 56U);
    t5 = *((char **)t2);
    t6 = (t5 + 56U);
    t7 = *((char **)t6);
    *((unsigned char *)t7) = (unsigned char)3;
    xsi_driver_first_trans_fast(t1);
    goto LAB19;

LAB22:    xsi_set_current_line(325, ng0);
    t1 = (t0 + 6792U);
    t2 = *((char **)t1);
    t12 = (1 - 7);
    t13 = (t12 * -1);
    t14 = (1U * t13);
    t15 = (0 + t14);
    t1 = (t2 + t15);
    t3 = *((unsigned char *)t1);
    t4 = (t3 == (unsigned char)3);
    if (t4 != 0)
        goto LAB30;

LAB32:
LAB31:    xsi_set_current_line(328, ng0);
    t1 = (t0 + 10768);
    t2 = (t1 + 56U);
    t5 = *((char **)t2);
    t6 = (t5 + 56U);
    t7 = *((char **)t6);
    *((unsigned char *)t7) = (unsigned char)2;
    xsi_driver_first_trans_fast(t1);
    goto LAB19;

LAB23:    goto LAB19;

LAB24:    xsi_set_current_line(317, ng0);
    t8 = (t0 + 10704);
    t18 = (t8 + 56U);
    t19 = *((char **)t18);
    t20 = (t19 + 56U);
    t21 = *((char **)t20);
    *((unsigned char *)t21) = (unsigned char)4;
    xsi_driver_first_trans_fast(t8);
    xsi_set_current_line(318, ng0);
    t1 = (t0 + 10960);
    t2 = (t1 + 56U);
    t5 = *((char **)t2);
    t6 = (t5 + 56U);
    t7 = *((char **)t6);
    *((unsigned char *)t7) = (unsigned char)3;
    xsi_driver_first_trans_fast(t1);
    goto LAB25;

LAB27:    t5 = (t0 + 6792U);
    t7 = *((char **)t5);
    t12 = (1 - 7);
    t13 = (t12 * -1);
    t14 = (1U * t13);
    t15 = (0 + t14);
    t5 = (t7 + t15);
    t11 = *((unsigned char *)t5);
    t16 = (t11 == (unsigned char)2);
    t4 = t16;
    goto LAB29;

LAB30:    xsi_set_current_line(326, ng0);
    t5 = (t0 + 10704);
    t6 = (t5 + 56U);
    t7 = *((char **)t6);
    t8 = (t7 + 56U);
    t18 = *((char **)t8);
    *((unsigned char *)t18) = (unsigned char)0;
    xsi_driver_first_trans_fast(t5);
    goto LAB31;

}


extern void work_a_0096048784_3212880686_init()
{
	static char *pe[] = {(void *)work_a_0096048784_3212880686_p_0,(void *)work_a_0096048784_3212880686_p_1,(void *)work_a_0096048784_3212880686_p_2};
	xsi_register_didat("work_a_0096048784_3212880686", "isim/SerialPort_tb_isim_beh.exe.sim/work/a_0096048784_3212880686.didat");
	xsi_register_executes(pe);
}
