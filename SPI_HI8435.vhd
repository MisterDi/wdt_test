----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    21:13:35 03/16/2018 
-- Design Name: 
-- Module Name:    SPI_HI8435 - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

library UNISIM;
use UNISIM.Vcomponents.ALL;

entity SPI_HI8435 is
	Port ( 
		RESET : in  STD_LOGIC;
		CLK : in  STD_LOGIC;
		
		DIN : in  STD_LOGIC_VECTOR (39 downto 0);
		DOUT : out  STD_LOGIC_VECTOR (31 downto 0);
		BITS : in  STD_LOGIC_VECTOR (6 downto 0);
		EN : in  STD_LOGIC;
		DONE : out  STD_LOGIC;
		SCK : out  STD_LOGIC;
		MISO : in  STD_LOGIC;
		MOSI : out  STD_LOGIC
	);
end SPI_HI8435;

architecture Behavioral of SPI_HI8435 is

type spi_state_type is (
	SPI_WAIT,
	SPI_W_L,
	SPI_W_H,
	SPI_W_DONE
);
signal spi_state : spi_state_type;

signal i_DIN : STD_LOGIC_VECTOR (39 downto 0);
signal i_DOUT : STD_LOGIC_VECTOR (31 downto 0);
signal bits_cnt : STD_LOGIC_VECTOR (6 downto 0);

begin

DRV: process (CLK, RESET)
begin
	if RESET = '1' then
		SCK <= '0';
		MOSI <= '0';
		DONE <= '0';
		spi_state <= SPI_WAIT;
	elsif CLK'event and CLK = '1' then
		case (spi_state) is
			when SPI_WAIT =>
				if EN = '1' then
					i_DIN <= DIN;
					bits_cnt <= BITS;
					spi_state <= SPI_W_L;
				end if;
				
			when SPI_W_L =>
				if bits_cnt = "0000000" then
					spi_state <= SPI_W_DONE;
					DOUT <= i_DOUT;
				else
					SCK <= '0';
					MOSI <= i_DIN(39);
					i_DIN <= i_DIN(38 downto 0) & '0';
					spi_state <= SPI_W_H;
					bits_cnt <= bits_cnt - 1;
				end if;
			
			when SPI_W_H =>
				SCK <= '1';
				spi_state <= SPI_W_L;
				i_DOUT <= i_DOUT(30 downto 0) & MISO;
			
			when SPI_W_DONE =>
				if EN = '1' then
					DONE <= '1';
				else
					DONE <= '0';
					spi_state <= SPI_WAIT;
				end if;
				
			when others =>
			
		end case;
	end if;
end process;
end Behavioral;

