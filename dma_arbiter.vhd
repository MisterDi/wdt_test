----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    22:32:36 03/23/2018 
-- Design Name: 
-- Module Name:    dma_arbiter - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;

package dma_pack is
    type slv64_t is array(integer range<>) of std_logic_vector(63 DOWNTO 0);
	 type slv34_t is array(integer range<>) of std_logic_vector(33 DOWNTO 0);
	 type slv32_t is array(integer range<>) of std_logic_vector(31 DOWNTO 0);
	 type slv11_t is array(integer range<>) of std_logic_vector(10 DOWNTO 0);
end package;

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

library UNISIM;
use UNISIM.Vcomponents.ALL;

use work.dma_pack.all;

entity dma_arbiter is
	generic (
        clients : integer := 1;
		  GNT_CNT_size : integer := 0
    );
	Port (
		RESET : in  STD_LOGIC;
		RESET_CLK : in STD_LOGIC;
		
		DMA_CLK : in  STD_LOGIC;
		
		-- Connection to DMA controller
		start_address            : out STD_LOGIC_VECTOR(63 DOWNTO 0);
		pci_dma_ready            : in  STD_LOGIC;
		dma_cfg_address_set      : out STD_LOGIC;
		
		dma_cmd                  : out STD_LOGIC_VECTOR(33 DOWNTO 0);
		dma_cmd_rd_en            : in  STD_LOGIC;
		dma_cmd_empty            : out STD_LOGIC;
		
		dma_data                 : out STD_LOGIC_VECTOR(31 DOWNTO 0);
		dma_data_rd_en           : in  STD_LOGIC;
		dma_data_rd_data_count   : out STD_LOGIC_VECTOR(10 DOWNTO 0);
		
		-- Connection to slaves
		IN_CLK : in  STD_LOGIC_VECTOR (clients-1 downto 0) := (others => '0');
		REQ : in  STD_LOGIC_VECTOR (clients-1 downto 0) := (others => '0');
		GNT : out  STD_LOGIC_VECTOR (clients-1 downto 0);

		s_start_address          : in  slv64_t(clients-1 downto 0) := (others => (others => '0'));
		s_pci_dma_ready          : out STD_LOGIC_VECTOR(clients-1 downto 0) := (others => '0');
		s_dma_cfg_address_set    : in  STD_LOGIC_VECTOR(clients-1 downto 0) := (others => '0');
		
		dma_data_a               : in  slv32_t(clients-1 downto 0) := (others => (others => '0'));
		dma_data_rd_en_a         : out STD_LOGIC_VECTOR(clients-1 downto 0) := (others => '0');
		dma_data_rd_data_count_a : in  slv11_t(clients-1 downto 0) := (others => (others => '0'));
		
		dma_cmd_a                : in  slv34_t(clients-1 downto 0) := (others => (others => '0'));
		dma_cmd_rd_en_a          : out STD_LOGIC_VECTOR(clients-1 downto 0);
		dma_cmd_empty_a          : in  STD_LOGIC_VECTOR(clients-1 downto 0) := (others => '0')
	);
end dma_arbiter;

architecture Behavioral of dma_arbiter is

signal i_REQ : STD_LOGIC_VECTOR (clients-1 downto 0);
signal i_GNT : STD_LOGIC_VECTOR (clients-1 downto 0);
signal GNT_SEL : STD_LOGIC_VECTOR (GNT_CNT_size downto 0);

signal GNT_CNT : STD_LOGIC_VECTOR (GNT_CNT_size downto 0);

signal i_s_start_address : slv64_t(clients-1 downto 0);

type arbiter_state_type is (
	ARB_WAIT,
	ARB_PREGRANT,
	ARB_GRANTED
);
signal state : arbiter_state_type;

COMPONENT cdc_bit
	PORT(
		CLK_IN : IN std_logic;
		D_IN : IN std_logic;
		CLK_OUT : IN std_logic;          
		D_OUT : OUT std_logic
	);
	END COMPONENT;

COMPONENT cdc_64_dir
	PORT(
		CLK_IN : IN std_logic;
		D_IN : IN std_logic_vector(63 downto 0);
		CLK_OUT : IN std_logic;          
		D_OUT : OUT std_logic_vector(63 downto 0)
		);
	END COMPONENT;

signal t_reset : std_logic;

begin

	reset_cdc_i: cdc_bit PORT MAP(
		CLK_IN => RESET_CLK,
		D_IN => RESET,
		CLK_OUT => DMA_CLK,
		D_OUT => t_reset
	);

cdc_64_dir_generate: 
	for I in 0 to clients-1 generate
		cdc_64_dir_i_X: cdc_64_dir PORT MAP(
			CLK_IN => IN_CLK(I),
			D_IN => s_start_address(I),
			CLK_OUT => DMA_CLK,
			D_OUT => i_s_start_address(I)
		);
	end generate;

GEN_REQ_CDC: 
   for I in 0 to clients-1 generate
		cdc_bit_ix: cdc_bit PORT MAP(
			CLK_IN => IN_CLK(I),
			D_IN => REQ(I),
			CLK_OUT => DMA_CLK,
			D_OUT => i_REQ(I)
		);
	end generate;

GEN_GNT_CDC: 
   for I in 0 to clients-1 generate
		cdc_bit_ix: cdc_bit PORT MAP(
			CLK_IN => DMA_CLK,
			D_IN => i_GNT(I),
			CLK_OUT => IN_CLK(I),
			D_OUT => GNT(I)
		);
	end generate;

start_address <= i_s_start_address(to_integer(unsigned(GNT_SEL)));
	 
s_pci_dma_ready <= (others => pci_dma_ready);
dma_cfg_address_set <= s_dma_cfg_address_set(to_integer(unsigned(GNT_SEL)));

dma_data <= dma_data_a(to_integer(unsigned(GNT_SEL)));

dma_data_rd_en_a_generate: 
	for I in 0 to clients-1 generate
		dma_data_rd_en_a(I) <= dma_data_rd_en when to_integer(unsigned(GNT_SEL)) = I else '0';
	end generate;

dma_data_rd_data_count <= dma_data_rd_data_count_a(to_integer(unsigned(GNT_SEL)));

dma_cmd <= dma_cmd_a(to_integer(unsigned(GNT_SEL)));

dma_cmd_rd_en_a_generate: 
	for I in 0 to clients-1 generate
		dma_cmd_rd_en_a(I) <= dma_cmd_rd_en when to_integer(unsigned(GNT_SEL)) = I else '0';
	end generate;

dma_cmd_empty <= dma_cmd_empty_a(to_integer(unsigned(GNT_SEL)));

DRV: process (DMA_CLK, t_reset)
begin
	if t_reset = '1' then
		state   <= ARB_WAIT;
		GNT_CNT <= (others => '0');
		--i_GNT   <= (others => '0');
		--GNT_SEL <= (others => '0');

	elsif DMA_CLK'event and DMA_CLK = '1' then
		case (state) is
			when ARB_WAIT =>
				if i_REQ(to_integer(unsigned(GNT_CNT))) = '1' then
					state   <= ARB_PREGRANT;
					GNT_SEL <= GNT_CNT;
				else
					GNT_CNT <= GNT_CNT + 1;
				end if;
				
			when ARB_PREGRANT =>
				i_GNT(to_integer(unsigned(GNT_CNT))) <= '1';
				state <= ARB_GRANTED;
				
			when ARB_GRANTED =>
				if i_REQ(to_integer(unsigned(GNT_CNT))) = '0' then
					i_GNT(to_integer(unsigned(GNT_CNT))) <= '0';
					state  <= ARB_WAIT;
				end if;
				
			when others =>
		end case;
	end if;
end process;

end Behavioral;
