----------------------------------------------------------------------------------
-- Company: Aerotech LTD, LLC
-- Engineer: Eugene Shamaev
-- 
-- Create Date:    09:32:51 12/24/2017 
-- Design Name:    Video 45
-- Module Name:    reset - Behavioral 
-- Project Name:   10D MFD/PFD
-- Target Devices: Spartan-6 45T
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: A
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

library UNISIM;
use UNISIM.VComponents.all;

entity reset is
	Port (
		CLK      : in  STD_LOGIC;
		RESET    : out  STD_LOGIC;
		PLL_LOCK : in  STD_LOGIC;
		PCI_RESET : in STD_LOGIC
	);
end reset;

architecture Behavioral of reset is

signal RESET_CNT     : integer range 0 to 10000 := 0;

begin

process (CLK, PLL_LOCK, PCI_RESET)
begin
	if PLL_LOCK = '0' or PCI_RESET = '0' then
		RESET_CNT <= 0;
	else
		if CLK'event and CLK = '1' then
			if RESET_CNT < 10000 then
				RESET_CNT <= RESET_CNT + 1;
				RESET <= '1';
			else
				RESET <= '0';
			end if;
		end if;
	end if;
end process;

end Behavioral;

