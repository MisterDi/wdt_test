----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    10:17:27 03/22/2018 
-- Design Name: 
-- Module Name:    ARINC_708 - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

library UNISIM;
use UNISIM.Vcomponents.ALL;

entity ARINC_708 is
	generic (
		enable_test_transmit : boolean := false;
		
		check_place : integer := 4
	);
	Port (
		RESET                    : in  STD_LOGIC;
		RESET_CLK                : in  STD_LOGIC;
		
		CLK16                    : in  STD_LOGIC;

		TXINHA                   : out  STD_LOGIC;
		TXA_P                    : out  STD_LOGIC;
		TXA_N                    : out  STD_LOGIC;
		CLKA                     : out  STD_LOGIC;
		ENCLKA                   : out  STD_LOGIC;
		TOC0A                    : out  STD_LOGIC;
		TOC1A                    : out  STD_LOGIC;
		TOC2A                    : out  STD_LOGIC;
		RXA_P                    : in  STD_LOGIC;
		RXA_N                    : in  STD_LOGIC;
		RXENA                    : out  STD_LOGIC;
		ENPEXTA                  : out  STD_LOGIC;
		
		TXINHB                   : out  STD_LOGIC;
		TXB_P                    : out  STD_LOGIC;
		TXB_N                    : out  STD_LOGIC;
		CLKB                     : out  STD_LOGIC;
		ENCLKB                   : out  STD_LOGIC;
		TOC0B                    : out  STD_LOGIC;
		TOC1B                    : out  STD_LOGIC;
		TOC2B                    : out  STD_LOGIC;
		RXB_P                    : in  STD_LOGIC;
		RXB_N                    : in  STD_LOGIC;
		RXENB                    : out  STD_LOGIC;
		ENPEXTB                  : out  STD_LOGIC;
		
		A708_DMA_ADDR_CLK        : in STD_LOGIC;
		A708_DMA_ADDR            : in STD_LOGIC_VECTOR (63 downto 0);
		
		A708_STATE               : out STD_LOGIC_VECTOR (31 downto 0);
		A708_STATE_CLK           : in STD_LOGIC;
		
		A708_CMD                 : in STD_LOGIC_VECTOR (31 downto 0);
		A708_CMD_EN              : in STD_LOGIC;
		A708_CMD_CLK             : in STD_LOGIC;
		
		-- DMA
		dma_start_address        : out STD_LOGIC_VECTOR(63 DOWNTO 0);
		pci_dma_ready            : in STD_LOGIC;
		dma_cfg_address_set      : out  STD_LOGIC;
		
		dma_clk						 : in STD_LOGIC;
		
		dma_cmd                  : out STD_LOGIC_VECTOR(33 DOWNTO 0);
		dma_cmd_rd_en            : in  STD_LOGIC;
		dma_cmd_empty            : out STD_LOGIC;
		
		dma_data                 : out STD_LOGIC_VECTOR(31 DOWNTO 0);
		dma_data_rd_en           : in  STD_LOGIC;
		dma_data_rd_data_count   : out STD_LOGIC_VECTOR(10 DOWNTO 0);
		
		dma_gnt                  : in  STD_LOGIC;
		dma_req                  : out STD_LOGIC;
		
		debug                    : out std_logic;
		debug2                   : out std_logic
	);
end ARINC_708;

architecture Behavioral of ARINC_708 is

constant retransmit_counter : integer := 640000;

	COMPONENT dma_cmd_fifo
	PORT (
		rst               : IN  STD_LOGIC;
		
		wr_clk            : IN  STD_LOGIC;
		din               : IN  STD_LOGIC_VECTOR(33 DOWNTO 0);
		wr_en             : IN  STD_LOGIC;
		full              : OUT STD_LOGIC;
		
		rd_clk            : IN  STD_LOGIC;
		rd_en             : IN  STD_LOGIC;
		dout              : OUT STD_LOGIC_VECTOR(33 DOWNTO 0);
		empty             : OUT STD_LOGIC
	);
	END COMPONENT;
	
	COMPONENT dma_data_fifo
	PORT (
		rst               : IN  STD_LOGIC;
		
		wr_clk            : IN  STD_LOGIC;
		din               : IN  STD_LOGIC_VECTOR(31 DOWNTO 0);
		wr_en             : IN  STD_LOGIC;
		full              : OUT STD_LOGIC;
		wr_data_count     : OUT STD_LOGIC_VECTOR(10 DOWNTO 0);
		
		rd_clk            : IN  STD_LOGIC;
		dout              : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
		rd_en             : IN  STD_LOGIC;
		empty             : OUT STD_LOGIC;
		rd_data_count     : OUT STD_LOGIC_VECTOR(10 DOWNTO 0)
	);
	END COMPONENT;
	
	signal dma_data_wr_en : std_logic;
	signal dma_cmd_wr_en  : std_logic;
	signal dma_data_din   : std_logic_vector(31 downto 0);
	signal dma_cmd_din    : std_logic_vector(33 downto 0);

	COMPONENT cdc_32_reg
	PORT(
		reset : IN std_logic;
		clk_in : IN std_logic;
		data_in : IN std_logic_vector(31 downto 0);
		clk_out : IN std_logic;          
		data_out : OUT std_logic_vector(31 downto 0)
	);
	END COMPONENT;
	
	COMPONENT cdc_64_dir
	PORT(
		CLK_IN : IN std_logic;
		D_IN : IN std_logic_vector(63 downto 0);
		CLK_OUT : IN std_logic;
		D_OUT : OUT std_logic_vector(63 downto 0)
		);
	END COMPONENT;

	COMPONENT fifo_32_aux
	PORT (
		rst            : IN STD_LOGIC;

		wr_clk         : IN STD_LOGIC;
		din            : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
		wr_en          : IN STD_LOGIC;
		full           : OUT STD_LOGIC;

		rd_clk         : IN STD_LOGIC;
		rd_en          : IN STD_LOGIC;
		dout           : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
		empty          : OUT STD_LOGIC
	);
	END COMPONENT;
	
	COMPONENT fifo_32_one_clk
	PORT (
		clk : IN STD_LOGIC;
		srst : IN STD_LOGIC;
		din : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
		wr_en : IN STD_LOGIC;
		rd_en : IN STD_LOGIC;
		dout : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
		full : OUT STD_LOGIC;
		empty : OUT STD_LOGIC
	);
	END COMPONENT;
	
signal fifo_32_rx_reset : std_logic;
signal fifo_32_rx_din : std_logic_vector(31 downto 0);
signal fifo_32_rx_wr_en : std_logic;
signal fifo_32_rx_rd_en : std_logic;
signal fifo_32_rx_dout : std_logic_vector(31 downto 0);
signal fifo_32_rx_full : std_logic;
signal fifo_32_rx_empty : std_logic;

signal rx_word_cnt : integer range 0 to 50;
signal rx_subbit_cnt : integer range 0 to 31;

signal i_CMD_IN       : std_logic_vector(31 downto 0);
signal i_CMD_IN_EN    : std_logic;
signal i_CMD_IN_EMPTY : std_logic;

signal i_status_out   : std_logic_vector(31 downto 0);

signal transmit_cnt   : integer range 0 to retransmit_counter := 0;
signal tx_sub_cnt     : integer range 0 to 31 := 0;

type transmit_state_type is (
	TX_WAIT,
	TX_PRESTART,
	TX_START,
	TX_START2,
	TX_FIRST_HALF,
	TX_SECOND_HALF,
	TX_END,
	TX_END2,
	TX_END3
);
signal tx_state : transmit_state_type;

type rx_state_type is (
	RX_WAIT,
	RX_FIRST_LH,
	RX_FIRST_HL,
	RX_BIT_IN1,
	RX_BIT_IN2,
	RX_END_HL,
	RX_END_LH,
	RX_END_WAIT,
	RX_FILL_DMA_FIFO,
	
	RX_ERROR_BIT_TIME,
	RX_ERROR_TAIL
);
signal rx_state : rx_state_type;

type dma_state_type is (
	DMA_WAIT,
	DMA_TRY_REQ,
	DMA_GRANTED,
	DMA_WAIT_START,
	DMA_WAIT_FINISH,
	DMA_RELEASE
);
signal dma_state       : dma_state_type;

signal tx_buff         : std_logic;
signal tx_bit_cnt      : integer range 0 to 1599 := 0;

signal rx_buff         : std_logic_vector (31 downto 0);
signal rx_bit_cnt      : integer range 0 to 1599 := 0;
signal rx_sub_cnt      : integer range 0 to 63 := 0;
signal rx_silence_cnt  : integer range 0 to (16*4) := 0;

signal RX_PLUS         : std_logic;
signal RX_MINUS        : std_logic;

signal rx_f_p_bit      : std_logic;
signal rx_f_n_bit      : std_logic;

signal fifo_reset      : std_logic;

signal s_enable        : std_logic;
signal s_channel       : std_logic;
signal s_inverse_0     : std_logic;
signal s_inverse_1     : std_logic;
signal s_reset         : std_logic;
signal s_reset_err     : std_logic;
signal s_err_bit       : std_logic;
signal s_err_tail      : std_logic;
signal s_dma_en        : std_logic;
signal s_core_ready    : std_logic;

signal rx_out_ready    : std_logic;
signal rx_scan         : std_logic_vector (11 downto 0);
signal rx_out_scan     : std_logic_vector (11 downto 0);
signal dma_fill_cnt    : integer range 0 to 50 := 0;

signal dma_set_address : std_logic_vector (63 downto 0);
signal i_reset : std_logic;


COMPONENT cdc_bit
	PORT(
		CLK_IN : IN std_logic;
		D_IN : IN std_logic;
		CLK_OUT : IN std_logic;          
		D_OUT : OUT std_logic
	);
	END COMPONENT;
signal t_reset : std_logic;

begin

reset_cdc_i: cdc_bit PORT MAP(
		CLK_IN => RESET_CLK,
		D_IN => RESET,
		CLK_OUT => CLK16,
		D_OUT => t_reset
	);

dma_cmd_fifo_i : dma_cmd_fifo
	PORT MAP(
		rst           => i_reset,
		
		wr_clk        => CLK16,
		din           => dma_cmd_din,
		wr_en         => dma_cmd_wr_en,
		
		rd_clk        => dma_clk,
		rd_en         => dma_cmd_rd_en,
		dout          => dma_cmd,
		empty         => dma_cmd_empty
	);
	
dma_data_fifo_i : dma_data_fifo
	PORT MAP(
		rst           => i_reset,
		
		wr_clk        => CLK16,
		din           => dma_data_din,
		wr_en         => dma_data_wr_en,
		
		rd_clk        => dma_clk,
		dout          => dma_data,
		rd_en         => dma_data_rd_en,
		rd_data_count => dma_data_rd_data_count
	);

cdc_64_dma_addr: cdc_64_dir PORT MAP(
		CLK_IN        => A708_DMA_ADDR_CLK,
		D_IN          => A708_DMA_ADDR,
		
		CLK_OUT       => CLK16,
		D_OUT         => dma_set_address
	);

i_status_out(0)            <= s_core_ready;
i_status_out(1)            <= s_enable;
i_status_out(2)            <= s_channel;
i_status_out(3)            <= s_inverse_0;
i_status_out(4)            <= s_inverse_1;
i_status_out(5)            <= s_reset;
i_status_out(6)            <= s_err_bit;
i_status_out(7)            <= s_err_tail;
i_status_out(15 downto 8)  <= (others => '0');
i_status_out(16)           <= s_dma_en;
i_status_out(31 downto 17) <= (others => '0');

	status_cdc_32_reg: cdc_32_reg PORT MAP(
		reset => fifo_reset,
		
		clk_in => CLK16,
		data_in => i_status_out,
		
		clk_out => A708_STATE_CLK,
		data_out => A708_STATE
	);
	
	fifo_32_cmd_i : fifo_32_aux
	PORT MAP (
		rst           => fifo_reset,

		wr_clk        => A708_CMD_CLK,
		din           => A708_CMD,
		wr_en         => A708_CMD_EN,

		rd_clk        => CLK16,
		rd_en         => i_CMD_IN_EN,
		dout          => i_CMD_IN,
		empty         => i_CMD_IN_EMPTY
	);
	
	fifo_32_rx_i : fifo_32_one_clk
	PORT MAP (
		clk => CLK16,
		srst => fifo_32_rx_reset,
		din => fifo_32_rx_din,
		wr_en => fifo_32_rx_wr_en,
		rd_en => fifo_32_rx_rd_en,
		dout => fifo_32_rx_dout,
		full => fifo_32_rx_full,
		empty => fifo_32_rx_empty
	);
	
with s_channel select RX_PLUS <=
	(RXA_P xor s_inverse_0) when '0',
	(RXB_P xor s_inverse_0) when '1';
	
with s_channel select RX_MINUS <=
	(RXA_N xor s_inverse_0) when '0',
	(RXB_N xor s_inverse_0) when '1';

DRV: process (CLK16, RESET)
begin
--	if RESET = '1' then
--	
--		TXB_P <= '0';
--		TXB_N <= '0';
--		ENCLKA <= '1';
--		TOC0A <= '0';
--		TOC1A <= '0';
--		TOC2A <= '0';
--		ENPEXTA <= '1';
--		
--		TXB_P <= '0';
--		TXB_N <= '0';
--		CLKB <= '0';
--		ENCLKB <= '1';
--		TOC0B <= '0';
--		TOC1B <= '0';
--		TOC2B <= '0';
--		ENPEXTB <= '1';
--		
--		TXINHA <= '1';
--		TXINHB <= '1';
--		
--		RXENA <= '0';
--		RXENB <= '0';
--		
--		i_CMD_IN_EN <= '0';
--		
--		if enable_test_transmit then
--			transmit_cnt <= 0;
--			tx_state <= TX_WAIT;
--		end if;
--		
--		rx_state <= RX_WAIT;
--		rx_out_ready <= '0';
--		rx_silence_cnt <= 0;
--		
--		dma_state <= DMA_WAIT;
--		dma_cfg_address_set <= '0';
--		dma_req <= '0';
--		
--		fifo_reset <= '1';
--		
--		s_core_ready <= '0';
--		s_enable <= '0';
--		s_channel <= '0';
--		s_inverse_0 <= '0';
--		s_inverse_1 <= '0';
--		s_reset <= '0';
--		s_reset_err <= '0';
--		s_err_bit <= '0';
--		s_err_tail <= '0';
--		s_dma_en <= '0';
--		
--		fifo_32_rx_reset <= '0';
--		fifo_32_rx_wr_en <= '0';
--		fifo_32_rx_rd_en <= '0';
--
--	els
	if CLK16'event and CLK16 = '1' then
		i_reset <= s_reset or t_reset;
		
		if i_reset = '1' then
		
			TXA_P <= '0';
			TXA_N <= '0';
			CLKA <= '0';
			ENCLKA <= '1';
			TOC0A <= '0';
			TOC1A <= '0';
			TOC2A <= '0';
			ENPEXTA <= '1';
			
			TXB_P <= '0';
			TXB_N <= '0';
			CLKB <= '0';
			ENCLKB <= '1';
			TOC0B <= '0';
			TOC1B <= '0';
			TOC2B <= '0';
			ENPEXTB <= '1';
			
			TXINHA <= '1';
			TXINHB <= '1';
			
			RXENA <= '0';
			RXENB <= '0';

			s_core_ready <= '0';
			s_enable <= '0'; ------------------------------------------------------------- 0
			s_channel <= '0';
			s_inverse_0 <= '0'; ------------------------------------------------------------- 0
			s_inverse_1 <= '0'; ------------------------------------------------------------- 0
			s_reset <= '0';
			s_reset_err <= '0';
			s_err_bit <= '0';
			s_err_tail <= '0';
			s_dma_en <= '0';
			
			rx_state <= RX_WAIT;
			rx_out_ready <= '0';
			rx_silence_cnt <= 0;
			
			dma_state <= DMA_WAIT;
			dma_cfg_address_set <= '0';
			dma_req <= '0';
			
			i_CMD_IN_EN <= '0';
			
			if enable_test_transmit then
				transmit_cnt <= 0;
				tx_state <= TX_WAIT;
			end if;
			
			fifo_reset <= '1';
			
			fifo_32_rx_reset <= '1';
			fifo_32_rx_wr_en <= '0';
			fifo_32_rx_rd_en <= '0';

		else
			fifo_reset <= '0';
			s_core_ready <= '0';
--			if dma_state = DMA_WAIT then
--				debug <= '0';
--			else
--				debug <= '1';
--			end if;
			
			RXENA <= s_enable;
			RXENB <= s_enable;
			ENPEXTA <= '1';
			ENPEXTB <= '1';
			
			i_CMD_IN_EN <= '0';
			fifo_32_rx_reset <= '0';
			fifo_32_rx_wr_en <= '0';
			fifo_32_rx_rd_en <= '0';
			
			if rx_state /= RX_WAIT and RX_PLUS = '0' and RX_MINUS = '0' then
				rx_silence_cnt <= rx_silence_cnt + 1;
			else
				rx_silence_cnt <= 0;
			end if;
			
			if rx_silence_cnt >= 40*4 then
				rx_state <= RX_WAIT;
				rx_silence_cnt <= 0;
			else
				case (rx_state) is
					when RX_WAIT=>
						s_core_ready <= '1';
						
						rx_sub_cnt <= 0;
						rx_bit_cnt <= 0;
						rx_word_cnt <= 0;
						rx_subbit_cnt <= 0;
						fifo_32_rx_reset <= '1';
						
						debug <= '0';
						
						if RX_PLUS = '0' and RX_MINUS = '1' and s_enable = '1' then
							rx_state <= RX_FIRST_LH;
							debug <= '1';
							
						elsif i_CMD_IN_EMPTY = '0' then
							i_CMD_IN_EN <= '1';
							
							s_enable <= i_CMD_IN(1);
							s_channel <= i_CMD_IN(2);
							s_inverse_0 <= i_CMD_IN(3);
							s_inverse_1 <= i_CMD_IN(4);
							s_reset <= i_CMD_IN(5);
							s_reset_err <= i_CMD_IN(6);
							s_dma_en <= i_CMD_IN(16);
						end if;
						
					when RX_FIRST_LH =>
						if RX_PLUS = '1' then
							rx_state <= RX_FIRST_HL;
						end if;
						
					when RX_FIRST_HL =>
						if rx_sub_cnt < 23 then
							rx_sub_cnt <= rx_sub_cnt + 1;
						else
							rx_state <= RX_BIT_IN1;
							rx_sub_cnt <= 0;
						end if;
						
					when RX_BIT_IN1 =>
						if rx_sub_cnt < 7 then
							rx_sub_cnt <= rx_sub_cnt + 1;
						else
							rx_state <= RX_BIT_IN2;
							rx_sub_cnt <= 0;
						end if;
						
						if rx_sub_cnt = check_place then
							rx_f_p_bit <= RX_PLUS;
							rx_f_n_bit <= RX_MINUS;
						end if;
					
					when RX_BIT_IN2 =>
						if rx_sub_cnt < 7 then
							rx_sub_cnt <= rx_sub_cnt + 1;
						else
							if rx_bit_cnt < 1599 then
								rx_state <= RX_BIT_IN1;
								rx_sub_cnt <= 0;
								rx_bit_cnt <= rx_bit_cnt + 1;
							else
								rx_state <= RX_END_HL;
								rx_sub_cnt <= 0;
							end if;
						end if;
						
						if rx_sub_cnt = check_place then
							if rx_f_p_bit = '1' and rx_f_n_bit = '0' and RX_PLUS = '0' and RX_MINUS = '1' then
								rx_buff <= '1' & rx_buff(31 downto 1);
							elsif rx_f_p_bit = '0' and rx_f_n_bit = '1' and RX_PLUS = '1' and RX_MINUS = '0' then
								rx_buff <= '0' & rx_buff(31 downto 1);
							else
								rx_state <= RX_ERROR_BIT_TIME;
							end if;
						end if;
						
						if rx_sub_cnt = (check_place + 1) then
							if rx_subbit_cnt = 31 then
								rx_subbit_cnt <= 0;
								fifo_32_rx_din <= rx_buff;
								fifo_32_rx_wr_en <= '1';
							else
								rx_subbit_cnt <= rx_subbit_cnt + 1;
							end if;
							
							--rx_out_scan <= rx_buff(63 downto 52); -- becomes:
							if rx_bit_cnt = 63 then
								rx_scan <= rx_buff(31 downto 20);
							end if;
						end if;
					
					when RX_END_HL =>
						if rx_sub_cnt < 23 then
							rx_sub_cnt <= rx_sub_cnt + 1;
						else
							rx_state <= RX_END_LH;
							rx_sub_cnt <= 0;
						end if;
						
						if rx_sub_cnt = check_place then
							if not (RX_PLUS = '1' and RX_MINUS = '0') then
								rx_state <= RX_ERROR_TAIL;
							end if;
						end if;
					
					when RX_END_LH =>
						if rx_sub_cnt < 23 then
							rx_sub_cnt <= rx_sub_cnt + 1;
						else
							rx_state <= RX_END_WAIT;
							debug2 <= '1';
							rx_sub_cnt <= 0;
						end if;
						
						if rx_sub_cnt = check_place then
							if not (RX_PLUS = '0' and RX_MINUS = '1') then
								rx_state <= RX_ERROR_TAIL;
							end if;
							
						end if;
						
					when RX_END_WAIT =>
						if rx_sub_cnt < 63 then
							rx_sub_cnt <= rx_sub_cnt + 1;
						else
							
							rx_sub_cnt <= 0;
							debug2 <= '0';							

							-- only if last message was written to CPU
							if rx_out_ready = '0' then
								rx_out_scan <= rx_scan;
								rx_state <= RX_FILL_DMA_FIFO;
							else
								rx_state <= RX_WAIT;
							end if;
						end if;
					
					when RX_FILL_DMA_FIFO =>
						if fifo_32_rx_empty = '0' then
							dma_data_din <= fifo_32_rx_dout;
							dma_data_wr_en <= '1';
							fifo_32_rx_rd_en <= '1';
						else
							rx_state <= RX_WAIT;
							rx_out_ready <= '1';
						end if;
						
					when RX_ERROR_BIT_TIME =>
						s_err_bit <= '1';
						rx_state <= RX_WAIT;
						rx_sub_cnt <= 0;
					
					when RX_ERROR_TAIL =>
						s_err_tail <= '1';
						rx_state <= RX_WAIT;
						rx_sub_cnt <= 0;
						
					when others =>
				end case;
			end if;
			
			if s_reset_err = '1' then
				s_err_bit <= '0';
				s_err_tail <= '0';
			end if;
			
			----------------------------------------------------------------------- DMA
			dma_data_wr_en <= '0';
			dma_cmd_wr_en <= '0';
			
			case (dma_state) is
				when DMA_WAIT =>
					if rx_out_ready = '1' and s_dma_en = '1' then
						dma_state <= DMA_TRY_REQ;
						dma_req <= '1';
					end if;
					
				when DMA_TRY_REQ =>
					if dma_gnt = '1' then
						rx_out_ready <= '0';
						
						if pci_dma_ready = '1' then
							dma_state <= DMA_GRANTED;
							dma_start_address <= dma_set_address + (rx_out_scan & "000000");
							dma_fill_cnt <= 0;
							dma_cfg_address_set <= '1';
						end if;
					end if;
				
				when DMA_GRANTED =>
					dma_state <= DMA_WAIT_START;
					
					dma_cmd_din <= "00" & std_logic_vector(to_unsigned(50, 32));
					dma_cmd_wr_en <= '1';
				
				when DMA_WAIT_START =>
					if pci_dma_ready = '0' then
						dma_state <= DMA_WAIT_FINISH;
					end if;
				
				when DMA_WAIT_FINISH =>
					if pci_dma_ready = '1' then
						dma_state <= DMA_RELEASE;
						dma_cfg_address_set <= '0';
						dma_req <= '0';
					end if;
				
				when DMA_RELEASE =>
					if dma_gnt = '0' then
						dma_state <= DMA_WAIT;
					end if;
					
				when others =>
			end case;
			
			----------------------------------------------------------------------- TRANSMIT
			if enable_test_transmit then
				ENPEXTA <= '1';
				
				if transmit_cnt >= retransmit_counter then
					transmit_cnt <= 0;
					if tx_state = TX_WAIT then
						tx_sub_cnt <= 0;
						tx_state <= TX_PRESTART;
						tx_buff <= '0';
					end if;
				else
					transmit_cnt <= transmit_cnt + 1;
				end if;
				
				case (tx_state) is
					when TX_WAIT =>
						tx_sub_cnt <= 0;
						ENCLKA <= '1';
						TXINHA <= '1';
						
					when TX_PRESTART =>
						if tx_sub_cnt < 31 then
							tx_sub_cnt <= tx_sub_cnt + 1;
							TXINHA <= '0';
							TXA_P <= '0';
							TXA_N <= '0';
						else
							tx_state <= TX_START;
							tx_sub_cnt <= 0;
						end if;
						if tx_sub_cnt = 1 then
							CLKA <= '1';
						end if;
						if tx_sub_cnt = 2 then
							CLKA <= '0';
						end if;
						
					when TX_START =>
						if tx_sub_cnt < 23 then
							tx_sub_cnt <= tx_sub_cnt + 1;
							TXA_P <= '1';
							TXA_N <= '0';
						else
							tx_state <= TX_START2;
							tx_sub_cnt <= 0;
						end if;
						if tx_sub_cnt = 1 then
							CLKA <= '1';
						end if;
						if tx_sub_cnt = 2 then
							CLKA <= '0';
						end if;
						
					when TX_START2 =>
						if tx_sub_cnt < 23 then
							tx_sub_cnt <= tx_sub_cnt + 1;
							TXA_P <= '0';
							TXA_N <= '1';
						else
							tx_state <= TX_FIRST_HALF;
							tx_sub_cnt <= 0;
							tx_bit_cnt <= 0;
						end if;
						if tx_sub_cnt = 1 then
							CLKA <= '1';
						end if;
						if tx_sub_cnt = 2 then
							CLKA <= '0';
						end if;
						
					when TX_FIRST_HALF =>
						if tx_sub_cnt < 7 then
							if tx_buff = '0' then
								TXA_P <= '0';
								TXA_N <= '1';
							else
								TXA_P <= '1';
								TXA_N <= '0';
							end if;
							
							tx_sub_cnt <= tx_sub_cnt + 1;
						else
							tx_state <= TX_SECOND_HALF;
							tx_sub_cnt <= 0;
						end if;
						if tx_sub_cnt = 1 then
							CLKA <= '1';
						end if;
						if tx_sub_cnt = 2 then
							CLKA <= '0';
						end if;
					
					when TX_SECOND_HALF =>
						if tx_sub_cnt < 7 then
							if tx_buff = '0' then
								TXA_P <= '1';
								TXA_N <= '0';
							else
								TXA_P <= '0';
								TXA_N <= '1';
							end if;
							
							tx_sub_cnt <= tx_sub_cnt + 1;
						else
							if tx_bit_cnt < 1599 then
								tx_state <= TX_FIRST_HALF;
								tx_sub_cnt <= 0;
								tx_buff <= not tx_buff;
								tx_bit_cnt <= tx_bit_cnt + 1;
							else
								tx_state <= TX_END;
								tx_sub_cnt <= 0;
							end if;
						end if;
						
						if tx_sub_cnt = 1 then
							CLKA <= '1';
						end if;
						if tx_sub_cnt = 2 then
							CLKA <= '0';
						end if;
					
					when TX_END =>
						if tx_sub_cnt < 23 then
							tx_sub_cnt <= tx_sub_cnt + 1;
							TXA_P <= '0';
							TXA_N <= '1';
						else
							tx_state <= TX_END2;
							tx_sub_cnt <= 0;
						end if;
						
						if tx_sub_cnt = 1 then
							CLKA <= '1';
						end if;
						if tx_sub_cnt = 2 then
							CLKA <= '0';
						end if;
						
					when TX_END2 =>
						if tx_sub_cnt < 23 then
							tx_sub_cnt <= tx_sub_cnt + 1;
							TXA_P <= '1';
							TXA_N <= '0';
						else
							tx_state <= TX_END3;
							tx_sub_cnt <= 0;
						end if;
						
						if tx_sub_cnt = 1 then
							CLKA <= '1';
						end if;
						if tx_sub_cnt = 2 then
							CLKA <= '0';
						end if;
					
					when TX_END3 =>
						if tx_sub_cnt < 31 then
							tx_sub_cnt <= tx_sub_cnt + 1;
							TXA_P <= '0';
							TXA_N <= '0';
						else
							tx_state <= TX_WAIT;
							tx_sub_cnt <= 0;
							TXINHA <= '1';
						end if;
						
						if tx_sub_cnt = 1 then
							CLKA <= '1';
						end if;
						if tx_sub_cnt = 2 then
							CLKA <= '0';
						end if;
						
					when others =>
				end case;
			end if;
		end if;
	end if;
end process;

end Behavioral;

