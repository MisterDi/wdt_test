----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    22:42:38 01/21/2018 
-- Design Name: 
-- Module Name:    Spi_top - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

library UNISIM;
use UNISIM.VComponents.all;

entity DigOut_spi_master is
    Port ( SCK 		: out STD_LOGIC;
           MOSI 		: out STD_LOGIC;
           MISO 		: in  STD_LOGIC;
			  SSEL		: out	STD_LOGIC;
           DataIn 	: in  STD_LOGIC_VECTOR (31 downto 0);
           DataOut 	: out STD_LOGIC_VECTOR (31 downto 0);
           ReadDev 	: in  STD_LOGIC;
           WriteDev	: in  STD_LOGIC;
           ClkTop		: in  STD_LOGIC;
			  ClkInn		: in  STD_LOGIC;
           Reset 		: in  STD_LOGIC;
           DevRdy		: out STD_LOGIC;
			  Cmd			: in  STD_LOGIC_VECTOR(0 downto 0);
			  Enable_1	: out STD_LOGIC;
			  RST_PIN_1	: out STD_LOGIC;
			  FAULT_1	: in  STD_LOGIC;
			  Enable_2	: out STD_LOGIC;
			  RST_PIN_2	: out STD_LOGIC;
			  FAULT_2	: in  STD_LOGIC;
			  Enable_3	: out STD_LOGIC;
			  RST_PIN_3	: out STD_LOGIC;
			  FAULT_3	: in  STD_LOGIC;
			  Enable_4	: out STD_LOGIC;
			  RST_PIN_4	: out STD_LOGIC;
			  FAULT_4	: in  STD_LOGIC
			  );
end DigOut_spi_master;

architecture Behavioral of DigOut_spi_master is

	COMPONENT SPI_MASTER
		generic ( 
        N : positive := 8;                                              -- 8 bit serial word length is default
        CPOL : std_logic := '0';                                        -- SPI mode selection (mode 0 default)
        CPHA : std_logic := '0';                                        -- CPOL = clock polarity, CPHA = clock phase.
        PREFETCH : positive := 1;                                       -- prefetch lookahead cycles
        SPI_2X_CLK_DIV : positive := 5                                  -- for a 100MHz sclk_i, yields a 10MHz SCK
		);
		PORT(
        sclk_i 		: in std_logic := 'X';                               -- high-speed serial interface system clock
        pclk_i 		: in std_logic := 'X';                               -- high-speed parallel interface system clock
        rst_i 			: in std_logic := 'X';                               -- reset core
        ---- serial interface ----
        spi_ssel_o 	: out std_logic;                                     -- spi bus slave select line
        spi_sck_o 	: out std_logic;                                     -- spi bus sck
        spi_mosi_o 	: out std_logic;                                     -- spi bus mosi output
        spi_miso_i 	: in std_logic := 'X';                               -- spi bus spi_miso_i input
        ---- parallel interface ----
        di_req_o 		: out std_logic;                                     -- preload lookahead data request line
        di_i 			: in  std_logic_vector (7 downto 0) := (others => 'X');  -- parallel data in (clocked on rising spi_clk after last bit)
        wren_i 		: in std_logic := 'X';                               -- user data write enable, starts transmission when interface is idle
        wr_ack_o 		: out std_logic;                                     -- write acknowledge
        do_valid_o 	: out std_logic;                                     -- do_o data valid signal, valid during one spi_clk rising edge.
        do_o 			: out  std_logic_vector (7 downto 0)               -- parallel output (clocked on rising spi_clk after last bit)
		);
	END COMPONENT;
		
	COMPONENT fifo
		PORT(
		   rst 		: IN STD_LOGIC;
			wr_clk 	: IN STD_LOGIC;
			rd_clk 	: IN STD_LOGIC;
			din 		: IN STD_LOGIC_VECTOR(31 DOWNTO 0);
			wr_en 	: IN STD_LOGIC;
			rd_en 	: IN STD_LOGIC;
			dout 		: OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
			full 		: OUT STD_LOGIC;
			empty 	: OUT STD_LOGIC
		);
	END COMPONENT;
	
	COMPONENT InitROM
		PORT(
			clka : IN STD_LOGIC;
			addra : IN STD_LOGIC_VECTOR(10 DOWNTO 0);
			douta : OUT STD_LOGIC_VECTOR(23 DOWNTO 0)
		);
	END COMPONENT;	
	
	COMPONENT fifo_cmd
		PORT (
			rst 	: IN STD_LOGIC;
			wr_clk: IN STD_LOGIC;
			rd_clk: IN STD_LOGIC;
			din 	: IN STD_LOGIC_VECTOR(0 DOWNTO 0);
			wr_en : IN STD_LOGIC;
			rd_en : IN STD_LOGIC;
			dout 	: OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
			full 	: OUT STD_LOGIC;
			empty : OUT STD_LOGIC
		);
	END COMPONENT;

	COMPONENT reg8
		PORT(
			Clk 		: in  STD_LOGIC;
         Reset		: in  STD_LOGIC;
         ClkEn 	: in  STD_LOGIC;
         DataIn 	: in  STD_LOGIC_VECTOR (7 downto 0);
         DataOut	: out STD_LOGIC_VECTOR (7 downto 0)
		);
	END COMPONENT;
	
	signal GetData		: std_logic;
	signal PutData		: std_logic;

	signal GetDataWait	: std_logic;
	signal PutDataWait	: std_logic;
	
	signal GetDataRdy	: std_logic;
	signal PutDataRdy	: std_logic;

	signal LocDataIn 	: std_logic_vector(31 downto 0);
	signal LocDataOut	: std_logic_vector(31 downto 0);

--	signal StatusReg 	: std_logic_vector(3 downto 0);
	
	signal SpiDataIn 	: std_logic_vector(7 downto 0);
	signal SpiDataOut 	: std_logic_vector(7 downto 0);

	signal DevStatus : std_logic_vector(3 downto 0);
	
--	type LineCmd is (
--		CodeCmd	:	std_logic_vector(3 downto 0),
		-- 0000 Init
		-- 0001 Read Byte
		-- 0010 Write Byte
		-- 0011 Read Status
		-- 0100 Write Comand
		-- 0101 Read Block
		-- 0110 Write Block
--		CmdData	:	std_logic_vector(27 downto 0);
--	);
	
	type dev_state is (
		DevReset,		-- after reset
		DevGetCmd,		-- get command data from host
		DevReady,		-- data  for host is ready
		DevBusy,		-- transfer in progress
		DevWait			-- wait  next command
		);
		
	type cmd_fsm is(
		fsm_init,
		init_wait1,
		init_wait2,
		init_wait3,
		prep_cmd,
		write_spi_cmd,
		wait_spi_cmd,
		write_spi_data1,
		wait_spi_data1,
		write_spi_data2,
		wait_spi_data2,
		read_spi_data1,
		wait_spi_read1,
		read_spi_data2,
		wait_spi_read2,
		fsm_wait_host
	);

	signal DeviceState: dev_state;
	signal Command		: std_logic_vector(3 downto 0);
	signal CmdOut		: std_logic_vector(0 downto 0);
	signal InitRomAdr	: std_logic_vector(10 downto 0);
	signal InitRomData: std_logic_vector(23 downto 0);
	signal fsm			: cmd_fsm;
	signal fsm_adr		: std_logic_vector(7 downto 0);
	signal WriteSpi	: std_logic;
	signal DatReq		: std_logic;
	signal WrAsk		: std_logic;
	signal DoValid		: std_logic;
	signal DataCnt		: std_logic_vector(7 downto 0);
	signal ByteCnt		: std_logic_vector(1 downto 0);
--	signal ShiftData	: std_logic;
	signal DataClr		: std_logic;

	
	signal CmdFull		: std_logic;
	signal CmdEmpty	: std_logic;


begin

 fifo_input:fifo
	PORT MAP(
		   rst 		=> Reset,
			wr_clk 	=> ClkTop,
			rd_clk 	=> ClkInn,
			din 		=> DataIn,
			wr_en 	=> WriteDev,
			rd_en 	=> GetData,
			dout 		=> LocDataIn,
			full 		=> GetDataWait,
			empty 	=> GetDataRdy
	);
	
 fifo_output:fifo
	PORT MAP(
		   rst 		=> Reset,
			wr_clk 	=> ClkInn,
			rd_clk 	=> ClkTop,
			din 		=> LocDataOut,
			wr_en 	=> PutData,
			rd_en 	=> ReadDev,
			dout 		=> DataOut,
			full 		=> PutDataWait,
			empty 	=> PutDataRdy
	);
	
 fifo_cmd_i:fifo_cmd
   PORT MAP(
		   rst 		=> Reset,
			wr_clk 	=> ClkInn,
			rd_clk 	=> ClkTop,
			din 		=> Cmd,
			wr_en 	=> WriteDev,
			rd_en 	=> GetData,
			dout 		=> CmdOut,
			full 		=> CmdFull,
			empty 	=> CmdEmpty
  );
	
 SPI_master_i: SPI_MASTER
	PORT MAP(
		  sclk_i	=> ClkInn,                            -- high-speed serial interface system clock
        pclk_i	=> ClkInn,                            -- high-speed parallel interface system clock
        rst_i 	=> Reset,                             -- reset core
        ---- serial interface ----
        spi_ssel_o 	=> SSEL,                        -- spi bus slave select line
        spi_sck_o 	=> SCK,                         -- spi bus sck
        spi_mosi_o 	=> MOSI,                        -- spi bus mosi output
        spi_miso_i 	=> MISO,                        -- spi bus spi_miso_i input
        ---- parallel interface ----
        di_req_o 		=> DatReq,                               -- preload lookahead data request line
        di_i 			=> SpiDataIn,						  -- parallel data in (clocked on rising spi_clk after last bit)
        wren_i 		=> WriteSpi,                               -- user data write enable, starts transmission when interface is idle
        wr_ack_o 		=> WrAsk,		                          -- write acknowledge
        do_valid_o 	=> DoValid,                               -- do_o data valid signal, valid during one spi_clk rising edge.
        do_o 			=> SpiDataOut		              -- parallel output (clocked on rising spi_clk after last bit)
	);
	
 InitROM_i:InitROM
	PORT MAP (
		clka 	=> ClkInn,
		addra	=> InitRomAdr,
		douta => InitRomData
	);

-- Reg_D3: Reg8
--	PORT MAP (
--			Clk		=> ClkInn, 		
--         Reset		=> DataClr,
--         ClkEn 	=> ShiftData,
--         DataIn 	=> LocDataOut(23 downto 16),
--         DataOut	=> LocDataOut(31 downto 24)
--	);
-- Reg_D2: Reg8
--	PORT MAP (
--			Clk		=> ClkInn, 		
--         Reset		=> DataClr,
--         ClkEn 	=> ShiftData,
--         DataIn 	=> LocDataOut(15 downto 8),
--         DataOut	=> LocDataOut(23 downto 16)
--	);
-- Reg_D1: Reg8
--	PORT MAP (
--			Clk		=> ClkInn, 		
--         Reset		=> DataClr,
--         ClkEn 	=> ShiftData,
--         DataIn 	=> LocDataOut(7 downto 0),
--         DataOut	=> LocDataOut(15 downto 8)
--	);
-- Reg_D0: Reg8
--	PORT MAP (
--			Clk		=> ClkInn, 		
--         Reset		=> DataClr,
--         ClkEn 	=> ShiftData,
--         DataIn 	=> SpiDataOut,
--         DataOut	=> LocDataOut(7 downto 0)
--	);
	
	process(ClkInn,Reset)
   begin
		if  Reset = '1' then
			DeviceState <= DevReset;
			DevStatus <= "1111";
			WriteSpi <= '0';
		else
			if ClkInn'event and ClkInn = '1' then
				DevRdy <= not GetDataWait;
				case (DeviceState) is
					when	DevReset =>
						-- init spi peripheral device
						Command <= "0000";
						fsm <= fsm_init;
						fsm_adr <= "00000000";
						DeviceState <= DevBusy;
					when	DevGetCmd	=>
						DevStatus <= "1000"; -- Device get command
						GetData <= '0';
						if CmdOut = "1" then
							Command <= DataIn(31 downto 28);
						end if;
						if DataIn(31 downto 28) = "0000" then
							fsm <= fsm_init;
						else 	
							fsm <= write_spi_cmd;
						end if;	
--						LocDataIn <= DataIn;
						DeviceState <= DevBusy;
					when  DevBusy	=>
						case (Command) is
							when "0000" => -- Init
							 DevStatus <= "0010"; -- Device perfom initialization
							 case (fsm) is
								when fsm_init =>
									SpiDataIn <= "00111000"; -- Read MSR
									WriteSpi <= '1';
 									fsm <= init_wait1;
								when init_wait1 =>
									WriteSpi <= '0';
									if DatReq = '1' then
									   SpiDataIn <= X"00";
									end if;
									if DoValid = '1' then
										fsm <= init_wait2;
									end if;
								when init_wait2 =>
									WriteSpi <= '1';
									fsm <= init_wait3;
								when init_wait3 =>
									WriteSpi <='0';
									fsm_adr <= "00000000";
									InitRomAdr <= "000"&fsm_adr;
									if DoValid = '1' then
										if SpiDataOut(7) = '1' then 
											fsm <= write_spi_cmd;
										else
											fsm <= fsm_init;
										end if;
									end if;
								when prep_cmd =>
									fsm <= fsm_init; 
								when write_spi_cmd =>
									SpiDataIn <= InitRomData(23 downto 16);
									WriteSpi <= '1';
									fsm <= wait_spi_cmd;
								when wait_spi_cmd =>
									WriteSpi <= '0';
									if DatReq = '1' then
										SpiDataIn <= InitRomData(15 downto 8);
									end if;	
									if DoValid = '1' then 
										fsm <= write_spi_data1;
									end if;	
								when write_spi_data1 =>
									WriteSpi <= '1';
									fsm <= wait_spi_data1;
								when wait_spi_data1 =>
									WriteSpi <= '0';
									if DatReq = '1' then
										SpiDataIn <= InitRomData(7 downto 0);
									end if;	
									if DoValid = '1' then 
										fsm <= write_spi_data2;
										fsm_adr <= fsm_adr + 1;
									end if;	
								when write_spi_data2 =>
									WriteSpi <= '1';
									fsm <= wait_spi_data2;
									InitRomAdr <= "000"&fsm_adr;
								when wait_spi_data2 =>
									WriteSpi <= '0';
									if DatReq = '1' then
										SpiDataIn <= InitRomData(23 downto 16);
									end if;	
									if DoValid = '1' then 
										if InitRomData = "00000000" then
											DeviceState <= DevWait;
											fsm <= prep_cmd;
										else
											fsm <= write_spi_cmd;											
										end if;
									end if;	
								 when others =>
							 end case;
							 
							when "0001" => -- Read one byte
								DevStatus <= "0011"; -- Device read one byte
								case (fsm) is
									when prep_cmd =>
										fsm <= write_spi_cmd;
									when write_spi_cmd =>
										SpiDataIn <= "10001100"; -- set MAP command
--										DataCnt <= "00000001";
										fsm <= wait_spi_cmd;
										WriteSpi <='1';
									when wait_spi_cmd =>
										WriteSpi <= '0';
										if DatReq = '1' then
											SpiDataIn <= DataIn(15 downto 8); -- set adr high byte
										end if;	
										if DoValid = '1' then
											fsm <= write_spi_data1;
										end if;
									when write_spi_data1 =>
										WriteSpi <= '1';
										fsm <= wait_spi_data1;
									when wait_spi_data1 =>
										WriteSpi <= '0';
										if DatReq = '1' then
											SpiDataIn <= DataIn(7 downto 0); -- set adr low byte
										end if;	
										if DoValid = '1' then
											fsm <= write_spi_data2;
										end if;
									when write_spi_data2 =>
										WriteSpi <= '1';
										fsm <=wait_spi_data2;
									when wait_spi_data2 =>
										WriteSpi <= '0';
										if DatReq = '1' then
											SpiDataIn <= "10000000"; -- Read command
										end if;	
										if DoValid = '1' then
											fsm <= read_spi_data1;
										end if;
									when read_spi_data1 =>
										WriteSpi <= '1';
										fsm <= wait_spi_read1;
									when wait_spi_read1 =>
										WriteSpi <= '0';
										if DoValid = '1' then
											SpiDataIn <= X"FF";
											fsm <=read_spi_data2;
										end if;
									when read_spi_data2 =>
										WriteSpi <= '1';
										fsm <= wait_spi_read2;
									when wait_spi_read2 =>
										WriteSpi <= '0';
										if DoValid = '1' then
											LocDataOut <= X"000000" & SpiDataOut;
											if PutDataWait = '0' then
												PutData <= '1';
												fsm <= prep_cmd;
												DeviceState <= DevWait;
											else
												fsm <= fsm_wait_host;
											end if;
										end if;
									when fsm_wait_host =>
										if PutDataWait = '0' then
											PutData <= '1';
											fsm <= prep_cmd;
											DeviceState <= DevWait;
										end if;
									when others =>	
								end case;
							
							when "0010" => -- Write one byte
								DevStatus <= "0100";	-- Device read one byte
								case (fsm) is
									when prep_cmd =>
										fsm <= write_spi_cmd;
									when write_spi_cmd =>
										SpiDataIn <= "10001100"; -- set MAP command
--										DataCnt <= "00000001";
										fsm <= wait_spi_cmd;
										WriteSpi <='1';
									when wait_spi_cmd =>
										WriteSpi <= '0';
										if DatReq = '1' then
											SpiDataIn <= DataIn(15 downto 8); -- set adr high byte
										end if;	
										if DoValid = '1' then
											fsm <= write_spi_data1;
										end if;
									when write_spi_data1 =>
										WriteSpi <= '1';
										fsm <= wait_spi_data1;
									when wait_spi_data1 =>
										WriteSpi <= '0';
										if DatReq = '1' then
											SpiDataIn <= DataIn(7 downto 0); -- set adr low byte
										end if;	
										if DoValid = '1' then
											fsm <= write_spi_data2;
										end if;
									when write_spi_data2 =>
										WriteSpi <= '1';
										fsm <=wait_spi_data2;
									when wait_spi_data2 =>
										WriteSpi <= '0';
										if DatReq = '1' then
											SpiDataIn <= "10000100"; -- Write Command
										end if;	
										if DoValid = '1' then
											fsm <= read_spi_data1;
										end if;
									when read_spi_data1 =>
										WriteSpi <= '1';
										fsm <= wait_spi_read1;
									when wait_spi_read1 =>
										WriteSpi <= '0';
										if DatReq = '1' then
											SpiDataIn <= DataIn(23 downto 16);
										end if;
										if DoValid = '1' then
											fsm <= read_spi_data2;
										end if;
									when read_spi_data2 =>
										WriteSpi <= '1';
										fsm <= wait_spi_read2;
									when wait_spi_read2 =>
										WriteSpi <= '0';
										if DoValid = '1' then 
											fsm <= prep_cmd;
											DeviceState <= DevWait;
										end if;
									when others =>	
								end case;
								
							when "0011" => -- Read Status
								LocDataOut <= X"0000000" & DevStatus;
								PutData <= '1';
								DeviceState <= DevWait;
								
							when "0100" => -- Read Block
								DevStatus <= "0101";	-- Device read block
								case (fsm) is
									when prep_cmd =>
										fsm <= write_spi_cmd;
									when write_spi_cmd =>
										DataCnt <= DataIn(23 downto 16);
										ByteCnt <= "00";
										LocDataOut <= X"00000000";
										SpiDataIn <= "10001100"; -- set MAP command
--										DataCnt <= "00000001";
										fsm <= wait_spi_cmd;
										WriteSpi <='1';
									when wait_spi_cmd =>
										WriteSpi <= '0';
										if DatReq = '1' then
											SpiDataIn <= DataIn(15 downto 8); -- set adr high byte
										end if;	
										if DoValid = '1' then
											fsm <= write_spi_data1;
										end if;
									when write_spi_data1 =>
										WriteSpi <= '1';
										fsm <= wait_spi_data1;
									when wait_spi_data1 =>
										WriteSpi <= '0';
										if DatReq = '1' then
											SpiDataIn <= DataIn(7 downto 0); -- set adr low byte
										end if;	
										if DoValid = '1' then
											fsm <= write_spi_data2;
										end if;
									when write_spi_data2 =>
										WriteSpi <= '1';
										fsm <=wait_spi_data2;
										
									when wait_spi_data2 =>
										WriteSpi <= '0';
										if DatReq = '1' then
											SpiDataIn <= "10000000";	-- Read command
										end if;	
										if DoValid = '1' then
											LocDataOut <= X"00000000";
											fsm <= read_spi_data2;
										end if;
									when read_spi_data2 =>
										DataCnt <= DataCnt - 1;
										WriteSpi <= '1';
										fsm <= wait_spi_read2;
									when wait_spi_read2 =>
										WriteSpi <='0';
										if DoValid = '1' then
											SpiDataIn <= X"FF";
											fsm <= read_spi_data1;
										end if;
									when read_spi_data1 =>
										WriteSpi <= '1';
										PutData <= '0';
										fsm <= wait_spi_read1;
									when wait_spi_read1 =>
										WriteSpi <= '0';
										if DoValid = '1' then
											case (ByteCnt) is -- load data to output reg
												when "00" =>
													LocDataOut(7 downto 0) <= SpiDataOut;
												when "01" =>
													LocDataOut(15 downto 8) <= SpiDataOut;
												when "10" =>
													LocDataOut(23 downto 16) <= SpiDataOut;
												when "11" =>
													LocDataOut(31 downto 24) <= SpiDataOut;
												when others =>	
											end case;
											if PutDataWait = '1' then 
												fsm <= fsm_wait_host;
											else
												if ByteCnt = "11" then
													PutData <= '1';
												end if;
												ByteCnt <= ByteCnt + 1;
											   if DataCnt = X"00" then
													DeviceState <= DevWait;
													fsm <= prep_cmd;
												else 
													DataCnt <= DataCnt - 1;
													fsm <= read_spi_data1;
												end if;
											end if;
										end if;
									when fsm_wait_host =>
										if PutDataWait = '0' then
											fsm <= wait_spi_read1;
										end if;
									when others =>	
								end case;
							
							when "0101" => -- Write Block
								DevStatus <= "0110";	-- Device write block
								case (fsm) is
									when prep_cmd =>
										fsm <= write_spi_cmd;
									when write_spi_cmd =>
										DataCnt <= DataIn(23 downto 16);
										ByteCnt <= "00";
										SpiDataIn <= "10001100"; -- set MAP command
--										DataCnt <= "00000001";
										fsm <= wait_spi_cmd;
										WriteSpi <='1';
									when wait_spi_cmd =>
										WriteSpi <= '0';
										if DatReq = '1' then
											SpiDataIn <= DataIn(15 downto 8); -- set adr high byte
										end if;	
										if DoValid = '1' then
											fsm <= write_spi_data1;
										end if;
									when write_spi_data1 =>
										WriteSpi <= '1';
										fsm <= wait_spi_data1;
									when wait_spi_data1 =>
										WriteSpi <= '0';
										if DatReq = '1' then
											SpiDataIn <= DataIn(7 downto 0); -- set adr low byte
										end if;	
										if DoValid = '1' then
											fsm <= write_spi_data2;
										end if;
									when write_spi_data2 =>
										GetData <= '1';
										WriteSpi <= '1';
										fsm <=wait_spi_data2;
										
									when wait_spi_data2 =>
										GetData <= '0';
										WriteSpi <= '0';
										if DatReq = '1' then
											SpiDataIn <= "10000100";	-- Write command
										end if;	
										if DoValid = '1' then
											SpiDataIn <= DataIn(7 downto 0);
											ByteCnt <= ByteCnt+1;
											DataCnt <= DataCnt - 1;
											fsm <= read_spi_data1;
										end if;

									when read_spi_data1 =>
										WriteSpi <= '1';
										GetData <= '0';
										fsm <= wait_spi_read1;
									when wait_spi_read1 =>
										WriteSpi <= '0';
										if DatReq = '1' then
											case (ByteCnt) is -- load data from input reg
												when "00" =>
													SpiDataIn <= DataIn(7 downto 0);
												when "01" =>
													SpiDataIn <= DataIn(15 downto 8);
												when "10" =>
													SpiDataIn <= DataIn(23 downto 16);
												when "11" =>
													SpiDataIn <= DataIn(31 downto 24);
												when others =>	
											end case;
										end if;
										if DoValid ='1' then
											if GetDataWait = '1' then 
												fsm <= fsm_wait_host;
											else
												if ByteCnt = "11" then
													GetData <= '1';
												end if;
												ByteCnt <= ByteCnt + 1;
											   if DataCnt = X"00" then
													DeviceState <= DevWait;
													fsm <= prep_cmd;
												else 
													DataCnt <= DataCnt - 1;
													fsm <= read_spi_data1;
												end if;
											end if;
										end if;
									when fsm_wait_host =>
										if GetDataWait = '0' then
											GetData <= '1';
											fsm <= wait_spi_read1;
										end if;
									when others =>	
								end case;
							when "0110" => -- Write Pin
								RST_PIN_4	<= LocDataIn(7);
								ENABLE_4		<= LocDataIn(6);
								RST_PIN_3	<= LocDataIn(5);
								ENABLE_3		<= LocDataIn(4);
								RST_PIN_2	<= LocDataIn(3);
								ENABLE_2		<= LocDataIn(2);
								RST_PIN_1	<= LocDataIn(1);
								ENABLE_1		<= LocDataIn(0);
--								GetData <= '1';
								DeviceState <= DevWait;
							when "0111" => -- Read Pin
								LocDataOut(31 downto 4) <= X"0000000";
								LocDataOut(3) <= FAULT_4;
								LocDataOut(2) <= FAULT_3;
								LocDataOut(1) <= FAULT_2;
								LocDataOut(0) <= FAULT_1;
								if PutDataWait = '0' then 
									PutData <= '1';
									DeviceState <= DevWait;
								end if;	
							when others =>
						end case;
--						DeviceState <= DevWait;
					when	DevWait	=>
--						ShiftData <= '0';
						if PutDataWait = '0' then
							PutData <= '0';
							DevStatus <= "0000";	-- Device ready
							if GetDataRdy = '0' then
								GetData <= '1';
								DeviceState <= DevGetCmd;
							end if;
						else
							DevStatus <= "0001";	-- Device wait host read
						end if;		
					when others =>
				end case;
--				if GetDataRdy = '0' then
--					GetData <= '1';
--				else GetData <= '0';
--				end if;
			end if;
		end if;
	end process;

end Behavioral;
