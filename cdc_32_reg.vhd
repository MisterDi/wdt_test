----------------------------------------------------------------------------------
-- Company: Aerotech LTD, LLC
-- Engineer: Eugene Shamaev
-- 
-- Create Date:    15:20:52 01/07/2018 
-- Design Name: 
-- Module Name:    cdc_32_reg - Behavioral 
-- Project Name:   Video10D
-- Target Devices: Spartan-6 45T
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
use ieee.numeric_std.all;

library UNISIM;
use UNISIM.Vcomponents.ALL;

entity cdc_32_reg is
	Port (
		reset          : in  STD_LOGIC;
		
		clk_in         : in  STD_LOGIC;
		data_in        : in  STD_LOGIC_VECTOR(31 downto 0);
		
		clk_out        : in  STD_LOGIC;
		data_out       : out STD_LOGIC_VECTOR(31 downto 0)
	);
end cdc_32_reg;

architecture Behavioral of cdc_32_reg is

signal rd_en         : STD_LOGIC;
signal empty         : STD_LOGIC;
signal wr_en         : STD_LOGIC;
signal dout          : STD_LOGIC_VECTOR(31 DOWNTO 0);
signal full				: STD_LOGIC;

COMPONENT fifo_32_aux
	PORT (
		rst            : IN STD_LOGIC;

		wr_clk         : IN STD_LOGIC;
		din            : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
		wr_en          : IN STD_LOGIC;
		full           : OUT STD_LOGIC;

		rd_clk         : IN STD_LOGIC;
		rd_en          : IN STD_LOGIC;
		dout           : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
		empty          : OUT STD_LOGIC
	);
END COMPONENT;

begin

fifo_32_aux_i : fifo_32_aux
	PORT MAP (
		rst           => reset,

		wr_clk        => clk_in,
		din           => data_in,
		wr_en         => wr_en,
		full			  => full,

		rd_clk        => clk_out,
		rd_en         => rd_en,
		dout          => dout,
		empty         => empty
	);
	
process (clk_in, reset)
begin
	if reset = '1' then
		wr_en <= '0';
	else
		if clk_in'event and clk_in = '1' then
			if full = '0' then wr_en <= '1';
 			else	wr_en <= '0';
			end if;
		end if;
	end if;
end process;

process (clk_out, reset)
begin
	if reset = '1' then
		rd_en <= '0';
	else
		if clk_out'event and clk_out = '1' then
			if empty = '0' then
				data_out <= dout;
				rd_en    <= '1';
			else
				rd_en <= '0';
			end if;
		end if;
	end if;
end process;

end Behavioral;

